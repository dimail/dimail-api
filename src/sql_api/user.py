"""SQL API for user management.

This module contains the functions to interact with the SQL database for user management.

Functions:
    - count_users: count the number of users in the database
    - get_users: get all users
    - get_user: get a user by name
    - create_user: create a user
    - update_user_password: update a user's password
    - update_user_is_admin: update a user's admin status
    - delete_user: delete a user
"""
import sqlalchemy as sa
import sqlalchemy.orm as orm

from . import models


def count_users(
    session: orm.Session
) -> int:
    """Count the number of users in the database.

    Args:
        session: ORM session

    Returns:
        int: the number of users in the database
    """
    return session.query(models.User).count()


def get_users(
        session: orm.Session
)-> list[models.User] | None:
    """Get all users.

    Args:
        session: ORM session

    Returns:
        list[User]: a list of all users
    """
    return session.query(models.User).all()


def get_user(
        session: orm.Session,
        user_name: str
)-> models.User | None:
    """Get a user by name.

    Args:
        session: the database session
        user_name: the name of the user

    Returns:
        User: the user with the given name
    """
    return session.get(models.User, user_name)


def create_user(
        session: orm.Session,
        name: str,
        password: str,
        is_admin: bool,
        perms: list[str],
)-> models.User | None:
    """Create a user.

    Args:
        session: ORM session
        name: the name of the user
        password: the password of the user
        is_admin: whether the user is an admin

    Returns:
        User: the created user
        None: if the user could not be created
    """
    db_user = models.User(
        name=name,
        is_admin=is_admin,
        perms=perms,
    )
    try:
        db_user.set_password(password)
        session.add(db_user)
        session.commit()
    except Exception as e:
        print(str(e))
        session.rollback()
        return None
    session.refresh(db_user)
    return db_user


def update_user_password(
        session: orm.Session,
        name: str,
        password: str
)-> models.User | None:
    """Update a user's password.

    Args:
        session: ORM session
        name: the name of the user
        password: the new password

    Returns:
        User: the updated user
        None: if the user could not be updated
    """
    db_user = get_user(session, name)
    if db_user is None:
        return None
    try:
        db_user.set_password(password)
        session.flush()
        session.commit()
    except Exception as e:
        print(str(e))
        session.rollback()
    session.refresh(db_user)
    return db_user


def update_user_is_admin(
        session: orm.Session,
        name: str, is_admin: bool
) -> models.User | None:
    """Update a user's admin status.

    Args:
        session: ORM session
        name: the name of the user
        is_admin: whether the user is an admin

    Returns:
        User: the updated user
        None: if the user could not be updated
    """
    db_user = get_user(session, name)
    if db_user is None:
        return None
    db_user.is_admin = is_admin
    try:
        session.flush()
        session.commit()
    except Exception as e:
        print(str(e))
        session.rollback()
    session.refresh(db_user)
    return db_user


def update_user_perms(
    session: orm.Session,
    name: str,
    perms: list[str]
) -> models.User | None:
    """Updates the user list of global perms (JSON).

    Args:
        session: ORM session
        name: the user name
        perms: the new set of perms

    Returns:
        the updated user (None if anything fails)
    """
    db_user = get_user(session, name)
    if db_user is None:
        return None
    db_user.perms = perms
    try:
        session.flush()
        session.commit()
    except Exception as e:
        print(str(e))
        session.rollback()
    session.refresh(db_user)
    return db_user


def delete_user(
    session: orm.Session,
    name: str
) -> models.User | None:
    """Delete a user.

    Args:
        session: ORM session
        name: the name of the user

    Returns:
        User: the deleted user

        None: if the user could not be deleted
    """
    db_user = get_user(session, name)
    if db_user is not None:
        session.delete(db_user)
        session.commit()
    return db_user
