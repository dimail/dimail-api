"""This module contains the functions to manage the manageds in the database.

The manageds are the permissions given to a user to manage another user. They are
stored in the table `managed` in the database. The table has the following columns:
    - `user`: the user that is being managed by someone else
    - `managed_by`: who is managing him

The allows are managed by the following functions:
    - get_manageds: get all the allows in the database
    - get_managed: get an allow in the database
    - create_managed: make a user being managed by another one
    - delete_managed: stop a user being managed by another one
    - delete_managed_by_user: make a user being managed by no-one
    - delete_managed_by_manager: make someone not managing anyone else

"""
import sqlalchemy as sa
import sqlalchemy.orm as orm

from . import models


def get_manageds(session: orm.Session, user: str = "", managed_by: str = "") -> list[models.Managed]:
    """Gets all the manageds in the database that match the parameters.

    Args:
        session (orm.Session): the database session
        user (str): if provided, only lines allowing that user to be managed will
            be returned
        managed_by (str): if provided, only lines allowing the user to be managing
            someone will be returned

    Returns:
        list[models.Managed]: the manageds matching your request
    """
    query = session.query(models.Managed)
    if user != "":
        query = query.filter_by(user=user)
    if managed_by != "":
        query = query.filter_by(managed_by=managed_by)
    return query.all()


def get_managed(session: orm.Session, user: str, managed_by: str) -> models.Managed:
    """Gets the managed allowing this user to be managed by the other one.

    Args:
        session (orm.Session): the database session
        user (str): the user being managed
        managed_by (str): the user managing

    Returns:
        models.Managed: the entry, if it exists
    """
    return session.get(models.Managed, {"user": user, "managed_by": managed_by})

def create_managed(session: orm.Session, user: str, managed_by: str) -> models.Managed:
    """Makes a user manageable by another.

    Args:
        session (orm.Session): the database session
        user (str): the user that can be managed
        managed_by (str): the user who is going to be managing

    Returns:
        models.Managed: the 'manged' entry newly created
    """
    managed = models.Managed(user=user, managed_by=managed_by)
    if session.info["url"].startswith("sqlite"):
        tmpa = session.get(models.User, user)
        tmpb = session.get(models.User, managed_by)
        if tmpa is None or tmpb is None:
            raise Exception("Failed on foreign key constraint")
    try:
        session.add(managed)
        session.commit()
    except Exception:
        session.rollback()
        raise
    session.refresh(managed)
    return managed

def delete_managed(session: orm.Session, user: str, managed_by: str) -> models.Managed:
    """Remove a line from the 'managed' table.

    Args:
        session (orm.Session): the database session
        user (str): the user that can be managed
        managed_by (str): the user who is going to be managing

    Returns:
        models.Managed: the entry as it was before deleting.
    """
    managed = get_managed(session, user, managed_by)
    if managed is not None:
        session.delete(managed)
        session.commit()
    return managed


def delete_managed_by_user(session: orm.Session, user: str) -> int:
    """Deletes all the lines saying by who this user can be managed.

    Args:
        session (orm.Session): the database session
        user (str): the user that is not managed anymore by anyone

    Returns:
        int: the number of rows deleted
    """
    res = session.execute(sa.delete(models.Managed).where(models.Managed.user == user))
    session.commit()
    return res.rowcount


def delete_managed_by_manager(session: orm.Session, managed_by: str) -> int:
    """Deletes all the lines saying that user used to be a manager.

    Args:
        session (orm.Session): the database session
        managed_by (str): the user who use to be a manager

    Returns:
        int: the number of deleted rows
    """
    res = session.execute(sa.delete(models.Managed).where(models.Managed.managed_by == managed_by))
    session.commit()
    return res.rowcount


