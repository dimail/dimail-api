"""This modules contains the models for the SQL database.

Classes:
    - User: a user of the system
    - Domain: a domain of the system
    - Allowed: a many-to-many relationship between users and domains
"""
import datetime
import uuid

import jwt
import passlib.hash
import pydantic

# from passlib.hash import argon2
import sqlalchemy as sa
import sqlalchemy.orm as orm

from .. import config, web_models
from .database import Api

class Managed(Api):
    """A many-to-many relationship between users and users.

    Attributes:
        user: the user being managed
        managed_by: the user managing
    """
    __tablename__ = "managed"
    user = sa.Column(sa.String(64), sa.ForeignKey("users.name"), primary_key=True)
    managed_by = sa.Column(sa.String(64), sa.ForeignKey("users.name"), primary_key=True)


class User(Api):
    """A user of the system.

    This classes modelize a users table in the database.
    Its attributes represent the different columns of the table it models

    Attributes:
        name: the username
        uuid: a unique identifier
        hashed_password: the password, hashed
        is_admin: whether the user is an admin
        fullname: the full name of the user
        domains: the domains the user has access to

    Functions:
        set_password: set the password of the user
        verify_password: verify a password against the user's password
        create_token: create a JWT token for the user
        get_creds: get the credentials
    """
    __tablename__ = "users"
    __table_args__ = (sa.UniqueConstraint("uuid", name="uuid_is_unique"),)
    name = sa.Column(sa.String(64), primary_key=True)
    uuid = sa.Column(sa.UUID, default=uuid.uuid4)
    hashed_password = sa.Column(sa.String(128), nullable=True)
    is_admin = sa.Column(sa.Boolean, default=False)
    fullname = sa.Column(sa.String(64), nullable=True)
    perms = sa.Column(sa.JSON(), nullable=False, default=[])
    domains: orm.Mapped[list["Domain"]] = orm.relationship(
        secondary="allowed", back_populates="users"
    )
    _managers: orm.Mapped[list["User"]] = orm.relationship(
        secondary="managed",
        back_populates="_managees",
        primaryjoin=name == Managed.user,
        secondaryjoin=name == Managed.managed_by,
    )
    _managees: orm.Mapped[list["User"]] = orm.relationship(
        secondary="managed",
        back_populates="_managers",
        primaryjoin=name == Managed.managed_by,
        secondaryjoin=name == Managed.user,
    )

    def managers(self):
        result = []
        for item in self._managers:
            result.append(item.name)
        return result

    def managees(self):
        result = []
        for item in self._managees:
            result.append(item.name)
        return result

    def __eq__(self, other):
        if not isinstance(self, other.__class__):
            return False
        if (
            self.name == other.name
            and self.is_admin == other.is_admin
            and self.fullname == other.fullname
        ):
            return True
        return False

    def set_password(self, password: str) -> None:
        """Set the password of the user.

        Args:
            password: the password to set

        Returns:
            None
        """
        if password == "no":
            self.hashed_password = '*'
        else:
            self.hashed_password = passlib.hash.argon2.hash(password)

    def verify_password(self, password: str)-> bool:
        """Verify a password against the user's password.

        Args:
            password: the password to verify

        Returns:
            bool: whether the password is correct
        """
        if self.hashed_password == '*':
            return False
        return passlib.hash.argon2.verify(password, self.hashed_password)

    def create_token(self)-> str:
        """Create a JWT token for the user.

        Returns:
            str: the JWT token
        """
        now = datetime.datetime.now(datetime.timezone.utc)
        delta = datetime.timedelta(minutes=47)
        expire = now + delta
        data = {
            "sub": self.name,
            "exp": expire,
        }
        secret = config.settings["JWT_SECRET"]
        algo = "HS256"
        return jwt.encode(data, secret, algo)

    def to_web(self) -> web_models.User:
        return web_models.User(
            name = self.name,
            is_admin = self.is_admin,
            uuid = self.uuid,
            perms = self.perms,
        )


class Domain(Api):
    """A domain of the system.

    This classes modelize a domains table in the database.

    Attributes:
        name: the domain name
        features: the features of the domain
        webmail_domain: the webmail domain
        imap_domain: the IMAP domains
        smtp_domain: the SMTP domains
        users: the users that have access to the domain

    Functions:
        has_feature: check if the domain has a feature
    """
    __tablename__ = "domains"
    name = sa.Column(sa.String(200), primary_key=True)
    features = sa.Column(sa.JSON(), nullable=False)
    moving = sa.Column(sa.String(50), nullable=True, default=None)
    webmail_domain = sa.Column(sa.String(200), nullable=True)
    imap_domain = sa.Column(sa.String(200), nullable=True)
    smtp_domain = sa.Column(sa.String(200), nullable=True)
    state = sa.Column(sa.String(15), nullable=False, default="new")
    errors = sa.Column(sa.JSON(), nullable=True)
    dtcreated = sa.Column(sa.DateTime(), nullable=False, server_default=sa.sql.func.now())
    dtupdated = sa.Column(
        sa.DateTime(),
        nullable=False,
        server_default=sa.sql.func.now(),
        onupdate=sa.sql.func.now()
    )
    dtchecked = sa.Column(sa.DateTime(), nullable=True)
    dtaction = sa.Column(sa.DateTime(), nullable=True, index=True)
    users: orm.Mapped[list["User"]] = orm.relationship(
        secondary="allowed", back_populates="domains"
    )

    def has_feature(self, feature: str) -> bool:
        """Check if the domain has a feature.

        Args:
            feature: the feature to check

        Returns:
            bool: whether the domain has the feature
        """
        return feature in self.features

    def get_webmail_domain(self) -> str:
        if self.webmail_domain is not None:
            return self.webmail_domain
        return "webmail." + self.name

    def get_webmail_name(self) -> str:
        name = "." + self.name
        if self.webmail_domain is not None:
            if self.webmail_domain.endswith(name):
                return self.webmail_domain.removesuffix(name)
            else:
                return self.webmail_domain + "."
        return "webmail"

    def get_imap_domain(self) -> str:
        if self.imap_domain is not None:
            return self.imap_domain
        return "imap." + self.name

    def get_imap_name(self) -> str:
        name = "." + self.name
        if self.imap_domain is not None:
            if self.imap_domain.endswith(name):
                return self.imap_domain.removesuffix(name)
            else:
                return self.imap_domain + "."
        return "imap"

    def get_smtp_domain(self) -> str:
        if self.smtp_domain is not None:
            return self.smtp_domain
        return "smtp." + self.name

    def get_smtp_name(self) -> str:
        name = "." + self.name
        if self.smtp_domain is not None:
            if self.smtp_domain.endswith(name):
                return self.smtp_domain.removesuffix(name)
            else:
                return self.smtp_domain + "."
        return "smtp"

    def get_domain(self, what: str) -> str:
        if what == "mailbox":
            return self.get_mailbox_domain()
        if what == "imap":
            return self.get_imap_domain()
        if what == "smtp":
            return self.get_smtp_domain()
        if what == "webmail":
            return self.get_webmail_domain()

    def get_transport(self) -> str:
        return "coincoin"

class Allowed(Api):
    """A many-to-many relationship between users and domains.

    This classes modelize an allowed table in the database.

    Attributes:
        user: the user
        domain: the domain
    """
    __tablename__ = "allowed"
    user = sa.Column(sa.String(64), sa.ForeignKey("users.name"), primary_key=True)
    domain = sa.Column(
        sa.String(200),
        sa.ForeignKey("domains.name"),
        primary_key=True,
    )

    def __repr__(self):
        return 'sql_api.Allowed("%s","%s")' % (self.user, self.domain)



