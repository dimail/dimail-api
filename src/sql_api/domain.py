# ruff: noqa: E711
import datetime

"""SQL API for domain operations.

This module contains functions to interact with the database for domain operations.

Functions:
    - get_domains: Get all domains from the database.
    - get_domain: Get a domain from the database.
    - create_domain: Create a domain in the database.

Classes:
    - None

Exceptions:
    - None
"""
import sqlalchemy.orm as orm

from . import models


def get_domains(
    session: orm.Session
) -> list[models.Domain]:
    """Get all domains from the database.

    Args:
        session: ORM session

    Returns:
        List of all domains in the database.
    """
    return session.query(models.Domain).all()


def get_domain(
    session: orm.Session,
    domain_name: str
) -> models.Domain:
    """Get a domain from the database.

    Args:
        session: ORM session object.

    Returns:
        Domain object from the database.
    """
    return session.get(models.Domain, domain_name)


def create_domain(
    session: orm.Session,
    name: str,
    features: list[str],
    webmail_domain: str | None = None,
    imap_domain: str | None = None,
    smtp_domain: str | None = None,
) -> models.Domain:
    """Create a domain in the database.

    Args:
        session: ORM session object.
        name: Domain name.
        features: List of features for the domain.
        webmail_domain: Webmail domain.
        imap_domain: IMAP domain.
        smtp_domain: SMTP domain.

    Returns:
        Domain object from the database

    Raises:
        None
    """
    db_domain = models.Domain(name=name, features=[str(f) for f in features])
    if webmail_domain is not None:
        db_domain.webmail_domain = webmail_domain
    if imap_domain is not None:
        db_domain.imap_domain = imap_domain
    if smtp_domain is not None:
        db_domain.smtp_domain = smtp_domain
    session.add(db_domain)
    session.commit()
    session.refresh(db_domain)
    return db_domain


def update_domain_state(
    session: orm.Session,
    name: str,
    state: str
) -> models.Domain:
    """Updates the 'state' column of a domain.

    Args:
        session: ORM session object
        name: domain name
        state: the new state

    Returns:
        Domain object from the database
    """
    db_domain = get_domain(session, name)
    if db_domain is None:
        return None
    try:
        db_domain.state = state
        session.flush()
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_domain)
    return db_domain


def update_domain_errors(
    session: orm.Session, name: str, errors: list[str] | None
) -> models.Domain:
    """Updates the errors list of a domain.

    Args:
        session: ORM session
        name: the domain name
        errors: list of errors for the domain

    Returns:
        The updated domain object from database
    """
    db_domain = get_domain(session, name)
    if db_domain is None:
        return None
    try:
        db_domain.errors = errors
        session.flush()
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_domain)
    return db_domain


def update_domain_dtchecked(
    session: orm.Session,
    name: str,
    dtchecked: datetime.datetime | str | None = None
) -> models.Domain:
    """Updates the dtchecked (date of last check) of a domain. A dtchecked in the
    future has no meaning (how can you be sure it will be checked at that future
    time ?), and a date in the past has almost no meaning (it if happened then, you
    should have updated at that time).

    Args:
        session: ORM session
        name: domain name
        dtchecked: the new date, as a datatime, as a string (must be 'now'), or None (will set to now)

    Returns:
        the updated domain object from the daabase
    """
    if dtchecked is None:
        dtchecked = datetime.datetime.now(datetime.timezone.utc)
    if isinstance(dtchecked, str):
        if dtchecked == "now":
            dtchecked = datetime.datetime.now(datetime.timezone.utc)
        else:
            raise Exception("La seule chaine possible ici est 'now'")
    if not isinstance(dtchecked, datetime.datetime):
        raise Exception(
            f"Je ne peux pas mettre '{dtchecked}' comme date de dernier " +
            "controle du domaine"
        )
    db_domain = get_domain(session, name)
    if db_domain is None:
        return None
    try:
        db_domain.dtchecked = dtchecked
        session.flush()
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_domain)
    return db_domain


def update_domain_dtaction(
    session: orm.Session,
    name: str,
    dtaction: datetime.datetime
) -> models.Domain:
    """Updates the dtaction of a domain. It is supposed to be a date in the future (the next
    time an action will be required).

    Args:
        session: ORM session
        name: domain name
        dtaction: the date (in the future, hopefully)

    Returns:
        The updated domain object from the database
    """
    db_domain = get_domain(session, name)
    if db_domain is None:
        return None
    try:
        db_domain.dtaction = dtaction
        session.flush()
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_domain)
    return db_domain


def update_domain_smtp_domain(
    session: orm.Session,
    name: str,
    smtp: str,
) -> models.Domain:
    db_domain = get_domain(session, name)
    if db_domain is None:
        return None
    try:
        db_domain.smtp_domain = smtp
        session.flush()
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_domain)
    return db_domain


def update_domain_imap_domain(
    session: orm.Session,
    name: str,
    imap: str,
) -> models.Domain:
    db_domain = get_domain(session, name)
    if db_domain is None:
        return None
    try:
        db_domain.imap_domain = imap
        session.flush()
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_domain)
    return db_domain


def update_domain_webmail_domain(
    session: orm.Session,
    name: str,
    webmail: str,
) -> models.Domain:
    db_domain = get_domain(session, name)
    if db_domain is None:
        return None
    try:
        db_domain.webmail_domain = webmail
        session.flush()
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_domain)
    return db_domain


def update_domain_features(
    session: orm.Session,
    name: str,
    features: list[str],
) -> models.Domain:
    db_domain = get_domain(session, name)
    if db_domain is None:
        return None
    try:
        db_domain.features = features
        session.flush()
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_domain)
    return db_domain


def update_domain_moving(
    session: orm.Session,
    name: str,
    moving: str | None,
) -> models.Domain:
    db_domain = get_domain(session, name)
    if db_domain is None:
        return None

    try:
        db_domain.moving = moving
        session.flush()
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_domain)
    return db_domain


def first_domain_need_action(
    session: orm.Session
) -> models.Domain | None:
    """Fetch the first domain that will need action. If dtaction of the feched domain is in
    the future, then all past expected actions have been already done for all the other
    domains. Will return None is there is no domain with a dtaction.

    Args:
        session: ORM session

    Returns:
        the domain object from the database
    """
    db_domain = ( session.query(models.Domain).
        filter(models.Domain.dtaction != None).
        order_by(models.Domain.dtaction.asc()).
        with_for_update(skip_locked=True).
        first()
    )
    return db_domain
