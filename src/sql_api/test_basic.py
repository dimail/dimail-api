import datetime
import pytest

from .. import sql_api
from . import database



def test_database():
    database.maker = None
    with pytest.raises(Exception) as e:
        database.get_maker()
    assert "You need to init the API db by giving me a valid URL" in str(e.value)

    database.init_db("sqlite:///:memory:")

    maker = database.get_maker()
    assert maker is not None

    database.maker = None

def test_health_check(db_api_session):
    status = sql_api.health_check()
    assert status.status is True
    assert "sql_api_status = OK" in status.detail


def test_create_user(db_api_session):
    db_user = sql_api.create_user(
        db_api_session, name="toto", password="titi", is_admin=False, perms=[]
    )
    assert db_user == sql_api.User(name="toto", is_admin=False)
    db_api_session.expunge_all()

    # When trying to create a user that already exists -> fail
    db_user = sql_api.create_user(
        db_api_session,
        name="toto",
        password="titi",
        is_admin=False,
        perms=[]
    )
    assert db_user is None

    # When trying to create a user with an invalid password -> fail
    db_user = sql_api.create_user(
        db_api_session,
        name="new",
        password=None,
        is_admin=True,
        perms=[]
    )
    assert db_user is None

    # When trying to create a user with an invalid is_admin -> fail
    db_user = sql_api.create_user(
        db_api_session,
        name="test",
        password="test",
        is_admin="invalid",
        perms=[],
    )
    assert db_user is None


def test_delete_user(db_api_session, log):
    # First, we create a user
    sql_api.create_user(
        db_api_session,
        name="toto",
        password="titi",
        is_admin=False,
        perms=[],
    )
    # Then, we retrieve the user
    user = sql_api.get_user(db_api_session, "toto")
    assert user == sql_api.User(name="toto", is_admin=False)

    # Delete returns the user as it was before deletion, so, unchanged
    user = sql_api.delete_user(db_api_session, "toto")
    assert user == sql_api.User(name="toto", is_admin=False)

    # When trying to fetch it again, we fail
    user = sql_api.get_user(db_api_session, "toto")
    assert user is None

    # when trying to delete a user that does not exist -> fail
    user = sql_api.delete_user(db_api_session, "tutu")
    assert user is None


min_eq = datetime.timedelta(seconds=-1)
max_eq = datetime.timedelta(seconds=1)
def date_eq(a: datetime.datetime, b: datetime.datetime) -> bool:
    a = a.replace(tzinfo=None)
    b = b.replace(tzinfo=None)
    delta = a - b
    if delta > min_eq and delta < max_eq:
        return True
    return False

def test_create_domain(db_api_session, log):
    now = datetime.datetime.now(datetime.timezone.utc)
    db_dom = sql_api.create_domain(
        db_api_session,
        name="example.com",
        features=["webmail", "mailbox"],
    )
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.name == "example.com"
    assert db_dom.features == ["webmail", "mailbox"]
    assert db_dom.webmail_domain is None
    assert db_dom.imap_domain is None
    assert db_dom.smtp_domain is None
    assert db_dom.state == "new"
    assert db_dom.dtaction is None
    assert db_dom.dtchecked is None
    assert db_dom.errors is None
    assert db_dom.moving is None
    assert date_eq(db_dom.dtcreated, now)
    assert date_eq(db_dom.dtupdated, now)

    # On peut mettre dtchecked
    before = now + datetime.timedelta(minutes=-30)
    db_dom = sql_api.update_domain_dtchecked(db_api_session, "example.com", before)
    assert isinstance(db_dom, sql_api.Domain)
    assert date_eq(db_dom.dtchecked, before)

    # Si on set dtchecked avec None, ca met now
    now = datetime.datetime.now(datetime.timezone.utc)
    db_dom = sql_api.update_domain_dtchecked(db_api_session, "example.com", None)
    assert isinstance(db_dom, sql_api.Domain)
    assert date_eq(db_dom.dtchecked, now)

    # On remet before
    db_dom = sql_api.update_domain_dtchecked(db_api_session, "example.com", before)
    assert isinstance(db_dom, sql_api.Domain)
    assert date_eq(db_dom.dtchecked, before)

    # Si on met le texte 'now', ca met now
    db_dom = sql_api.update_domain_dtchecked(db_api_session, "example.com", "now")
    assert isinstance(db_dom, sql_api.Domain)
    assert date_eq(db_dom.dtchecked, now)

    # Aucun domaine n'a besoin d'action
    db_dom = sql_api.first_domain_need_action(db_api_session)
    assert db_dom is None

    before = now + datetime.timedelta(seconds=-30)
    db_dom = sql_api.update_domain_dtaction(db_api_session, "example.com", before)
    assert date_eq(db_dom.dtaction, before)

    # Maintenant, le domain "example.com" a besoin d'action
    db_dom = sql_api.first_domain_need_action(db_api_session)
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.name == "example.com"

    db_dom = sql_api.create_domain(
        db_api_session,
        name="domain_name",
        features=["coin", "pan"],
        webmail_domain="webmail_domain",
        imap_domain="imap1",
        smtp_domain="smtp1",
    )
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.name == "domain_name"
    assert db_dom.features == ["coin", "pan"]
    assert db_dom.webmail_domain == "webmail_domain"
    assert db_dom.imap_domain == "imap1"
    assert db_dom.smtp_domain == "smtp1"

    # C'est toujours "example.com" qui a besoin d'action
    db_dom = sql_api.first_domain_need_action(db_api_session)
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.name == "example.com"

    before = now + datetime.timedelta(seconds=-31)
    db_dom = sql_api.update_domain_dtaction(db_api_session, "domain_name", before)
    assert db_dom.name == "domain_name"

    # Maintenant, c'est "domain_name" qui a besoin d'action en premier
    db_dom = sql_api.first_domain_need_action(db_api_session)
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.name == "domain_name"

    # On peut changer 'moving'
    db_dom = sql_api.update_domain_moving(db_api_session, "domain_name", "from:to")
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.moving == "from:to"
    db_dom = sql_api.update_domain_moving(db_api_session, "domain_name", None)
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.moving is None


def test_update_domain(db_api_session):
    db_dom = sql_api.create_domain(
        db_api_session,
        name="example.com",
        features=["webmail", "mailbox"],
    )

    # On peut changer l'état d'un domaine
    assert db_dom.state == "new"
    db_dom = sql_api.update_domain_state(db_api_session, "example.com", "broken")
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.name == "example.com"
    assert db_dom.state == "broken"

    # On peut ajouter des erreurs à un domaine
    assert db_dom.errors is None
    db_dom = sql_api.update_domain_errors(db_api_session, "example.com", [ "coin", "pan", "kaï" ])
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.errors == [ "coin", "pan", "kaï" ]

    # On peut changer le smtp_domain
    assert db_dom.smtp_domain is None
    db_dom = sql_api.update_domain_smtp_domain(db_api_session, "example.com", "smtp.new.com")
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.smtp_domain == "smtp.new.com"
    db_dom = sql_api.update_domain_smtp_domain(db_api_session, "example.com", None)
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.smtp_domain is None

    # On peut changer le imap_domain
    assert db_dom.imap_domain is None
    db_dom = sql_api.update_domain_imap_domain(db_api_session, "example.com", "imap.new.com")
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.imap_domain == "imap.new.com"
    db_dom = sql_api.update_domain_imap_domain(db_api_session, "example.com", None)
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.imap_domain is None

    # On peut changer le webmail_domain
    assert db_dom.webmail_domain is None
    db_dom = sql_api.update_domain_webmail_domain(db_api_session, "example.com", "webmail.new.com")
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.webmail_domain == "webmail.new.com"
    db_dom = sql_api.update_domain_webmail_domain(db_api_session, "example.com", None)
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.webmail_domain is None

    # On peut changer les features d'un domaine
    assert db_dom.features == [ "webmail", "mailbox" ]
    db_dom = sql_api.update_domain_features(db_api_session, "example.com", [])
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.features == []
    db_dom = sql_api.update_domain_features(db_api_session, "example.com", ["coin", "pan"])
    assert isinstance(db_dom, sql_api.Domain)
    assert db_dom.features == ["coin", "pan"]


def test_domain_model(db_api_session):
    db_dom = sql_api.create_domain(
        db_api_session,
        name="example.com",
        features=["webmail", "mailbox"],
    )

    tmp = db_dom.get_imap_domain()
    assert tmp == "imap.example.com"
    tmp = db_dom.get_smtp_domain()
    assert tmp == "smtp.example.com"
    tmp = db_dom.get_webmail_domain()
    assert tmp == "webmail.example.com"
    tmp = db_dom.get_imap_name()
    assert tmp == "imap"
    tmp = db_dom.get_smtp_name()
    assert tmp == "smtp"
    tmp = db_dom.get_webmail_name()
    assert tmp == "webmail"

    db_dom = sql_api.create_domain(
        db_api_session,
        name="example.net",
        features=[],
        imap_domain="dovecot.example.net",
        smtp_domain="postfix.example.net",
        webmail_domain="ox.example.net",
    )

    tmp = db_dom.get_imap_domain()
    assert tmp == "dovecot.example.net"
    tmp = db_dom.get_smtp_domain()
    assert tmp == "postfix.example.net"
    tmp = db_dom.get_webmail_domain()
    assert tmp == "ox.example.net"
    tmp = db_dom.get_imap_name()
    assert tmp == "dovecot"
    tmp = db_dom.get_smtp_name()
    assert tmp == "postfix"
    tmp = db_dom.get_webmail_name()
    assert tmp == "ox"


    db_dom = sql_api.create_domain(
        db_api_session,
        name="mail.numerique.gouv.fr",
        features=[],
        imap_domain="imap.numerique.gouv.fr",
        smtp_domain="smtp.numerique.gouv.fr",
        webmail_domain="webmail.numerique.gouv.fr",
    )

    tmp = db_dom.get_imap_domain()
    assert tmp == "imap.numerique.gouv.fr"
    tmp = db_dom.get_smtp_domain()
    assert tmp == "smtp.numerique.gouv.fr"
    tmp = db_dom.get_webmail_domain()
    assert tmp == "webmail.numerique.gouv.fr"
    tmp = db_dom.get_imap_name()
    assert tmp == "imap.numerique.gouv.fr."
    tmp = db_dom.get_smtp_name()
    assert tmp == "smtp.numerique.gouv.fr."
    tmp = db_dom.get_webmail_name()
    assert tmp == "webmail.numerique.gouv.fr."


def test_create_user_bis(db_api_session):
    db_user = sql_api.create_user(
        db_api_session,
        name="essai-test",
        password="toto",
        is_admin=False,
        perms=[],
    )
    assert isinstance(db_user, sql_api.User)
    assert db_user.name == "essai-test"
    assert db_user.is_admin is False
    assert db_user.verify_password("toto")
    assert not db_user.verify_password("titi")


def test_update_user(db_api_session):
    db_user = sql_api.create_user(
        db_api_session,
        name="tom",
        password="toto",
        is_admin=False,
        perms=[],
    )
    assert isinstance(db_user, sql_api.User)
    assert db_user.verify_password("toto")

    db_user = sql_api.update_user_is_admin(db_api_session, "tom", True)
    assert isinstance(db_user, sql_api.User)

    db_user = sql_api.get_user(db_api_session, "tom")
    assert isinstance(db_user, sql_api.User)
    assert db_user.is_admin is True

    db_user = sql_api.update_user_password(db_api_session, "tom", "nouvo")
    assert isinstance(db_user, sql_api.User)

    db_user = sql_api.get_user(db_api_session, "tom")
    assert isinstance(db_user, sql_api.User)
    assert db_user.verify_password("nouvo")
    assert not db_user.verify_password("toto")

    db_user = sql_api.update_user_perms(db_api_session, "tom", [ "coin", "pan" ])
    assert isinstance(db_user, sql_api.User)
    assert db_user.perms == [ "coin", "pan" ]

    db_user = sql_api.update_user_perms(db_api_session, "tom", [])
    assert isinstance(db_user, sql_api.User)
    assert db_user.perms == []


def test_update_user_errors(db_api_session):
    db_user = sql_api.create_user(
        db_api_session,
        "toto",
        "titi",
        False,
        [],
    )
    assert db_user is not None

    # Je change le flag is_admin sur mon user, ça se passe bien
    db_user = sql_api.update_user_is_admin(db_api_session, "toto", True)
    assert db_user.is_admin is True

    # Je change le flag is_admin sur un user qui n'existe pas...
    db_user = sql_api.update_user_is_admin(db_api_session, "tutu", True)
    assert db_user is None

    # Je change le flag is_admin sur mon user, avec une valeur idiote
    # -> Objet inchangé, is_admin vaut toujours True (la valeur précédente)
    db_user = sql_api.update_user_is_admin(db_api_session, "toto", "idiote")
    assert db_user.is_admin is True

    # Le même test, mais avec "False" comme valeur précédente
    db_user = sql_api.update_user_is_admin(db_api_session, "toto", False)
    assert db_user.is_admin is False
    db_user = sql_api.update_user_is_admin(db_api_session, "toto", "idiote")
    assert db_user.is_admin is False

    # Je change le mot de passe d'un user qui n'existe pas -> echec
    db_user = sql_api.update_user_password(db_api_session, "tutu", "mot de passe")
    assert db_user is None

    # Je remonte mon user toto, et je stock son ancien mot de passe (haché) pour
    # vérifier que les updates échouent
    db_user = sql_api.get_user(db_api_session, "toto")
    previous = db_user.hashed_password
    db_user = sql_api.update_user_password(db_api_session, "toto", None)
    assert db_user.hashed_password == previous


def test_allows(db_api_session):
    allows = sql_api.get_allows(db_api_session, user="", domain="")
    assert allows == []

    sql_api.create_user(
        db_api_session,
        name="toto",
        password="toto",
        is_admin=False,
        perms=[],
    )
    sql_api.create_user(
        db_api_session,
        name="tutu",
        password="toto",
        is_admin=False,
        perms=[],
    )
    sql_api.create_domain(
        db_api_session,
        name="example.com",
        features=[],
    )
    sql_api.create_domain(
        db_api_session,
        name="example.net",
        features=[],
    )

    db_allow = sql_api.allow_domain_for_user(
        db_api_session,
        user="toto",
        domain="example.com",
    )
    assert isinstance(db_allow, sql_api.Allowed)
    assert db_allow.user == "toto"
    assert db_allow.domain == "example.com"

    db_allow = sql_api.allow_domain_for_user(
        db_api_session,
        user="toto",
        domain="example.net",
    )
    assert isinstance(db_allow, sql_api.Allowed)

    db_allow = sql_api.allow_domain_for_user(
        db_api_session,
        user="tutu",
        domain="example.com",
    )
    assert isinstance(db_allow, sql_api.Allowed)

    allows = sql_api.get_allows(db_api_session, user="toto")
    assert len(allows) == 2
    for item in allows:
        assert item.user == "toto"
        assert item.domain in ["example.com", "example.net"]

    allows = sql_api.get_allows(db_api_session, domain="example.com")
    assert len(allows) == 2
    for item in allows:
        assert item.domain == "example.com"
        assert item.user in ["toto", "tutu"]

    sql_api.deny_domain_for_user(db_api_session, "toto", "example.com")
    allows = sql_api.get_allows(db_api_session, user="toto")
    assert len(allows) == 1
    assert allows[0].user == "toto"
    assert allows[0].domain == "example.net"

    sql_api.delete_allows_by_user(db_api_session, user="toto")
    allows = sql_api.get_allows(db_api_session, user="toto")
    assert len(allows) == 0


def test_managed(db_api_session):
    session = db_api_session

    user_boss = sql_api.create_user(
        session, name="boss", password="boss", is_admin=True, perms=[]
    )
    user_toto = sql_api.create_user(
        session, name="toto", password="toto", is_admin=False, perms=[]
    )
    user_titi = sql_api.create_user(
        session, name="titi", password="toto", is_admin=False, perms=[]
    )
    user_tutu = sql_api.create_user(
        session, name="tutu", password="toto", is_admin=False, perms=[]
    )

    with pytest.raises(Exception) as e:
        managed = sql_api.create_managed(session, user="titi", managed_by="pasvrai")
    assert "foreign key constraint" in str(e.value)

    with pytest.raises(Exception) as e:
        managed = sql_api.create_managed(session, user="pasvrai", managed_by="toto")
    assert "foreign key constraint" in str(e.value)

    managed = sql_api.create_managed(
        session, user="titi", managed_by="toto"
    )
    assert managed.user == "titi"
    assert managed.managed_by == "toto"
    assert user_toto.managers() == []
    assert user_toto.managees() == ["titi"]
    assert user_titi.managers() == ["toto"]
    assert user_titi.managees() == []

    managed = sql_api.create_managed(
        session, user="tutu", managed_by="toto"
    )
    assert managed.user == "tutu"
    assert managed.managed_by == "toto"
    assert user_tutu.managers() == [ "toto" ]
    assert "titi" in user_toto.managees()
    assert "tutu" in user_toto.managees()

    managed = sql_api.create_managed(
        session, user="toto", managed_by="boss"
    )
    assert user_toto.managers() == [ "boss" ]
    assert len(user_toto.managees()) == 2
    assert "titi" in user_toto.managees()
    assert "tutu" in user_toto.managees()

    managed = sql_api.get_managed(session, user="toto", managed_by="titi")
    assert managed is None
    managed = sql_api.get_managed(session, user="toto", managed_by="boss")
    assert managed is not None
    assert managed.user == "toto"
    assert managed.managed_by == "boss"

    with pytest.raises(Exception) as e:
        managed = sql_api.create_managed(
            session, user="toto", managed_by="boss"
        )
    assert "Duplicate" in str(e.value) or "UNIQUE" in str(e.value)

    managed = sql_api.create_managed(
        session, user="tutu", managed_by="boss"
    )
    managed = sql_api.create_managed(
        session, user="titi", managed_by="boss"
    )

    items = sql_api.get_manageds(session)
    assert len(items) == 5
    items = sql_api.get_manageds(session, managed_by="toto")
    assert len(items) == 2
    items = sql_api.get_manageds(session, user="toto")
    assert len(items) == 1

    # On essaye de supprimer des choses qui n'existent pas (des users qui n'existent pas,
    # ou simplement des relations managed qui n'existent pas)
    managed = sql_api.delete_managed(session, user="pasvrai", managed_by="toto")
    assert managed is None
    managed = sql_api.delete_managed(session, user="toto", managed_by="pasvrai")
    assert managed is None
    managed = sql_api.delete_managed(session, user="toto", managed_by="tutu")
    assert managed is None

    count = sql_api.delete_managed_by_user(session, user="pasvrai")
    assert count == 0
    count = sql_api.delete_managed_by_user(session, user="boss")
    assert count == 0
    count = sql_api.delete_managed_by_manager(session, managed_by="pasvrai")
    assert count == 0
    count = sql_api.delete_managed_by_manager(session, managed_by="titi")
    assert count == 0

    managed = sql_api.delete_managed(session, user="toto", managed_by="boss")
    assert managed is not None
    assert managed.user == "toto"
    assert managed.managed_by == "boss"
    assert user_toto.managers() == []
    assert len(user_boss.managees()) == 2 # Il nous reste titi et tutu

    count = sql_api.delete_managed_by_manager(session, managed_by="boss")
    assert count == 2
    assert user_titi.managers() == [ "toto" ]
    assert user_tutu.managers() == [ "toto" ]

    count = sql_api.delete_managed_by_user(session, user="titi")
    assert count == 1
    assert user_titi.managers() == []

def test_count_users(db_api_session):
    # Au début, il n'y a aucun utilisateur
    n = sql_api.count_users(db_api_session)
    assert n == 0

    li = sql_api.get_users(db_api_session)
    assert len(li) == 0

    # On crée un utilisateur
    user = sql_api.create_user(db_api_session, "toto", "pass", False, [])
    assert user is not None

    # Maintenant on a 1 utilisateur
    n = sql_api.count_users(db_api_session)
    assert n == 1

    li = sql_api.get_users(db_api_session)
    assert len(li) == 1
    assert li[0].name == "toto"

    # On crée un deuxième utilisateur
    user = sql_api.create_user(db_api_session, "toto2", "pass", False, [])
    assert user is not None

    # Maintenant on a 2 utilisateur
    n = sql_api.count_users(db_api_session)
    assert n == 2

    li = sql_api.get_users(db_api_session)
    assert len(li) == 2
    assert li[0].name in [ "toto", "toto2" ]
    assert li[1].name in [ "toto", "toto2" ]


