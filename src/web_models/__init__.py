"""Web models package.

This package contains all the web models used by the FastAPI application.

Attributes:
    __all__: List of modules to import when using `from web_models import *`

Modules:
    - admin: Admin models.
    - alias: Alias models.
    - mailbox: Mailbox models.

"""
from .alias import (
    Alias,
    CreateAlias,
    UpdateAlias,
)
from .database import (
    UpdateDatabase,
)
from .domain import (
    Allowed,
    CreateDomain,
    Domain,
    ImportDomain,
    MovingAlias,
    MovingDkim,
    MovingMailbox,
    MovingSender,
    UpdateDomain,
)
from .logs import (
    CrashInfo,
    ServerLogs,
)
from .mailbox import (
    CreateMailbox,
    Mailbox,
    NewMailbox,
    UpdateMailbox,
)
from .move import (
    CheckContext,
    CheckContexts,
    CheckedContext,
    CheckedContexts,
)
from .user import (
    CreateUser,
    UpdateUser,
    User,
)
from .schema import (
    SchemaName,
)
from .server_status import (
    ModuleStatus,
    ServerStatus,
)
from .version import (
    ApiVersion,
)
from .tech import (
    TechDomain,
)
from .token import (
    Token,
)
    

__all__ = [
    Allowed,
    ApiVersion,
    CheckContext,
    CheckContexts,
    CheckedContext,
    CheckedContexts,
    CrashInfo,
    CreateAlias,
    CreateDomain,
    CreateUser,
    Domain,
    ImportDomain,
    TechDomain,
    Token,
    User,
    Alias,
    CreateMailbox,
    Mailbox,
    ModuleStatus,
    MovingAlias,
    MovingDkim,
    MovingMailbox,
    MovingSender,
    NewMailbox,
    ServerLogs,
    ServerStatus,
    UpdateAlias,
    UpdateDatabase,
    UpdateDomain,
    UpdateMailbox,
    UpdateUser,
]
