import pydantic


class ServerLogs(pydantic.BaseModel):
    logs: list[str]

class CrashInfo(pydantic.BaseModel):
    name: str
    size: int
    date: str
