"""This module contains the models used in the API about aliases.

Classes:
    - Alias: Alias model as returned by the various API verbs.
    - CreateAlias: Request to create a new alias.
    - UpdateAlias: Request to patch an already existing alias.
"""
import pydantic

class Alias(pydantic.BaseModel):
    username: str
    domain: str
    destination: str
    allow_to_send: bool | None = None

class CreateAlias(pydantic.BaseModel):
    user_name: str
    destination: str
    allow_to_send: bool | None = None

class UpdateAlias(pydantic.BaseModel):
    allow_to_send: bool | None = None
