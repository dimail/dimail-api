"""This module contains the models for users.

Classes:
    - User: A user
    - CreateUser: A user creation request
    - UpdateUser: A user modification request
"""
import uuid

import pydantic


class User(pydantic.BaseModel):
    """User model.

    Attributes:
        name (str): User name.
        is_admin (bool): Admin flag.
        uuid (uuid.UUID): User UUID.
        perms (list[str]): The global perms for that user.

    """
    name: str
    is_admin: bool
    uuid: uuid.UUID
    perms: list[str]


class CreateUser(pydantic.BaseModel):
    """CreateUser model.

    Attributes:
        name (str): User name.
        password (str): User password.
        is_admin (bool): Admin flag.
        perms (list[str]): Global perms to gove to that user

    Example:
        >>> user = CreateUser(name="user", password="password", is_admin=False)
        >>> print(user.name)
    """
    name: str
    password: str
    is_admin: bool
    perms: list[str]


class UpdateUser(pydantic.BaseModel):
    """UpdateUser model.

    Attributes:
        name (str): User name.
        password (str): User password.
        is_admin (bool): Admin flag.
        perms (list[str]): Global perms to add (begins with a +), to remove (begins with a -),
            or to set (no perm begins either with a + or a -)

    Example:
        >>> user = UpdateUser(name="user", password="password", is_admin=False)
        >>> print(user.name)
    """
    password: str | None = None
    is_admin: bool | None = None
    perms: list[str] | None = None


