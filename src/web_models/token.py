"""This module contains the Token model
"""
import pydantic


class Token(pydantic.BaseModel):
    """Token model.

    Attributes:
        access_token (str): Access token.
        token_type (str): Token type.
    """
    access_token: str
    token_type: str


