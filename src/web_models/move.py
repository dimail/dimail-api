import pydantic

from .. import enums

class CheckContext(pydantic.BaseModel):
    cid: int
    name: str
    schema_name: str
    domains: list[str]

class CheckContexts(pydantic.BaseModel):
    contexts: list[CheckContext]

class CheckedContext(pydantic.BaseModel):
    cid: int
    name: str
    schema_name: str
    domains: list[str]
    status: enums.ContextStatus
    comment: list[str]

class CheckedContexts(pydantic.BaseModel):
    contexts: list[CheckedContext]
    schemas: dict[str, str]
