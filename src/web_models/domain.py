"""This module contains the models for domains and related things.

Classes:
    - Allowed: an Allowed
    - Domain: a Domain
    - CreateDomain: a Domain creation request
    - UpdateDomain: a Domain modification request
"""
import pydantic

from .. import enums


class TestErr(pydantic.BaseModel):
    code: str
    detail: str

class TestResult(pydantic.BaseModel):
    ok: bool = True
    internal: bool = False
    errors: list[TestErr] = []

    def add_err(self, err):
        self.ok = False
        self.errors.append(TestErr(code=err["code"], detail=err["detail"]))

class CreateDomain(pydantic.BaseModel):
    """Domain creation request model. Quite the same as a Domain, but without the
    echnical attributes that will be provided internaly (test results, and so on).

    Should only be created by FastAPI as part of a POST request.
    """
    name: str
    delivery: enums.Delivery = enums.Delivery.Virtual
    features: list[enums.Feature]
    webmail_domain: str = ""
    imap_domain: str = ""
    smtp_domain: str = ""
    context_id: int | None = None
    context_name: str | None = None
    transport: str | None = None
    schema_name: str | None = None

class UpdateDomain(pydantic.BaseModel):
    delivery: enums.Delivery | None = None
    transport: str | None = None
    features: list[str] | None = None
    webmail_domain: str | None = None
    imap_domain: str | None = None
    smtp_domain: str | None = None
    context_name: str | None = None
    schema_name: str | None = None


class MovingMailbox(pydantic.BaseModel):
    username: str
    password: str
    active: str

class MovingAlias(pydantic.BaseModel):
    alias: str
    destination: str

class MovingSender(pydantic.BaseModel):
    login: str
    sender: str

class MovingDkim(pydantic.BaseModel):
    selector: str | None
    private: str | None
    public: str | None

class ImportDomain(pydantic.BaseModel):
    moving_from: str
    moving_mailboxes: list[MovingMailbox]
    moving_aliases: list[MovingAlias]
    moving_senders: list[MovingSender]
    moving_dkim: MovingDkim | None

all_tests = [
    "domain_exist",
    "mx",
    "cname_imap",
    "cname_smtp",
    "cname_webmail",
    "spf",
    "dkim",
    "postfix",
    "ox",
    "cert",
]
class Domain(pydantic.BaseModel):
    """Domain model. Contains the results of the sanity checks that may have
    been performed on the domain.

    Attributes:
        name (str): Domain name.
        state (str): may be:
            "new" (not tested yet)
            "ok" (domain is valid, tests were good)
            "broken" (errors where found during tests)
        valid (bool): says the tests have gone fine.

        delivery (str): enums.Delivery mode for the domain, one of 'relay', 'virutal' or 'alias'
        features (list[enums.Feature]): List of features.
        webmail_domain (str): Webmail domain.
        imap_domain (str): IMAP domain.
        smtp_domain (str): SMTP domain.
        context_name (str): Context name.
        transport (str): Default transport when delivery=relay

    Attributes giving informations on the various tests (ok, and/or a list of errors):
        domain_exist: fails if the domain does not exist
        mx: fails if there is no MX record or it is not the right value
        cname_imap: fails if there is no "imap" CNAME
        cname_smtp: fails if there is no "smtp" CNAME
        cname_webmail: fails if there is no "webmail" CNAME
        spf: fails if there is no SPF record or not a correct one
        dkim: fails if there is no DKIM record or not a correct one

    Example:
        >>> domain = Domain(name="example.com",
        >>>               features=[enums.Feature.Mailbox],
        >>>               webmail_domain="webmail.example.com",
        >>>               imap_domain="imap.example.com",
        >>>               smtp_domain="smtp.example.com",
        >>>               context_name="example")
        >>> print(domain.name)
    """
    name: str
    state: str
    valid: bool = True

    delivery: enums.Delivery | str
    features: list[enums.Feature]
    webmail_domain: str | None = None
    imap_domain: str | None = None
    smtp_domain: str | None = None
    context_name: str | None = None
    transport: str | None = None

    domain_exist: TestResult = TestResult()
    mx: TestResult = TestResult()
    cname_imap: TestResult = TestResult()
    cname_smtp: TestResult = TestResult()
    cname_webmail: TestResult = TestResult()
    spf: TestResult = TestResult()
    dkim: TestResult = TestResult()
    postfix: TestResult = TestResult(internal=True)
    ox: TestResult = TestResult(internal=True)
    cert: TestResult = TestResult(internal=True)

    def add_errors(self, errors: list):
        if errors is None:
            return
        for err in errors:
            self.valid = False
            getattr(self, err["test"]).add_err(err)

    def set_no_test(self):
        for test in all_tests:
            getattr(self, test).add_err({"code": "no_test", "detail": "Did not check yet"})

class Allowed(pydantic.BaseModel):
    """Allowed model.

    Attributes:
        user (str): User name.
        domain (str): Domain name.

    Example:
        >>> allowed = Allowed(user="user", domain="example.com")
        >>> print(allowed.user)
    """
    user: str
    domain: str

