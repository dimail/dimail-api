import pydantic

class ApiVersion(pydantic.BaseModel):
    version: str
    changelog: list[str]

