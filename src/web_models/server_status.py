import pydantic

class ModuleStatus(pydantic.BaseModel):
    status: bool = True
    detail: list[str] = []

    def __str__(self):
        return "\n".join(self.detail)

    def fail(self):
        self.status = False

    def add_detail(self, item: str | list[str]):
        if isinstance(item, str):
            if "\n" in item:
                self.detail += item.split("\n")
            else:
                self.detail.append(item)
        elif isinstance(item, list[str]):
            selt.detail += item
        else:
            raise Exception(f"Je ne sais pas gérer l'item {item}")

    def add_exec_failure(self,
        label: str,
        stdout: str,
        stderr: str,
    ):
        self.fail()
        self.add_detail(label)
        self.add_detail("Stdout")
        self.add_detail(stdout)
        self.add_detail("Stderr")
        self.add_detail(stderr)

    def add(self, item: "ModuleStatus"):
        if not item.status:
            self.fail()
        self.detail += item.detail

class ServerStatus(pydantic.BaseModel):
    remote_module: ModuleStatus
    certbot_module: ModuleStatus
    opendkim_module: ModuleStatus
    openxchange_module: ModuleStatus
    sql_api_module: ModuleStatus
    sql_dkim_module: ModuleStatus
    sql_imap_module: ModuleStatus
    sql_postfix_module: ModuleStatus
    status: bool

