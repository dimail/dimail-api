"""This module contains the pydantic models for the mailbox.

The mailbox is a model that represents a mailbox in the system.
It can be either an alias or a mailbox.
The mailbox can be in different states, such as OK, Broken, or Unknown.

The mailbox can be created from both the OX user and the Dovecot user.
The mailbox can be created from the OX user and the Dovecot user,
and the mailbox can be created from the OX user and the Dovecot user.

The mailbox can be created from the OX user and the Dovecot user,
and the mailbox can be created from the OX user and the Dovecot user.

Classes:
    Mailbox: Pydantic model that represents the mailbox.
    CreateMailbox: Pydantic model that represents the creation of a mailbox.
    NewMailbox: Pydantic model that represents the new mailbox.
    UpdateMailbox: Pydantic model that represents the update of a mailbox.
"""
import enum

import pydantic
from .. import enums

class Mailbox(pydantic.BaseModel):
    """Pydantic model that represents the mailbox.

    The mailbox is a model that represents a mailbox in the system.

    Attributes:
        type: enums.MailboxType: The type of the mailbox.
        status: enums.MailboxStatus: The status of the mailbox.
        active: enums.MailboxActive: The 'active' field from dovecot.
        email: str: The email of the mailbox.
        givenName: str | None: The given name of the mailbox.
        surName: str | None: The surname of the mailbox.
        displayName: str | None: The display name of the mailbox.
        additionalSenders: list[str]: The senders this user can use for sending e-mails.
    """
    type: enums.MailboxType
    status: enums.MailboxStatus
    active: enums.MailboxActive
    email: str
    givenName: str | None = None
    surName: str | None = None
    displayName: str | None = None
    additionalSenders: list[str]

    # username: str | None = None
    # domain: str | None = None
    # uuid: pydantic.UUID4

    def __eq__(self, other):
        return isinstance(self, Mailbox) and self.email == other.email

    def __hash__(self):
        return hash(self.email)


class CreateMailbox(pydantic.BaseModel):
    """Pydantic model that represents the creation of a mailbox.

    Attributes:
        givenName: str: The given name of the mailbox.
        surName: str: The surname of the mailbox.
        displayName: str: The display name of the mailbox.
    """
    givenName: str | None = None
    surName: str | None = None
    displayName: str | None = None
    additionalSenders: list[str] | None = None


class NewMailbox(pydantic.BaseModel):
    """Pydantic model that represents the new mailbox.

    Attributes:
        email: str: The email of the mailbox.
        password: str: The password of the mailbox.
        uuid: pydantic.UUID4: The UUID of the mailbox.
    """
    email: str
    password: str


class UpdateMailbox(pydantic.BaseModel):
    """Pydantic model that represents the update of a mailbox.

    Attributes:
        domain: str | None: The domain of the mailbox.
        user_name: str | None: The username of the mailbox.
        givenName: str | None: The given name of the mailbox.
        surName: str | None: The surname of the mailbox.
        displayName: str | None: The display name of the mailbox.
    """
    domain: str | None = None
    user_name: str | None = None
    givenName: str | None = None
    surName: str | None = None
    displayName: str | None = None
    active: enums.MailboxActive | None = None 
    additionalSenders: list[str] | None = None

