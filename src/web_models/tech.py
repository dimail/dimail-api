"""This module contains the TechDomain model
"""
import pydantic


class TechDomain(pydantic.BaseModel):
    """TechDomain model.

    Attributes:
        tech_name (str): the tech domain name.
        host_name (str): the host domain name.
    """
    tech_name: str
    host_name: str


