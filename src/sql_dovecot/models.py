"""SQLAlchemy models for Dovecot.

Classes:
    - ImapUser: a user of the system
"""
import passlib.hash
import sqlalchemy as sa
import sqlalchemy.dialects.mysql

from . import database
from .. import enums


class ImapUser(database.Dovecot):
    """A user of the dovecot system.

    This class models a users table in the database.

    Attributes:
        username: the username
        domain: the domain of the user
        password: the password, hashed
        active: whether the user is active

    Functions:
        set_password: set the password of the user
        check_password: check a password against the user's password
        email: get the email of the user
    """
    __tablename__ = "users"
    username = sa.Column(
        sa.String(128), nullable=False, primary_key=True
    )
    domain = sa.Column(
        sa.String(128), nullable=False, primary_key=True
    )
    password = sa.Column(sa.String(150), nullable=False)
    active = sa.Column(
        sa.dialects.mysql.CHAR(length=1), nullable=False, server_default="Y"
    )
    proxy = sa.Column(sa.CHAR(length=1), nullable=False, server_default="N")

    def set_password(self, password: str) -> None:
        """Set the password of the user.

        Hashes the password using argon2id and sets it as the user's password.

        Args:
            password: the password to set
        """
        self.password = "{ARGON2ID}" + passlib.hash.argon2.hash(password)

    def check_password(self, password: str) -> bool:
        """Check a password against the user's password.

        Args:
            password: the password to check

        Returns:
            bool: whether the password is correct

        Raises:
            Exception: if the password was not encoded by this class
        """
        if not self.password.startswith("{ARGON2ID}"):
            raise Exception("This password was not encoded by me, i can't check it")
        return passlib.hash.argon2.verify(password, self.password[len("{ARGON2ID}") :])

    def email(self) -> str:
        """Get the email of the user.

        Returns:
            str: the email of the user

        Example:
            >>> user = ImapUser(username="test", domain="example.com")
            >>> user.email()

        """
        return self.username + "@" + self.domain

    def is_active(self) -> enums.MailboxActive:
        if self.active == "Y":
            return enums.MailboxActive.Yes
        if self.active == "N":
            return enums.MailboxActive.No
        if self.active == "W":
            return enums.MailboxActive.Wait
        raise Exception(f"Je ne comprend pas active={self.active} sur un sql_dovecot.ImapUser")


#  CREATE TABLE `users` (
#   `username` varchar(128) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
#   `domain` varchar(128) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
#   `password` varchar(150) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
#   `active` char(1) NOT NULL DEFAULT 'Y',
#   `proxy` char(1) NOT NULL DEFAULT 'N',
#   PRIMARY KEY (`username`,`domain`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci |

class ImapProxy(database.Dovecot):
    """A proxy domain of the dovecot system.

    """
    __tablename__ = "domain_proxy"
    domain = sa.Column(
        sa.String(128), nullable=False, primary_key=True
    )
    host = sa.Column(
        sa.String(128), nullable=False
    )
    master = sa.Column(
        sa.String(128), nullable=False
    )
    password = sa.Column(
        sa.String(128), nullable=False
    )


# CREATE TABLE `domain_proxy` (
#   `domain` varchar(128) NOT NULL,
#   `host` varchar(128) NOT NULL,
#   `master` varchar(128) NOT NULL,
#   `pass` varchar(128) NOT NULL,
#   PRIMARY KEY (`domain`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci
