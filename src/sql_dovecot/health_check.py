from .database import get_maker
from .. import web_models

def health_check() -> web_models.ModuleStatus:
    status = web_models.ModuleStatus()
    status.add_detail("Checking the IMAP database")

    maker = None
    try:
        maker = get_maker()
        status.add_detail("get_maker = OK")
    except Exception as e:
        status.add_detail(f"ERROR: Je n'arrive pas a get_maker(), exception = {e}. Est-ce que la base est configurée?")
        status.fail()

    session = None
    if maker is not None:
        try:
            session = maker()
            status.add_detail("session = OK")
        except Exception as e:
            status.add_detail(f"ERROR: Je n'arrive pas a ouvrir une session, exception = {e}")
            status.fail()

    if session is not None:
        try:
            session.close()
            status.add_detail("close_session = OK")
        except Exception as e:
            status.add_detail(f"ERROR: Je n'arrive pas a fermer la session (WTF?), exception = {e}")
            status.fail()

    if status.status:
        status.add_detail("sql_imap_status = OK")
    else:
        status.add_detail("sql_imap_status = KO")
    print(status)
    return status

