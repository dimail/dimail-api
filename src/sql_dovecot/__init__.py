"""SQL Alchemy models and CRUD functions for Dovecot IMAP users.

Classes:
    - ImapUser: a user of the system

Functions:
    - create_user: create a user
    - delete_user: delete a user
    - get_users: get all users
    - get_user: get a user by name
    - get_maker: get a session maker
    - init_db: initialize the database

Modules:
    - crud: functions to interact with the database
    - database: database setup
    - models: the database models
"""
from .crud import (
    create_proxy,
    create_user,
    delete_user,
    get_proxy,
    get_user,
    get_users,
    insert_user,
    update_user_active,
    update_user_password,
    update_user_proxy,
    update_user_raw_password,
)
from .database import Dovecot, get_maker, init_db
from .health_check import health_check
from .models import ImapProxy, ImapUser

__all__ = [
    create_proxy,
    create_user,
    delete_user,
    Dovecot,
    get_proxy,
    get_users,
    get_user,
    get_maker,
    health_check,
    init_db,
    ImapProxy,
    ImapUser,
    insert_user,
    update_user_active,
    update_user_password,
    update_user_proxy,
    update_user_raw_password,
]
