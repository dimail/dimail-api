import sqlalchemy.orm as orm

from . import models


def get_user(
    session: orm.Session,
    username: str,
    domain: str
) -> models.ImapUser | None:
    """Get a user by name.

    Args:
        session: ORM session
        username: the name of the user
        domain: the domain of the user

    Returns:
        ImapUser: the user with the given name
    """
    return session.get(models.ImapUser, {"username": username, "domain": domain})


def get_users(
    session: orm.Session,
    domain_name: str
) -> list[models.ImapUser]:
    """Get all users for a domain name.

    Args:
        session: ORM session
        domain_name: the name of the domain

    Returns:
        list[ImapUser]: a list of all users in the domain
    """
    return session.query(models.ImapUser).filter(models.ImapUser.domain == domain_name).all()


def insert_user(
    session: orm.Session,
    username: str,
    domain: str,
    password: str,
    active: str,
    proxy: str,
) -> models.ImapUser:
    imap_user = models.ImapUser(
        username = username,
        domain = domain,
        active = active,
        password = password,
        proxy = proxy,
    )
    try:
        session.add(imap_user)
        session.commit()
    except Exception as e:
        print(str(e))
        session.rollback()
        return None
    session.refresh(imap_user)
    return imap_user

def create_user(
    session: orm.Session,
    username: str,
    domain: str,
    password: str
) -> models.ImapUser:
    """Create a user.

    Args:
        session: ORM session
        username: the name of the user
        domain: the domain of the user
        password: the password of the user

    Returns:
        ImapUser: the created user

        None: if the user could not be created
    """
    if domain is None or username is None:
        return None
    imap_user = models.ImapUser(
        username=username,
        domain=domain,
        active="Y",
        password="WILL BE ENCODED",
        proxy="N",
    )
    try:
        imap_user.set_password(password)
        session.add(imap_user)
        session.commit()
    except Exception as e:
        print(str(e))
        session.rollback()
        return None
    session.refresh(imap_user)
    return imap_user


def update_user_password(
    session: orm.Session,
    username: str,
    domain: str,
    password: str,
) -> models.ImapUser:
    if domain is None or username is None or password is None:
        return None
    imap_user = get_user(
        session = session,
        username = username,
        domain = domain,
    )
    if imap_user is None:
        return None
    try:
        imap_user.set_password(password)
        session.flush()
        session.commit()
    except Exception as e:
        print(str(e))
        session.rollback()
        return None
    session.refresh(imap_user)
    return imap_user


def delete_user(
    session: orm.Session,
    username: str,
    domain: str
) -> models.ImapUser :
    """Delete a user.

    Args:
        session: ORM session
        username: the name of the user
        domain: the domain of the user

    Returns:
        ImapUser: the deleted user

        None: if the user could not be deleted (does not exist)
    """
    user = get_user(session, username, domain)
    if user is not None:
        session.delete(user)
        session.commit()
    return user


def update_user_active(
    session: orm.Session,
    username: str,
    domain: str,
    active: str
) -> models.ImapUser:
    user = get_user(session, username, domain)
    if user is None:
        return None
    if not active in [ "Y", "N", "W" ]:
        raise Exception(f"Invalid value '{active}' for 'active', should be 'Y' or 'N' or 'W'")
    try:
        user.active = active
        session.flush()
        session.commit()
    except Exception as e:
        print(str(e))
        session.rollback()
    session.refresh(user)
    return user


def update_user_raw_password(
    session: orm.Session,
    username: str,
    domain: str,
    password: str,
) -> models.ImapUser:
    user = get_user(session, username, domain)
    if user is None:
        return None
    try:
        user.password = password
        session.flush()
        session.commit()
    except Exception as e:
        print(str(e))
        session.rollback()
    session.refresh(user)
    return user


def update_user_proxy(
    session: orm.Session,
    username: str,
    domain: str,
    proxy: str,
) -> models.ImapUser:
    user = get_user(session, username, domain)
    if user is None:
        return None
    if not proxy in [ "Y", "N" ]:
        raise Exception(f"Invalid value '{proxy}' for 'proxy', should be 'Y' or 'N'")
    try:
        user.proxy = proxy
        session.flush()
        session.commit()
    except Exception as e:
        print(str(e))
        session.rollback()
    session.refresh(user)
    return user



def get_proxy(
    session: orm.Session,
    domain: str,
) -> models.ImapProxy | None:
    return session.get(models.ImapProxy, {"domain": domain})


def create_proxy(
    session: orm.Session,
    domain: str,
    host: str,
    master: str,
    password: str,
) -> models.ImapProxy | None:
    imap_proxy = models.ImapProxy(
        domain = domain,
        host = host,
        master = master,
        password = password,
    )
    try:
        session.add(imap_proxy)
        session.commit()
    except Exception as e:
        print(str(e))
        session.rollback()
        return None
    session.refresh(imap_proxy)
    return imap_proxy


