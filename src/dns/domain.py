import logging

import dns.name
import dns.asyncresolver
import sqlalchemy.orm as orm

from .. import sql_api 
from . import dkim, spf, utils

# tech_domain = "ox.numerique.gouv.fr"
tech_domain = "unknown"

def set_tech_domain(tech: str):
    global tech_domain
    tech_domain = tech

def get_tech_domain() -> str:
    global tech_domain
    return tech_domain

class Err:
    name: str
    code: str
    detail: str
    def __init__(self, name: str, code: str, detail: str):
        self.name = name
        self.code = code
        self.detail = detail

    def __repr__(self):
        return f"Err({self.name}, code='{self.code}', detail='{self.detail}')"


async def get_auth_resolver(domain: str, insist: bool = False) -> dns.asyncresolver.Resolver:
    try:
        resolver = await utils.make_auth_resolver(domain, insist)
    except Exception:
        return None
    return resolver


async def check_exists(domain: str):
    name = dns.name.from_text(domain)
    resolver = await get_auth_resolver(domain, False)
    if resolver is None:
        return Err("domain_exist", "must_exist", f"Le domaine {domain} n'existe pas")
    return None

async def try_cname_for_mx(domain: str, required_mx: str) -> Err | None:
    name = dns.name.from_text(domain)
    resolver = await get_auth_resolver(domain, False)
    try:
        answer = await resolver.resolve(name, rdtype = "CNAME")
        target = str(answer[0].target)
    except (dns.resolver.NXDOMAIN, dns.resolver.NoAnswer):
        return Err("mx", "no_mx", "Il n'y a pas d'enregistrement MX ou CNAME sur le domaine")
    except Exception as e:
        print(f"Unexpected exception while searching for a CNAME for a MX : {e}")
        raise
    print(f"Je trouve un CNAME vers {target}, je le prend comme domaine")
    return check_mx(target, required_mx)


async def check_mx(domain: str, required_mx: str) -> Err | None:
    name = dns.name.from_text(domain)
    resolver = await utils.make_auth_resolver(domain, True)
    try:
        print(f"Je cherche un MX pour {domain}")
        answer = await resolver.resolve(name, rdtype = "MX")
    except dns.resolver.NXDOMAIN:
        print("NXDOMAIN")
        return Err("mx", "no_mx", "Il n'y a pas d'enregistrement MX sur le domaine")
    except dns.resolver.NoAnswer:
        return await try_cname_for_mx(domain, required_mx)
    except Exception as e:
        print(f"Unexpected exception while searching for MX {e}")
        raise

    nb_mx = len(answer)
    if nb_mx != 1 and False:
        return Err("mx", "one_mx", f"Je veux un seul MX, et j'en trouve {nb_mx}")
    mx = str(answer[0].exchange)
    if not mx == required_mx:
        return Err( "mx", "wrong_mx",
            f"Je veux que le MX du domaine soit {required_mx}, "
            f"or je trouve {mx}"
        )
    return None

async def check_cname(domain: str, name: str, target: str) -> Err | None:
    need_cname = f"Il faut un CNAME '{domain}' qui renvoie vers '{target}'"

    resolver = await get_auth_resolver(domain)
    if resolver is None:
        return Err(f"cname_{name}", f"no_cname_{name}", need_cname)
    try:
        answer = await resolver.resolve(domain, rdtype = "CNAME")
    except (dns.resolver.NXDOMAIN, dns.resolver.NoAnswer):
        return Err(f"cname_{name}", f"no_cname_{name}", need_cname)
    except Exception as e:
        print(f"Unexpected exception when searching for a CNAME : {e}")
        raise
    got_target = str(answer[0].target)
    if not got_target == target:
        wrong_cname = (
            f"Le CNAME pour '{domain}' n'est pas bon, " +
            f"il renvoie vers '{got_target}' et je veux '{target}'"
        )
        return Err(f"cname_{name}", f"wrong_cname_{name}", wrong_cname)
    return None

async def check_spf(domain: str, required_spf: str) -> Err | None:
    name = dns.name.from_text(domain)
    resolver = await get_auth_resolver(domain)
    try:
        answer = await resolver.resolve(name, rdtype="TXT")
    except (dns.resolver.NXDOMAIN, dns.resolver.NoAnswer):
        answer = []
    except Exception as e:
        print(f"Unexpected exception when searching for the SPF record : {e}")
        raise
    found_spf = False
    got_spf = None
    for item in answer:
        txt = str(item)
        if txt.startswith("\"v=spf1 "):
            found_spf = True
            try:
                got_spf = spf.SpfInfo(txt)
            except Exception:
                raise
    if not found_spf:
        need_spf = f"Il faut un SPF record, et il doit contenir include:{required_spf}"
        return Err("spf", "no_spf", need_spf)
    if got_spf is None:
        invalid_spf = "Le SPF record n'est pas valide (pas réussi à le lire)"
        return Err("spf", "invalid_spf", invalid_spf)
    if not got_spf.does_include(required_spf):
        wrong_spf = f"Le SPF record ne contient pas include:{required_spf}"
        return Err("spf", "wrong_spf", wrong_spf)
    return None

async def check_dkim(domain: str, selector: str | None, want_dkim: dkim.DkimInfo | None):
    if selector is None:
        print("Je ne peux pas controler la clef DKIM du domaine (pas de selecteur).")
        return None
    if want_dkim is None:
        print("Je ne peux pas controler la clef DKIM du domaine (pas de clef).")
        return None
    dkim_domain = selector + "._domainkey." + domain
    dkim_name = dns.name.from_text(dkim_domain)
    resolver = await get_auth_resolver(dkim_domain, True)
    if resolver is None:
        return Err("dkim", "no_dkim", "Il faut un DKIM record, avec la bonne clef")

    try:
        answer = await resolver.resolve(dkim_name, rdtype="TXT")
    except (dns.resolver.NXDOMAIN, dns.resolver.NoAnswer):
        answer = []
    except Exception as e:
        print(f"Unexpected exception when searching for the DKIM record : {e}")
        raise
    print(f"Je cherche les champs TXT sur dkim_name={dkim_name} pour dkim_domain={dkim_domain} et je trouve {answer}")
    found_dkim = False
    valid_dkim = False
    for item in answer:
        txt = str(item)
        if txt.startswith("\"v=DKIM1; "):
            found_dkim = True
            try:
                got_dkim = dkim.DkimInfo(txt)
            except Exception as e:
                print(f"Failed to parse the DKIM in: {txt}")
                print(f"Exception when parsing: {e}")
                got_dkim = "Wrong dkim"
            if want_dkim == got_dkim:
                valid_dkim = True
                return None
            else:
                print("Ce TXT record ne me plait pas:")
                print(f"want_dkim = {want_dkim}")
                print(f"got_dkim = {got_dkim}")
    if not found_dkim:
        return Err("dkim", "no_dkim", "Il faut un DKIM record, avec la bonne clef")
    if not valid_dkim:
        return Err("dkim", "wrong_dkim", "Le DKIM record ne contient pas la bonne clef")

    raise Exception("Je ne devrais jamais arriver ici...")
    return None

