from . import utils

class DkimInfo:
    elems: dict
    def __init__(self, text: str):
        msg = f"Parsing '{text}':\n"
        (_, text) = utils.get_txt_from_dns_record(text)
        msg = msg + f"- After extraction from dns record: '{text}'\n"
        text = utils.join_text_parts(text)
        msg = msg + f"- After joining the different parts: '{text}'\n"
        if not text.startswith("v=DKIM1;"):
            msg = msg + "- The text does not start with v=DKIM1\n"
            raise Exception(f"This does not look like a Dkim TXT record\nDetails:\n{msg}")
        self.elems = utils.get_dkim_dict(text)
        msg = msg + f"- The elems after parsing: {self.elems}\n"
        if "v" not in self.elems:
            raise Exception(f"Missing v part (version)\nDetails:\n{msg}")
        if "p" not in self.elems:
            raise Exception(f"Missing p part (public key)\nDetails:\n{msg}")
        if self.elems["p"] != "":
            if "h" not in self.elems:
                raise Exception(f"Missing h part (hash algo)\nDetails:\n{msg}")
            if "k" not in self.elems:
                raise Exception(f"Missing k part (key type)\nDetails:\n{msg}")

    def __eq__(self, other) -> bool:
        if not isinstance(other, DkimInfo):
            return False
        if self.elems == other.elems:
            return True
        return False

    def __str__(self) -> str:
        if self.elems["p"] == "":
            return f"DkimInfo(v={self.elems['v']}, p={self.elems['p']})"
        return f"DkimInfo(v={self.elems['v']}, h={self.elems['h']}, k={self.elems['k']}, p={self.elems['p']})"

    def to_spec(self) -> str:
        return f"v={self.elems['v']}; h={self.elems['h']}; k={self.elems['k']}; p={self.elems['p']}"

