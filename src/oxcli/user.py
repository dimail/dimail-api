import logging

from .. import utils

class OxUser(metaclass=utils.Traceable, debug=False):
    def __init__(self, *,
        uid: int,
        username: str,
        givenName: str,
        surName: str,
        displayName: str,
        email: str,
        ctx: "OxContext",
        senders: list[str],
    ):
        self.uid = int(uid)
        self.username = username
        self.givenName = givenName
        self.surName = surName
        self.displayName = displayName
        self.email = email
        self.ctx = ctx
        if email not in senders:
            raise Exception(f"J'ai un OxUser qui semble invalide: son adresse ({email}) n'est pas dans les senders ({senders}).")
        self.__senders = [ x for x in senders ]

    def __eq__(self, other) -> bool:
        log = logging.getLogger(__name__)
        log.debug(f"Je cherche si {self} et {other} ça serait deux fois le même OxUser")
        if not isinstance(other, OxUser):
            log.debug("Pas le bon type")
            return False
        if self.ctx.cid != other.ctx.cid:
            log.debug("Pas le meme cid")
            return False
        if self.email != other.email:
            log.debug("Pas le meme email")
            return False
        if self.uid != other.uid:
            log.debug(f"Pas le meme uid {self.uid} != {other.uid}")
            return False
        if self.username != other.username:
            log.debug("Pas le meme username")
            return False
        if self.givenName != other.givenName:
            log.debug("Pas le meme givenName")
            return False
        if self.surName != other.surName:
            log.debug("Pas le meme surName")
            return False
        if self.displayName != other.displayName:
            log.debug("Pas le meme displayName")
            return False
        if len(self.__senders) != len(other.__senders):
            log.debug("Pas le meme nombre de senders")
            return False
        if set(self.__senders) != set(other.__senders):
            log.debug("Pas les meme senders")
            return False
        log.debug("Pareil!")
        return True

    def __repr__(self) -> str:
        items = []
        if hasattr(self, 'ctx'):
            items.append(f"cid={self.ctx.cid}")
        if hasattr(self, 'uid'):
            items.append(f"uid={self.uid}")
        if hasattr(self, 'email'):
            items.append(f"email={self.email}")
        if hasattr(self, 'username'):
            items.append(f"username={self.username}")
        if hasattr(self, 'givenName'):
            items.append(f"givenName={self.givenName}")
        if hasattr(self, 'surName'):
            items.append(f"surName={self.surName}")
        if hasattr(self, 'displayName'):
            items.append(f"displayName={self.displayName}")
        if hasattr(self, '__senders'):
            items.append(f"senders={self.__senders}")
        if len(items) == 0:
            return "OxUser( virgin user in __init__ )"
        return "OxUser( " + ", ".join(items) + " )"

    @property
    def senders(self) -> list[str]:
        if self.email not in self.__senders:
            raise Exception(f"Je suis très malade, je devrais avoir mon adresse mail dans mes senders")
        return [ x for x in self.__senders ]

    async def add_senders(self,
        senders: list[str],
    ) -> None:
        all_senders = set(senders + self.__senders)
        await self.set_senders(list(all_senders))

    async def remove_sender(self,
        sender: str,
    ) -> None:
        old_senders = self.__senders
        new_senders = [
            x for x in old_senders if not x == sender
        ]
        await self.set_senders(new_senders)

    async def set_senders(self,
        senders: list[str],
    ) -> None:
        if self.email not in senders:
            print(f"In ox, dans la liste des sender, il manque moi, je me rajoute")
            senders = [ x for x in senders ]
            senders.append(self.email)
        senders = set(senders)
        if self.is_fake():
            print(f"In ox, je modifie mes senders, avant : {self.__senders}")
            self.__senders = list(senders)
            print(f"In ox, je modifie mes senders, après : {self.__senders}")
            return
        command = [
            "/opt/open-xchange/sbin/changeuser",
            "-A", self.ctx.cluster.admin_username,
            "-P", self.ctx.cluster.admin_password,
            "-c", f"{self.ctx.cid}",
            "-u", self.username,
            "-a", ",".join(senders),
        ]
        _ = await self.ctx.cluster.run_for_item(command)
        # On suppose hardiment que ça s'est bien passé...
        self.__senders = list(senders)
    
    async def change(self,
        givenName: str | None = None,
        surName: str | None = None,
        displayName: str | None = None,
    ) -> None:
        if self.is_fake():
            if givenName is not None:
                self.givenName = givenName
            if surName is not None:
                self.surName = surName
            if displayName is not None:
                self.ctx.by_displayname.pop(self.displayName)
                self.displayName = displayName
                self.ctx.by_displayname[displayName] = self
            return
        command = [
            "/opt/open-xchange/sbin/changeuser",
            "-A", self.ctx.cluster.admin_username,
            "-P", self.ctx.cluster.admin_password,
            "-c", f"{self.ctx.cid}",
            "-u", self.username,
        ]
        if givenName is not None:
            command.extend(["--givenname", givenName])
        if surName is not None:
            command.extend(["--surname", surName])
        if displayName is not None:
            command.extend(["--displayname", displayName])
        _ = await self.ctx.cluster.run_for_item(command)
    
    async def delete(self
    ) -> None:
        if self.is_fake():
            self.ctx.by_id.pop(self.uid)
            self.ctx.by_username.pop(self.username)
            self.ctx.by_displayname.pop(self.displayName)
            self.ctx.by_email.pop(self.email)
            (_, domain) = utils.split_email(self.email)
            short_list = []
            for user in self.ctx.by_domain[domain]:
                if user.uid != self.uid:
                    short_list.append(user)
            self.ctx.by_domain[domain] = short_list
            return
        command = [
            "/opt/open-xchange/sbin/deleteuser",
            "-A", self.ctx.cluster.admin_username,
            "-P", self.ctx.cluster.admin_password,
            "-c", f"{self.ctx.cid}",
            "-u", self.username,
        ]
        _ = await self.ctx.cluster.run_for_item(command)

    @classmethod
    def read_from_csv(cls, ctx: "OxContext", line: dict):
        """This method reads the user from a CSV file.

        read_from_csv reads the user from a CSV file and returns the user object.

        Args:
            ctx (OxContext): The OxContext object
            line (dict): The CSV line

        Returns:
            OxUser: The user object

        Example:
            ```python

            user = OxUser.read_from_csv(ctx, line)

            ```
        """
        uid = line["Id"]
        username = line["Name"]
        email = line["PrimaryEmail"]
        givenName = line["Given_name"]
        surName = line["Sur_name"]
        displayName = line["Display_name"]
        senders = line["Aliases"]
        if senders == "":
            senders = []
        else:
            senders = senders.split(", ")
        if email not in senders:
            raise Exception(f"OX est très malade. Dans le CSV, il ne met pas {email} dans les senders ({senders}).")

        return cls(
            uid=uid,
            username=username,
            email=email,
            givenName=givenName,
            surName=surName,
            displayName=displayName,
            ctx=ctx,
            senders=senders,
        )

    def is_fake(self) -> bool:
        return self.ctx.is_fake()


