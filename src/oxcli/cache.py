import time
import logging

from .database import OxDatabase
from .schema import OxSchema

class Cache():
    def __init__(self):
        self.reset()

    def reset(self):
        self.timestamp = 0
        self.empty = True
        self.by_id = {}
        self.by_name = {}
        self.by_domain = {}
        self.db_by_name = {
            "ox_test_database": OxDatabase(db_id="3", db_name="ox_test_database", clus=None)
        }
        self.schema_by_name = {
            "ox_test_database_5": OxSchema(db_id="3", db_name="ox_test_database", name="ox_test_database_5", clus=None)
        }

    def is_valid(self) -> bool:
        # Le cache est valide 300 secondes
        if self.empty:
            return False
        if time.time() - self.timestamp < 300:
            return True
        return False

    def fill(self, elems: list["OxContext"]):
        self.reset()
        self.timestamp = time.time()
        self.empty = False
        for ctx in elems:
            self.add_ctx(ctx)

    def add_ctx(self, ctx: "OxContext"):
        log = logging.getLogger(__name__)
        if self.empty:
            raise Exception("It is foolish to add a domain in an empty cache")
        log.debug(f"Ajout du context {ctx.name} dans le cache")
        self.by_id[f"{ctx.cid}"] = ctx
        self.by_name[ctx.name] = ctx
        for domain in ctx.domains:
            log.debug(f"- il traite aussi le domaine {domain}")
            self.by_domain[domain] = ctx

