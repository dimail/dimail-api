import logging

from .. import utils

class OxSchema(metaclass=utils.Traceable, debug=False):
    def __init__(self, *,
        db_id: int,
        db_name: str,
        name: str,
        clus: "OxCluster",
    ):
        self.db_id = db_id
        self.db_name = db_name
        self.name = name
        self.clus = clus

    @classmethod
    def read_from_csv(cls, clus: "OxCluster", line: dict):
        # id,display_name,schema_name,url,currentunits
        # "3","oxdatabase","oxdatabase_5","jdbc:mysql://172.21.0.164","16"
        # "11","ox_moving","ox_moving_15","jdbc:mysql://172.21.0.164","1"

        db_id = line["id"]
        db_name = line["display_name"]
        schema_name = line["schema_name"]

        return cls(
            db_id = db_id,
            db_name = db_name,
            name = schema_name,
            clus = clus,
        )

    # De manière amusante, createschema ne renvoie pas le même type de CSV.
    @classmethod
    def read_from_csv_creation(cls, clus: "OxCluster", db_name: str, line: dict):
        # ID, Scheme
        # "3", "oxdatabase_6"

        db_id = line["ID"]
        schema_name = line["Scheme"]
    
        return cls(
            db_id = db_id,
            db_name = db_name,
            name = schema_name,
            clus = clus,
        )



