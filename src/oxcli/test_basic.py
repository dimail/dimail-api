import pytest

from .. import oxcli

@pytest.mark.asyncio
async def test_ox(ox_cluster):
    res = await ox_cluster.list_contexts()
    assert res == []
    ctx = await ox_cluster.create_context(1, "testing", "example.com")
    assert ctx == oxcli.OxContext(
        cid=1, name="testing", domains=["example.com"], cluster=ox_cluster
    )
    s_name = await ctx.get_schema_name()
    assert s_name.startswith("ox_test_database_")

    db_list = await ox_cluster.list_databases()
    assert len(db_list) == 1
    assert isinstance(db_list[0], oxcli.OxDatabase)
    assert db_list[0].db_id == "3"
    assert db_list[0].db_name == "ox_test_database"

    schema_list = await ox_cluster.list_schemas()
    assert len(schema_list) == 1
    assert isinstance(schema_list[0], oxcli.OxSchema)
    assert schema_list[0].db_id == "3"
    assert schema_list[0].db_name == "ox_test_database"
    assert schema_list[0].name.startswith("ox_test_database_")

    new_schema = await ox_cluster.create_schema(db_name = "ox_test_database")
    assert isinstance(new_schema, oxcli.OxSchema)
    assert new_schema.db_id == "3"
    assert new_schema.db_name == "ox_test_database"

    schema_list = await ox_cluster.list_schemas()
    assert len(schema_list) == 2

    # Domain can be added via the cluster...
    ctx = await ox_cluster.add_mapping(1, "toto.com")
    assert ctx == oxcli.OxContext(
        cid=1, name="testing", domains=["example.com", "toto.com"], cluster=ox_cluster
    )

    # or via the context
    ctx = await ctx.add_mapping("tutu.net")
    assert ctx == oxcli.OxContext(
        cid=1,
        name="testing",
        domains=["example.com", "toto.com", "tutu.net"],
        cluster=ox_cluster,
    )

    ctx = await ox_cluster.get_context(1)
    assert isinstance(ctx, oxcli.OxContext)
    assert ctx.cid == 1

    ctx = await ox_cluster.get_context(2)
    assert ctx is None

    ctx = await ox_cluster.get_context_by_name("testing")
    assert isinstance(ctx, oxcli.OxContext)
    assert ctx.cid == 1

    ctx = await ox_cluster.get_context_by_name("doest_not_exist")
    assert ctx is None

    ctx = await ox_cluster.get_context_by_domain("example.com")
    assert isinstance(ctx, oxcli.OxContext)
    assert ctx.cid == 1

    ctx = await ox_cluster.get_context_by_domain("not-a-real-domain.biz")
    assert ctx is None

    # At the beginning, we have the admin user
    ctx = await ox_cluster.get_context(1)
    admin_user = oxcli.OxUser(
        uid=2,
        username="admin_user",
        givenName="Admin",
        surName="Context",
        displayName="Context Admin",
        email="oxadmin@example.com",
        ctx=ctx,
        senders = [ "oxadmin@example.com" ],
    )

    res = await ctx.list_users()
    assert res == [admin_user]

    # User can be created via the context...
    want_user = oxcli.OxUser(
        uid=3,
        username="toto",
        givenName="Given",
        surName="Sur",
        displayName="Given Sur",
        email="toto@tutu.net",
        ctx=ctx,
        senders = [ "toto@tutu.net" ],
    )
    got_user = await ctx.create_user(
        givenName="Given",
        surName="Sur",
        username="toto",
        domain="tutu.net",
    )
    assert got_user == want_user

    # Non régression: on peut mettre une apostrophe dans un nom
    got_user = await ctx.create_user(
        givenName="O\"name",
        surName="d\"Azure",
        username="quotes",
        domain="tutu.net",
    )
    assert got_user.uid == 4

    # and not yet from the cluster
    # user = ox_cluster.create_user(...)
    # return ox_cluster.get_context_by_domain(domain).create_user(...)

    res = await ctx.list_users()
    assert len(res) == 3

    got_user = await ctx.search_user("toto")
    assert got_user == [want_user]

    got_user = await ctx.search_user("quotes")
    assert len(got_user) == 1
    assert got_user[0].givenName == "O\"name"
    assert got_user[0].surName == "d\"Azure"
    assert got_user[0].displayName == "O\"name d\"Azure"

    got_user = await ctx.search_user("titi")
    assert got_user == []

    got_user = await ctx.get_user_by_email("toto@tutu.net")
    assert got_user == want_user

    got_user = await ctx.get_user_by_email("titi@tutu.net")
    assert got_user is None

    got_user = await ctx.get_user_by_name("toto")
    assert got_user == want_user

    got_user = await ctx.get_user_by_name("titi")
    assert got_user is None

    modified_user = oxcli.OxUser(
        uid=3,
        username="toto",
        givenName="newGiven",
        surName="newSur",
        displayName="Coin coin",
        email="toto@tutu.net",
        ctx=ctx,
        senders = [ "toto@tutu.net" ],
    )
    my_user = await ctx.get_user_by_name("toto")
    await my_user.change(givenName="newGiven", surName="newSur", displayName="Coin coin")
    my_user = await ctx.get_user_by_name("toto")
    assert my_user == modified_user

    # User can be created via the context, on another domain...
    want_user = oxcli.OxUser(
        uid=5,
        username="new_one",
        givenName="Given",
        surName="Surnew",
        displayName="Given Surnew",
        email="new_one@toto.com",
        ctx=ctx,
        senders = [ "new_one@toto.com" ],
    )
    got_user = await ctx.create_user(
        givenName="Given",
        surName="Surnew",
        username="new_one",
        domain="toto.com",
    )
    assert got_user == want_user

    # We can set the senders on a OxUser
    await got_user.set_senders(["coin@example.com", "pan@example.com"])
    got_user = await ctx.get_user_by_name("new_one")
    assert got_user.uid == 5
    assert len(got_user.senders) == 3
    assert "new_one@toto.com" in got_user.senders
    assert "coin@example.com" in got_user.senders
    assert "pan@example.com" in got_user.senders

    # We can add senders on a OxUser
    await got_user.add_senders(["un@example.com", "vilain@gmail.com"])
    got_user = await ctx.get_user_by_name("new_one")
    assert got_user.uid == 5
    assert len(got_user.senders) == 5
    assert "new_one@toto.com" in got_user.senders
    assert "un@example.com" in got_user.senders
    assert "vilain@gmail.com" in got_user.senders
    assert "coin@example.com" in got_user.senders
    assert "pan@example.com" in got_user.senders

    # If you try to change the senders, it fails
    with pytest.raises(Exception) as e:
        got_user.senders = [ "coin@pan.com" ]
    assert "no setter" in str(e.value)

    # When we list the users, we get all of them
    res = await ctx.list_users()
    assert len(res) == 4

    # When we list the users on domain "tutu.net", we get two of them (admin is on example.com)
    res = await ctx.list_users_in_domain("tutu.net")
    assert len(res) == 2
    assert "toto@tutu.net" in [ res[0].email, res[1].email ]
    assert "quotes@tutu.net" in [ res[0].email, res[1].email ]

    # When we list the users on domain "toto.com", we get one of them
    res = await ctx.list_users_in_domain("toto.com")
    assert len(res) == 1
    assert res == [ got_user ]

    # On supprime l'utilisateur toto, donc il n'existe plus
    await my_user.delete()
    my_user = await ctx.get_user_by_name("toto")
    assert my_user is None

    # On peut retirer un mapping d'un context, via le cluster
    ctx = await ox_cluster.remove_mapping(1, "toto.com")
    assert isinstance(ctx, oxcli.OxContext)
    assert ctx.cid == 1
    assert ctx.domains == {"example.com", "tutu.net"}

    # On peut retirer un mapping directement du context
    ctx = await ctx.remove_mapping("tutu.net")
    assert isinstance(ctx, oxcli.OxContext)
    assert ctx.cid == 1
    assert ctx.domains == {"example.com"}
