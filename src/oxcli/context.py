import logging

from .user import OxUser
from .. import utils

class OxContext():
    def __init__(self,
        cid: int,
        name: str,
        domains: set[str],
        cluster: "OxCluster",
    ):
        self.cid = cid
        self.name = name
        self.domains = set(domains)
        self.cluster = cluster
        # For fake contexts in fake clusters
        self.next_uid = 1
        # For caching
        self.by_id = {}
        self.by_email = {}
        self.by_username = {}
        self.by_displayname = {}
        self.by_domain = {}

    def __eq__(self, other) -> bool:
        if not isinstance(other, OxContext):
            return False
        if other.name == self.name and other.domains == self.domains:
            return True
        return False

    async def add_mapping(self,
        domain: str,
    ) -> "OxContext":
        if not self.cluster.is_fake():
            await self.cluster.run_for_item(
                [
                    "/opt/open-xchange/sbin/changecontext",
                    "-A", self.cluster.master_username,
                    "-P", self.cluster.master_password,
                    "--contextid", f"{self.cid}",
                    "--addmapping", domain,
                ]
            )
        # On suppose que ça s'est bien passé et on update le cache
        self.cluster.cache().by_domain[domain] = self
        self.domains.add(domain)
        return self

    async def remove_mapping(self,
        domain: str,
    ) -> "OxContext":
        if not self.cluster.is_fake():
            await self.cluster.run_for_item(
                [
                    "/opt/open-xchange/sbin/changecontext",
                    "-A", self.cluster.master_username,
                    "-P", self.cluster.master_password,
                    "--contextid", f"{self.cid}",
                    "--removemapping", domain,
                ]
            )
        # On suppose que ça s'est bien passé et on update le cache
        self.cluster.cache().by_domain.pop(domain)
        self.domains.remove(domain)
        return self

    async def __cmd_create_user(
        self,
        username: str,
        givenName: str,
        surName: str,
        displayName: str,
        email: str,
    ) -> None:
        if self.is_fake():
            uid = self.get_next_uid()
            user = OxUser(
                uid = uid,
                username = username,
                givenName = givenName,
                surName = surName,
                displayName = displayName,
                email = email,
                ctx = self,
                senders = [ email ],
            )
            self.by_id[uid] = user
            self.by_email[email] = user
            self.by_username[username] = user
            self.by_displayname[displayName] = user
            (_, domain) = utils.split_email(email)
            if not domain in self.by_domain:
                self.by_domain[domain] = []
            self.by_domain[domain].append(user)
            return
        await self.cluster.run_for_item(
            [
                "/opt/open-xchange/sbin/createuser",
                "-A", self.cluster.admin_username,
                "-P", self.cluster.admin_password,
                "-c", f"{self.cid}",
                "--username", username,
                "--password", "useless",
                "--givenname", givenName,
                "--surname", surName,
                "--displayname", displayName,
                "--email", email,
                "--language", "fr_FR",
                "--timezone", "Europe/Paris",
            ]
        )

    async def create_user(self,
        surName: str,
        givenName: str,
        displayName: str | None = None,
        email: str | None = None,
        username: str | None = None,
        domain: str | None = None,
    ) -> OxUser:
        if email is None and username is not None and domain is not None:
            email = username + "@" + domain
        elif username is None and domain is None and email is not None:
            (username, domain) = utils.split_email(email)
        else:
            raise Exception(
                "Please provide either the 'email' or both 'username' and 'domain'"
            )
        if displayName is None and givenName is not None and surName is not None:
            displayName = " ".join([givenName, surName])
        if displayName is None:
            raise Exception("displayName is mandatory")
        await self.__cmd_create_user(
            username=username,
            givenName=givenName,
            surName=surName,
            displayName=displayName,
            email=email
        )
        user = await self.get_user_by_name(username)
        if user is None:
            raise Exception("user seems created, but fail to get it")
        return user
    
    async def list_users(self, *,
        filter_func = None,
        pre_filter_func = None,
    ) -> list[OxUser]:
        if self.is_fake():
            return list(self.by_id.values())
        data = await self.cluster.run_for_csv(
            command = [
                "/opt/open-xchange/sbin/listuser",
                "-A", self.cluster.admin_username,
                "-P", self.cluster.admin_password,
                "-c", f"{self.cid}",
                "--csv",
            ],
            filter_func = filter_func,
            pre_filter_func = pre_filter_func,
            process_func = lambda x: OxUser.read_from_csv(self, x)
        )
        return data
        res = []
        for line in data:
            user = OxUser.read_from_csv(self, line)
            res.append(user)
        return res

    async def list_users_in_domain(self,
        domain: str
    ) -> list[OxUser]:
        log = logging.getLogger(__name__)
        log.info(f"Sur le context {self} dans le cluster {self.cluster} je liste les users du domaine {domain}")
        if self.is_fake():
            if domain in self.by_domain:
                return self.by_domain[domain]
            return []
        data = await self.cluster.run_for_csv(
            [
                "/opt/open-xchange/sbin/listusersbyaliasdomain",
                "-A", self.cluster.admin_username,
                "-P", self.cluster.admin_password,
                "-c", f"{self.cid}",
                "-d", "@"+domain,
                "--csv",
            ],
            process_func = lambda x: OxUser.read_from_csv(self, x)
        )
        return data
        res = []
        for line in data:
            user = OxUser.read_from_csv(self, line)
            res.append(user)
        return res
    
    async def search_user(self,
        username: str,
        *,
        pre_filter_func = None,
        filter_func = None,
    ) -> list[OxUser]:
        if self.is_fake():
            if username in self.by_username:
                return [ self.by_username[username] ]
            return []
        data = await self.cluster.run_for_csv(
            command = [
                "/opt/open-xchange/sbin/listuser",
                "-A", self.cluster.admin_username,
                "-P", self.cluster.admin_password,
                "-c", f"{self.cid}",
                "-s", username,
                "--csv",
            ],
            pre_filter_func = pre_filter_func,
            filter_func = filter_func,
            process_func = lambda x: OxUser.read_from_csv(self, x)
        )
        return data

    async def get_user_by_name(self, username: str) -> OxUser | None:
        if self.is_fake():
            if username in self.by_username:
                return self.by_username[username]
            return None
        all_users = await self.search_user(username, pre_filter_func = lambda x: x["Name"] == username) # x.username == username, in a filter_func
        if len(all_users) > 0:
            return all_users[0]
        return None
    
    async def get_user_by_email(self, email: str) -> OxUser | None:
        if self.is_fake():
            if email in self.by_email:
                return self.by_email[email]
            return None
        all_users = await self.list_users(pre_filter_func = lambda x: x["PrimaryEmail"] == email) # x.email == email, in a filter_func
        if len(all_users) > 0:
            return all_users[0]
        return None

    async def get_schema_name(self) -> str:
        # getschemaname -A oxadminmaster -P admin_master_password -c 1
        # Schema name for context 1: oxdatabase_5
        if hasattr(self, "schema_name"):
            return self.schema_name

        if self.is_fake():
            raise Exception("Je ne sais pas faire ça en mode FAKE")
        data = await self.cluster.run_for_item(
            [
                "/opt/open-xchange/sbin/getschemaname",
                "-A", self.cluster.master_username,
                "-P", self.cluster.master_password,
                "-c", f"{self.cid}",
            ]   
        )
        label, value = data.split(": ")
        if not label == f"Schema name for context {self.cid}":
            raise Exception(f"Failed to get schema name for context {self.cid}")
        self.schema_name = value.strip()
        return self.schema_name
        
    
#    async def username_exists(self, username: str) -> bool:
#        if self.is_fake():
#            if username in self.by_username:
#                return True
#            return False
#        await res = self.cluster.run_for_item(
#            [
#                "/opt/open-xchange/sbin/existsuser",
#                "-A", self.cluster.admin_username,
#                "-P", self.cluster.admin_password,
#                "-c", self.cid,
#                "--username", username,
#            ]
#        )
#        if "does not exist" in res:
#            return False
#        return True
#    
#    
#    async def displayname_exists(self, display_name: str) -> bool:
#        if self.is_fake():
#            if display_name in self.by_displayname:
#                return True
#            return False
#        await res = self.cluster.run_for_item(
#            [
#                "/opt/open-xchange/sbin/existsuser",
#                "-A", self.cluster.admin_username,
#                "-P", self.cluster.admin_password,
#                "-c", self.cid,
#                "--username", display_name,
#            ]
#        )
#        if "does not exists" in res:
#            return False
#        return True
    
    @classmethod
    def read_from_csv(cls, cluster: "OxCluster", line: dict):
        """This method reads the context from a CSV file.

        read_from_csv reads the context from a CSV file and returns the context object.

        Args:
            cluster (OxCluster): The OxCluster object
            line (dict): The CSV line

        Returns:
            OxContext: The context object

        Example:
            ```python

            ctx = OxContext.read_from_csv(cluster, line)

            ```
        """
        maps = line["lmappings"].split(",")
        cid = line["id"]
        name = line["name"]
        domains = []
        for key in maps:
            if key != cid and key != name and key != "defaultcontext":
                domains.append(key)
        return cls(cid=int(cid), name=name, domains=domains, cluster=cluster)

    def is_fake(self):
        return self.cluster.is_fake()

    def get_next_uid(self):
        self.next_uid = self.next_uid + 1
        return self.next_uid


