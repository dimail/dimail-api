
from .cluster import OxCluster
from .. import remote, web_models

async def health_check() -> web_models.ModuleStatus:
    status = web_models.ModuleStatus()
    status.add_detail("Checking that everything is fine for oxcli")

    hosts = remote.get_hosts_by_role("ox")
    names = []
    for hostname in hosts:
        infos = remote.host_infos(hostname, "ox")
        if len(infos) != 1:
            status.add_detail(f"Host {hostname} has role 'ox' and no cluster name")
            status.fail()
            continue
        names.append(infos[0])

    for name in names:
        cluster = OxCluster(name)
        c_status = await cluster.health_check()
        status.add(c_status)

    if status.status:
        status.add_detail("oxcli_status = OK")
    else:
        status.add_detail("oxcli_status = KO")
    print(status)
    return status

