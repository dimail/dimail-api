import aiocsv
import asyncio
import logging
import subprocess

from .cache import Cache
from .context import OxContext
from .user import OxUser
from .database import OxDatabase
from .schema import OxSchema
from .. import config, remote, utils, web_models

_cmd_count = 0
def cmd_count(nb: int = 0) -> int:
    global _cmd_count
    _cmd_count = _cmd_count + nb
    return _cmd_count

caches = {}

class OxCluster():
    def __init__(
        self,
        name: str | None = None,
    ):
        super().__init__()
        log = logging.getLogger(__name__)
        log.debug(f"Je construit le cluster {name}")

        hosts = remote.get_hosts_by_role("ox")
        if len(hosts) == 0:
            raise Exception("Il n'y a pas de serveur OX sur ta plateforme.")
        if (name is None or name == "default") and len(hosts) > 1:
            raise Exception(f"Je ne sais pas comment choisir mon cluster par defaut parmi {hosts}")

        if name is None or name == "default":
            log.debug("Je cherche le nom du cluster par defaut")
            hostname = hosts[0]
            infos = remote.host_infos(hostname, "ox")
            if len(infos) != 1:
                raise Exception("Le serveur {hostname} a bien le role OX, mais pas de nom de cluster.")
            name = infos[0]
            log.debug(f"Le cluster par defaut est '{name}'")

        self.name = name

        log.debug(f"Je cherche le serveur responsable du cluster {self.name}")
        for hostname in hosts:
            infos = remote.host_infos(hostname, "ox")
            if len(infos) == 1 and infos[0] == name:
                self.hostname = hostname
                break

        if not hasattr(self, 'hostname'):
            raise Exception(f"Aucun serveur n'est responsable du cluster {self.name}")
        log.debug(f"C'est le serveur {self.hostname} qui est responsable du cluster {self.name}")

        global caches
        if not name in caches:
            log.debug(f"- creating a new cache for this cluster")
            caches[name] = Cache()
        else:
            log.debug(f"- there is already a cache for this cluster")

        self.master_username = config.settings.ox_master_username
        self.master_password = config.settings.ox_master_password
        self.admin_username = config.settings.ox_admin_username
        self.admin_password = config.settings.ox_admin_password

    def __eq__(self, other) -> bool:
        if not isinstance(other, OxCluster):
            return False
        if ( self.master_username == other.master_username and
             self.master_password == other.master_password and
             self.name == self.name ):
            return True
        return False

    def cache(self) -> Cache:
        global caches
        return caches[self.name]

    def is_fake(self) -> bool:
        return remote.fake_mode()

    async def health_check(self) -> web_models.ModuleStatus:
        status = web_models.ModuleStatus()
        status.add_detail(f"Checking for cluster {self.name}")

        if self.is_fake():
            status.add_detail("I am a fake cluster, so I am fine")
            return status

        try:
            await self.run_for_item(["echo", "OK"])
            status.add_detail("ssh = OK")
        except Exception as e:
            status.add_detail(f"ERROR: Echec de l'appel ssh sur le cluster")
            status.fail()

        try:
            await self.list_contexts()
            status.add_detail("list_contexts = OK")
        except Exception as e:
            valid = False
            status.add_detail(f"ERROR: Echec de la lecture de la liste des contextes, exception = {e}")
            status.fail()

        print(status)
        return status

    ###
    ### Global cluster methods
    ### 

    async def purge(self) -> None:
        log = logging.getLogger(__name__)
        log.info(f"Purge everything from the cluster '{self.name}'")
        # Reset the cache
        log.debug("- reset the cache for this cluster")
        self.cache().reset()
        # Purge the real cluster
        if not self.is_fake():
            log.debug("- call /root/purge.sh for this cluster")
            await self.run_for_item(["/root/purge.sh"])
        log.info("Ox server is empty")
    
    def show_cache(self):
        log = logging.getLogger(__name__)
        log.debug("BEGIN - Etat du cache")
        log.debug(f"- timestamp: {self.cache().timestamp}")
        log.debug(f"- empty flag: {self.cache().empty}")
        log.debug(f"- liste des ids: {self.cache().by_id.keys()}")
        log.debug(f"- liste des noms: {self.cache().by_name.keys()}")
        log.debug(f"- liste des domaines: {self.cache().by_domain.keys()}")
        log.debug("END - Etat du cache")
    
    def make_cache_old(self):
        # Utilisé par les fixtures, pour purger le cache avant de lancer les tests.
        # Il ne faut pas faire ça en mode FAKE, parce que le cache est la totalité
        # de notre univers (nous détruirions les données en virant le cache)
        if not self.is_fake():
            self.cache().timestamp = 0

    ###
    ### Remote command execution
    ###

    async def run_for_csv(self,
        command: list[str],
        *,
        pre_filter_func = None,
        filter_func = None,
        process_func = None,
    ) -> list:
        log = logging.getLogger(__name__)
        cmd_count(1)
        log.debug(f"Run for csv... {command[0]}")
        stdout, stderr, proc = remote.stream_from_host(self.hostname, command)

        async def read_stdout(stdout):
            data = []        
            async for item in aiocsv.AsyncDictReader(stdout):
                log.debug(f"Got an item from csv reader {item}")
                if pre_filter_func is not None and not pre_filter_func(item):
                    log.debug("- pre filter it out")
                    continue
                if process_func is not None:
                    item = process_func(item)
                    log.debug("- processed the item, now: {item}")
                if filter_func is None or filter_func(item):
                    log.debug("- add it to result")
                    data.append(item)
                else:
                    log.debug("- filter it out")
            return data

        async def read_stderr(stderr):
            err = ""
            async for line in stderr:
                log.error(f"Got error : {line}")
                err += line
            return err

        data, err = await asyncio.gather(read_stdout(stdout), read_stderr(stderr))

        proc.wait()
        log.debug("Now proc is ended")
        if not proc.returncode == 0:
            print(f"stderr:\n{err}")
            log.error(f"Failed to run ssh command {command} on host {self.hostname} for csv")
            log.error(f"Stderr:\n{err}")
            raise Exception("Failed to call ssh")

        log.debug(f"Returning {len(data)} item(s)")
        return data

    async def run_for_item(self, command: list[str]) -> str:
        log = logging.getLogger(__name__)
        cmd_count(1)
        log.debug(f"Run for item... {command[0]}")
        stdout, stderr, proc = remote.stream_from_host(self.hostname, command)

        async def read_stdout(stdout):
            text = ""
            async for line in stdout:
                text += line
            return text

        async def read_stderr(stderr):
            err = ""
            async for line in stderr:
                err += line
            return err

        text, err = await asyncio.gather(read_stdout(stdout), read_stderr(stderr))

        proc.wait()
        log.debug("Now proc is ended")
        if not proc.returncode == 0:
            print(f"stderr:\n{err}")
            log.error(f"Failed to run ssh command {command} on host {self.hostname} for item")
            log.error(f"Stderr:\n{err}")
            log.error(f"Stdout;\n{text}")
            raise Exception("Failed to call ssh")

        log.debug(f"Command success... {command[0]}")
        return text

    ###
    ### Context management
    ### 

    async def list_contexts_in_db(self, db_name: str) -> [OxContext]:
        log = logging.getLogger(__name__)
        log.debug(f"Je liste les contextes sur le cluster {self.name} dans la database {db_name}")
        if self.is_fake():
            data = []
            for ctx in self.cache().by_id.values():
                if hasattr(ctx, 'schema_name') and ctx.schema_name.startswith(db_name):
                    data.append(ctx)
            return data
        data = []
        data = await self.run_for_csv(
            command = [
                "/opt/open-xchange/sbin/listcontextsbydatabase",
                "-A", self.master_username,
                "-P", self.master_password,
                "-n", db_name,
                "--csv",
            ],
            process_func = lambda x: OxContext.read_from_csv(self, x)
        )
        return data

    async def list_contexts(self) -> [OxContext]:
        log = logging.getLogger(__name__)
        log.debug(f"Je liste les contextes sur le cluster {self.name}")
        if self.cache().is_valid():
            log.debug(f"Je prend en cache")
            return list(self.cache().by_id.values())
    
        log.debug("Le cache ne suffit pas, je vais chercher...")
        data = []
        if not self.is_fake():
            log.debug("Pas un fake, on va chercher pour de vrai")
            data = await self.run_for_csv(
                command = [
                    "/opt/open-xchange/sbin/listcontext",
                    "-A", self.master_username,
                    "-P", self.master_password,
                    "--csv",
                ],
                process_func = lambda x: OxContext.read_from_csv(self, x)
            )
        log.debug("Je rempli le cache")
        self.cache().fill(data)
        return data


    async def get_context(self, cid: int) -> OxContext | None:
        log = logging.getLogger(__name__)
        log.debug(f"Je cherche le context {cid} sur le cluster {self.name}")
        if not self.cache().is_valid():
            log.debug("Le cache n'est pas valide, je le purge et je le recharge")
            self.cache().reset()
            await self.list_contexts()
    
        if f"{cid}" in self.cache().by_id:
            log.debug("Je trouve le contexte dans le cache")
            return self.cache().by_id[f"{cid}"]
        log.debug("Je ne trouve pas le contexte")
        return None


    async def get_context_by_name(self, name: str) -> OxContext | None:
        log = logging.getLogger(__name__)
        log.debug(f"Je cherche un contexte par son nom '{name}'")
        if not self.cache().is_valid() and not self.is_fake():
            log.debug("- Je refais la liste des contextes")
            _ = await self.list_contexts()
        if name in self.cache().by_name:
            log.debug(f"- J'ai trouvé un contexte pour le nom '{name}'")
            return self.cache().by_name[name]
        log.debug(f"- Je n'ai pas trouvé de contexte pour le nom '{name}'")
        return None


    async def get_context_by_domain(self, domain: str) -> OxContext | None:
        log = logging.getLogger(__name__)
        log.debug(f"Je cherche un contexte pour le domaine '{domain}'")
        if not self.cache().is_valid() and not self.is_fake():
            _ = await self.list_contexts()
        if domain in self.cache().by_domain:
            log.debug(f"- J'ai trouvé un contexte pour le domaine '{domain}'")
            ctx = self.cache().by_domain[domain]
            return ctx
        log.debug(f"- Je n'ai pas trouvé de contexte pour le domaine '{domain}'")
        return None

    async def __cmd_create_context(self,
        cid: int,
        name: str,
        domain: str,
        schema_name: str | None = None,
    ) -> None:
        log = logging.getLogger(__name__)
        ctx = OxContext(
            cid=cid,
            name=name,
            domains=[domain],
            cluster=self,
        )
        log.debug("Creation d'un contexte")
        if self.is_fake():
            log.debug("- Je suis en mode fake, je forge un user")
            if schema_name is None:
                schema_name = "ox_test_database_5"
            await ctx.create_user(
                username="oxadmin",
                givenName="Admin",
                surName="Context",
                displayName="Context Admin",
                domain=domain,
            )
            user = ctx.by_username["oxadmin"]
            user.username="admin_user"
            ctx.by_username["admin_user"] = user
            ctx.by_username.pop("oxadmin")
            if schema_name is not None:
                ctx.schema_name = schema_name
        else:
            log.debug("- Je suis en mode reel, je parle avec java")
            cmd = [
                "/opt/open-xchange/sbin/createcontext",
                "-A", self.master_username,
                "-P", self.master_password,
                "--contextid", f"{cid}",
                "--contextname", name,
                "--addmapping", domain,
                "--quota", "1024",
                "--access-combination-name=groupware_standard",
                "--language=fr_FR",
                "--username", self.admin_username,
                "--password", self.admin_password,
                "--displayname", "Context Admin",
                "--givenname", "Admin",
                "--surname", "Context",
                "--email", f"oxadmin@{domain}",
            ]
            if schema_name is not None:
                schema = await self.get_schema_by_name(schema_name)
                if schema is not None:
                    cmd.append("--destination-database-id")
                    cmd.append(str(schema.db_id))
                    cmd.append("--schema")
                    cmd.append(schema_name)
            log.info(f"Je crée le contexte avec la commande: {cmd}")
            await self.run_for_item(cmd)
        # On suppose que ça s'est bien passé et on injecte dans le cache de force
        log.debug("- J'injecte mon contexte dans le cache")
        self.cache().add_ctx(ctx)
    
    async def create_context(self,
        cid: int | None,
        name: str,
        domain: str,
        schema_name: str | None = None,
    ) -> OxContext:
        log = logging.getLogger(__name__)
        log.debug(f"Create context, on controle les parametres.")
        log.debug(f"- Appel avec cid=<{cid}> name=<{name}> et domain=<{domain}>.")

        all_contexts = await self.list_contexts()
        max_id = 0
        log.debug(f"- On cherche le context id")
        for ctx in all_contexts:
            if cid is not None and ctx.cid == cid:
                log.error(f"- Le context id {cid} est déjà utilisé")
                raise Exception(f"Context id {cid} already exists")
            if ctx.name == name:
                log.error(f"- Il y a déjà un contexte qui s'appelle {name}")
                raise Exception(f"Context name {name} already exists")
            if domain in ctx.domains:
                log.error(f"- Il y a déjà un contexte qui se charge du domaine {domain}")
                raise Exception(
                    f"Domain already {domain} mapped to context name={ctx.name}, id={ctx.id}"
                )
            if ctx.cid > max_id:
                max_id = ctx.cid
        if cid is None:
            cid = max_id + 1
            log.debug(f"Le context_id calculé est {cid}")
        log.debug("Create context: le domaine n'est déclaré nulle part pour le moment, let's do it")

        await self.__cmd_create_context(cid, name, domain, schema_name)
        ctx = await self.get_context(cid)
        if ctx is None:
            raise Exception(
                "Created the context, but failed to list it, and with a message that is longer..."
            )
        return ctx

    ###
    ### Database and Schema management
    ###

    async def list_databases(self) -> [OxDatabase]:
        log = logging.getLogger(__name__)
        log.debug(f"Je liste les database du cluster {self.name}")
        # listdatabase -A oxadminmaster -P admin_master_password --csv
        if self.is_fake():
            data = []
            for db in self.cache().db_by_name.values():
                data.append(db)
            return data
        data = await self.run_for_csv(
            command = [
                "/opt/open-xchange/sbin/listdatabase",
                "-A", self.master_username,
                "-P", self.master_password,
                "--csv",
            ],
            process_func = lambda x: OxDatabase.read_from_csv(self, x),
        )
        cache = self.cache()
        cache.db_by_name = {}
        for db in data:
            cache.db_by_name[db.db_name] = db
        return data

    async def list_schemas(self) -> [OxSchema]:
        log = logging.getLogger(__name__)
        log.debug(f"Je liste les schemas du cluster {self.name}")
        # listdatabaseschema -A oxadminmaster -P admin_master_password --csv
        if self.is_fake():
            data = []
            for schema in self.cache().schema_by_name.values():
                data.append(schema)
            return data
        data = await self.run_for_csv(
            command = [
                "/opt/open-xchange/sbin/listdatabaseschema",
                "-A", self.master_username,
                "-P", self.master_password,
                "--csv",
            ],
            process_func = lambda x: OxSchema.read_from_csv(self, x),
        )
        cache = self.cache()
        cache.schema_by_name = {}
        for schema in data:
            cache.schema_by_name[schema.name] = schema
        return data


    async def get_schema_by_name(self, schema_name: str) -> OxSchema:
        log = logging.getLogger(__name__)
        if self.is_fake():
            if schema_name in self.cache().schema_by_name:
                return self.cache().schema_by_name[schema_name]
            return None
        if schema_name in self.cache().schema_by_name:
            return self.cache().schema_by_name[schema_name]
        _ = await self.list_schemas()
        if schema_name in self.cache().schema_by_name:
            return self.cache().schema_by_name[schema_name]
        return None


    async def create_schema(self, db_name) -> [OxSchema]:
        if not db_name in self.cache().db_by_name:
            raise Exception(f"Je ne connais pas cette db {db_name}")
        db_id = self.cache().db_by_name[db_name].db_id
        if self.is_fake():
            count = 42
            schema_name = db_name + f"_{count}"
            while schema_name in self.cache().schema_by_name:
                count += 1
                schema_name = db_name + f"_{count}"
            self.cache().schema_by_name[schema_name] = OxSchema(
                db_id = db_id,
                db_name = db_name,
                name = schema_name,
                clus = self,
            )
            return self.cache().schema_by_name[schema_name]

        log = logging.getLogger(__name__)
        cmd = [
            "/opt/open-xchange/sbin/createschema",
            "-A", self.master_username,
            "-P", self.master_password,
            "-i", db_id,
            "--csv",
        ]
        log.info(f"Je crée le schema dans la base {db_name} avec la commande {cmd}")
        data = await self.run_for_csv(
            command = cmd,
            process_func = lambda x: OxSchema.read_from_csv_creation(self, db_name, x),
        )
        if not len(data) == 1:
            raise Exception(f"Echec de la création d'un schema dans la base {db_name}")
        schema_name = data[0].name
        log.info(f"Le nom du schema demandé obtenu est {schema_name}")
        self.cache().schema_by_name[schema_name] = data[0]
        return data[0]

    ###
    ### Mapping management
    ###

    async def add_mapping(self,
        cid: int,
        domain: str,
    ) -> OxContext:
        log = logging.getLogger(__name__)
        log.debug("Ajout du mapping du domaine '{domain}' sur le context cid='{cid}'")
        ctx = await self.get_context(cid)
        if ctx is None:
            log.error("- Ce contexte n'existe pas")
            raise Exception("Context not found")
        return await ctx.add_mapping(domain)

    async def remove_mapping(self,
        cid: int,
        domain: str,
    ) -> OxContext:
        log = logging.getLogger(__name__)
        log.debug("On retire le mapping du domaine '{domain}' du context cid='{cid}'")
        ctx = await self.get_context(cid)
        if ctx is None:
            log.error("- Ce contexte n'existe pas")
            raise Exception("Context not found")
        return await ctx.remove_mapping(domain)

    ###
    ### User management
    ###

    async def get_user_by_email(self,
        email: str,
    ) -> OxUser:
        username, domain = utils.split_email(email)
        ctx = await self.get_context_by_domain(domain)
        if ctx is None:
            # Aucun contexte ne gère ce domaine
            return None
        user = await ctx.get_user_by_name(username)
        return user
