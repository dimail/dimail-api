import logging

from .. import utils

class OxDatabase(metaclass=utils.Traceable, debug=False):
    def __init__(self, *,
        db_id: int,
        db_name: str,
        clus: "OxCluster",
    ):
        self.db_id = db_id
        self.db_name = db_name
        self.clus = clus

    @classmethod
    def read_from_csv(cls, clus: "OxCluster", line: dict):
        # id,display_name,url,master,masterid,maxUnits,currentunits,poolHardLimit,poolMax,poolInitial,login,password,driver,read_id,scheme
        # "3","oxdatabase","jdbc:mysql://172.21.0.164","true","0","1000","16","1","100","0","openexchange","db_password","com.mysql.jdbc.Driver",,

        db_id = line["id"]
        db_name = line["display_name"]

        return cls(
            db_id = db_id,
            db_name = db_name,
            clus = clus,
        )



