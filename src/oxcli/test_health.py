import pytest

from .. import oxcli

@pytest.mark.asyncio
async def test_ox(ox_cluster):
    status = await oxcli.health_check()
    assert status.status is True
    assert "oxcli_status = OK" in status.detail
