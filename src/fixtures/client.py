import typing

import fastapi
import httpx
import pytest
import pytest_asyncio

from .. import dns, main, others

@pytest.fixture(scope="session")
def dns_tech(log) -> typing.Generator:
    log.info("SETUP dns tech domain")
    dns.set_tech_domain("ox.numerique.gouv.fr")
    yield "ox.numerique.gouv.fr"
    log.info("TEARDOWN dns tech domain")
    dns.set_tech_domain("noconf.fr")

@pytest.fixture(scope="function")
def set_others(log) -> typing.Generator:
    log.info("SETUP others")
    others.set_myself("testing")
    others.set_others({"recent": "http://admin:coincoin@localhost:8002"}, reset = True)
    yield "testing"
    log.info("TEARDOWN others")
    others.reset_others()
    others.reset_myself()

@pytest.fixture(scope="function")
def client(log, db_api, db_dkim, db_postfix, db_dovecot, ox_cluster, setup_remote, cb_manager, dk_manager, dns_tech) -> typing.Generator:
    """Fixture to get the API client"""
    log.info("SETUP the api client")
    main.set_testing(True)
    with fastapi.testclient.TestClient(main.app) as client:
        yield client
    log.info("TEARDOWN the api client")

@pytest_asyncio.fixture(scope="function")
async def async_client(log, db_api, db_dkim, db_postfix, db_dovecot, ox_cluster, setup_remote, cb_manager, dk_manager, dns_tech):
    log.info("SETUP the async api client")
    main.set_testing(True)
    async with httpx.AsyncClient(
        transport = httpx.ASGITransport(
            app = main.app
        ),
        base_url = 'http://test',
        follow_redirects = True,
    ) as client:
        yield client
    log.info("TEARDOWN the async api client")
