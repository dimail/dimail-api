import os
import pathlib
import typing

import pytest
import sqlalchemy as sa

import alembic.command
import alembic.config

def make_db_with_user(name: str, user: str, password: str, conn: sa.Connection) -> str:
    """Utility function, for internal use. Ensure a database, named 'name'
    is created and empty. 'conn' is a mariadb/mysql connection as root user"""
    conn.execute(sa.text(f"drop database if exists {name};"))
    conn.execute(sa.text(f"create database {name};"))
    conn.execute(
        sa.text(f"grant ALL on {name}.* to {user}@'%' identified by '{password}';")
    )
    port = conn.engine.url.port
    host = conn.engine.url.host
    return f"mysql+pymysql://{user}:{password}@{host}:{port}/{name}"

def make_db(name: str, conn: sa.Connection) -> str:
    """Utility function, for internal use. Ensure a database, named 'name'
    is created and empty. 'conn' is a mariadb/mysql connection as root user"""
    if conn == "sqlite":
        if pathlib.Path(f"/tmp/{name}.db").is_file():
            pathlib.Path.unlink(f"/tmp/{name}.db")
        return f"sqlite:////tmp/{name}.db"
    return make_db_with_user(name, "test", "toto", conn)

def drop_db(name: str, conn: sa.Connection):
    """Utility function, for internal use. Drops a database."""
    if conn == "sqlite":
        pathlib.Path.unlink(f"/tmp/{name}.db")
        return
    conn.execute(sa.text(f"drop database if exists {name};"))


@pytest.fixture(scope="session")
def root_db(log, dimail_mode, root_db_url) -> typing.Generator:
    """
    Fixtures that makes a connection to mariadb/mysql as root available.
    """
    if dimail_mode == "real":
        log.info(f"CONNECTING as mysql root to {root_db_url}")
        root_db = sa.create_engine(root_db_url)
        conn = root_db.connect()
        yield conn
        conn.close()
        log.info(f"CLOSING root mysql connection to {root_db_url}")
    else:
        yield "sqlite"

@pytest.fixture(scope="session")
def alembic_config(dimail_mode, log) -> typing.Generator:
    """Fixtures that makes available the alambic config, for future use. The
    list of databases is made empty. Alembic is told not to do anything to
    the logger, so that we can have logs where we want them."""
    log.info("SETUP alembic config")
    if not os.path.exists("alembic.ini"):
        raise Exception("Je veux tourner dans src, avec mon fichier alembic.ini")
    cfg = None
    if dimail_mode == "fake":
        cfg = {}
    else:
        cfg = alembic.config.Config("alembic.ini")
        cfg.set_main_option("databases", "")
        cfg.set_main_option("init_logger", "False")
    yield cfg
    log.info("TEARDOWN alembic config")


def add_db_in_alembic_config(alembic_config, db_name: str, db_url: str, log):
    """Utility function, for internal use. Modify an alembic config object,
    adds a database, set the right url for this database. This makes sure the
    tests are run against databases which are not used by the server running
    outside on our systems."""
    if isinstance(alembic_config, dict):
        alembic_config[db_name]=db_url
        return
    before = alembic_config.get_main_option("databases")
    db_list = before.split(",")
    if before == "":
        db_list = []
    if db_name in db_list:
        raise Exception(
            f"The database {db_name} is already in alembic config. Can't add it twice."
        )
    db_list.append(db_name)
    after = ",".join(db_list)
    log.info(
        f"After adding {db_name}, the list of databases is {db_list} leading to {after}"
    )
    alembic_config.set_main_option("databases", after)
    alembic_config.set_section_option(db_name, "sqlalchemy.url", db_url)


def remove_db_from_alembic_config(alembic_config, db_name: str, log):
    """Utility function, for internal use. Removes a database that was previously
    added into the alembic config."""
    if isinstance(alembic_config, dict):
        alembic_config.pop(db_name)
        return
    before = alembic_config.get_main_option("databases")
    db_list = before.split(",")
    if before == "":
        db_list = []
    if db_name not in db_list:
        raise Exception(
            f"The database {db_name} is not in alembic config. Can't remove it twice."
        )
    db_list = [x for x in db_list if x != db_name]
    after = ",".join(db_list)
    log.info(
        f"After removing {db_name}, the list of databases is {db_list} leading to {after}"
    )
    alembic_config.set_main_option("databases", after)


@pytest.fixture(scope="session")
def db_api_url(dimail_mode, root_db, alembic_config, log) -> typing.Generator:
    """Fixture that makes a database available as the API db for testing.
    Adds the database and the url in alembic config. Yields an url to
    connect to that db."""
    log.info("SETUP api database (drop and create)")
    url = make_db("test_api", root_db)
    add_db_in_alembic_config(alembic_config, "api", url, log)
    yield url
    remove_db_from_alembic_config(alembic_config, "api", log)
    drop_db("test_api", root_db)
    log.info("TEARDOWN api database (drop)")


@pytest.fixture(scope="session")
def db_dkim_url(dimail_mode, root_db, alembic_config, log) -> typing.Generator:
    """Fixture that makes a database available as the Opendkim db for testing.
    Adds the database and the url in alembic config. Yields an url to connect
    to that db."""
    log.info("SETUP dkim database (drop and create)")
    url = make_db("test_dkim", root_db)
    add_db_in_alembic_config(alembic_config, "opendkim", url, log)
    yield url
    remove_db_from_alembic_config(alembic_config, "opendkim", log)
    drop_db("test_dkim", root_db)
    log.info("TEARDOWN dkim database (drop)")

@pytest.fixture(scope="session")
def db_dovecot_url(dimail_mode, root_db, alembic_config, log) -> typing.Generator:
    """Fixture that makes a database available as the Dovecot db for testing.
    Adds the database and the url in alembic config. Yields an url to
    connect to that db."""
    log.info("SETUP dovecot database (drop and create)")
    url = make_db("test_dovecot", root_db)
    add_db_in_alembic_config(alembic_config, "dovecot", url, log)
    yield url
    remove_db_from_alembic_config(alembic_config, "dovecot", log)
    drop_db("test_dovecot", root_db)
    log.info("TEARDOWN dovecot database (drop)")


@pytest.fixture(scope="session")
def db_postfix_url(dimail_mode, root_db, alembic_config, log) -> typing.Generator:
    """Fixtures that makes a database available as the Postfix db for testing.
    Adds the database and the url in alembic config. Yields an url to
    connect to that db."""
    log.info("SETUP postfix database (drop and create)")
    url = make_db("test_postfix", root_db)
    add_db_in_alembic_config(alembic_config, "postfix", url, log)
    yield url
    remove_db_from_alembic_config(alembic_config, "postfix", log)
    drop_db("test_postfix", root_db)
    log.info("TEARDOWN postfix database (drop)")


@pytest.fixture(scope="function")
def alembic_run(dimail_mode, alembic_config, log) -> typing.Generator:
    """Fixture that makes sure alembic has run on all the registered
    databases."""
    log.info("SETUP with alembic (upgrade)")
    if dimail_mode == "real":
        bases = alembic_config.get_main_option("databases")
        log.info(f" - Here are the databases : {bases}")
        alembic.command.upgrade(alembic_config, "head")
    yield ""
    for sess in sa.orm.session._sessions.values():
        if sess._close_state != sa.orm.session._SessionCloseState.CLOSED:
            caller = sess.info["caller"]
            file   = sess.info["file"]
            db     = sess.info["db"]
            url    = sess.info["url"]
            log.error(f"We have an UNCLOSED session in the orm: {sess}")
            log.error(f"   caller: {caller} @ {file}")
            log.error(f"   db: {db} at url {url}")
            sess.close()
    log.info("TEARDOWN with alembic (downgrade)")
    if dimail_mode == "real":
        bases = alembic_config.get_main_option("databases")
        alembic.command.downgrade(alembic_config, "base")
        log.info(f" - Here are the databases : {bases}")


