import typing

import pytest

@pytest.fixture(scope="session", params=["fake", "real"])
def dimail_mode(log, request) -> typing.Generator:
    mode = request.param
    log.info(f"SETUP testing mode={mode}")
    yield mode
    log.info(f"TEARDOWN testing mode={mode}")

