import typing

import pytest
import pytest_asyncio
import fastapi

@pytest.fixture(scope="function")
def admin(db_api, log, client) -> typing.Generator:
    # Database is empty, fake auth, creating the first admin
    user = "admin"
    password = "admin"
    res = client.post(
        "/users/",
        json={"name": user, "password": password, "is_admin": True, "perms": []},
        auth=("useless", "useless"),
    )
    assert res.status_code == fastapi.status.HTTP_201_CREATED

    log.info("SETUP admin api user")
    yield {"user": user, "password": password}
    log.info("TEARDOWN admin api user")

def _make_user(log, client, admin, login, password):
    log.info(f"- creating the user {login}")
    res = client.post(
        "/users/",
        json={"name": login, "password": password, "is_admin": False, "perms": []},
        auth=(admin["user"], admin["password"]),
    )
    assert res.status_code == fastapi.status.HTTP_201_CREATED

    log.info(f"- creating a token for user {login}")
    res = client.get("/token/", auth=(login, password))
    assert res.status_code == 200
    token = res.json()["access_token"]

    return token

@pytest.fixture(scope="function")
def admin_token(log, client, admin):
    log.info("SETUP admin user with token")
    res = client.get("/token/", auth=(admin["user"], admin["password"]))
    assert res.status_code == 200
    token = res.json()["access_token"]
    yield {**admin, "token": token}

    log.info("TEARDOWN admin user with token")

@pytest.fixture(scope="function")
def virgin_user(log, client, admin, request):
    login, password = request.param.split(":",1)

    log.info(f"SETUP api user {login} (with no permissions)")
    token = _make_user(log, client, admin, login, password)
    yield {"user": login, "password": password, "token": token}

    log.info(f"TEARDOWN api user {login}")


@pytest.fixture(scope="function")
def normal_user(log, client, admin, request):
    login, password = request.param.split(':',1)

    log.info(f"SETUP api user {login}")
    token = _make_user(log, client, admin, login, password)

    yield {"user": login, "token": token, "password": password}
    log.info(f"TEARDOWN api user {login}")

def _make_domain(
    log, client, admin,
    name: str,
    delivery: str,
    features: list[str],
    user: str | None = None,
    context_name: str = "useless",
):

    log.info("- creating the domain")
    res = client.post(
        "/domains/",
        params = {
            "no_check": "true",
        },
        json = {
            "name": name,
            "delivery": delivery,
            "features": features,
            "context_name": context_name,
        },
        auth = (admin["user"], admin["password"]),
    )
    assert res.status_code == fastapi.status.HTTP_201_CREATED

    if user is not None:
        res = client.post(
            "/allows/",
            json={"user": user, "domain": name},
            auth=(admin["user"], admin["password"]),
        )
        assert res.status_code == fastapi.status.HTTP_201_CREATED

@pytest.fixture(scope="function")
def domain(log, db_api_session, db_postfix_session, client, admin, dk_manager, request):
    names = request.param.split(",")
    features = []

    log.info(f"SETUP domain {names}, no features, no allowed users")
    for name in names:
        _make_domain(log, client, admin, name, "virtual", features, None)

    yield {"name": names[0], "features": features, "all_domains": names}
    log.info(f"TEARDOWN domain {names}")

# Syntaxe du param: les noms des domaines séparés par une virgule
# Les domaines ont la feature mailbox, et sont allow pour normal_user
# Ex: toto.fr,machin.com,truc.net
@pytest.fixture(scope="function")
def domain_mail(log, db_api_session, db_postfix_session, client, admin, normal_user, dk_manager, request):
    names = request.param.split(",")
    login = normal_user["user"]
    features = ["mailbox"]

    if len(names) > 1:
        log.info(f"SETUP domains {names}, allowed for user {login}, features: mailbox only")
    else:
        log.info(f"SETUP domain {names[0]}, allowed for user {login}, features: mailbox only")
    for name in names:
        _make_domain(log, client, admin, name, "virtual", features, login)

    yield {"name": names[0], "features": features, "all_domains": names}
    if len(names) > 1:
        log.info(f"TEARDOWN domain {names}")
    else:
        log.info(f"TEARDOWN domain {names[0]}")

# Syntaxe du param: les noms de domaines séparés par une virgule, les
# différents contextes séparés par un point-virgule.
# Ex: toto.fr,toto.net:dimail;machin.com:ctx2
@pytest_asyncio.fixture(scope="function")
async def domain_web(log, db_api_session, db_postfix_session, client, admin, normal_user, ox_cluster, dk_manager, request):
    contexts = request.param.split(";")
    login = normal_user["user"]
    features = ["mailbox", "webmail"]

    all_domains = []
    all_contexts = []
    ctx_by_domain = {}
    domains_by_ctx = {}

    log.info(f"SETUP webmail domains, allowed for user {login}, features {features}")
    for ctx_info in contexts:
        domains, context_name = ctx_info.split(":",1)
        names = domains.split(",")

        all_contexts.append(context_name)
        domains_by_ctx[context_name] = []
        for name in names:
            log.info(f"- create domain {name}")
            all_domains.append(name)
            domains_by_ctx[context_name].append(name)
            ctx_by_domain[name] = context_name
            _make_domain(log, client, admin, name, "virtual", features, login, context_name)
            log.info(f"- check the domain {name} is declared in the context {context_name}")
            ctx = await ox_cluster.get_context_by_name(context_name)
            if ctx is None:
                print(f"Je ne trouve pas de contexte pour le nom {context_name}")
                exit(1)
            assert name in ctx.domains
        ox_cluster.make_cache_old()

    yield {
        "name": all_domains[0],
        "delivery": "virtual",
        "features": features,
        "context_name": all_contexts[0],
        "all_domains": all_domains,
        "all_contexts": all_contexts,
        "ctx_by_domain": ctx_by_domain,
        "domains_by_ctx": domains_by_ctx,
    }
    log.info(f"TEARDOWN web domains {all_domains}")

