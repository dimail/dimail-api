import typing

import pytest

from .. import sql_api, sql_dkim, sql_dovecot, sql_postfix

@pytest.fixture(scope="function")
def db_api(dimail_mode, alembic_run, db_api_url, log) -> typing.Generator:
    """Fixture that makes sure a database is registered and ready to
    be used as the API db during tests."""
    log.info(f"SETUP sql_api to use the testing db (url={db_api_url})")
    engine = sql_api.init_db(db_api_url)
    if dimail_mode == "fake":
        log.info("- sqlite database, creating the tables")
        sql_api.Api.metadata.create_all(engine)
    yield
    log.info(f"TEARDOWN sql_api on testing db (url={db_api_url})")
    if dimail_mode == "fake":
        log.info("- sqlite database, removing the tables")
        sql_api.Api.metadata.drop_all(engine)


@pytest.fixture(scope="function")
def db_dkim(dimail_mode, alembic_run, db_dkim_url, log) -> typing.Generator:
    """Fixture that makes sure a database is reagistered and ready to
    be used as the dkim db during tests."""
    log.info(f"SETUP sql_dkim to use the testing db (url={db_dkim_url})")
    engine = sql_dkim.init_db(db_dkim_url)
    if dimail_mode == "fake":
        log.info("- sqlite database, creating the tables")
        sql_dkim.Dkim.metadata.create_all(engine)
    yield
    log.info(f"TEARDOWN sql_dkim on testing db (url={db_dkim_url})")
    if dimail_mode == "fake":
        log.info("- sqlite database, removing the tables")
        sql_dkim.Dkim.metadata.drop_all(engine)


@pytest.fixture(scope="function")
def db_dovecot(dimail_mode, alembic_run, db_dovecot_url, log) -> typing.Generator:
    """Fixture that makes sure a database is registered and ready to
    be used as the Dovecot db during tests."""
    log.info(f"SETUP sql_dovecot to use the testing db (url={db_dovecot_url})")
    engine = sql_dovecot.init_db(db_dovecot_url)
    if dimail_mode == "fake":
        log.info("- sqlite database, creating the tables")
        sql_dovecot.Dovecot.metadata.create_all(engine)
    yield
    log.info(f"TEARDOWN sql_dovecot on testing db (url={db_dovecot_url})")
    if dimail_mode == "fake":
        log.info("- sqlite database, removing the tables")
        sql_dovecot.Dovecot.metadata.drop_all(engine)


@pytest.fixture(scope="function")
def db_postfix(dimail_mode, alembic_run, db_postfix_url, log) -> typing.Generator:
    """Fixture that makes sure a database is registered and ready to
    be used as the Dovecot db during tests."""
    log.info(f"SETUP sql_postfix to use the testing db (url={db_postfix_url})")
    engine = sql_postfix.init_db(db_postfix_url)
    if dimail_mode == "fake":
        log.info("- sqlite database, creating the tables")
        sql_postfix.Postfix.metadata.create_all(engine)
    yield
    log.info(f"TEARDOWN sql_dovecot on testing db (url={db_postfix_url})")
    if dimail_mode == "fake":
        log.info("- sqlite database, droping the tables")
        sql_postfix.Postfix.metadata.drop_all(engine)


@pytest.fixture(scope="function")
def db_api_session(db_api, log) -> typing.Generator:
    """Fixture that makes a connection to the API database available."""
    log.info("SETUP sql_api orm session")
    maker = sql_api.get_maker()
    session = maker()
    try:
        yield session
    finally:
        log.info("TEARDOWN sql_api orm session")
        session.close()


@pytest.fixture(scope="function")
def db_dkim_session(db_dkim, log) -> typing.Generator:
    """Fixtures that makes a connection to the dkim database available."""
    log.info("SETUP sql_dkim orm session")
    maker = sql_dkim.get_maker()
    session = maker()
    try:
        yield session
    finally:
        log.info("TEARDOWN sql_dkim orm session")
        session.close()


@pytest.fixture(scope="function")
def db_dovecot_session(db_dovecot, log) -> typing.Generator:
    """Fixture that makes a connecion to the Dovecot database available."""
    log.info("SETUP sql_dovecot orm session")
    maker = sql_dovecot.get_maker()
    session = maker()
    try:
        yield session
    finally:
        log.info("TEARDOWN sql_dovecot orm session")
        session.close()


@pytest.fixture(scope="function")
def db_postfix_session(db_postfix, log) -> typing.Generator:
    """Fixture that makes a connecion to the Dovecot database available."""
    log.info("SETUP sql_postfix orm session")
    maker = sql_postfix.get_maker()
    session = maker()
    try:
        yield session
    finally:
        log.info("TEARDOWN sql_postfix orm session")
        session.close()


