import pytest
import subprocess
import time
import typing

import python_on_whales as pyow
import testcontainers.core as tc_core
import testcontainers.mysql as tc_mysql

from .. import config

def make_image(log, name: str) -> str:
    pyw = pyow.DockerClient(log_level="WARN")
    tag = f"dimail-{name}"
    log.info(f"[START] construction de l'image -> {tag}")
    pyw.build(f"../{name}/", tags=tag)
    time.sleep(2)  # pour être sûr que le tag soit bien arrivé dans le registry
    log.info(f"[END] construction de l'image -> {tag}")
    return tag

def create_ssh_key(log, name: str):
    return_code = subprocess.call(["/bin/sh", "../ssh_docker/add_ssh_key.sh", name])
    if return_code != 0:
        raise Exception("Impossible to create ssh key")
    log.info(f"- creation de la clef ssh {name}")

@pytest.fixture(scope="session")
def dimail_test_network(log) -> typing.Generator:
    if not config.settings.test_containers:
        yield None
        return

    with tc_core.network.Network() as dimail_test_network:
        log.info("SETUP network for containers")
        log.info(f"crée un réseau Docker -> {dimail_test_network}")
        try:
            yield dimail_test_network
        finally:
            log.info("TEARDOWN network for containers")

@pytest.fixture(scope="session")
def dk_image(log, dimail_test_network) -> typing.Generator:
    if not config.settings.test_containers:
        yield None
        return

    log.info("SETUP DK container image")
    create_ssh_key(log, "dktest")
    image = make_image(log,"dktest")
    dk = (
        tc_core.container.DockerContainer(image)
        .with_network(dimail_test_network)
        .with_network_aliases("dimail_dk")
        .with_bind_ports(23)
    )
    yield dk
    log.info("TEARDOWN DK container image")

@pytest.fixture(scope="session")
def ox_image(log, dimail_test_network) -> typing.Generator:
    if not config.settings.test_containers:
        yield None
        return

    log.info("SETUP OX container image")
    create_ssh_key(log, "oxtest")
    image = make_image(log,"oxtest")
    ox = (
        tc_core.container.DockerContainer(image)
        .with_network(dimail_test_network)
        .with_network_aliases("dimail_ox")
        .with_bind_ports(22)
    )
    yield ox
    log.info("TEARDOWN OX container image")

 

@pytest.fixture(scope="session")
def dk_container(log, dk_image) -> typing.Generator:
    if not config.settings.test_containers:
        yield None
        return

    log.info("SETUP DK CONTAINER")
    dk_image.start()
    delay = tc_core.waiting_utils.wait_for_logs(dk_image, "Starting ssh daemon")
    log.info(f"dk started in -> {delay}s")
    time.sleep(1)  # pour être sûr que le service ssh est up

    dk_ssh_url = f"ssh://root@{dk_image.get_container_host_ip()}:{dk_image.get_exposed_port(23)}"
    dk_ssh_args = [
        "-o",
        "StrictHostKeyChecking=no",
        "-o",
        "UserKnownHostsFile=/dev/null",
        "-i",
        "/tmp/dimail_dktest_id_rsa",
    ]
    dk_ssh_args = ",".join(dk_ssh_args)
    yield f"{dk_ssh_url};{dk_ssh_args}"
    log.info("TEARDOWN DK CONTAINER")
    if config.settings.test_containers_logs:
        stdout, stderr = dk_image.get_logs()
        stdout = stdout.decode()
        log.info(f"- dk logs stdout = {stdout}")
        stderr = stderr.decode()
        log.info(f"- dk logs stderr = {stderr}")
    dk_image.stop()

@pytest.fixture(scope="session")
def cb_image(log, dimail_test_network) -> tc_core.container.DockerContainer:
    if not config.settings.test_containers:
        yield None
        return

    log.info("SETUP CB container image")
    create_ssh_key(log, "cbtest")
    image = make_image(log,"cbtest")
    cb = (
        tc_core.container.DockerContainer(image)
        .with_network(dimail_test_network)
        .with_network_aliases("dimail_cb")
        .with_bind_ports(24)
    )
    yield cb
    log.info("TEARDOWN CB container image")

@pytest.fixture(scope="session")
def cb_container(log, cb_image) -> typing.Generator:
    if not config.settings.test_containers:
        yield None
        return

    log.info("SETUP CB CONTAINER")
    cb_image.start()
    delay = tc_core.waiting_utils.wait_for_logs(cb_image, "Starting ssh daemon")
    log.info(f"- cb started in -> {delay}s")
    time.sleep(1) # pour être sûr que le service ssh est up

    cb_ssh_url = f"ssh://root@{cb_image.get_container_host_ip()}:{cb_image.get_exposed_port(24)}"
    cb_ssh_args = [
        "-o",
        "StrictHostKeyChecking=no",
        "-o",
        "UserKnownHostsFile=/dev/null",
        "-i",
        "/tmp/dimail_cbtest_id_rsa",
    ]
    cb_ssh_args = ",".join(cb_ssh_args)

    yield f"{cb_ssh_url};{cb_ssh_args}"
    log.info("TEARDOWN CB CONTAINER")
    if config.settings.test_containers_logs:
        stdout, stderr = cb_image.get_logs()
        stdout = stdout.decode()
        log.info(f"- cb logs stdout = {stdout}")
        stderr = stderr.decode()
        log.info(f"- cb logs stderr = {stderr}")
    cb_image.stop()


@pytest.fixture(scope="session")
def root_db_url(log, dimail_test_network) -> typing.Generator:
    """Fixture that yields an URL to connect to the root database. The database
    will be running either in a dedicated container, or in your local docker
    compose, according to the settings `DIMAIL_TEST_CONTAINERS`."""
    if not config.settings.test_containers:
        yield "mysql+pymysql://root:toto@localhost:3306/mysql"
        return

    mariadb = (
        tc_mysql.MySqlContainer(
            "mariadb:11.6", username="root", password="toto", dbname="mysql"
        )
        # .with_name("mariadb")
        .with_bind_ports(3306)
        .with_network(dimail_test_network)
        .with_network_aliases("mariadb")
    )
    log.info("SETUP MARIADB CONTAINER")
    mariadb.start()
    delay = tc_core.waiting_utils.wait_for_logs(
        mariadb, "MariaDB init process done. Ready for start up."
    )
    log.info(f"MARIADB started in {delay}s")

    root_url = mariadb.get_connection_url()
    yield root_url

    mariadb.stop()
    log.info("TEARDOWN MARIADB CONTAINER")

# Le container oxtest va se connecter à mariadb -> dependance vers root_db_url
@pytest.fixture(scope="session")
def ox_container(log, ox_image, root_db_url) -> typing.Generator:
    if not config.settings.test_containers:
        yield None
        return

    log.info("SETUP OX CONTAINER")
    ox_image.start()
    try:
        delay = tc_core.waiting_utils.wait_for_logs(ox_image, "Starting ssh daemon")
    except Exception:
        log.error("Echec du démarrage du container oxtext. Voilà les logs...")
        stdout, stderr = ox_image.get_logs()
        stdout = stdout.decode()
        log.info(f"- ox logs stdout = {stdout}")
        stderr = stderr.decode()
        log.info(f"- ox logs stderr = {stderr}")
        exit(1)
    log.info(f"ox started in -> {delay}s")
    time.sleep(1)  # pour être sûr que le service ssh est up

    ox_ssh_url = f"ssh://root@{ox_image.get_container_host_ip()}:{ox_image.get_exposed_port(22)}"
    # on ne veut pas vérifier la clé sur chaque port randon du conteneur
    # et utilisation de la clé ssh générée par le script
    ox_ssh_args = [
        "-o",
        "StrictHostKeyChecking=no",
        "-o",
        "UserKnownHostsFile=/dev/null",
        "-i",
        "/tmp/dimail_oxtest_id_rsa",
    ]
    log.info(f"url de connexion ssh vers le cluster OX -> {ox_ssh_url}")

    ox_ssh_args = ",".join(ox_ssh_args)

    yield f"{ox_ssh_url};{ox_ssh_args}"
    log.info("TEARDOWN OX CONTAINER")
    if config.settings.test_containers_logs:
        stdout, stderr = ox_image.get_logs()
        stdout = stdout.decode()
        log.info(f"- ox logs stdout = {stdout}")
        stderr = stderr.decode()
        log.info(f"- ox logs stderr = {stderr}")
    ox_image.stop()


