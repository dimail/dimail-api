import logging
import pytest
import typing

from .. import config

@pytest.fixture(scope="session", name="log")
def fix_logger(scope="session") -> typing.Generator:
    """Fixture making the logger available"""
    logging.getLogger("urllib3.connectionpool").setLevel(logging.WARNING)
    logging.getLogger("docker").setLevel(logging.INFO)
    logging.getLogger("asyncio").setLevel(logging.WARNING)
    logging.getLogger("testcontainers.core.container").setLevel(logging.WARNING)
    logging.getLogger("testcontainers.core.waiting_utils").setLevel(logging.WARNING)
    logging.getLogger("src").setLevel(logging.INFO)
    logger = logging.getLogger("")
    logger.setLevel(logging.INFO)
    logger.info("SETUP logger")
    if config.settings.test_containers:
        logger.warning("Will be generating test containers")
    yield logger
    logger.info("TEARDOWN logger")


