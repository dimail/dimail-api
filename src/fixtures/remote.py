import typing

import pytest
import pytest_asyncio

from .. import cbcli, config, dkcli, oxcli, remote

@pytest.fixture(scope="session")
def setup_remote(log, dimail_mode, cb_container, dk_container, ox_container) -> typing.Generator:
    if remote.is_initialised():
        raise Exception("Il y a quelqu'un qui a init le module remote, et ce n'est pas moi")
    if dimail_mode == "fake":
        log.info("SETUP remote using fake mode")
        remote.init_remote("FAKE")
        remote.purge()
        yield "FAKE"
        remote.end_remote()
        log.info("TEARDOWN remote using fake mode")
        return
    if not config.settings.test_containers:
        log.info(f"SETUP remote using real mode {config.settings.remote_config}")
        remote.init_remote(config.settings.remote_config)
        remote.purge()
        yield ""
        remote.end_remote()
        log.info("TEARDOWN remote using real mode")
        return
    log.info("SETUP remote using real mode on containers")
    
    remote_config = [
        f"main;{cb_container};imap,smtp,cert_manager,api",
        f"webmail;{cb_container};webmail,cert_user",
        f"monitor;{cb_container};monitor,cert_user",
        f"dkim;{dk_container};dkim",
        f"ox;{ox_container};ox=testing",
    ]
    remote.init_remote(remote_config)
    remote.purge()
    yield ""
    log.info("TEARDOWN remote using real mode on containers")
    remote.end_remote()

@pytest.fixture(scope="session")
def dk_manager(log, dimail_mode, setup_remote) -> typing.Generator:
    if dimail_mode == "fake":
        log.info("SETUP using fake DK manager")
        yield "FAKE"
        log.info("TEARDOWN using fake DK manager")
        return
    else:
        log.info("SETUP using real DK manager")
        yield ""
        log.info("TEARDOWN using real DK manager")
        return

@pytest.fixture(scope="function")
def cb_manager(log, dimail_mode, setup_remote) -> typing.Generator:
    if dimail_mode == "fake":
        log.info("SETUP using fake CB manager")
        cbcli.init_certbot("FAKE")
        cbcli.purge()
        yield "FAKE"
        log.info("TEARDOWN using fake CB manager")
        return
    else:
        log.info("SETUP using real CB manager")
        cbcli.init_certbot("real")
        cbcli.purge()
        yield ""
        log.info("TEARDOWN using real CB manager")
        return

@pytest_asyncio.fixture(scope="function")
async def ox_cluster(log, dimail_mode, setup_remote) -> typing.Generator:
    """Fixture that provides an empty OX cluster."""
    log.info("SETUP empty ox cluster")
    ox_cluster = oxcli.OxCluster()
    await ox_cluster.purge()
    yield ox_cluster
    log.info("TEARDOWN ox cluster")
    await ox_cluster.purge()



