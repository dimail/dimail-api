import logging
import pytest
import typing
import os, os.path

#from .. import config
from .. import logs

def purge_directory(dir_name: str):
    if os.path.isdir(dir_name):
        for file_name in os.listdir(dir_name):
            if file_name == "." or file_name == "..":
                continue
            os.unlink(f"{dir_name}/{file_name}")
        os.rmdir(dir_name)

@pytest.fixture(scope="function")
def crash(log) -> typing.Generator:
    """Fixture changing the crash_log_dir"""
    log.info("SETUP crash_log_dir = /tmp/testing")
    purge_directory("/tmp/testing")
    logs.set_crash_log_dir("/tmp/testing")
    yield ""
    purge_directory("/tmp/testing")
    log.info("TEARDOWN crash_log_dir")

