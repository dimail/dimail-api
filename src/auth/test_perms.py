import pytest

from .. import sql_api

from . import perms


def test_is_authenticated(db_api_session):

    with pytest.raises(Exception) as e:
        perms.is_authenticated("not even a user")
    assert "it is not a user" in str(e.value)

    user_toto = sql_api.create_user(
        db_api_session,
        name="toto",
        password="toto",
        is_admin=False,
        perms=None
    )

    perms.is_authenticated(user_toto)
    assert isinstance(user_toto, perms.Perms)
    assert user_toto.perms == []

    tmp = sql_api.get_user(db_api_session, "toto")
    assert tmp.perms == []

    sql_api.update_user_perms(db_api_session, "toto", [ "not_a_perm" ])
    with pytest.raises(Exception) as e:
        perms.is_authenticated(user_toto)
    assert "PANIC" in str(e.value)
    assert "not_a_perm" in str(e.value)

    sql_api.update_user_perms(db_api_session, "toto", [ "create_users", "new_domain", "manage_users" ])
    perms.is_authenticated(user_toto)
    assert user_toto.perms == [ "create_users", "new_domain", "manage_users" ]

    sql_api.update_user_perms(db_api_session, "toto", [ "old_perm", "create_domain" ])
    perms.is_authenticated(user_toto)
    assert user_toto.perms == [ "new_domain" ]

    tmp = sql_api.get_user(db_api_session, "toto")
    assert tmp.perms == ["new_domain"]


@pytest.mark.parametrize(
    "perm_users",
    ["toto:false:new_domain,manage_users"],
    indirect=True,
)
def test_has_perms(db_api_session,perm_users):
    user_toto = perm_users[0]

    # On peut tester la présence d'une permission
    assert user_toto.has_global_perm("new_domain")
    assert not user_toto.has_global_perm("create_users")

    # Et on lève une exception (PANIC) si quelqu'un pose des question
    # sur une perm qui n'existe pas.
    with pytest.raises(Exception) as e:
        assert not user_toto.has_global_perm("pas_une_vraie_perm")
    assert "PANIC" in str(e.value)


@pytest.mark.parametrize(
    "perm_users",
    ["toto:false:;tutu:false:;chef:true:"],
    indirect=True,
)
def test_can_manage_domain(db_api_session, perm_users):
    (user_toto, user_tutu, user_admin) = perm_users

    example_com = sql_api.create_domain(
        db_api_session,
        name="example.com",
        features=[],
    )
    example_net = sql_api.create_domain(
        db_api_session,
        name="example.net",
        features=[],
    )

    assert user_toto.is_admin is False
    assert not user_toto.can_manage_domain("example.com")

    # Quand on donne les droits au user en SQL, ça se reflète dans les creds
    sql_api.allow_domain_for_user(db_api_session, "toto", "example.com")
    sql_api.allow_domain_for_user(db_api_session, "tutu", "example.net")
    user_toto = sql_api.get_user(db_api_session, "toto")
    assert user_toto.can_manage_domain("example.com")
    assert user_tutu.can_manage_domain("example.net")
    assert not user_toto.can_manage_domain("example.net")
    assert not user_tutu.can_manage_domain("example.com")
    assert user_admin.can_manage_domain("example.com")
    assert user_admin.can_manage_domain("example.net")
    assert user_admin.can_manage_domain("not even a fucking domain")

    # Un admin n'a pas besoin de droits spécifiques
    assert user_admin.can_manage_domain("example.com")

    # Quand on retire les droits au user en SQL, ça se reflète dans les creds
    sql_api.deny_domain_for_user(db_api_session, "toto", "example.com")
    user_toto = sql_api.get_user(db_api_session, "toto")
    assert not user_toto.can_manage_domain("example.com")

    # Si on rappelle une seconde fois deny_domain_for_user, ça ne plante pas
    # Mais le domaine n'est pas trouvé dans la BDD
    data = sql_api.deny_domain_for_user(db_api_session, "toto", "example.com")
    assert data is None


    # Si on test sur un objet domaine, ça marche aussi
    assert not user_toto.can_manage_domain(example_com)
    assert user_admin.can_manage_domain(example_net)
    sql_api.allow_domain_for_user(db_api_session, "toto", "example.com")
    # Ce droit n'a pas été supprimé
    # sql_api.allow_domain_for_user(db_api_session, "tutu", "example.net")
    assert user_toto.can_manage_domain(example_com)
    assert user_tutu.can_manage_domain(example_net)
    assert not user_toto.can_manage_domain(example_net)


def test_can_list_domain():
    pass


@pytest.mark.parametrize(
    "perm_users",
    ["regie:false:new_domain;toto:false:;tutu:false:;chef:true:"],
    indirect=True,
)
def test_can_delete_domain(db_api_session, perm_users):
    ( user_regie, user_toto, user_tutu, user_admin ) = perm_users

    example_com = sql_api.create_domain(
        db_api_session,
        name="example.com",
        features=[],
    )
    sql_api.allow_domain_for_user(db_api_session, "regie", "example.com")
    sql_api.allow_domain_for_user(db_api_session, "toto", "example.com")

    # Notre admin a toujours le droit
    assert user_admin.can_delete_domain("example.com")
    assert user_admin.can_delete_domain(example_com)
    assert user_admin.can_delete_domain("not even a domain")

    # Le user regie a le droit
    assert user_regie.can_delete_domain("example.com")
    assert user_regie.can_delete_domain(example_com)
    assert not user_regie.can_delete_domain("anything else")

    # Le user toto n'a pas le droit (il gère les mailbox, il ne drop pas le domaine)
    assert not user_toto.can_delete_domain("example.com")
    assert not user_toto.can_delete_domain(example_com)
    assert not user_toto.can_delete_domain("anything else")

    # Le user tutu non plus, d'ailleurs, n'a pas le droit
    assert not user_tutu.can_delete_domain("example.com")
    assert not user_tutu.can_delete_domain(example_com)
    assert not user_tutu.can_delete_domain("anything else")


@pytest.mark.parametrize(
    "perm_users",
    ["regie:false:new_domain;toto:false:create_users,manage_users;tutu:false:;chef:true:"],
    indirect=True,
)
def test_can_create_domain(db_api_session, perm_users):
    ( user_regie, user_toto, user_tutu, user_admin ) = perm_users

    # Notre admin a toujours le droit
    assert user_admin.can_create_domain()

    # La regie n'est pas admin, mais a le global_perm 'new_domain', donc oui
    assert user_regie.can_create_domain()

    # Le user toto a d'autres global_perm, mais pas 'new_domain', donc non
    assert not user_toto.can_create_domain()

    # Le user tutu n'a aucun droit particulier, donc non
    assert not user_tutu.can_create_domain()


@pytest.mark.parametrize(
    "perm_users",
    ["regie:false:new_domain;toto:false:create_users,manage_users;tutu:false:;chef:true:"],
    indirect=True,
)
def test_can_modify_domain(db_api_session, perm_users):
    ( user_regie, user_toto, user_tutu, user_admin ) = perm_users

    # L'admin peut modifier un domaine qui n'existe pas
    assert user_admin.can_modify_domain("not even a domain")
    assert not user_regie.can_modify_domain("not even a domain")
    assert not user_toto.can_modify_domain("not even a domain")
    assert not user_tutu.can_modify_domain("not even a domain")

    # Un domaine sans allows, seul l'admin peut le modifier
    example_com = sql_api.create_domain(
        db_api_session,
        name="example.com",
        features=[],
    )

    assert user_admin.can_modify_domain("example.com")
    assert user_admin.can_modify_domain(example_com)
    assert not user_regie.can_modify_domain("example.com")
    assert not user_regie.can_modify_domain(example_com)
    assert not user_toto.can_modify_domain("example.com")
    assert not user_toto.can_modify_domain(example_com)
    assert not user_tutu.can_modify_domain("example.com")
    assert not user_tutu.can_modify_domain(example_com)

    # Si la régie est allowed sur le domaine, la régie peut le modifier
    sql_api.allow_domain_for_user(db_api_session, "regie", "example.com")
    assert user_admin.can_modify_domain("example.com")
    assert user_admin.can_modify_domain(example_com)
    assert user_regie.can_modify_domain("example.com")
    assert user_regie.can_modify_domain(example_com)
    assert not user_toto.can_modify_domain("example.com")
    assert not user_toto.can_modify_domain(example_com)
    assert not user_tutu.can_modify_domain("example.com")
    assert not user_tutu.can_modify_domain(example_com)

    # Si le user toto est allowed, ça ne suffit pas pour modifier le domaine
    sql_api.allow_domain_for_user(db_api_session, "toto", "example.com")
    assert user_admin.can_modify_domain("example.com")
    assert user_admin.can_modify_domain(example_com)
    assert user_regie.can_modify_domain("example.com")
    assert user_regie.can_modify_domain(example_com)
    assert not user_toto.can_modify_domain("example.com")
    assert not user_toto.can_modify_domain(example_com)
    assert not user_tutu.can_modify_domain("example.com")
    assert not user_tutu.can_modify_domain(example_com)



@pytest.mark.parametrize(
    "perm_users",
    ["regie:false:new_domain;toto:false:create_users,manage_users;tutu:false:;chef:true:"],
    indirect=True,
)
def test_can_allow_for_domain(db_api_session, perm_users):
    ( user_regie, user_toto, user_tutu, user_admin ) = perm_users

    example_com = sql_api.create_domain(
        db_api_session,
        name="example.com",
        features=[],
    )
    sql_api.allow_domain_for_user(db_api_session, "regie", "example.com")
    sql_api.allow_domain_for_user(db_api_session, "toto", "example.com")
    example_net = sql_api.create_domain(
        db_api_session,
        name="example.net",
        features=[],
    )


    # Notre admin a toujours le droit
    assert user_admin.can_allow_for_domain("example.com")
    assert user_admin.can_allow_for_domain(example_com)
    assert user_admin.can_allow_for_domain("not even a domain")

    # Notre utilisateur la_regie a le droit de créer des domaine et est allowed, donc, oui
    assert user_regie.can_allow_for_domain("example.com")
    assert user_regie.can_allow_for_domain(example_com)
    # Mais pas sur example.net qui n'est pas a lui
    assert not user_regie.can_allow_for_domain("example.net")
    assert not user_regie.can_allow_for_domain(example_net)

    # Notre utilisateur toto peut gerer les mailbox, mais n'a pas 'new_domain' (il a d'autres
    # global perms), donc non
    assert not user_toto.can_allow_for_domain("example.com")
    assert not user_toto.can_allow_for_domain(example_com)

    # Notre utilisateur tutu n'a aucun droit particulier, donc non
    assert not user_tutu.can_allow_for_domain("example.com")
    assert not user_tutu.can_allow_for_domain(example_com)

@pytest.mark.parametrize(
    "perm_users",
    ["regie:false:create_users,manage_users;toto:false:new_domain;tutu:false:;titi:false:;chef:true:"],
    indirect=True,
)
def test_can_see_user(db_api_session, perm_users):
    ( user_regie, user_toto, user_tutu, user_titi, user_admin ) = perm_users

    sql_api.create_managed(db_api_session, "tutu", "regie")
    # Notre admin peut tout voir, même les choses qui n'existent pas
    assert user_admin.can_see_user(user_regie)
    assert user_admin.can_see_user(user_toto)
    assert user_admin.can_see_user(user_tutu)
    assert user_admin.can_see_user(user_titi)
    assert user_admin.can_see_user(user_admin)
    assert user_admin.can_see_user(None)
    assert user_admin.can_see_user("pas un user")

    # La régie (create_users,manage_user) peut voir les users qu'elle manage, et elle même
    assert user_regie.can_see_user(user_regie)
    assert not user_regie.can_see_user(user_toto)
    assert user_regie.can_see_user(user_tutu)
    assert not user_regie.can_see_user(user_titi)
    assert not user_regie.can_see_user(user_admin)
    assert not user_regie.can_see_user(None)
    assert not user_regie.can_see_user("pas un user")

    # Le user toto a d'autres droits, mais ne voit personne d'autre que lui
    assert not user_toto.can_see_user(user_regie)
    assert user_toto.can_see_user(user_toto)
    assert not user_toto.can_see_user(user_tutu)
    assert not user_toto.can_see_user(user_titi)
    assert not user_toto.can_see_user(user_admin)
    assert not user_toto.can_see_user(None)
    assert not user_toto.can_see_user("pas un user")

    # Le user tutu, managé par la régie, ne veut personne d'autre que lui
    assert not user_tutu.can_see_user(user_regie)
    assert not user_tutu.can_see_user(user_toto)
    assert user_tutu.can_see_user(user_tutu)
    assert not user_tutu.can_see_user(user_titi)
    assert not user_tutu.can_see_user(user_admin)
    assert not user_tutu.can_see_user(None)
    assert not user_tutu.can_see_user("pas un user")

    # Le user titi, aucun droit, managé par personne, ne voit que lui-même
    assert not user_titi.can_see_user(user_regie)
    assert not user_titi.can_see_user(user_toto)
    assert not user_titi.can_see_user(user_tutu)
    assert user_titi.can_see_user(user_titi)
    assert not user_titi.can_see_user(user_admin)
    assert not user_titi.can_see_user(None)
    assert not user_titi.can_see_user("pas un user")


@pytest.mark.parametrize(
    "perm_users",
    ["regie:false:create_users,manage_users;toto:false:new_domain;tutu:false:;chef:true:"],
    indirect=True,
)
def test_can_create_user(db_api_session, perm_users):
    ( user_regie, user_toto, user_tutu, user_admin ) = perm_users

    # Notre admin peut créer des users
    assert user_admin.can_create_user()

    # La regie peut créer des users
    assert user_regie.can_create_user()

    # Toto a d'autres global_perm, mais ne peut pas créer de user
    assert not user_toto.can_create_user()

    # Tutu n'a aucun droit
    assert not user_tutu.can_create_user()


@pytest.mark.parametrize(
    "perm_users",
    ["regie:false:create_users,manage_users;toto:false:new_domain;tutu:false:;chef:true:"],
    indirect=True,
)
def test_can_create_admin(db_api_session, perm_users):
    ( user_regie, user_toto, user_tutu, user_admin ) = perm_users

    # Notre admin peut créer des admins
    assert user_admin.can_create_admin()

    # La régie ne peut pas
    assert not user_regie.can_create_admin()

    # Toto ne peut pas
    assert not user_toto.can_create_admin()

    # Tutu encore moins
    assert not user_tutu.can_create_admin()


@pytest.mark.parametrize(
    "perm_users",
    ["regie:false:create_users,manage_users;toto:false:new_domain;tutu:false:;chef:true:"],
    indirect=True,
)
def test_can_set_perms(db_api_session, perm_users):
    ( user_regie, user_toto, user_tutu, user_admin ) = perm_users

    # Only the admin can set global_perm on another user
    assert user_admin.can_set_perms()
    assert not user_regie.can_set_perms()
    assert not user_toto.can_set_perms()
    assert not user_tutu.can_set_perms()


@pytest.mark.parametrize(
    "perm_users",
    ["regie:false:create_users,manage_users;toto:false:new_domain,manage_users,create_users;tutu:false:;titi:false:;chef:true:"],
    indirect=True,
)
def test_can_manage_user(db_api_session, perm_users):
    ( user_regie, user_toto, user_tutu, user_titi, user_admin ) = perm_users

    # Le user tutu est managé par la régie
    sql_api.create_managed(db_api_session, "tutu", "regie")

    # Notre admin peut manager tout le monde s'il a envie, même les users qui n'existent pas
    assert user_admin.can_manage_user(user_admin)
    assert user_admin.can_manage_user(user_regie)
    assert user_admin.can_manage_user(user_toto)
    assert user_admin.can_manage_user(user_tutu)
    assert user_admin.can_manage_user(user_titi)
    assert user_admin.can_manage_user(None)
    assert user_admin.can_manage_user("not even a user")

    # La regie peut manager des users et ne manage que tutu
    assert not user_regie.can_manage_user(user_admin)
    assert user_regie.can_manage_user(user_regie)
    assert not user_regie.can_manage_user(user_toto)
    assert user_regie.can_manage_user(user_tutu)
    assert not user_regie.can_manage_user(user_titi)
    assert not user_regie.can_manage_user(None)
    assert not user_regie.can_manage_user("not even a user")

    # Le user toto pourrait manager des gens, mais il ne le fait pas (ergo, il se manage lui meme)
    assert not user_toto.can_manage_user(user_admin)
    assert not user_toto.can_manage_user(user_regie)
    assert user_toto.can_manage_user(user_toto)
    assert not user_toto.can_manage_user(user_tutu)
    assert not user_toto.can_manage_user(user_titi)
    assert not user_toto.can_manage_user(None)
    assert not user_toto.can_manage_user("not even a user")

    # Le user tutu ne peut pas manager des gens (mais il est managé par la régie)
    assert not user_tutu.can_manage_user(user_admin)
    assert not user_tutu.can_manage_user(user_regie)
    assert not user_tutu.can_manage_user(user_toto)
    assert user_tutu.can_manage_user(user_tutu)
    assert not user_tutu.can_manage_user(user_titi)
    assert not user_tutu.can_manage_user(None)
    assert not user_tutu.can_manage_user("not even a user")

    # Le user titi ne peut pas manager des gens et n'est managé par personne
    assert not user_titi.can_manage_user(user_admin)
    assert not user_titi.can_manage_user(user_regie)
    assert not user_titi.can_manage_user(user_toto)
    assert not user_titi.can_manage_user(user_tutu)
    assert user_titi.can_manage_user(user_titi)
    assert not user_titi.can_manage_user(None)
    assert not user_titi.can_manage_user("not even a user")


@pytest.mark.parametrize(
    "perm_users",
    ["regie:false:create_users,manage_users;toto:false:new_domain,manage_users;tutu:false:;titi:false:;chef:true:"],
    indirect=True,
)
def test_can_modify_user(db_api_session, perm_users):
    ( user_regie, user_toto, user_tutu, user_titi, user_admin ) = perm_users

    # Le user tutu est managé par la régie et par toto
    sql_api.create_managed(db_api_session, "tutu", "regie")
    sql_api.create_managed(db_api_session, "tutu", "toto")

    # Notre admin peut modifier tous les users, même lui, même les gens qui n'existent pas
    assert user_admin.can_modify_user(user_admin)
    assert user_admin.can_modify_user(user_regie)
    assert user_admin.can_modify_user(user_toto)
    assert user_admin.can_modify_user(user_tutu)
    assert user_admin.can_modify_user(user_titi)
    assert user_admin.can_modify_user(None)
    assert user_admin.can_modify_user("not even a user")

    # La régie manage tutu, et peut 'create_users', donc peut modifier tutu
    assert not user_regie.can_modify_user(user_admin)
    assert not user_regie.can_modify_user(user_regie)
    assert not user_regie.can_modify_user(user_toto)
    assert user_regie.can_modify_user(user_tutu)
    assert not user_regie.can_modify_user(user_titi)
    assert not user_regie.can_modify_user(None)
    assert not user_regie.can_modify_user("not even a user")

    # Le user toto manage tutu, mais ne peut pas "create_users", donc il ne peut modifier personne
    assert not user_toto.can_modify_user(user_admin)
    assert not user_toto.can_modify_user(user_regie)
    assert not user_toto.can_modify_user(user_toto)
    assert not user_toto.can_modify_user(user_tutu)
    assert not user_toto.can_modify_user(user_titi)
    assert not user_toto.can_modify_user(None)
    assert not user_toto.can_modify_user("not even a user")

    # Le user tutu n'a aucun droit particulier (mais est managé par la régie et par toto), il
    # ne peut modifier personne
    assert not user_tutu.can_modify_user(user_admin)
    assert not user_tutu.can_modify_user(user_regie)
    assert not user_tutu.can_modify_user(user_toto)
    assert not user_tutu.can_modify_user(user_tutu)
    assert not user_tutu.can_modify_user(user_titi)
    assert not user_tutu.can_modify_user(None)
    assert not user_tutu.can_modify_user("not even a user")

    # Le user titi n'a aucun droit, et n'est managé par personne, il ne peut modifier personne
    assert not user_titi.can_modify_user(user_admin)
    assert not user_titi.can_modify_user(user_regie)
    assert not user_titi.can_modify_user(user_toto)
    assert not user_titi.can_modify_user(user_tutu)
    assert not user_titi.can_modify_user(user_titi)
    assert not user_titi.can_modify_user(None)
    assert not user_titi.can_modify_user("not even a user")


@pytest.mark.parametrize(
    "perm_users",
    ["regie:false:create_users,manage_users;toto:false:manage_users;tutu:false:;titi:false:;chef:true:"],
    indirect=True,
)
def test_can_delete_user(db_api_session, perm_users):
    ( user_regie, user_toto, user_tutu, user_titi, user_admin ) = perm_users

    # Le user tutu est managé par la régie et par toto
    sql_api.create_managed(db_api_session, "tutu", "regie")
    sql_api.create_managed(db_api_session, "tutu", "toto")

    # Notre admin peut supprimer tous les utilisateurs qui existent, sauf lui
    assert not user_admin.can_delete_user(user_admin)
    assert user_admin.can_delete_user(user_regie)
    assert user_admin.can_delete_user(user_toto)
    assert user_admin.can_delete_user(user_tutu)
    assert user_admin.can_delete_user(user_titi)
    assert not user_admin.can_delete_user(None)
    assert not user_admin.can_delete_user("Not even a user")

    # La régie peut "create_users" et manage tutu, donc peut le supprimer.
    assert not user_regie.can_delete_user(user_admin)
    assert not user_regie.can_delete_user(user_regie)
    assert not user_regie.can_delete_user(user_toto)
    assert user_regie.can_delete_user(user_tutu)
    assert not user_regie.can_delete_user(user_titi)
    assert not user_regie.can_delete_user(None)
    assert not user_regie.can_delete_user("Not even a user")

    # Le user toto manage "tutu", mais ne peut pas "create_users", donc il ne peut supprimer personne.
    assert not user_toto.can_delete_user(user_admin)
    assert not user_toto.can_delete_user(user_regie)
    assert not user_toto.can_delete_user(user_toto)
    assert not user_toto.can_delete_user(user_tutu)
    assert not user_toto.can_delete_user(user_titi)
    assert not user_toto.can_delete_user(None)
    assert not user_toto.can_delete_user("Not even a user")

    # Le user tutu ne manage personne (mais est managé), il ne peut supprimer personne.
    assert not user_tutu.can_delete_user(user_admin)
    assert not user_tutu.can_delete_user(user_regie)
    assert not user_tutu.can_delete_user(user_toto)
    assert not user_tutu.can_delete_user(user_tutu)
    assert not user_tutu.can_delete_user(user_titi)
    assert not user_tutu.can_delete_user(None)
    assert not user_tutu.can_delete_user("Not even a user")

    # Le user titi ne manage personne, n'est managé par personne, ne peut supprimer personne.
    assert not user_titi.can_delete_user(user_admin)
    assert not user_titi.can_delete_user(user_regie)
    assert not user_titi.can_delete_user(user_toto)
    assert not user_titi.can_delete_user(user_tutu)
    assert not user_titi.can_delete_user(user_titi)
    assert not user_titi.can_delete_user(None)
    assert not user_titi.can_delete_user("Not even a user")








