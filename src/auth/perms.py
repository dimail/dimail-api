import fastapi
import sqlalchemy as sa

from .. import enums, sql_api


#known_perms = {
#    "new_domain": "The user can create a domain. If he is not an admin, he will aumatically get an 'allow' on that domain. He can delete any domain he is allowed on.",
#    "manage_users": "The user can be set as the manager of another user. And/or, if the user is the manager of a user, it works.",
#    "create_users": "The user can create users. If the user is not an admin, they becomes a manager for the users they creates.",
#}

#deprecated_perms = {
#    "old_perm": None,
#    "create_domain": "new_domain",
#}

class Perms(sql_api.User):
    """A user of the system, that have been authenticated. So it is reasonable
    to check perms on that object.

    There is no proper way to build an object of that class, you have to first
    build a User from the database (e.g. sql_api.get_user(session, "tom")) and
    then change its class.
    """

    def is_real_admin(self) -> bool:
        if self.is_admin:
            if self.name == "FAKE":
                print("L'utilisateur courant est un FAKE admin.")
                print("Il a été admis en authent' parce que la base est vide.")
                print("Tu essayes de lui faire faire quelque chose qui n'est pas la création d'un premier admin.")
                print("Corrige tes tests. Il faut créer un premier admin.")
                raise fastapi.HTTPException(status_code=403, detail="Un FAKE admin, authentifié sur une base vide, essaye de faire quelque chose. Mauvaise idée.")
            return True
        return False

    def is_fake_admin(self) -> bool:
        if self.is_admin and self.name == "FAKE":
            return True
        return False

    def has_global_perm(self, perm: str):
        if perm not in enums.Perm:
            raise Exception("PANIC. Our code is using a permission that does not exist.")
        if perm in self.perms:
            return True
        return False


    # Permissions on the domains
    def can_manage_domain(self, domain: str | sql_api.Domain) -> bool:
        """Says if the user is allowed to manage the domain. An admin always can, the users
        allowed on that domain can too. The user will be allowed to create/delete aliases and
        mailboxes. They will not be allowed to modify the domain itself."""
        if self.is_real_admin():
            return True
        # Si on me donne un vrai domaine, j'irai plus vite à boucler sur ses
        # users.
        if isinstance(domain, sql_api.Domain):
            for user in domain.users:
                if user.name == self.name:
                    return True
            return False
        for dom in self.domains:
            if dom.name == domain:
                return True
        return False

    def can_list_domains(self) -> bool:
        """Says if a user can list all domains. We should be doing something else..."""
        if self.is_real_admin():
            return True
        return False

    def can_delete_domain(self, domain: str | sql_api.Domain) -> bool:
        """Says if the user can delete a domain. Admins can delete any domain. Users
        who can create domains and are allowed to manage **that** domain can delete it."""
        if self.is_real_admin():
            return True
        if self.has_global_perm("new_domain"):
            if self.can_manage_domain(domain):
                return True
        return False

    def can_create_domain(self) -> bool:
        """Says if the user can create a new domain. Admins can always create domains. Some
        users may do it too if they have the 'new_domain' global permission."""
        if self.is_real_admin():
            return True
        if self.has_global_perm("new_domain"):
            return True
        return False

    def can_modify_domain(self, domain: str) -> bool:
        """Says if the user can modify a domain. An admin can always do that. A use who has
        global_perms 'new_domain' and is allowed to manage **that** domain can also do it.
        """
        if self.is_real_admin():
            return True
        if self.has_global_perm("new_domain"):
            if self.can_manage_domain(domain):
                return True
        return False

    def can_allow_for_domain(self, domain: str | sql_api.Domain) -> bool:
        """Says if the user can allow/revoke another user for that domain. An admin can always do
        that. A user who has global_perm 'new_domain' and is allowed to manage **that** domain can
        allow/revoke for it."""
        if self.is_real_admin():
            return True
        if self.has_global_perm("new_domain"):
            if self.can_manage_domain(domain):
                return True
        return False

    # Permissions on the users
    def can_see_user(self, user: sql_api.User) -> bool:
        """Says if the user can see another user. Admins can see everything. If you have the
        global perm 'manage_user' and are managing that specific user, you can see him. You
        can always see youself."""
        if self.is_real_admin():
            return True
        if user is None:
            return False
        if not isinstance(user, sql_api.User):
            return False
        if self.name == user.name:
            return True
        if self.has_global_perm("manage_users"):
            # self peut manager beaucoup de monde, user n'est probablement managé que
            # par un seul utilisateur
            if self.name in user.managers():
                return True
        return False
        
    def can_create_user(self) -> bool:
        """Says if the user can create a new user. For now, admin only."""
        if self.is_fake_admin():
            return True
        if self.is_real_admin():
            return True
        if self.has_global_perm("create_users"):
            return True
        return False

    def can_create_admin(self) -> bool:
        """Says if the user can make an admin. For now, admin only. It is used
        when creating an admin, or when promoting a normal user to admin, or when
        demoting admin user to normal."""
        if self.is_fake_admin():
            return True
        if self.is_real_admin():
            return True
        return False

    def can_set_perms(self) -> bool:
        """Says if a user can set global perms on an other user. Admin only."""
        if self.is_fake_admin():
            return True
        if self.is_real_admin():
            return True
        return False

    def can_manage_user(self, user: sql_api.User) -> bool:
        """Says if the user can manage another user. It is allowed when the user
        wants to manage themself, or when the user is an admin."""
        if self.is_real_admin():
            return True
        if user is None:
            return False
        if not isinstance(user, sql_api.User):
            return False
        if user.name == self.name:
            return True
        # On verifie que je suis dans la (très courte) liste des managers de l'autre,
        # plutôt que de vérifier que l'autre est dans ma (très longue) liste de managés.
        if self.has_global_perm("manage_users") and self.name in user.managers():
            return True
        return False

    def can_modify_user(self, user: sql_api.User) -> bool:
        """Says if the user can modify another user. Admins can always modify a user.
        If you have the global perm "create_users" and you manage this user, and it is
        not you, you can modify him."""
        if self.is_real_admin():
            return True
        if user is None:
            return False
        if not isinstance(user, sql_api.User):
            return False
        # We prevent a user from midifying himself, to avoid privilege escalation of any kind
        if self.has_global_perm("create_users") and user.name != self.name:
            if self.can_manage_user(user):
                return True
        return False

    def can_delete_user(self, user: sql_api.User) -> bool:
        """Says if the user can delete another user. Nobody can delete a user that does
        not exist. You can never delete yourself. Admins can delete everybody (except themself).
        When you manage a user, and if you have the 'create_users' global perm, you can delete
        the users you manage."""
        if user is None:
            return False
        if not isinstance(user, sql_api.User):
            return False
        if self.name == user.name:
            # Suicide prevention
            return False
        if self.is_real_admin():
            return True
        if self.has_global_perm("create_users"):
            if self.can_manage_user(user):
                return True
        return False

    def can_health_check(self) -> bool:
        """Says if the user can ask for a health check on the server. Only admins for now."""
        if self.is_real_admin():
            return True
        return False

    def can_get_tech_domain(self) -> bool:
        return True

    def can_get_server_logs(self) -> bool:
        """Says if the user can read the servers logs. Allowed for admin and users having
        the global 'get_logs' permission."""
        if self.is_real_admin():
            return True
        if self.has_global_perm("get_logs"):
            return True
        return False

    def can_get_version(self) -> bool:
        return True

    def can_shutdown(self) -> bool:
        if self.is_real_admin():
            return True
        return False

    def can_post_database(self) -> bool:
        if self.is_real_admin():
            return True
        return False

    def can_patch_database(self) -> bool:
        if self.is_real_admin():
            return True
        return False


def clean_perms_in_arg(perms: list[str]) -> list[str]:
    if not isinstance(perms, list):
        raise fastapi.HTTPException(status_code=422, detail="perms is not a list")
    new_perms = []
    for perm in perms:
        if perm in enums.Perm:
            new_perms.append(perm)
            continue
        if perm in enums.OldPerm:
            if enums.OldPerm(perm).replacement is not None:
                new_perms.append(perm)
            continue
        raise fastapi.HTTPException(status_code=422, detail=f"You cannot set perm '{perm}', it does not exist")
    return new_perms


def is_authenticated(user: sql_api.User) -> None:
    if not isinstance(user, sql_api.User):
        raise Exception("You cannot claim an object is an authenticated user when it is not a user.")
    update_needed = False
    new_perms = []
    if user.perms is None:
        update_needed = True
    else:
        for perm in user.perms:
            if perm in enums.Perm:
                new_perms.append(perm)
                continue
            if perm in enums.OldPerm:
                update_needed = True
                if enums.OldPerm(perm).replacement is not None:
                    new_perms.append(enums.OldPerm(perm).replacement)
                continue
            raise Exception(
                f"PANIC. You authenticated a user {user.name} who has perm {perm} which does not exist"
            )
    if update_needed:
        session = sa.inspect(user).session
        sql_api.update_user_perms(session, user.name, new_perms)
    user.__class__ = Perms
