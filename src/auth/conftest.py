import pytest

from .. import enums, sql_api

from . import perms

# Here is the expected syntax:
# toto:false:new_domain,create_user;titi:true:
# <login>:<admin>:<comma_perms>;<login...
@pytest.fixture(scope="function")
def perm_users(log, db_api_session, request):
    items = request.param.split(";")
    users = []
    for item in items:
        (login, admin, user_perms) = item.split(":")
        if user_perms == "":
            user_perms = []
        else:
            user_perms = user_perms.split(',')
            for perm in user_perms:
                if not perm in enums.Perm:
                    raise Exception(f"PANIC. This test uses an unkown perm '{perm}'.")

        user = sql_api.create_user(
            db_api_session,
            name=login,
            password="toto",
            is_admin=admin == "true",
            perms=user_perms
        )
        perms.is_authenticated(user)
        log.info(f"SETUP user {login} with perms {user_perms}")
        users.append(user)
    yield users
    while len(users) > 0:
        user = users.pop()
        log.info(f"TEARDOWN user {user.name}")



