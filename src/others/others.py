import pydantic

from .. import client

_myself = None
def set_myself(name: str):
    global _myself
    _myself = name

def reset_myself():
    global _myself
    _myself = None

def get_myself() -> str:
    global _myself
    return _myself

class Other(pydantic.BaseModel):
    model_config = pydantic.ConfigDict(arbitrary_types_allowed=True)

    name: str
    client: client.ApiClient

    def get_tech_domain(self) -> str:
        return self.client.tech_domain()

_others = {}
def set_others(infos: dict, reset: bool = False):
    global _others
    for name in infos.keys():
        _others[name] = Other(
            name = name,
            client = client.ApiClient(name, infos[name], reset)
        )

def reset_others():
    global _others
    _others = {}

def get_other(name: str) -> client.ApiClient:
    if name in _others:
        return _others[name]
    raise Exception(f"Je ne connait pas cette plateforme '{name}' dont tu me parles")

def list_others() -> list[str]:
    res = list(_others.keys())
    return res

