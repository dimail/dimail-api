from .. import enums, remote
from . import certbot, fake, templates

def make_nagios_config(
    cert_name: str,
    imap_name: str | None,
    smtp_name: str | None,
    webmail_name: str | None,
):
    cert_manager = remote.get_host_by_role(enums.Role.CertManager)
    for hostname in remote.get_hosts_by_role(enums.Role.Monitor):
        config = "# No certificate available for the client.\n"
        if certbot.cert_exists(cert_name):
            config = templates.render("nagios",
                cert_name = cert_name,
                imap_name = imap_name,
                smtp_name = smtp_name,
                webmail_name = webmail_name,
            )
        remote.store_on_host(
            hostname=cert_manager,
            dirname=f"/opt/certs/config/nagios/{hostname}/",
            filename=f"{cert_name}.cfg",
            content=config,
        )

def remove_nagios_config(
    cert_name: str,
):
    cert_manager = remote.get_host_by_role(enums.Role.CertManager)
    for hostname in remote.get_hosts_by_role(enums.Role.Monitor):
        remote.remove_from_host(
            hostname = cert_manager,
            filename = f"/opt/certs/config/nagios/{hostname}/{cert_name}.cfg",
        )
    remote.remove_from_host(
        hostname = cert_manager,
        filename = f"/opt/certs/config/nagios/{hostname}/",
    )

def check_nagios_config(
    cert_name: str,
    imap_name: str | None,
    smtp_name: str | None,
    webmail_name: str | None,
):
    have_cert = certbot.cert_exists(cert_name)
    res = True
    cert_manager = remote.get_host_by_role(enums.Role.CertManager)
    for hostname in remote.get_hosts_by_role(enums.Role.Monitor):
        filename = f"/opt/certs/config/nagios/{hostname}/{cert_name}.cfg"
        if not remote.file_exists_on_host(cert_manager, filename):
            print(f"En mode {fake.get_mode()}, je ne trouve pas {filename} pour la machine {hostname}")
            res = False
            continue

        content = remote.read_from_host(cert_manager, filename)
        if not have_cert:
            if "dimail-service" in content:
                print(f"En mode {fake.get_mode()}, je n'ai pas de certificat pour {cert_name}, mais nagios veut superviser un truc sur {hostname}. Ce n'est pas normal.")
                res = False
            continue

        if imap_name is not None:
            if not "imap-servers" in content:
                print(f"En mode {fake.get_mode()}, pour {cert_name}, nagios devrait superviser les serveurs IMAP sur {hostname}. Ce n'est pas le cas.")
                res = False
        if smtp_name is not None:
            if not "smtp-servers" in content:
                print(f"En mode {fake.get_mode()}, pour {cert_name}, nagios devrait superviser les serveurs SMTP sur {hostname}. Ce n'est pas le cas.")
                res = False
        if webmail_name is not None:
            if not "webfront-servers" in content:
                print(f"En mode {fake.get_mode()}, pour {cert_name}, nagios devrait superviser les serveurs Webfront sur {hostname}. Ce n'est pas le cas.")
                res = False
    
    return res

