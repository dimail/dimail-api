
from .. import enums, remote
from . import fake

fake_postfix_cert = {}
def init():
    if fake.fake_mode():
        __purge()

def purge():
    if fake.fake_mode():
        __purge()

def __purge():
    global fake_postfix_cert
    fake_postfix_cert = {}
    

def make_postfix_config(
    cert_name: str,
    smtp_name: str,
):
    if fake.fake_mode():
        global fake_postfix_cert
        fake_postfix_cert[smtp_name] = cert_name
        return

    cert_manager = remote.get_host_by_role(enums.Role.CertManager)
    print(f"Dans notre config, le cert manager est {cert_manager}")

    # On s'assure que notre fichier existe
    dir_name = "/opt/certs/config/postfix"
    file = f"{dir_name}/certs-api"
    command = f"if [ ! -f {file} ]; then mkdir -p {dir_name}; touch {file}; fi"
    remote.run_on_host(cert_manager, command)

    # On garde toutes les lignes qui ne parlent pas de notre domaine (grep -v | cat)
    # Puis on ajoute notre domaine (echo)
    # Puis on trie
    # On range tout ça dans {file}.new.$$
    # On supprime l'ancien {file}
    # On renomme {file}.new.$$ en {file}
    command = f"grep -v '^{smtp_name} ' {file} | (cat; echo {smtp_name} /etc/letsencrypt/live/{cert_name}/privkey.pem /etc/letsencrypt/live/{cert_name}/fullchain.pem ) | sort > {file}.new.$$; rm {file}; mv {file}.new.$$ {file}"
    remote.run_on_host(cert_manager, command)

def check_postfix_config(
    cert_name: str,
    smtp_name: str,
) -> bool:
    if fake.fake_mode():
        global fake_postfix_cert
        if smtp_name in fake_postfix_cert and fake_postfix_cert[smtp_name] == cert_name:
            return True
        return False
    # smtp.dimail.ovh /etc/letsencrypt/live/client_dimail.ovh/privkey.pem /etc/letsencrypt/live/client_dimail.ovh/fullchain.pem
    cert_manager = remote.get_host_by_role(enums.Role.CertManager)
    command = f"grep '^{smtp_name} ' /opt/certs/config/postfix/certs-api"
    (stdout, stderr, returncode) = remote.run_on_host(cert_manager, command, want_success = False)
    if returncode != 0:
        if stderr == "":
            print(f"Je ne trouve pas le domaine {smtp_name} dans le fichier de configuration de postfix.")
            return False
        print(f"Failed ssh command {command} on host {cert_manager}.")
        print("Stdout:")
        print(stdout)
        print(stderr)
        return False

    line = stdout.rstrip()
    (domain, key, cert) = line.split(" ",3)
    if key != f"/etc/letsencrypt/live/{cert_name}/privkey.pem" or cert != f"/etc/letsencrypt/live/{cert_name}/fullchain.pem":
        print(f"Je trouve le domaine {smtp_name}, avec un autre certificat que {cert_name}: '{key}' != '/etc/letsencrypt/live/{cert_name}/privkey.pem' ou '{cert}' != '/etc/letsencrypt/live/{cert_name}/fullchain.pem'")
        return False

    print(f"Je trouve bien le certificat {cert_name} sur le domaine {domain}")
    return True


def remove_postfix_config(
    cert_name: str,
    smtp_name: str,
):
    if fake.fake_mode():
        global fake_postfix_cert
        if smtp_name in fake_postfix_cert:
            del fake_postfix_cert[smtp_name]
        return

    cert_manager = remote.get_host_by_role(enums.Role.CertManager)
    file = "/opt/certs/config/postfix/certs-api"
    command = f"grep -v '^{smtp_name} ' {file} > {file}.new.$$; rm {file}; mv {file}.new.$$ {file}"
    remote.run_on_host(cert_manager, command)
