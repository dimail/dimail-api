import logging

from .. import enums, remote
from . import fake, templates

fake_certs = {}
def init():
    if fake.fake_mode():
        return __purge()

def purge():
    return __purge()

def __purge():
    if fake.fake_mode():
        global fake_certs
        fake_certs = {}
        return

    # FIXME Il ne faut *jamais* faire tourner ça sur la prod.
    hostname = remote.get_host_by_role(enums.Role.CertManager)
    remote.run_on_host(hostname, "./purge.sh")


def call_certbot(
    cert_name: str,
    domains: list[str],
) -> bool:
    if fake.fake_mode():
        global fake_certs
        fake_certs[cert_name] = domains
        return True

    log = logging.getLogger(__name__)

    command = [ "sudo", "certbot", "-n", "--cert-name", cert_name ]
    for dom in domains:
        command.extend(["-d", dom])
    command.extend(["certonly", "--webroot"])
    hostname = remote.get_host_by_role(enums.Role.CertManager)
    remote.run_on_host(hostname, command)


def cert_info(
    cert_name: str
):
    if fake.fake_mode():
        global fake_certs
        if cert_name in fake_certs:
            static = templates.render("cert_info",
                cert_name = cert_name,
                domains = fake_certs[cert_name],
            )
            return static
        return None

    log = logging.getLogger(__name__)
    hostname = remote.get_host_by_role(enums.Role.CertManager)

    command = [ "sudo", "/usr/bin/api_read_cert.sh", cert_name ]
    print(f"Je lit le certificat avec la command {command}")
    (stdout, stderr, returncode) = remote.run_on_host(hostname, command, want_success=False)
    if returncode != 0:
        print(f"Failed to run command {command}. Probably the cert does not exist.")
        print("Stdout:")
        print(stdout)
        print("Stderr:")
        print(stderr)
        return None
    cert_info = stdout
    return cert_info

def cert_exists(
    cert_name: str
) -> bool:
    if fake.fake_mode():
        global fake_certs
        if cert_name in fake_certs:
            return True
        return False

    log = logging.getLogger(__name__)
    hostname = remote.get_host_by_role(enums.Role.CertManager)
    command = [ "sudo", "/usr/bin/api_list_cert.sh", cert_name ]
    log.debug(f"Je cherche le certificat avec la commande {command}")
    (stdout, stderr, returncode) = remote.run_on_host(hostname, command, want_success=False)
    if returncode != 0:
        print("Echec de l'appel ssh:")
        print("Stdout:")
        print(stdout)
        print("Stderr:")
        print(stderr)
        return False
    return True

def remove_cert(
    cert_name: str
):
    if fake.fake_mode():
        global fake_certs
        if cert_name in fake_certs:
            del fake_certs[cert_name]

    log = logging.getLogger(__name__)
    hostname = remote.get_host_by_role(enums.Role.CertManager)
    command = [ "sudo", "/usr/bin/api_remove_cert.sh", cert_name ]
    log.debug(f"Je supprime le certificat avec la commande {command}")
    remote.run_on_host(hostname, command)
