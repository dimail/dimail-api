import pytest

from .. import cbcli, remote

def test_postfix_config(log, cb_manager):
    tmp = cbcli.check_postfix_config("toto", "example.com")
    assert tmp is False

    tmp = cbcli.check_postfix_config("coincoin", "example.net")
    assert tmp is False

    tmp = cbcli.check_postfix_config("plop", "pabon.fr")
    assert tmp is False


    cbcli.make_postfix_config("toto", "example.com")
    cbcli.make_postfix_config("coincoin", "example.net")

    tmp = cbcli.check_postfix_config("toto", "example.com")
    assert tmp is True

    tmp = cbcli.check_postfix_config("coincoin", "example.net")
    assert tmp is True

    tmp = cbcli.check_postfix_config("plop", "pabon.fr")
    assert tmp is False

    tmp = cbcli.check_postfix_config("toto", "example.net")
    assert tmp is False

    # Je peux changer le nom du certificat pour un domaine
    cbcli.make_postfix_config("trululu", "example.com")

    tmp = cbcli.check_postfix_config("toto", "example.com")
    assert tmp is False

    tmp = cbcli.check_postfix_config("trululu", "example.com")
    assert tmp is True

    # Je peux retirer la config d'un domaine
    cbcli.remove_postfix_config("trululu", "example.com")

    tmp = cbcli.check_postfix_config("trululu", "example.com")
    assert tmp is False

def test_nginx_config(log, cb_manager):
    tmp = cbcli.cert_exists("toto")
    assert tmp is False

    tmp = remote.file_exists_on_host(
        hostname="main",
        filename="/opt/certs/config/nginx/toto/main.conf",
    )
    assert tmp is False

    tmp = remote.file_exists_on_host(
        hostname="main",
        filename="/opt/certs/config/nginx/toto/",
    )
    assert tmp is False

    cbcli.make_nginx_config(
        cert_name="toto",
        imap_name="imap.example.com",
        smtp_name="smtp.example.com",
        webmail_name="webmail.example.com",
    )
    cfg = remote.read_from_host(
        hostname="main",
        filename="/opt/certs/config/nginx/toto/main.conf",
    )
    assert "ssl" not in cfg
    assert "snippet" not in cfg

    cfg = remote.read_from_host(
        hostname="webmail",
        filename="/opt/certs/config/nginx/toto/webmail.conf",
    )
    assert "ssl" not in cfg
    # Le snippet OX n'est include que dans la partie SSL
    assert "snippet" not in cfg

    cbcli.call_certbot("toto", ["example.com"])

    tmp = cbcli.cert_exists("toto")
    assert tmp is True

    cbcli.make_nginx_config(
        cert_name="toto",
        imap_name="imap.example.com",
        smtp_name="smtp.example.com",
        webmail_name="webmail.example.com",
    )
    cfg = remote.read_from_host(
        hostname="main",
        filename="/opt/certs/config/nginx/toto/main.conf",
    )
    assert "ssl" in cfg
    assert "snippet" not in cfg

    # Le fichier n'a pas encore été propagé aux autres machines (rsync n'a pas eu lieu)
    # Donc, on le trouve sur main (aka cert_manager) pour le moment.
    cfg = remote.read_from_host(
        hostname="main",
        filename="/opt/certs/config/nginx/toto/webmail.conf",
    )
    assert "ssl" in cfg
    assert "snippet" in cfg

    cbcli.remove_nginx_config(
        cert_name="toto"
    )
    tmp = remote.file_exists_on_host(
        hostname="main",
        filename="/opt/certs/config/nginx/toto/webmail.conf",
    )
    assert tmp is False
    tmp = remote.file_exists_on_host(
        hostname="main",
        filename="/opt/certs/config/nginx/toto/",
    )
    assert tmp is False

def test_dovecot_config(log, cb_manager):
    cbcli.make_dovecot_config(
        cert_name="toto",
        imap_name="imap.example.com",
    )
    cfg = remote.read_from_host(
        hostname="main",
        filename="/opt/certs/config/dovecot/toto/main.cfg",
    )
    assert "local_name" in cfg
    assert "imap.example.com" in cfg
    assert "fullchain.pem" in cfg

    cbcli.remove_dovecot_config(
        cert_name = "toto",
    )

    tmp = remote.file_exists_on_host(
        hostname = "main",
        filename = "/opt/certs/config/dovecot/toto/main.cfg",
    )
    assert tmp is False
    tmp = remote.file_exists_on_host(
        hostname = "main",
        filename = "/opt/certs/config/dovecot/toto/",
    )
    assert tmp is False


def test_nagios_config(log, cb_manager):
    cbcli.make_nagios_config(
        cert_name="toto",
        imap_name="imap.example.com",
        smtp_name="smtp.example.com",
        webmail_name="webmail.example.com",
    )
    cfg = remote.read_from_host(
        hostname="main",
        filename="/opt/certs/config/nagios/monitor/toto.cfg",
    )
    # Dans ce test-là, le certificat n'existe pas...
    assert "No certificate" in cfg

    cbcli.remove_nagios_config(
        cert_name = "toto",
    )

    tmp = remote.file_exists_on_host(
        hostname = "main",
        filename = "/opt/certs/config/nagios/monitor/toto.cfg",
    )
    assert tmp is False

    tmp = remote.file_exists_on_host(
        hostname = "main",
        filename = "/opt/certs/config/nagios/monitor/",
    )
    assert tmp is False


def test_make_cert(log, cb_manager):
    cbcli.make_cert(
        cert_name="toto",
        imap_name="imap.example.com",
        smtp_name="smtp.example.com",
        webmail_name="webmail.example.com",
    )

def test_check_cert(log, cb_manager):

    cbcli.make_cert(
        cert_name="toto2",
        imap_name="imap.example.com",
        smtp_name="smtp.example.com",
        webmail_name="webmail.example.com",

    )
    # On vérifie qu'on remonte bien la chaine static
    info = cbcli.cert_info("toto2")
    assert info is not None
    assert "imap.example.com" in info

    err = cbcli.check_cert(
        cert_name="toto2",
        imap_name="imap.example.com",
        smtp_name="smtp.example.com",
        webmail_name="webmail.example.com",
    )
    assert err is None

    err = cbcli.check_cert(
        cert_name="toto2",
        imap_name="imap2.example.com",
        smtp_name="smtp.example.com",
        webmail_name="webmail.example.com",
    )
    assert err is not None
    assert err.code == "wrong_domains"

def test_health_check(log, cb_manager):
    status = cbcli.health_check()
    assert status.status is True
    assert "cbcli_status = OK" in status.detail

