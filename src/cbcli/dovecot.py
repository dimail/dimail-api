from .. import enums, remote
from . import fake
from . import templates


def make_dovecot_config(
    cert_name: str,
    imap_name: str,
):
    cert_manager = remote.get_host_by_role(enums.Role.CertManager)
    print(f"Dans notre config, le cert manager est {cert_manager}")
    for hostname in remote.get_hosts_by_role(enums.Role.Imap):
#        config = tpl["dovecot"].render(
        config = templates.render("dovecot",
            cert_name = cert_name,
            imap_name = imap_name,
        )
        remote.store_on_host(
            hostname=cert_manager,
            dirname=f"/opt/certs/config/dovecot/{cert_name}/",
            filename=f"{hostname}.cfg",
            content=config,
        )

def check_dovecot_config(cert_name: str) -> bool:
    for hostname in remote.get_hosts_by_role(enums.Role.Imap):
        filename = f"/opt/certs/config/dovecot/{cert_name}/{hostname}.cfg"
        if not remote.file_exists_on_host(hostname, filename):
            print(f"En mode {fake.get_mode()}, je ne trouve pas {filename} sur la machine {hostname}")
            return False
    return True

def remove_dovecot_config(cert_name: str):
    cert_manager = remote.get_host_by_role(enums.Role.CertManager)
    for hostname in remote.get_hosts_by_role(enums.Role.Imap):
        filename = f"/opt/certs/config/dovecot/{cert_name}/{hostname}.cfg"
        remote.remove_from_host(
            hostname = cert_manager,
            filename = filename,
        )
    remote.remove_from_host(
        hostname = cert_manager,
        filename = f"/opt/certs/config/dovecot/{cert_name}/",
    )

