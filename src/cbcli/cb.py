import logging
import json
import subprocess

from .. import dns, enums, remote, utils, web_models
from . import certbot, fake, templates
from . import nagios, nginx, dovecot, postfix

# FIXME/TODO:
# Pour dovecot, postfix & nginx : 
# - Comment on supprime un domaine ?
# 
# Réflechir à un globalcheck de tous les domaines...

####
#### MODULE MANAGEMENT
#### (global vars, init, etc)
####

# class Host(pydantic.BaseModel):
#     ssh_url: str
#     roles: list[enums.Role]

def init_certbot(in_mode: str):
    fake.init(in_mode)
    templates.init()
    certbot.init()
    postfix.init()


def purge():
    print(f"On purge (mode={fake.get_mode()})")
    fake.purge()
    certbot.purge()
    postfix.purge()


def health_check() -> web_models.ModuleStatus:
    status = web_models.ModuleStatus()
    status.add_detail("Checking everything is fine")
    status.add_detail(f"mode = {fake.get_mode()}")
    if fake.fake_mode():
        status.add_detail(f"nb_cert = {len(fake.fake_certs)}")
        status.add_detail("cbcli_status = OK")
        return status

    cert_manager = None
    try:
        cert_manager = remote.get_host_by_role(enums.Role.CertManager)
        status.add_detail(f"cert_manager = {cert_manager}")
    except Exception as e:
        status.add_detail("ERROR: il n'y a pas de cert_manager dans les machines dont j'ai la liste")
        status.add_detail(f"Exception during remote.get_host_by_role: {e}")
        status.fail()

    if cert_manager is not None:
        (stdout, stderr, returncode) = remote.run_on_host(cert_manager, "sudo certbot --version", want_success = False)
        if returncode != 0:
            status.add_exec_failure(f"ERROR: (sur {cert_manager}) Echec a appeler sudo certbot --version:", stdout, stderr)
        else:
            status.add_detail("sudo certbot: OK")

        (stdout, stderr, returncode) = remote.run_on_host(cert_manager, "sudo /usr/bin/api_list_cert.sh check", want_success = False)
        if returncode != 0:
            status.add_exec_failure(f"ERROR: (sur {cert_manager}) Echec a appeler sudo /usr/bin/api_list_cert.sh en mode check;", stdout, stderr)
        else:
            status.add_detail(f"host {cert_manager}: sudo /usr/bin/api_list_cert.sh OK")

        (stdout, stderr, returncode) = remote.run_on_host(cert_manager, "sudo /usr/bin/api_read_cert.sh check", want_success = False)
        if returncode != 0:
            status.add_exec_failure(f"ERROR: (sur {cert_manager}) Echec a appeler sudo /usr/bin/api_read_cert.sh en mode check:", stdout, stderr)
        else:
            status.add_detail(f"host {cert_manager}: sudo /usr/bin/api_read_cert.sh OK")

    if status.status:
        status.add_detail("cbcli_status = OK")
    else:
        status.add_detail("cbcli_status = KO")
    print(status)
    return status

####
#### CERT MANAGEMENT
####

def make_cert(
    cert_name: str,
    imap_name: str,
    smtp_name: str,
    webmail_name: str,
) -> bool:
    if imap_name is None and smtp_name is None and webmail_domain is None:
        return True

    # Faire la config nginx sur chaque machine pour chaque domaine
    # (sans SSL s'il n'y a pas de certificat)
    nginx.make_nginx_config(cert_name, imap_name, smtp_name, webmail_name)

    # Rsycn & Reload sur les machines
    rsync_and_reload()

    # Lancer certbot
    domains = []
    if imap_name is not None:
        domains.append(imap_name)
    if smtp_name is not None:
        domains.append(smtp_name)
    if webmail_name is not None:
        domains.append(webmail_name)
    certbot.call_certbot(cert_name, domains)

    # Vérifier que les certificats sont bons (récupérer?)

    # Re-faire la config nginx
    nginx.make_nginx_config(cert_name, imap_name, smtp_name, webmail_name)

    # Faire la config dovecot, si besoin
    if imap_name is not None:
        dovecot.make_dovecot_config(cert_name, imap_name)

    # Faire la config postfix, si besoin
    if smtp_name is not None:
        postfix.make_postfix_config(cert_name, smtp_name)

    nagios.make_nagios_config(cert_name, imap_name, smtp_name, webmail_name)

    # Rsync & Reload sur les machines
    rsync_and_reload()

    return True

def make_nocert(
    cert_name: str,
) -> bool:
    if certbot.cert_exists(cert_name):
        certbot.remove_cert(cert_name)
    if nginx.check_nginx_config(cert_name):
        nginx.remove_nginx_config(cert_name)
    if dovecot.check_dovecot_config(cert_name):
        dovecot.remove_dovecot_config(cert_name)
    if postfix.check_postfix_config(cert_name):
        postfix.remove_postfix_config(cert_name)
    if nagios.check_nagios_config(cert_name):
        nagios.remove_nagios_config(cert_name)
    rsync_and_reload()

def check_cert(
    cert_name: str,
    imap_name: str,
    smtp_name: str,
    webmail_name: str,
) -> dns.Err | None:
    if imap_name is None and smtp_name is None and webmail_name is None:
        return None

    if not certbot.cert_exists(cert_name):
        return dns.Err("cert", "no_cert", f"Pas de certificat pour ce domaine (ls)")

    info = certbot.cert_info(cert_name)
    if info is None:
        return dns.Err("cert", "no_cert", f"Pas de certificat pour ce domaine (jc)")

    domains = []
    if imap_name is not None:
        domains.append(imap_name)
    if smtp_name is not None:
        domains.append(smtp_name)
    if webmail_name is not None:
        domains.append(webmail_name)
    want_names = set(domains)

    info = json.loads(info)
    subject_cn = info[0]["tbs_certificate"]["subject"]["common_name"]
    alt_names = []
    for ext in info[0]["tbs_certificate"]["extensions"]:
        if ext["extn_id"] == "subject_alt_name":
            for dom in ext["extn_value"]:
                alt_names.append(dom)
    have_names = set(alt_names + [subject_cn])

    if have_names != want_names:
        return dns.Err("cert", "wrong_domains", f"Les noms dans le certificat ne sont pas les bons. cert: {have_names} / want: {want_names}")

    if not nginx.check_nginx_config(cert_name):
        return dns.Err("cert", "missing_nginx", f"Il manque des fichiers de configuration pour nginx pour ce domaine.")

    if not dovecot.check_dovecot_config(cert_name):
        return dns.Err("cert", "missing_dovecot", f"Il manque le fichier de configuration pour dovecot pour ce domaine.")

    if not postfix.check_postfix_config(cert_name, smtp_name):
        return dns.Err("cert", "missing_postfix", f"Il manque la configuration postfix pour ce domaine.")

    if not nagios.check_nagios_config(cert_name, imap_name, smtp_name, webmail_name):
        return dns.Err("cert", "missing_nagios", f"Il manque la configuration nagios pour ce domaine, ou elle est invalide.")

    return None

def check_nocert(
    cert_name: str,
) -> dns.Err | None:
    if certbot.cert_exists(cert_name):
        return dns.Err("cert", "cert", f"J'ai un certificat pour le domaine, je ne devrais pas")
    if nginx.check_nginx_config(cert_name):
        return dns.Err("cert", "nginx", f"J'ai une configuration pour nginx, je ne devrais pas")
    if dovecot.check_dovecot_config(cert_name):
        return dns.Err("cert", "dovecot", f"J'ai une configuration pour dovecot, je ne devrais pas")
    if postfix.check_postfix_config(cert_name):
        return dns.Err("cert", "postfix", f"J'ai une configuration pour postfix, je ne devrais pas")
    if nagios.check_nagios_config(cert_name):
        return dns.Err("cert", "nagios", f"J'ai une configuration pour nagios, je ne devrais pas")




####
#### REMOTE EXECUTION
####

def rsync_and_reload():
    if fake.fake_mode():
        # This fakes the rsync part
        remote.copy_files_in_dir(
            from_role="cert_manager",
            to_role="cert_user",
            dirname="/opt/certs",
        )
    else:
        command = [ "sudo", "/usr/bin/api_update_stuff.sh" ]
        for hostname in remote.get_hosts_by_role("cert_user"):
            remote.run_on_host(hostname, command)
        for hostname in remote.get_hosts_by_role("cert_manager"):
            remote.run_on_host(hostname, command)



