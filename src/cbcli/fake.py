
mode = "invalid"
fake_certs = {}

def init(in_mode: str):
    global mode
    if in_mode == "fake" or in_mode == "FAKE":
        mode = "FAKE"
    else:
        mode = "real"

def purge():
    if fake_mode():
        global fake_certs
        fake_certs = {}

def get_mode() -> str:
    if mode == "FAKE":
        return "FAKE"
    if mode == "real":
        return "real"
    raise Exception("Le module 'cbcli' n'est pas été initialisé")

def fake_mode() -> bool:
    if mode == "FAKE":
        return True
    if mode == "real":
        return False
    raise Exception("Le module 'cbcli' n'a pas été initialisé")


