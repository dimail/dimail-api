import jinja2

tpl = {}

def init():
    env = jinja2.Environment(
        loader = jinja2.PackageLoader("src"),
        autoescape = jinja2.select_autoescape(),
    )

    tpl["cert_info"] = env.get_template("cert_info.json")
    tpl["nginx"] = env.get_template("nginx.conf")
    tpl["dovecot"] = env.get_template("dovecot.conf")
    tpl["nagios"] = env.get_template("nagios.cfg")

def render(name: str, **kwargs):
    if not name in tpl:
        raise Exception(f"Je ne connais pas ce template '{name}' dont tu me parles.")
    return tpl[name].render(kwargs)

