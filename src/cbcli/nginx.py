from .. import enums, remote
from . import certbot, fake, templates

def make_nginx_config(
    cert_name: str,
    imap_name: str,
    smtp_name: str,
    webmail_name: str,
) -> str:
    cert_manager = remote.get_host_by_role(enums.Role.CertManager)
    print(f"Dans notre config, le cert manager est {cert_manager}")
    for hostname in remote.get_all_hosts():
        domains = []
        if remote.host_has_role(hostname, enums.Role.Imap):
            domains.append({"name": imap_name, "is_webmail": False})
        if remote.host_has_role(hostname, enums.Role.Smtp):
            domains.append({"name": smtp_name, "is_webmail": False})
        if remote.host_has_role(hostname, enums.Role.Webmail):
            domains.append({"name": webmail_name, "is_webmail": True})
        if len(domains) == 0:
            continue
        config = templates.render("nginx",
            cert_name = cert_name,
            domains = domains,
            cert_exists = certbot.cert_exists(cert_name),
        )
        remote.store_on_host(
            hostname=cert_manager,
            dirname=f"/opt/certs/config/nginx/{cert_name}/",
            filename=f"{hostname}.conf",
            content=config,
        )

def check_nginx_config(cert_name: str) -> bool:
    for hostname in remote.get_all_hosts():
        if ( not remote.host_has_role(hostname, enums.Role.Imap) and
             not remote.host_has_role(hostname, enums.Role.Smtp) and
             not remote.host_has_role(hostname, enums.Role.Webmail) ):
            continue
        filename = f"/opt/certs/config/nginx/{cert_name}/{hostname}.conf"
        if not remote.file_exists_on_host(hostname, filename):
            print(f"En mode {fake.get_mode()}, je ne trouve pas {filename} sur la machine {hostname}")
            return False
    return True

def remove_nginx_config(cert_name: str):
    # On supprime les fichiers de conf sur cert_manager
    cert_manager = remote.get_host_by_role(enums.Role.CertManager)
    for hostname in remote.get_all_hosts():
        if ( not remote.host_has_role(hostname, enums.Role.Imap) and
             not remote.host_has_role(hostname, enums.Role.Smtp) and
             not remote.host_has_role(hostname, enums.Role.Webmail) ):
            continue
        filename = f"/opt/certs/config/nginx/{cert_name}/{hostname}.conf"
        remote.remove_from_host(
            hostname = cert_manager,
            filename = filename,
        )
    remote.remove_from_host(
        hostname = cert_manager,
        filename = f"/opt/certs/config/nginx/{cert_name}/",
    )


