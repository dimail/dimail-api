import os.path
import subprocess

def get_version_info() -> (str, list[str]):
    changelog = [""]
    version = "Unknown"
    if os.path.isfile("Changelog"):
        print("J'ai le changelog")
        with open("Changelog") as f:
            text = f.read()

        changelog = text.split("\n")
        for line in changelog:
            if line.startswith("New in ") and version == "Unknown":
                version = line[len("New in "):len(line)-1]

    if os.path.isdir(".git"):
        file = subprocess.Popen(
            ["git", "branch", "--show-current"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
        )
        file.wait()

        if file.returncode != 0:
            log.error(f"Failed to call 'git branch'")
            log.error(file.stderr.read())
            branch="unknown-branch"
        else:
            branch = file.stdout.read().strip()

        file = subprocess.Popen(
            ["git", "describe"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
        )
        file.wait()

        if file.returncode != 0:
            log.error(f"Failed to call 'git describe'")
            log.error(file.stderr.read())
            tag = "unknown-tag"
        else:
            tag = file.stdout.read().strip()

        if tag.startswith("t"):
            tag = tag.replace("t", "v", 1)
        version = f"Dev on {branch} at {tag}"

    return (version, changelog)
