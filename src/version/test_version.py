from .. import version

def test_version(log):
    ver, change = version.get_version_info()
    assert ver == "Unknown"
    assert change == [""]
