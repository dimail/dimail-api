from .version import get_version_info

__all__ = [
    get_version_info,
]
