"""This module contains the enumerated types needed in the various models.

Classes:
    - Feature: the permitted values for a domain list of features
    - Delivery: the possible delivery modes for a domain
    - Perm: the global perms a user can have
"""
import enum

class MetaEnum(enum.EnumMeta):
    def __contains__(cls, item):
        try:
            cls(item)
        except ValueError:
            return False
        return True    


class BaseEnum(enum.Enum, metaclass=MetaEnum):
    def __repr__(self):
        return self.value
    def __str__(self):
        return self.value
    pass

@enum.verify(enum.UNIQUE)
class Role(str, BaseEnum):
    """Known roles when describing the hosts for the cert manager.

    Roles:
        Imap: imap server (master or slave)
        Smtp: smtp server (main or backup)
        Webmail: webmail server
        CertManager: the cert manager host
    """
    Api = "api"
    Dkim = "dkim"
    Imap = "imap"
    Smtp = "smtp"
    Webmail = "webmail"
    Monitor = "monitor"
    CertManager = "cert_manager"
    CertUser = "cert_user"
    Ox = "ox"

@enum.verify(enum.UNIQUE)
class Feature(str, BaseEnum):
    """Feature enumeration.

    Attributes:
        Webmail: Webmail feature.
        Mailbox: Mailbox feature.
        Alias: Alias feature.
    """
    Webmail = "webmail"
    Mailbox = "mailbox"
    Alias = "alias"
    Mx = "mx"
    Relay = "relay"

@enum.unique
class Delivery(str, BaseEnum):
    Virtual="virtual"
    Alias="alias"
    Relay="relay"

@enum.unique
class Perm(str, BaseEnum):
    NewDomain = "new_domain"
    ManageUsers = "manage_users"
    CreateUsers = "create_users"
    GetLogs = "get_logs"

@enum.unique
class OldPerm(BaseEnum):
    OldPerm = "old_perm", None
    CreateDomain = "create_domain", "new_domain"
    def __new__(cls, value, replacement):
        obj = object.__new__(cls)
        obj._value_ = value
        obj.replacement = replacement
        return obj

@enum.unique
class MailboxType(str, BaseEnum):
    """Enum class that represents the type of the mailbox.

    The mailbox can be either an alias or a mailbox.

    Attributes:
        Alias: The mailbox is an alias.
        Mailbox: The mailbox is a mailbox.
    """
    Alias = "alias"
    Mailbox = "mailbox"

@enum.unique
class MailboxStatus(str, BaseEnum):
    """Enum class that represents the status of the mailbox.

    The mailbox can be in different states, such as OK, Broken, or Unknown.

    Attributes:
        OK: The mailbox is OK.
        Broken: The mailbox is broken.
        Unknown: The mailbox is unknown
        Disabled: The mailbox is disabled
    """
    OK = "ok"
    Broken = "broken"
    Unknown = "unknown"

@enum.unique
class MailboxActive(str, BaseEnum):
    """Mailbox status as seen by fdovecot (is it active or not).

    The possible values are 'yes' and 'no'. When active is 'no', the mailbox
    cannot receive e-mails, and the user cannot connect to his mailbox, neither on
    IMAP or on Webmail.

    Attributes:
        Yes: the mailbox is active.
        No: the mailbox is not active.
    """
    Yes = "yes"
    No = "no"
    Wait = "wait"
    Unknown = "unknown"

@enum.unique
class ContextStatus(str, BaseEnum):
    Available = "available"
    Differ = "differ"
    Partial = "partial"
    Same = "same"

