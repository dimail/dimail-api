import pytest

from .. import enums

def test_enum():
    assert "webmail" in enums.Feature
    assert "plop" not in enums.Feature
    assert enums.Feature("webmail") == enums.Feature.Webmail
    assert enums.Feature["Webmail"] == enums.Feature.Webmail
    with pytest.raises(Exception) as e:
        x = enums.Feature("plop")
    assert "is not a valid Feature" in str(e.value)

    assert "old_perm" in enums.OldPerm
    assert "create_domain" in enums.OldPerm
    assert enums.OldPerm("old_perm").replacement is None
    assert enums.OldPerm("create_domain").replacement is "new_domain"

    assert enums.Feature.Webmail in [ "webmail", "mailbox" ]
    assert enums.Feature.Webmail == "webmail"


