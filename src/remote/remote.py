import asyncio
import anyio
import logging
import os
import os.path
import pydantic
import subprocess

from .. import enums, others, utils, web_models

class Host(pydantic.BaseModel):
    ssh_url: str
    ssh_args: list[str]
    roles: list[enums.Role]
    infos: dict[str, list[str]]
    use_master: bool = False

hosts = {}
roles = {}
fake_store = {}
mode = "broken"

def is_initialised():
    if mode == "broken":
        return False
    return True

def init_remote(hosts_info: str | list[str]):
    # On prend le logger "uvicorn" pour laisser des traces dans la console.
    main_log = logging.getLogger("uvicorn")
    global hosts
    global roles
    global mode
    global fake_store

    hosts = {}
    roles = {}
    if not mode == "broken":
        main_log.info(f"Remote module already initialized. Known hosts: {hosts.keys()}, mode={mode}")
        return

    if isinstance(hosts_info, str):
        if hosts_info == "FAKE":
            mode = "FAKE"
            fake_store = {}
            hosts["main"] = Host(
                ssh_url = "FAKE",
                ssh_args = [],
                roles = [ "imap", "smtp", "cert_manager", "dkim", "cert_user" ],
                infos = {},
            )
            hosts["webmail"] = Host(
                ssh_url = "FAKE",
                ssh_args = [],
                roles = [ "webmail", "cert_user" ],
                infos = {},
            )
            hosts["monitor"] = Host(
                ssh_url = "FAKE",
                ssh_args = [],
                roles = [ "monitor", "cert_user" ],
                infos = {},
            )
            hosts["ox"] = Host(
                ssh_url = "FAKE",
                ssh_args = [],
                roles = [ "ox" ],
                infos = {
                    "ox": [ "touseulfake" ],
                },
            )
            roles["imap"] = ["main"]
            roles["smtp"] = ["main"]
            roles["webmail"] = ["webmail"]
            roles["cert_manager"] = ["main"]
            roles["dkim"] = ["main"]
            roles["monitor"] = ["monitor"]
            roles["cert_user"] = ["main", "webmail", "monitor"]
            roles["ox"] = [ "ox" ]
            return
        raise Exception(f"Je ne comprend pas {hosts_info}")
    else:
        mode = "real"

    for host_info in hosts_info:
        (hostname, ssh_url, ssh_args, h_roles) = host_info.split(";",3)
        main_log.debug(f"On ajoute le host {hostname}")
        use_master = True
        if ssh_args == "nomaster":
            ssh_args = ""
            use_master = False
        if ssh_args == "":
            ssh_args = []
        else:
            ssh_args = ssh_args.split(",")
        h_roles = h_roles.split(",")
        my_roles = []
        my_infos = {}
        for item in h_roles:
            elems = item.split("=")
            role = elems[0]
            my_roles.append(role)
            main_log.debug(f"- il a le role {role}")
            if len(elems) > 1:
                main_log.debug(f"- pour ce role, il y a des infos: {elems[1:]}")
                my_infos[role] = elems[1:]
        main_log.debug(f"- les roles de {hostname} sont {my_roles}")
        main_log.debug(f"- les infos de {hostname} sont {my_infos}")
        hosts[hostname] = Host(
            ssh_url = ssh_url,
            ssh_args = ssh_args,
            roles = my_roles,
            infos = my_infos,
            use_master = use_master,
        )
        for role in my_roles:
            if role not in roles:
                roles[role] = []
            roles[role].append(hostname)
        start_master(hostname)

def end_remote():
    global mode
    if fake_mode():
        mode="broken"
        return
    mode="broken"
    for hostname in hosts.keys():
        stop_master(hostname)

def fake_mode() -> bool:
    if mode == "FAKE":
        return True
    if mode == "real":
        return False
    raise Exception("Le module 'remote' n'a pas été initialisé")

def purge():
    global fake_store
    if mode == "FAKE":
        fake_store = {}
        return
    for hostname in hosts.keys():
        print(f"Je purge /tmp sur la machine {hostname}")
        # Il faut un shell pour avoir '*'
        run_on_host(hostname, ["/bin/sh", "-c", "rm -rf /tmp/*"], want_success=True)

###
### Get and list hosts
###

def get_host_by_role(role: str, mandatory: bool = True) -> str:
    if role not in roles:
        if mandatory:
            raise Exception(f"Personne n'a le role {role}")
        else:
            return None
    if len(roles[role]) == 0:
        if mandatory:
            raise Exception(f"Personne n'a le role {role}")
        else:
            return None
    if len(roles[role]) > 1:
        raise Exception(f"Plusieurs machines ont le role {role} roles={roles}")
    return roles[role][0]

def get_hosts_by_role(role: str, mandatory: bool = False) -> list[str]:
    if role not in roles:
        if mandatory:
            raise Exception(f"Personne n'a le role {role}")
        else:
            return []
    if len(roles[role]) == 0:
        if mandatory:
            raise Exception(f"Personne n'a le role {role}")
        else:
            return []
    return roles[role]

def get_all_hosts() -> list[str]:
    return [ x for x in hosts.keys() ]

def host_has_role(hostname: str, role: enums.Role) -> bool:
    if hostname not in hosts:
        raise Exception(f"Unknown host {hostname}")
    if role in hosts[hostname].roles:
        return True
    return False

def host_infos(hostname: str, role: enums.Role) -> list[str]:
    if hostname not in hosts:
        raise Exception(f"Unknown host {hostname}")
    if not role in hosts[hostname].roles:
        raise Exception(f"Host {hostname} does not have role {role}")
    if not role in hosts[hostname].infos:
        return []
    return [ x for x in hosts[hostname].infos[role] ]

###
### File management on remote host
###

def store_on_host(hostname: str, dirname: str, filename: str, content: str):
    if hostname is None:
        raise Exception("Missing hostname")
    if dirname is None:
        raise Exception("Missing dirname")
    if not dirname.startswith("/"):
        raise Exception("Must be an absolute dirname")
    if not dirname.endswith("/"):
        raise Exception("Dirname must end with /")
    if filename is None:
        raise Exception("Missing filename")
    if "/" in filename:
        raise Exception("Filename must be a file name, no directory part")
    if content is None:
        raise Exception("Missibg content")

    if fake_mode():
        global fake_store
        print(f"En mode FAKE, je range le fichier {hostname}:{dirname}{filename} dans le store")
        fake_store[f"{hostname}:{dirname}{filename}"] = content
        return
    if hostname not in hosts:
        raise Exception(f"Unknown host {hostname}")

    log = logging.getLogger(__name__)
    ssh_url = hosts[hostname].ssh_url
    ssh_args = hosts[hostname].ssh_args

    command=f"mkdir -p {dirname}; cat > {dirname}/{filename}"
    file = subprocess.Popen(
        ["ssh"] + ssh_args + [ ssh_url, command ],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    )
    (out, err) = file.communicate(input=content)
    file.wait()
    if file.returncode != 0:
        print(f"Failed to call {command} on {hostname} at {ssh_url}")
        print(out)
        print(err)
        raise Exception("Failed to run ssh command")
    return file

def remove_from_host(hostname: str, filename: str):
    if hostname is None:
        raise Exception("Missing hostname")
    if filename is None:
        raise Exception("Missing filename")
    if not filename.startswith("/"):
        raise Exception("Must be an absolute path for filename")
    if "*" in filename or "?" in filename:
        raise Exception("No wildcard allowed in filename")

    if fake_mode():
        global fake_store
        if f"{hostname}:{filename}" in fake_store:
            print(f"En mode FAKE, je retire le fichier {hostname}:{filename} du store")
            del fake_store[f"{hostname}:{filename}"]
        return
    if hostname not in hosts:
        raise Exception(f"Unknown host {hostname}")

    log = logging.getLogger(__name__)
    ssh_url = hosts[hostname].ssh_url
    ssh_args = hosts[hostname].ssh_args

    command = f"rm -f '{filename}'"
    if filename.endswith("/"):
        command = f"rmdir '{filename}'"
    (stdout, stderr, code) = run_on_host(hostname, command, want_success=False)
    if not code == 0:
        print(f"Failed ssh for command {command}")
        print("Stdout:")
        print(stdout)
        print("Stderr:")
        print(stderr)
        if filename.endswith("/"):
            files = run_on_host(hostname, f"ls -la '{filename}'")
            print("Files in dir:")
            print(files)


def read_from_host(hostname: str, filename: str) -> str:
    if hostname is None:
        raise Exception("Missing hostname")
    if filename is None:
        raise Exception("Missing filename")
    if not filename.startswith("/"):
        raise Exception("Filename must be an absolute path")
    if filename.endswith("/"):
        raise Exception("Filename must not be a directory name")

    if fake_mode():
        global fake_store
        if f"{hostname}:{filename}" in fake_store:
            return fake_store[f"{hostname}:{filename}"]
        return ""
    if not hostname in hosts:
        raise Exception(f"Unknown host {hostname}")

    command=["cat", filename]
    (stdout, stderr, returncode) = run_on_host(hostname, command, want_success=False)
    if returncode != 0:
        print(f"Failed to run command {command} on host {hostname}")
        print("Stdout:")
        print(stdout)
        print("Stderr:")
        print(stderr)
        return ""
    return stdout

def file_exists_on_host(hostname: str, filename: str) -> bool:
    if hostname is None:
        raise Exception("Missing hostname")
    if filename is None:
        raise Exception("Missing filename")
    if not filename.startswith("/"):
        raise Exception("Filename must be an absolute path")
    if fake_mode():
        global fake_store
        prefix = f"{hostname}:{filename}"
        if filename.endswith("/"):
            for name in fake_store.keys():
                if name.startswith(prefix):
                    return True
            return False
        if prefix in fake_store:
            return True
        return False
    if hostname not in hosts:
        raise Exception(f"Unknown host {hostname}")

    command = [ "sh", "-c", f"test -f '{filename}' && echo oui || echo non" ]
    if filename.endswith("/"):
        command = [ "sh", "-c", f"test -d '{filename}' && echo oui || echo non" ]
    (stdout, stderr, returncode) = run_on_host(hostname, command, want_success = False)
    if returncode != 0:
        print(f"Failed ssh command {command} on host {hostname}. Probable que le fichier n'existe pas.")
        print("Stdout:")
        print(stdout)
        print("Stderr:")
        print(stderr)
        raise Exception("Failed test on {hostname}, stderr={stderr}, stdout={stdout}")
    res = stdout
    if res.startswith("oui"):
        return True
    if res.startswith("non"):
        return False
    raise Exception(f"J'ai lu <{res}> et je ne comprend pas ce que ça veut dire.")


def copy_files_in_dir(from_role: str, to_role: str, dirname: str):
    # On va simuler rsync, en mode fake
    if not fake_mode():
        raise Exception("Il ne faut PAS FAIRE CA EN VRAI, pauvre fou.")

    from_host = get_host_by_role(from_role)
    to_hosts = get_hosts_by_role(to_role)

    files = [ name for name in fake_store ]
    for name in files:
        if name.startswith(f"{from_host}:{dirname}"):
            filename = name[len(from_host)+1:]
            for hostname in to_hosts:
                print(f"En mode FAKE, je 'rsync' le fichier {filename} de {from_host} vers {hostname}")
                fake_store[f"{hostname}:{filename}"] = fake_store[name]


###
### Remote execution
###

# Sur ma machine, j'ai deux fois l'appli qui tourne :
# - une dans uvicorn
# - une dans les tests
# Autant ne pas couper les connexions ssh de l'appli qui tourne pendant qu'on
# lance les tests.
pid = 0
def start_master(
    hostname: str,
):
    global pid
    if not hosts[hostname].use_master:
        return
    log = logging.getLogger(__name__)
    ssh_url = hosts[hostname].ssh_url
    ssh_args = hosts[hostname].ssh_args
    pid = os.getpid()
    log.error(f"Start master for {hostname}, mode = {mode}, pid = {pid}")
    if os.path.exists(f"/tmp/ssh.{hostname}.{pid}"):
        print(f"Le socket /tmp/ssh.{hostname}.{pid} existe deja")
        exit(1)
    proc = subprocess.Popen(
        ["ssh", "-M", "-o", f"ControlPath=/tmp/ssh.{hostname}.{pid}", "-o", "ControlPersist=yes"] + ssh_args + [ ssh_url, "/bin/true" ],
    )
    proc.communicate()
    proc.wait()
    if proc.returncode != 0:
        raise Exception("Failed to start master")
    hosts[hostname].ssh_args.append("-o")
    hosts[hostname].ssh_args.append(f"ControlPath=/tmp/ssh.{hostname}.{pid}")

def stop_master(
    hostname: str,
):
    global pid
    if not hosts[hostname].use_master:
        return
    log = logging.getLogger(__name__)
    ssh_url = hosts[hostname].ssh_url
    ssh_args = hosts[hostname].ssh_args
    log.error(f"Stop master for {hostname}")
    proc = subprocess.Popen(
        ["ssh", "-O", "exit",  "-o", f"ControlPath=/tmp/ssh.{hostname}.{pid}"] + ssh_args + [ ssh_url ],
    )
    proc.communicate()
    proc.wait()
    if proc.returncode != 0:
        raise Exception("Failed to stop master")


def run_on_host(
    hostname: str,
    command: list[str] | str,
    want_success: bool = True,
):
    if fake_mode():
        raise Exception("Il ne faut jamais appeler SSH quand on est en FAKE")
    log = logging.getLogger(__name__)
    ssh_url = hosts[hostname].ssh_url
    ssh_args = hosts[hostname].ssh_args
    if isinstance(command, list):
        command = utils.make_protected_cli(command)
    proc = subprocess.Popen(
        ["ssh"] + ssh_args + [ ssh_url, command ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    )
    (stdout, stderr) = proc.communicate()
    if proc.returncode != 0:
        if want_success:
            print(f"Failed to call {command} on {hostname} at {ssh_url} with args {ssh_args}")
            print("Stdout:")
            print(stdout)
            print("Stderr:")
            print(stderr)
            raise Exception("Failed to run ssh command")
    if want_success:
        return stdout
    else:
        return (stdout, stderr, proc.returncode)

async def arun_on_host(
    hostname: str,
    command: list[str] | str,
    want_success: bool = True,
):
    if fake_mode():
        raise Exception("Il ne faut jamais appeler SSH quand on est en FAKE")
    log = logging.getLogger(__name__)
    ssh_url = hosts[hostname].ssh_url
    ssh_args = hosts[hostname].ssh_args
    if isinstance(command, list):
        command = utils.make_protected_cli(command)
    args = [ "/usr/bin/ssh" ] + ssh_args + [ ssh_url, command ]
    proc = await asyncio.create_subprocess_exec(
        *args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=False,
    )
    (stdout, stderr) = await proc.communicate()
    stdout = stdout.decode("utf-8")
    stderr = stderr.decode("utf-8")
    if proc.returncode != 0:
        if want_success:
            print(f"Failed to call {command} on {hostname} at {ssh_url} with args {ssh_args}")
            print("Stdout:")
            print(stdout)
            print("Stderr:")
            print(stderr)
            raise Exception("Failed to run ssh command")
    if want_success:
        return stdout
    else:
        return (stdout, stderr, proc.returncode)


def stream_from_host(
    hostname: str,
    command: list[str] | str,
):
    if fake_mode():
        raise Exception("Il ne faut jamais appeler SSH quand on est en FAKE")
    log = logging.getLogger(__name__)
    ssh_url = hosts[hostname].ssh_url
    ssh_args = hosts[hostname].ssh_args
    if isinstance(command, list):
        command = utils.make_protected_cli(command)
    proc = subprocess.Popen(
        ["ssh"] + ssh_args + [ ssh_url, command ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    )
    return anyio.wrap_file(proc.stdout), anyio.wrap_file(proc.stderr), proc

###
### Health check
###

def health_check() -> web_models.ModuleStatus:
    status = web_models.ModuleStatus()
    status.add_detail("Checking everything is fine for remote module")
    status.add_detail(f"mode = {mode}")
    if fake_mode():
        status.add_detail(f"nb_files = {len(fake_store)}")
        status.add_detail("remote_status = OK")
        return status

    status.add_detail(f"nb_hosts = {len(hosts)}")
    status.add_detail(f"hosts = {hosts}")

    for hostname in get_all_hosts():
        (stdout, stderr, returncode) = run_on_host(hostname, "echo ok", want_success = False)
        if returncode != 0:
            status.add_exec_failure(f"ERROR: (sur {hostname}) Echec a contacter {hostname} en ssh:", stdout, stderr)
        else:
            status.add_detail(f"host {hostname}: ssh OK")

    for hostname in get_hosts_by_role("cert_user"):
        (stdout, stderr, returncode) = run_on_host(hostname, "sudo /usr/bin/api_update_stuff.sh check", want_success = False)
        if returncode != 0:
            status.add_exec_failure(f"ERROR: (sur {hostname}) Echec a appeler sudo /usr/bin/api_update_stuff.sh en mode check", stdout, stderr)
        else:
            status.add_detail(f"host {hostname}: sudo /usr/bin/api_update_stuff.sh OK")

    imap = get_host_by_role("imap", False)
    if imap is None:
        status.add_detail(f"ERROR: Il n'y a pas de serveur IMAP, ça n'a aucun sens")
        status.fail()
    else:
        for other_name in others.list_others():
            other = others.get_other(other_name)
            tech = other.get_tech_domain()
            (stdout, stderr, returncode) = run_on_host(imap, f"sudo /usr/bin/api_sync_box.sh check {tech}", want_success = False)
            if returncode != 0:
                status.add_exec_failure(f"ERROR: (sur {imap}) Echec a appeler (other = {other_name}), sudo /usr/bin/api_sync_box.sh check {tech}", stdout, stderr)
            else:
                status.add_detail(f"host {imap}, remote {tech} (for {other_name}): sudo /usr/bin/api_sync_box.sh OK")

    if status.status:
        status.add_detail("remote_status = OK")
    else:
        status.add_detail("remote_status = KO")
    return status


