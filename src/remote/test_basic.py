import pytest

from .. import enums, remote

# Il faut tourner ce test le plus tôt possible, avant que le setup de remote
# ait été fait
# Comme il ne dépend pas de savoir si on est en fake ou pas, il sera joué
# *avant* le setup du mode fake/real, donc très tôt.
@pytest.mark.order(0)
def test_init(log):
    if remote.is_initialised():
        # Si la fixture 'remote_setup' a été initialisée, on ne peut pas jouer
        # ces tests
        log.error("On ne peut pas jouer les tests d'init")
        return
    else:
        log.error("On peut jouer les tests d'init")

    with pytest.raises(Exception) as e:
        remote.init_remote("invalid")
    assert "comprend pas" in str(e.value)

    remote.init_remote("FAKE")
    assert remote.fake_mode() is True
    remote.end_remote()

    remote.init_remote(["un;ssh://nom:port;nomaster;imap,smtp"])
    tmp = remote.get_all_hosts()
    assert tmp == [ "un" ]
    tmp = remote.host_has_role("un", "imap")
    assert tmp is True
    tmp = remote.host_has_role("un", "webmail")
    assert tmp is False
    tmp = remote.get_host_by_role("imap")
    assert tmp == "un"
    tmp = remote.get_hosts_by_role("smtp")
    assert tmp == [ "un" ]
    remote.end_remote()

    remote.init_remote(["un;ssh://nom:port;nomaster;imap,smtp", "deux;ssh://nom:port;nomaster;smtp", "myself;ssh://nom:port;nomaster;api"])
    tmp = remote.get_all_hosts()
    assert set(tmp) == { "un", "deux", "myself" }
    tmp = remote.get_hosts_by_role("smtp")
    assert set(tmp) == { "un", "deux" }

    with pytest.raises(Exception):
        remote.get_host_by_role("webmail")
    with pytest.raises(Exception):
        remote.get_host_by_role("smtp")
    with pytest.raises(Exception):
        remote.get_hosts_by_role("webmail", mandatory=True)
    with pytest.raises(Exception):
        remote.host_has_role("pasmoi", enums.Role.Webmail)

    tmp = remote.get_hosts_by_role("webmail")
    assert tmp == []

    remote.end_remote()


def test_store(log, setup_remote):
    with pytest.raises(Exception) as e:
        remote.store_on_host(
            hostname=None,
            dirname=None,
            filename=None,
            content=None,
        )
    assert "hostname" in str(e.value)

    with pytest.raises(Exception) as e:
        remote.store_on_host(
            hostname="main",
            dirname=None,
            filename=None,
            content=None,
        )
    assert "dirname" in str(e.value)

    with pytest.raises(Exception) as e:
        remote.store_on_host(
            hostname="main",
            dirname=".",
            filename=None,
            content=None,
        )
    assert "absolute" in str(e.value)

    with pytest.raises(Exception) as e:
        remote.store_on_host(
            hostname="main",
            dirname="/tmp",
            filename=None,
            content=None,
        )
    assert "must end with /" in str(e.value)

    with pytest.raises(Exception) as e:
        remote.store_on_host(
            hostname="main",
            dirname="/tmp/",
            filename=None,
            content=None,
        )
    assert "filename" in str(e.value)

    with pytest.raises(Exception) as e:
        remote.store_on_host(
            hostname="main",
            dirname="/tmp/",
            filename="f/file.txt",
            content=None,
        )
    assert "no directory" in str(e.value)

    with pytest.raises(Exception) as e:
        remote.store_on_host(
            hostname="main",
            dirname="/tmp/",
            filename="file.txt",
            content=None,
        )
    assert "content" in str(e.value)

    if not remote.fake_mode():
        with pytest.raises(Exception) as e:
            remote.store_on_host(
                hostname="invalid",
                content="contenu",
                dirname="/tmp/",
                filename="test",
            )
        assert "Unknown host" in str(e.value)

    with pytest.raises(Exception) as e:
        tmp = remote.file_exists_on_host(
            hostname=None,
            filename=None,
        )
    assert "hostname" in str(e.value)

    with pytest.raises(Exception) as e:
        tmp = remote.file_exists_on_host(
            hostname="main",
            filename=None,
        )
    assert "filename" in str(e.value)

    with pytest.raises(Exception) as e:
        tmp = remote.file_exists_on_host(
            hostname="main",
            filename="./file.txt",
        )
    assert "absolute path" in str(e.value)

    with pytest.raises(Exception) as e:
        remote.remove_from_host(
            hostname="main",
            filename=None,
        )
    assert "filename" in str(e.value)

    with pytest.raises(Exception) as e:
        remote.remove_from_host(
            hostname=None,
            filename="/tmp/toto.txt",
        )
    assert "hostname" in str(e.value)

    with pytest.raises(Exception) as e:
        remote.remove_from_host(
            hostname="main",
            filename="/tmp/*.txt",
        )
    assert "wildcard" in str(e.value)

    with pytest.raises(Exception) as e:
        remote.remove_from_host(
            hostname="main",
            filename="toto.docx",
        )
    assert "absolute" in str(e.value)

    if not remote.fake_mode():
        with pytest.raises(Exception) as e:
            tmp = remote.file_exists_on_host(
                hostname="invalid",
                filename="/tmp/file.txt",
            )
        assert "Unknown host" in str(e.value)

        with pytest.raises(Exception) as e:
            remote.remove_from_host(
                hostname="invalid",
                filename="/tmp/file.txt",
            )
        assert "Unknown host" in str(e.value)

    with pytest.raises(Exception) as e:
        tmp = remote.read_from_host(
            hostname=None,
            filename=None,
        )
    assert "hostname" in str(e.value)

    with pytest.raises(Exception) as e:
        tmp = remote.read_from_host(
            hostname="main",
            filename=None,
        )
    assert "filename" in str(e.value)

    with pytest.raises(Exception) as e:
        tmp = remote.read_from_host(
            hostname="main",
            filename="./file.txt",
        )
    assert "absolute path" in str(e.value)

    with pytest.raises(Exception) as e:
        tmp = remote.read_from_host(
            hostname="main",
            filename="/tmp/dir/",
        )
    assert "directory" in str(e.value)

    if not remote.fake_mode():
        with pytest.raises(Exception) as e:
            tmp = remote.read_from_host(
                hostname="invalid",
                filename="/tmp/file.txt",
            )
        assert "Unknown host" in str(e.value)

    tmp = remote.file_exists_on_host(
        hostname="main",
        filename="/tmp/certs/config/nginx/cert_name/lefichier.txt",
    )
    assert tmp is False

    tmp = remote.file_exists_on_host(
        hostname="main",
        filename="/tmp/certs/config/nginx/cert_name/",
    )
    assert tmp is False

    remote.store_on_host(
        hostname="main",
        content="Le contenu du fichier\n",
        dirname="/tmp/certs/config/nginx/cert_name/",
        filename="lefichier.txt",
    )

    tmp = remote.file_exists_on_host(
        hostname="main",
        filename="/tmp/certs/config/nginx/cert_name/lefichier.txt",
    )
    assert tmp is True

    tmp = remote.file_exists_on_host(
        hostname="main",
        filename="/tmp/certs/config/nginx/cert_name/",
    )
    assert tmp is True

    tmp = remote.read_from_host(
        hostname="main",
        filename="/tmp/certs/config/nginx/cert_name/lefichier.txt",
    )
    assert tmp == "Le contenu du fichier\n"

    remote.remove_from_host(
        hostname="main",
        filename="/tmp/certs/config/nginx/cert_name/lefichier.txt",
    )

    tmp = remote.file_exists_on_host(
        hostname="main",
        filename="/tmp/certs/config/nginx/cert_name/lefichier.txt",
    )
    assert tmp is False

    tmp = remote.file_exists_on_host(
        hostname="main",
        filename="/tmp/certs/config/nginx/cert_name/",
    )
    if remote.fake_mode():
        # En mode fake, on n'a pas vraiment de répertoire, alors il disparait avec le fichier
        assert tmp is False
    else:
        assert tmp is True

    remote.remove_from_host(
        hostname="main",
        filename="/tmp/certs/config/nginx/cert_name/",
    )

    tmp = remote.file_exists_on_host(
        hostname="main",
        filename="/tmp/certs/config/nginx/cert_name/",
    )
    assert tmp is False

    # On peut bien le retirer une deuxième fois, ça ne change rien
    remote.remove_from_host(
        hostname="main",
        filename="/tmp/certs/config/nginx/cert_name/lefichier.txt",
    )

def test_health_check(log, setup_remote, set_others):
    status = remote.health_check()
    assert status.status is True
    assert "remote_status = OK" in status.detail


@pytest.mark.asyncio
async def test_async(log, setup_remote):
    if remote.fake_mode():
        return

    stdout = await remote.arun_on_host("main", ["echo", "Coincoin"])
    assert stdout == "Coincoin\n"

    stdout, stderr, code = await remote.arun_on_host("main", ["echo", "Des savanes"], want_success = False)
    assert stdout == "Des savanes\n"
    assert stderr == ""
    assert code == 0

    stdout, stderr, code = await remote.arun_on_host("main", ["/bin/sh", "-c", "echo Wrong >&2; echo Right; exit 12"], want_success = False)
    assert stdout == "Right\n"
    assert stderr == "Wrong\n"
    assert code == 12
