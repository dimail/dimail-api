
from .. import dkcli

def test_dkcli(log, dk_manager):
    log.info("Faisons des tests")
    (priv, dns) = dkcli.make_dkim_key("select", "domain.com")
    assert dns.startswith("select._domainkey.domain.com.\tIN\tTXT")

    status = dkcli.health_check()
    assert status.status is True
    assert "dkcli_status = OK" in status.detail
