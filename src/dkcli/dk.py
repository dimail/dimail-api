import subprocess

from .. import remote, utils, web_models

def health_check() -> web_models.ModuleStatus:
    status = web_models.ModuleStatus()
    status.add_detail("Checking health for dkcli")

    if remote.fake_mode():
        status.add_detail("Everything is always OK in fake mode")
        status.add_detail("dkcli_status = OK")
        return status

    (stdout, stderr, returncode)= run_command("/usr/sbin/opendkim-genkey --version", want_success = False)
    if returncode != 0:
        status.add_exec_failure(f"ERROR: Echec de l'appel opendkim-genkey --version:", stdout, stderr)
    else:
        status.add_detail("opendkim-genkey = OK")

    (stdout, stderr, returncode) = run_command("dir=`mktemp -d`; echo dir=$dir; rmdir $dir", want_success = False)
    if returncode != 0:
        status.add_exec_failure(f"ERROR: Echec de la création du répertoire temporaire:", stdout, stderr)
    else:
        status.add_detail("tempdir = OK")

    if status.status:
        status.add_detail("dkcli_status = OK")
    else:
        status.add_detail("dkcli_status = KO")

    print(status)
    return status


def make_dkim_key(selector: str, domain: str) -> (str, str):
    # opendkim-genkey -v -h sha256 -b 4096  --append-domain -s <selector> -d <domain>

    if remote.fake_mode():
        pub_key = f"{selector}._domainkey.{domain}.\tIN\tTXT (" + """"v=DKIM1; h=sha256; k=rsa; "
                "p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2dg8Bt0+p4EEIGF3udBpR"
                "psTd9B0UUzZPTJo64fwijJxFo8RgVUOe8vV6xzhGI22ldMAl6fYNsXih7p/AhEk+CpH"
                "QBFuittufD6Q8XyNrYMblHHfUKlkdy63Bi9v784qc1bWVI+/YRuFzEVnxQkNlbNyKFr"
                "ulZ6J/f7LR1sreSZakMHgy3ePp0QS9oUxs8tYxzWTSfnTS/VAv7"
                "GD4VoZMvLSa+u1fikagc5t3xg76P9twzBOjuFFqIFg+wPGzZZWpzSh/yfcMWHg+eLxk"
                "sxcronXnNZNnfPppNdu2Id28amHB/WB/4vqmgeM3xYIZWETDvZZIjVOzlxGtfgLuNlV"
                "LwIDAQAB"); -- This is a fake, but good enough to fool lot of people
                """
        priv_key = "This is a private key you should never see"
        return (priv_key, pub_key)


    command = f"dir=`mktemp -d`; echo dir=$dir; cd $dir; /usr/sbin/opendkim-genkey -v -h sha256 -b 4096  --append-domain -s '{selector}' -d '{domain}'"
    stdout = run_command(command)
    dirname = stdout.rstrip()
    if not dirname.startswith("dir="):
        raise Exception(f"Echec de l'appel ssh, j'aurai du avoir dir=<dirname>, et j'ai {dirname}")
    dirname = dirname[4:]
    
    command = f"cat {dirname}/{selector}.txt"
    dns = run_command(command)
    
    command = f"cat {dirname}/{selector}.private"
    private = run_command(command)

    command = f"rm -rf {dirname}"
    _ = run_command(command)

    return (private, dns)


def run_command(command: str, want_success: bool = True):
    dkim_manager = remote.get_host_by_role("dkim")
    return remote.run_on_host(dkim_manager, command, want_success)

