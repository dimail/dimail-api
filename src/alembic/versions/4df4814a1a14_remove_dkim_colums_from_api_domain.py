"""Remove dkim colums from api.domain

Revision ID: 4df4814a1a14
Revises: 982b2603bb1e
Create Date: 2024-10-02 18:34:39.254520

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision: str = '4df4814a1a14'
down_revision: Union[str, None] = '982b2603bb1e'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade(engine_name: str) -> None:
    globals()["upgrade_%s" % engine_name]()


def downgrade(engine_name: str) -> None:
    globals()["downgrade_%s" % engine_name]()





def upgrade_api() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('domains', 'dkim_selector')
    op.drop_column('domains', 'dkim_public')
    op.drop_column('domains', 'dkim_private')
    # ### end Alembic commands ###


def downgrade_api() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('domains', sa.Column('dkim_private', mysql.TEXT(), nullable=True))
    op.add_column('domains', sa.Column('dkim_public', mysql.TEXT(), nullable=True))
    op.add_column('domains', sa.Column('dkim_selector', mysql.VARCHAR(length=50), nullable=True))
    # ### end Alembic commands ###


def upgrade_dovecot() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade_dovecot() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def upgrade_opendkim() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade_opendkim() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def upgrade_postfix() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade_postfix() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###

