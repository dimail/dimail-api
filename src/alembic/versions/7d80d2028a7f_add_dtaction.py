"""add dtaction

Revision ID: 7d80d2028a7f
Revises: a836a69c1847
Create Date: 2024-07-24 17:53:42.383894

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '7d80d2028a7f'
down_revision: Union[str, None] = 'a836a69c1847'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade(engine_name: str) -> None:
    globals()["upgrade_%s" % engine_name]()


def downgrade(engine_name: str) -> None:
    globals()["downgrade_%s" % engine_name]()




def upgrade_opendkim() -> None:
    pass

def downgrade_opendkim() -> None:
    pass

def upgrade_api() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('domains', sa.Column('dtaction', sa.DateTime(), nullable=True))
    op.create_index(op.f('ix_domains_dtaction'), 'domains', ['dtaction'], unique=False)
    # ### end Alembic commands ###


def downgrade_api() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_domains_dtaction'), table_name='domains')
    op.drop_column('domains', 'dtaction')
    # ### end Alembic commands ###


def upgrade_dovecot() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade_dovecot() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def upgrade_postfix() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade_postfix() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###

