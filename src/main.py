import logging, logging.config, logging.handlers
import pathlib

import contextlib
import fastapi, fastapi.middleware.cors
import json
import starlette.responses
import yaml

from . import config, cbcli, dkcli, dns, logs, others, oxcli, remote, routes, sql_api, sql_dkim, sql_dovecot, sql_postfix, utils, version

if pathlib.Path("logs.yaml").is_file():
    with open("logs.yaml") as f:
        log_conf = yaml.load(f, Loader=yaml.SafeLoader)
    logging.config.dictConfig(log_conf)
else:
    print("Je ne trouve pas le fichier logs.yaml")

# On tourne les tests, ou on tourne en vrai ?
testing = False
def set_testing(t: bool):
    global testing
    testing = t

# Le logger de tout notre module
log = logging.getLogger("src")
log.setLevel(logging.DEBUG)
mem_handler = logs.setup_log_buffering(log)

# Le logger de uvicorn, pour tricher
main_log = logging.getLogger("uvicorn")

@contextlib.asynccontextmanager
async def lifespan(app: fastapi.FastAPI):
    if testing:
        main_log.info("Pas de lifespan pendant les tests")
        yield ""
        return
    main_log.info("Setup du lifespan de main.app")
    # Setup des bases de données en FAKE
    if config.settings.mode == "FAKE":
        main_log.info("[FAKE] Running in FAKE mode")
        for name in [ "api", "dkim", "imap", "postfix" ]:
            if pathlib.Path(f"/tmp/{name}.db").is_file():
                main_log.debug(f"Unlinking file /tmp/{name}.db")
                pathlib.Path.unlink(f"/tmp/{name}.db")
        config.settings.api_db_url = "sqlite:////tmp/api.db"
        config.settings.imap_db_url = "sqlite:////tmp/imap.db"
        config.settings.dkim_db_url = "sqlite:////tmp/dkim.db"
        config.settings.postfix_db_url = "sqlite:////tmp/postfix.db"
        config.settings.ox_ssh_url = "FAKE"
        config.settings.dk_ssh_url = "FAKE"

    # Connexion aux bases de données
    engine_api = sql_api.init_db(config.settings.api_db_url)
    engine_dovecot = sql_dovecot.init_db(config.settings.imap_db_url)
    engine_postfix = sql_postfix.init_db(config.settings.postfix_db_url)
    engine_dkim = sql_dkim.init_db(config.settings.dkim_db_url)

    # Configuration du module dns
    if dns.get_tech_domain() == "unknown":
        dns.set_tech_domain(config.settings.tech_domain)

    # Création des tables pour le mode FAKE
    if config.settings.mode == "FAKE":
        main_log.info("[FAKE] Creating the tables in the databases")
        sql_api.Api.metadata.create_all(engine_api)
        sql_dovecot.Dovecot.metadata.create_all(engine_dovecot)
        sql_postfix.Postfix.metadata.create_all(engine_postfix)
        sql_dkim.Dkim.metadata.create_all(engine_dkim)

    # Initialisation des modules remote et cbcli
    if config.settings.mode == "FAKE":
        remote.init_remote("FAKE")
        cbcli.init_certbot("FAKE")
    else:
        main_log.info("[REAL] Running in real mode")
        remote.init_remote(config.settings.remote_config)
        cbcli.init_certbot("real")

    if config.settings.JWT_SECRET == "bare secret":
        raise Exception("please configure JWT_SECRET")

    if not isinstance(config.settings.others, dict):
        config.settings.others = json.loads(config.settings.others)
    others.set_others(config.settings.others)
    others.set_myself(config.settings.myself)

    yield ""

    # Here, everything to shut down stuff
    remote.end_remote()
    others.reset_others()


# Le logger de uvicorn, pour tricher
main_log = logging.getLogger("uvicorn")

ver, changelog = "unkown", [ "Empty changelog" ]
try:
    ver, changelog = version.get_version_info()
except Exception as e:
    main_log.error("Failed to find the changelog.")
    main_log.error(f"Exception: {e}")
main_log.info(f"Nous tournons la version {ver}")


app = fastapi.FastAPI(
    responses={
        401: {"description": "Not authorized"},
        404: {"description": "Not found"},
    },
    title="Dimail API",
    description="Dimail API to manage mailboxes",
    version=ver,
    lifespan=lifespan,
)

app.include_router(routes.routers.users)
app.include_router(routes.routers.domains)
app.include_router(routes.routers.allows)
app.include_router(routes.routers.token)
app.include_router(routes.routers.mailboxes)
app.include_router(routes.routers.aliases)
app.include_router(routes.routers.system)
app.include_router(routes.routers.logs)

# Le middleware pour CORS
main_log.info("Adding CORS middleware")
app.add_middleware(
    fastapi.middleware.cors.CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

###
### Le middleware pour intercepter les logs des crash
### 


main_log.info("Adding the crash capture middleware")
def dump_crash_info(request = None, exc = None, response = None):
    (file_handler, file_name) = logs.make_handler_for_crash()
    # On log sur le fichier quelques infos sur l'appel
    file_handler.info("Got a crash")
    if request is not None:
        file_handler.info(f"During request {request.method} {request.url.path}\n\n")

    file_handler.debug("[BEGIN] captured logs during request")
    # On reprend les logs stockés en mémoire pendant le traitement
    mem_handler.setTarget(file_handler)
    mem_handler.flush()
    mem_handler.setNullTarget()

    file_handler.debug("[END] captured logs during request")
    # Si l'appel s'est terminé sur une exception, la stack trace
    if exc is not None:
        file_handler.info(f"Exception occured: {exc}\n\n", exc)
    return file_name

import uuid

# C'est un middleware : ça intervient avant et après la requête.
@app.middleware("http")
async def crash_capture(request, call_next):
    # Avant la requête...
    # On vide le buffer de logs
    mem_handler.flush()
    force = False
    reqId = uuid.uuid4()

    if "log_request" in request.query_params:
        if request.query_params["log_request"] == "force":
            force = True
            log.info("You did set log_request = force")
    # Au début, on trace l'appel et ses paramètres
    await logs.log_request(log, request)

    # On fait la vraie requête
    response = "coincoin"
    try:
        response = await call_next(request)
    except Exception as e:
        print(f"Il y a eu une exception pendant le traitement de la requête. e = {e}")
        file_name = dump_crash_info(request, e)
        print(f"Les logs sont conservés dans {file_name}")
        main_log.error(f"Il y a eu une exception, les logs sont dans {file_name}")

        return starlette.responses.JSONResponse(
            {"detail": "Internal server error"}, status_code=500
        )

    log.debug("Request ended")
    await logs.log_response(log, response)
    if response.status_code == 500:
        log.error("We got a status_code of 500 at end of request")
        file_name = dump_crash_info(request, None, response)
        main_log.error(f"La requête se termine sur un 'Internal server error', les logs sont dans {file_name}")

    if force:
        log.info("You forced to log the request (log_request=force).")
        file_name = dump_crash_info(request, None, response)
        main_log.info(f"Tu as forcé les logs (log_request=force), les logs sont dans {file_name}")
    # A la fin aussi, on vide le buffer de logs
    mem_handler.flush()
    return response



