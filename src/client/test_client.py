import datetime
import pytest
import time

from .client import ApiClient

def test_client_init(log):
    # Le nom de domaine n'existe pas
    with pytest.raises(Exception) as e:
        client = ApiClient(
            name = "Jean-Pierre",
            url = "https://api.jean-pierre.dimail1.numerique.gouv.fr",
        )
        _ = client.get_tech_domain()
    assert "not known" in str(e.value)

    # Pas de creds
    with pytest.raises(Exception) as e:
        client = ApiClient(
            name = "ovhdev",
            url = "https://api.ovhdev.dimail1.numerique.gouv.fr",
        )
        _ = client.get_tech_domain()
    assert "Poorly configured" in str(e.value)

    # Creds invalides
    with pytest.raises(Exception) as e:
        client = ApiClient(
            name = "ovhdev",
            url = "https://pasmoi:coincoin@api.ovhdev.dimail1.numerique.gouv.fr",
        )
        _ = client.get_tech_domain()
    assert "Poorly" in str(e.value)
    assert "403" in str(e.value)
    assert "Forbidden" in str(e.value)

    # Creds valide, sur une vieille version de l'API (v0.1.1)
    client = ApiClient(
        name = "local",
        url = "http://admin:coincoin@localhost:8001",
    )
    _ = client.get_tech_domain()
    assert client is not None


def test_others_version(log):
    client1 = ApiClient(
        name = "local",
        url = "http://admin:coincoin@localhost:8001",
    )
    assert client1 is not None
    assert client1.version_after("0.1.2") is False
    assert client1.version_after("0.1.1") is False
    assert client1.version_after("0.1.0") is True

    tech = client1.tech_domain()
    assert tech == "TooOld"

    client1.set_tech_domain("toto.gouv.fr")
    tech = client1.tech_domain()
    assert tech == "toto.gouv.fr"

    client2 = ApiClient(
        name = "recent",
        url = "http://admin:coincoin@localhost:8002",
    )
    assert client2 is not None
    assert client2.version_after("0.2.10") is True
    assert client2.version_after("0.2.11") is False

    tech = client2.tech_domain()
    assert tech == "new1.nox.numerique.gouv.fr"

    # Sans effet, puisque l'API existe
    client2.set_tech_domain("toto.gouv.fr")
    tech = client2.tech_domain()
    assert tech == "new1.nox.numerique.gouv.fr"

def test_client(log):
    client =  ApiClient(
        name = "recent",
        url = "http://admin:coincoin@localhost:8002",
        reset = True
    )

    token = client.get_token()
    assert token is not None
    client.token_expire = client.token_expire - 90*60
    token2 = client.get_token()
    assert token2 is not None
    # Pour le moment, on retrouve le même token... C'est étrange.
    # assert not token == token2

    response = client.get_domain("example.com")
    assert response is None

    response = client.post_domain(
        domain_name = "example.com",
        delivery = "virtual",
        features = ["mailbox", "webmail"],
        context_name = "dimail",
    )
    assert response is not None
    assert response["delivery"] == "virtual"

    response = client.patch_domain(
        domain_name = "example.com",
        updates = {
            "features": ["mailbox"],
        }
    )
    assert response is not None
    assert response["features"] == [ "mailbox" ]

    name = client.post_database("ox_test_database", "coincoin_42")
    assert name is not None
    assert name.startswith("ox_test_database_")

def test_mailqueue(log):
    client_old = ApiClient(
        name = "old",
        url = "http://admin:coincoin@localhost:8001",
    )
    with pytest.raises(Exception) as e:
        response = client_old.count_mail_in_queue("example.com", "toto")
    assert "too old" in str(e.value)

    with pytest.raises(Exception) as e:
        response = client_old.disconnect("example.com", "toto")
    assert "too old" in str(e.value)

    client_new = ApiClient(
        name = "recent",
        url = "http://admin:coincoin@localhost:8002",
        reset = True,
    )
    count = client_new.count_mail_in_queue("example.com", "toto")
    assert count == 0
    count = client_new.count_mail_in_queue("example.com", "many")
    assert count == 2

    client_new.disconnect("example.com", "toto")

