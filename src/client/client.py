import datetime
import fastapi
import httpx
import logging
import packaging.version
import time
import urllib.parse

class ApiClient:
    def __init__(self, name: str, url: str, reset: bool = False):
        self.name = name
        log = logging.getLogger(__name__)
        elems = urllib.parse.urlparse(url)
        log.info(f"Decoupage url : {elems}")
        host = f"{elems.netloc}"
        if elems.hostname is not None:
            host = f"{elems.hostname}"
            if elems.port is not None:
                host = host + f":{elems.port}"
        self.url = f"{elems.scheme}://{host}/"

        if elems.username is not None:
            self.login = elems.username
        else:
            self.login = "admin"
        if elems.password is not None:
            self.password = elems.password
        else:
            self.password = "admin"

        self._client = None
        self._async_client = None
        self._reset = reset

    def shutdown(self):
        log = logging.getLogger(__name__)
        auth = (self.login, self.password)
        log.debug(f"Shutdown on {self.name}")
        response = self.client.get(
            "/system/shutdown",
            auth = auth,
        )
        if not response.status_code == fastapi.status.HTTP_200_OK:
            log.error(f"Response to shutdown: {response}")
            raise Exception(f"Failed to shutdown {self.name}")
        count = 0
        while True:
            count = count + 1
            time.sleep(1)
            try:
                response = self.client.get(
                    "/system/version",
                    auth = auth,
                )
                log.debug(f"Uvicorn est de retour. count = {count}")
                return
            except:
                log.debug(f"Uvicorn n'est pas encore prêt. On attend. count = {count}")
            if count > 30:
                raise Exception("Plus de 30 secondes pour relancer uvicorn, ce n'est pas normal")

    ###
    ### Gestion du client
    ###

    @property
    def client(self):
        if self._client is not None:
            return self._client

        log = logging.getLogger(__name__)
        self._client = httpx.Client(base_url=self.url, timeout=3600)
        try:
            log.info(f"Setup du client avec l'url {self.url}")
            if self._reset:
                if not self.is_empty():
                    self.shutdown()
                self._is_empty = True
            self.read_version()
            self.get_tech_domain()
            if self.is_empty():
                log.info(f"Il faut créer le premier admin sur {self.name}")
                self.post_user(self.login, self.password, True, [])
                self._is_empty = False
        except Exception as e:
            self._client = None
            if hasattr(self, '_is_empty'):
                delattr(self, '_is_empty')
            log.error(f"Failed to connect to {self.name} : {e}")
            raise

        return self._client

    @property
    def async_client(self):
        if self._async_client is not None:
            return self._async_client

        _ = self.client
        self._async_client = httpx.AsyncClient(
            base_url = self.url,
            timeout = 3600,
            follow_redirects = True,
        )
        return self._async_client

    ###
    ### Version management
    ###

    @property
    def version(self):
        if not hasattr(self, "is_dev"):
            self.read_version()
        if not self.is_dev:
            return self._version
        return f"Dev on {self.branch} at {self._version}"

    def get_version(self):
        auth = (self.login, self.password)
        response = self.client.get(
            "/system/version",
            auth = auth,
        )
        if response.status_code == 404:
            # Maybe it is too old...
            response = self.client.get(
                "/version/",
                auth = auth,
            )
        if not response.status_code == 200:
            raise Exception(f"Poorly configured API client {response}")
        json = response.json()
        return json

    def read_version(self):
        version = self.get_version()
        cur_ver = version["version"]
        self._version = cur_ver
        self.is_dev = False
        self.branch = "main"
        if cur_ver.startswith("Dev on"):
            self.is_dev = True
            cur_ver = cur_ver[len("Dev on "):]
            branch, at, cur_ver = cur_ver.split(" ")
            self.branch = branch
            if "-" in cur_ver:
                cur_ver, other = cur_ver.split("-", 1)
                self._version = cur_ver
            else:
                self._version = cur_ver

    def at_least_version(self, ver: str):
        if not hasattr(self, '_version'):
            self.read_version()
        ver = packaging.version.Version(ver)
        cur_ver = packaging.version.Version(self._version)
        if cur_ver >= ver:
            return True
        return False

    def version_after(self, ver: str):
        if not hasattr(self, "_version"):
            self.read_version()
        ver = packaging.version.Version(ver)
        cur_ver = packaging.version.Version(self._version)
        if cur_ver > ver:
            return True
        if cur_ver == ver:
            if self.is_dev:
                return True
        return False

    ###
    ### Minimal users management
    ###

    def is_empty(self):
        if hasattr(self, "_is_empty"):
            return self._is_empty
        auth = (self.login, self.password)
        response = self.client.get("/users/", auth=auth)
        if not response.status_code == 200:
            raise Exception("Failed to list users")
        json = response.json()
        if len(json) > 0:
            self._is_empty = False
        else:
            self._is_empty = True
        return self._is_empty

    def post_user(self,
        login: str,
        password: str,
        is_admin: bool,
        perms: list[str],
    ):
        auth = (self.login, self.password)
        response = self.client.post("/users/",
            json = {
                "name": login,
                "password": password,
                "is_admin": is_admin,
                "perms": perms,
            },
            auth = auth,
        )
        if not response.status_code == 201:
            raise Exception("Failed to create a user")

    ###
    ### Tech domain
    ###

    def get_tech_domain(self):
        log = logging.getLogger(__name__)
        log.debug(f"Version pour {self.name} : {self.version}")
        if not self.version_after("v0.1.1"):
            self._tech_domain = "TooOld"
            return self._tech_domain
        response = None
        auth = (self.login, self.password)
        if self.at_least_version("v0.1.6"):
            response = self.client.get(
                "/system/tech_domain",
                auth = auth,
            )
        else:
            response = self.client.get(
                "/tech_domain/",
                auth = auth,
            )
        if not response.status_code == fastapi.status.HTTP_200_OK:
            raise Exception("Failed to get tech domain")
        json = response.json()
        self._tech_domain = json["tech_name"]
        return self._tech_domain

    def set_tech_domain(self, name: str):
        if self._tech_domain == "TooOld":
            self._tech_domain = name

    def tech_domain(self):
        if not hasattr(self, "_tech_domain"):
            return self.get_tech_domain()
        return self._tech_domain

    ###
    ### Token
    ###

    def get_token(self):
        log = logging.getLogger(__name__)
        now = datetime.datetime.now(datetime.timezone.utc).timestamp()
        if hasattr(self, "token"):
            if self.token_expire > now:
                log.debug(f"Reusing token for {self.name} now = {now}, expire = {self.token_expire}")
                return self.token
            log.debug(f"Token expired for {self.name}, getting a new one")

        auth = (self.login, self.password)
        response = self.client.get(
            "/token/",
            auth = auth
        )
        if not response.status_code == fastapi.status.HTTP_200_OK:
            raise Exception("Failed to get a token")
        self.token = response.json()["access_token"]
        self.token_date = now
        self.token_expire = now + 30*60

        log.debug(f"Got token for {self.name}")
        return self.token

    ###
    ### Domain management
    ###

    def post_domain(self,
        domain_name: str,
        delivery: str,
        features: list[str],
        transport: str | None = None,
        context_id: str | None = None,
        context_name: str | None = None,
        smtp_domain: str = "",
        imap_domain: str = "",
        webmail_domain: str = "",
        database_name: str | None = None,
        schema_name: str | None = None,
    ) -> dict:
        log = logging.getLogger(__name__)
        auth = (self.login, self.password)
        response = self.client.post(
            f"/domains/",
            json = {
                "name": domain_name,
                "delivery": delivery,
                "features": features,
                "transport": transport,
                "context_id": context_id,
                "context_name": context_name,
                "smtp_domain": smtp_domain,
                "imap_domain": imap_domain,
                "webmail_domain": webmail_domain,
                "database_name": database_name,
                "schema_name": schema_name,
            },
            #params = {
            #    "log_request": "force",
            #},
            auth = auth,
        )
        if not response.status_code == fastapi.status.HTTP_201_CREATED:
            raise Exception(f"Failed to post domain {domain_name} on {self.name} : response = {response}, detail = {response.json()}")
        json = response.json()
        return json

    def get_domain(self, domain_name: str) -> dict:
        log = logging.getLogger(__name__)
        token = self.get_token()
        response = self.client.get(
            f"/domains/{domain_name}",
            headers = { "Authorization": f"Bearer {token}", },
        )
        if response.status_code == fastapi.status.HTTP_404_NOT_FOUND:
            log.debug(f"Domain {domain_name} not found on {self.name}")
            return None
        if not response.status_code == fastapi.status.HTTP_200_OK:
            raise Exception(f"Got on problem getting domain {domain_name} from {self.name} : response = {response}, detail = {response.json()}")
        json = response.json()
        return json

    def patch_domain(self, domain_name: str, updates: dict[str]) -> dict:
        log = logging.getLogger(__name__)
        auth = (self.login, self.password)
        response = self.client.patch(
            f"/domains/{domain_name}",
            json = updates,
            auth = auth,
        )
        if not response.status_code == fastapi.status.HTTP_200_OK:
            raise Exception(f"Failed to patch domain {domain_name} on {self.name}")
        json = response.json()
        return json

    async def import_domain(self,
        domain_name: str,
        moving_from: str,
        moving_mailboxes: list[dict[str]],
        moving_aliases: list[dict[str]],
        moving_senders: list[dict[str]],
        moving_dkim: dict[str] | None,
    ):
        log = logging.getLogger(__name__)
        auth = (self.login, self.password)
        response = await self.async_client.patch(
            f"/system/import_domain/{domain_name}",
            json = {
                "moving_from": moving_from,
                "moving_mailboxes": moving_mailboxes,
                "moving_aliases": moving_aliases,
                "moving_senders": moving_senders,
                "moving_dkim": moving_dkim,
            },
            auth = auth,
        )
        json = response.json()
        if not response.status_code == fastapi.status.HTTP_200_OK:
            log.error(f"On import domain, expected 200 OK, got {response}")
            log.error(f"Detail: {json}")
            raise Exception(f"Failed to import domain {domain_name} on {self.name}")
        return json

    def check_contexts(self,
        contexts: list[dict[str, str]],
    ) -> dict:
        log = logging.getLogger(__name__)
        if not self.at_least_version("v0.1.9"):
            raise Exception(f"The api for '{self.name}' is too old for OX check contexts")
        auth = (self.login, self.password)
        log.info(f"Les contexts: {contexts}")
        response = self.client.post(
            "/system/check_contexts",
            json = {
                "contexts": contexts,
            },
            auth = auth,
        )
        if not response.status_code == fastapi.status.HTTP_200_OK:
            log.error(f"On check contexts, expected 200 OK, got {response}")
            raise Exception(f"Failed to check contexts on {self.name}")
        json = response.json()
        return json

    ###
    ### OX Database management
    ###

    def post_database(self, db_name: str, schema_name: str) -> str:
        log = logging.getLogger(__name__)
        if not self.at_least_version("v0.1.8"):
            raise Exception(f"The api for '{self.name}' is too old for OX database management")
        auth = (self.login, self.password)
        response = self.client.post(
            f"/system/database/{db_name}/{schema_name}",
            auth = auth,
        )
        if not response.status_code == fastapi.status.HTTP_200_OK:
            raise Exception(f"Failed to post a new schema {schema_name} in OX database {db_name} on {self.name}")
        json = response.json()
        return json["name"]

    ###
    ### Queue management
    ###

    def count_mail_in_queue(self, domain_name: str, user_name: str) -> int:
        log = logging.getLogger(__name__)
        if not self.at_least_version("v0.2.6"):
            raise Exception(f"The api for '{self.name}' is too old for queue management")
        auth = (self.login, self.password)
        response = self.client.get(
            f"/system/mailqueue/{domain_name}/{user_name}",
            auth = auth,
        )
        if not response.status_code == fastapi.status.HTTP_200_OK:
            raise Exception(f"Failed to get queue for {user_name}@{domain_name}")
        json = response.json()
        return len(json)

    def disconnect(self, domain_name: str, user_name: str) -> None:
        log = logging.getLogger(__name__)
        if not self.at_least_version("v0.2.6"):
            raise Exception(f"The api for '{self.name}' is too old for user disconnecting")
        auth = (self.login, self.password)
        response = self.client.get(
            f"/system/disconnect/{domain_name}/{user_name}",
            auth = auth,
        )
        if not response.status_code == fastapi.status.HTTP_200_OK:
            raise Exception(f"Failed to disconnect user {user_name}@{domain_name}")
        return None
