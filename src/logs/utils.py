import datetime
import json
import logging
import os, os.path

import fastapi
import uvicorn.logging

from .. import config

from .handlers import MyMemHandler, MyFileHandler

def setup_log_buffering(log):
    if len(log.handlers) > 0 and config.settings.log != "full":
        default_handler = log.handlers[0]
        # On retire celui par défaut
        log.removeHandler(default_handler)

    # On rajoute le buffering en mémoire
    mem_handler = MyMemHandler()
    log.addHandler(mem_handler)

    return mem_handler

crash_log_dir = "/tmp/crash"
crash_log_prefix = "crash.log."
crash_count_file = f"{crash_log_dir}/count"

def set_crash_log_dir(dir_name: str) -> None:
    global crash_log_dir
    crash_log_dir = dir_name
    global crash_count_file
    crash_count_file = f"{crash_log_dir}/count"

formatter = uvicorn.logging.DefaultFormatter(fmt = "%(levelprefix)s %(name)s:%(funcName)s:%(lineno)s %(message)s", use_colors = None)
def make_handler_for_crash():
    # Find the next file name to log the crash
    count = 1
    if not os.path.isdir(crash_log_dir):
        os.mkdir(crash_log_dir)
    if os.path.isfile(crash_count_file):
        with open(crash_count_file, "r") as file:
            count = file.readline()
        count = int(count)
    file_name = f"{crash_log_dir}/{crash_log_prefix}{count}"
    while os.path.isfile(file_name):
        count += 1
        file_name = f"{crash_log_dir}/{crash_log_prefix}{count}"
    with open(crash_count_file, "w") as file:
        file.write(f"{count+1}\n")

    # Setup a logging handler to this file.
    file_handler = MyFileHandler(file_name, mode="w") # w : création et tronqué
    file_handler.setFormatter(formatter)

    return (file_handler, file_name)

def list_crash_files():
    log = logging.getLogger(__name__)
    if not os.path.isdir(crash_log_dir):
        log.debug(f"Directory {crash_log_dir} does not exist yet. No crashes so far.")
        return []

    result = []
    for file_name in os.listdir(crash_log_dir):
        if file_name == "." or file_name == ".." or file_name == "count":
            continue
        if not file_name.startswith(crash_log_prefix):
            log.warning(f"Weird file {file_name} found in {crash_log_dir}. Who did that ?")
            continue
        index = int(file_name[len(crash_log_prefix):])
        file_info = {}
        file_info["index"] = index
        file_info["name"] = file_name
        stat = os.stat(crash_log_dir + "/" + file_name)
        file_info["size"] = stat.st_size
        date = datetime.datetime.fromtimestamp(stat.st_mtime)
        file_info["date"] = str(date)
        result.append(file_info)
    log.debug(f"Found {len(result)} log files in {crash_log_dir}.")
    return sorted(result, key = lambda x: x["index"])

def get_crash_file(file_name):
    log = logging.getLogger(__name__)
    if "/" in file_name:
        log.info(f"Attempt to inject / in file name '{file_name}'. Fuck off.")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if not file_name.startswith(crash_log_prefix):
        log.info(f"Attempt to access something that is not a crash log (file name = '{file_name}'). Fuck off.")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if not os.path.isdir(crash_log_dir):
        log.debug(f"Crash log directory {crash_log_dir} does not exist yet, so there are no crash logs on the server.")
        raise fastapi.HTTPException(status_code=404, detail="File not found")
    if not os.path.isfile(crash_log_dir + "/" + file_name):
        log.debug(f"The crash log {file_name} does not exist. Probably deleted, by you or during an upgrade.")
        raise fastapi.HTTPException(status_code=404, detail="File not found")

    res = ""
    with open(crash_log_dir + "/" + file_name, "r") as file:
        res = file.read()
    res = res.split("\n")
    log.info(f"Found the log file {file_name}, there are {len(res)} lines in there.")
    return res

def del_crash_file(file_name):
    log = logging.getLogger(__name__)
    if "/" in file_name:
        log.info(f"Attempt to inject / in file name '{file_name}'. Fuck off.")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if not file_name.startswith(crash_log_prefix):
        log.info(f"Attempt to access something that is not a crash log (file name = '{file_name}'). Fuck off.")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if not os.path.isdir(crash_log_dir):
        log.debug(f"Crash log directory {crash_log_dir} does not exist yet, so there are no crash logs on the server.")
        raise fastapi.HTTPException(status_code=404, detail="File not found")
    if not os.path.isfile(crash_log_dir + "/" + file_name):
        log.debug(f"The crash log {file_name} does not exist. Probably deleted, by you or during an upgrade.")
        raise fastapi.HTTPException(status_code=404, detail="File not found")

    os.unlink(crash_log_dir + "/" + file_name)

async def log_request(log, request):
    log.debug(f"Request: {request.method} {request.url.path}")
    log.debug(f"Path params: {request.path_params}")
    log.debug(f"Query params: {request.query_params}")
    if request.method == 'POST' or request.method == 'PATCH':
        # Je crois que je consomme la requête et que ça fout le bordel.
        # Pourtant, ça semble marcher.
        if "content-type" in request.headers and request.headers["content-type"] == "application/json":
            try:
                body = await request.json()
                body = json.dumps(body, indent=4)
                log.debug(f"Body: \n{body}")
            except Exception as e:
                log.error(f"Request is supposed to be in json. Failed to parse it with exception {e}. Will log request raw, instead of json")
                body = await request.body()
                log.debug(f"Body (raw): \n{body}")
        else:
            body = await request.body()
            log.debug(f"Body (raw): \n{body}")


async def log_response(log, response):

    log.debug(f"Response.status_code = {response.status_code}")

    # Je vais consommer la réponse, pour la logger. Alors je vais la stocker...
    parts = []
    async for part in response.body_iterator:
        parts.append(part)
    # ...et renvoyer ce que j'ai stocké
    async def the_body():
        for part in parts:
            yield part
    response.body_iterator = the_body()

    # Maintenant je peux reconstituer le body
    body = b""
    for part in parts:
        body += part

    # Et faire un peut de pretty-print du json
    if "content-type" in response.headers:
        if response.headers["content-type"] == "application/json":
            if len(body) > 1:
                body = json.dumps(json.loads(body), indent=4)
            log.debug(f"Response.body = {body}")
        else:
            log.debug(f"Response content-type: {response.headers['content-type']}")
    log.debug(f"Raw response.body: {body}")

