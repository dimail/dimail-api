import logging, logging.config, logging.handlers
import inspect


class MyMemHandler(logging.handlers.MemoryHandler):
    def __init__(self):
        self.my_null_handler = logging.NullHandler()
        self.handlers = {}
        return super().__init__(capacity=100000, target=self.my_null_handler, flushOnClose=False)

    def req_id(self):
        stack = inspect.stack()
        for info in stack:
            if info.function == "crash_capture":
                args = inspect.getargvalues(info.frame)
                if "reqId" in args.locals:
                    return args.locals["reqId"]
        return None

    def emit(self, msg, *args, **kwargs):
        req_id = self.req_id()
        if req_id is not None:
            if req_id not in self.handlers:
                self.handlers[req_id] = logging.handlers.MemoryHandler(capacity=100000, target=self.my_null_handler, flushOnClose=False)
                self.handlers[req_id].emit(logging.LogRecord(
                    name = "",
                    level = logging.DEBUG,
                    pathname = "",
                    lineno = "",
                    args="",
                    exc_info=None,
                    msg = f"Here are the logs for request id={req_id}",
                ))
            return self.handlers[req_id].emit(msg, *args, **kwargs)

        return super(MyMemHandler, self).emit(msg, *args, **kwargs)

    def flush(self, *args, **kwargs):
        req_id = self.req_id()
        if req_id is not None and req_id in self.handlers:
            tmp = self.handlers.pop(req_id)
            return tmp.flush(*args, **kwargs)
        else:
            return super(MyMemHandler, self).flush(*args, **kwargs)

    def setTarget(self, *args, **kwargs):
        req_id = self.req_id()
        if req_id is not None and req_id in self.handlers:
            return self.handlers[req_id].setTarget(*args, **kwargs)
        return super(MyMemHandler, self).setTarget(*args, **kwargs)

    def __repr__(self):
        return "<MyMemHandler at "+hex(id(self))+">"
    def shouldFlush(self, record):
        return False

    def setNullTarget(self):
        self.setTarget(self.my_null_handler)

class MyFileHandler(logging.FileHandler):
    def __init__(self, file_name: str, mode: str):
        return super().__init__(file_name, mode)

    def emit_record(self, level, msg, exc = None):
        exc_info = None
        if exc is not None:
            exc_info = (type(exc), exc, exc.__traceback__)
        self.emit(logging.LogRecord(
            name="",
            level=level,
            pathname="",
            lineno="",
            msg=msg,
            args="",
            exc_info=exc_info,
        ))

    def debug(self, msg, exc = None):
        self.emit_record(logging.DEBUG, msg, exc)
    def info(self, msg, exc = None):
        self.emit_record(logging.INFO, msg, exc)
    def warning(self, msg, exc = None):
        self.emit_record(logging.WARNING, msg, exc)
    def error(self, msg, exc = None):
        self.emit_record(logging.ERROR, msg, exc)


