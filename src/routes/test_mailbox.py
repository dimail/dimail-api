import fastapi.testclient
import pytest

from .. import sql_dovecot


# Dans la première serie de tests, on travaille sur un domaine avec le webmail.
@pytest.mark.asyncio
@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "virgin_user",
    ["bidi:toto"],
    indirect=True,
)
# Les domaines tutu.net et target.fr sont dans le CTX dimain
# Le domaine other.org est dans le CTX ctx2
@pytest.mark.parametrize(
    "domain_web",
    ["tutu.net,target.fr,sub.dom.example.com,dom.example.com:dimail;other.org:ctx2"],
    indirect=True,
)
async def test_with_webmail(
        log,
        async_client,
        normal_user,
        virgin_user,
        domain_web,
        db_dovecot_session,
        admin_token,
):
    token = normal_user["token"]
    virgin_token = virgin_user["token"]
    admin_token = admin_token["token"]
    domain_name = domain_web["name"]
    other_domain_name = domain_web["all_domains"][1]

    # On obtient un 404 sur une mailbox qui n'existe pas
    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/address",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # On get toutes les boites du domaine, on doit remonter une liste avec
    # une seule boite, le context admin, qui doit être broken (il n'existe
    # pas en imap)
    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    print(f"{json}")
    log.info(f"{json}")
    assert len(json) == 1
    assert json[0]["email"] == f"oxadmin@{domain_name}"
    assert json[0]["status"] == "broken"
    assert json[0]["active"] == "unknown"

    # Le virgin_user ne peut pas créer de mailbox -> forbidden
    response = await async_client.post(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "surName": "Essai",
            "givenName": "Test",
            "displayName": "Test Essai",
        },
        headers={"Authorization": f"Bearer {virgin_token}"},
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # On crée la mailbox qui n'existait pas à la ligne précédente
    response = await async_client.post(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "surName": "Essai",
            "givenName": "Test",
            "displayName": "Test Essai",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED
    # FIXME On a besoin de rendre prédictible les uuid qu'on génère pendant
    # les tests. On a aussi besoin de définir ce que c'est que l'uuid d'une
    # mailbox, parce que ce n'est pas clair pour l'instant...
    got = response.json()
    assert got["email"] == f"address@{domain_name}"
    # assert got["uuid"] == something smart here :)

    # Check the password is properly encoded in dovecot database
    imap_user = sql_dovecot.get_user(db_dovecot_session, "address", domain_name)
    assert isinstance(imap_user, sql_dovecot.ImapUser)
    assert imap_user.check_password(got["password"])

    # On refait un GET sur la boîte qu'on vient de créer
    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/address",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == {
        "type": "mailbox",
        "status": "ok",
        "active": "yes",
        "email": f"address@{domain_name}",
        "surName": "Essai",
        "givenName": "Test",
        "displayName": "Test Essai",
        "additionalSenders": []
    }

    # Si on demande à la créer une deuxième fois -> 409
    response = await async_client.post(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "surName": "Encore",
            "givenName": "Test2",
            "displayName": "Nouveau nom",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_409_CONFLICT

    # On modifie la mailbox
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "givenName": "AutreTest",
            "surName": "EssaiNouveau",
            "displayName": "Joli nom affichable",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == {
        "type": "mailbox",
        "status": "ok",
        "active": "yes",
        "email": f"address@{domain_name}",
        "surName": "EssaiNouveau",
        "givenName": "AutreTest",
        "displayName": "Joli nom affichable",
        "additionalSenders": [],
    }

    # Lorsque je modifie une mailbox (avec webmail) -> 200 OK
    # Test uniquement sur displayName (les autres champs inchangés)
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "displayName": "Joli nom afficha",
        },
        headers={"Authorization": f"Bearer {token}"},
    )

    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == {
        "type": "mailbox",
        "status": "ok",
        "active": "yes",
        "email": f"address@{domain_name}",
        "surName": "EssaiNouveau",
        "givenName": "AutreTest",
        "displayName": "Joli nom afficha",
        "additionalSenders": [],
    }

    # Lorque je veux modifier le givenName d'une mailbox (avec webmail) -> 200 OK
    # On ne change que le givenName
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "givenName": "Autre",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == {
        "type": "mailbox",
        "status": "ok",
        "active": "yes",
        "email": f"address@{domain_name}",
        "surName": "EssaiNouveau",
        "givenName": "Autre",
        "displayName": "Joli nom afficha",
        "additionalSenders": [],
    }

    # Même chose avec le surName seul -> 200 OK
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "surName": "Le sur name",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == {
        "type": "mailbox",
        "status": "ok",
        "active": "yes",
        "email": f"address@{domain_name}",
        "surName": "Le sur name",
        "givenName": "Autre",
        "displayName": "Joli nom afficha",
        "additionalSenders": [],
    }

    # Si j'essaye de mettre en wait une mailbox, je me fais jeter (il faut être admin)
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json = {
            "active": "wait",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # Si je suspend une boite mail, elle sort d'OX
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "active": "no",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == {
        "type": "mailbox",
        "status": "ok",
        "active": "no",
        "email": f"address@{domain_name}",
        "surName": None,
        "givenName": None,
        "displayName": None,
        "additionalSenders": [],
    }

    # Si je ré-active une boite mail, elle revient dans OX
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "active": "yes",
            "surName": "Le sur name",
            "givenName": "Autre",
            "displayName": "Joli nom afficha",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == {
        "type": "mailbox",
        "status": "ok",
        "active": "yes",
        "email": f"address@{domain_name}",
        "surName": "Le sur name",
        "givenName": "Autre",
        "displayName": "Joli nom afficha",
        "additionalSenders": [],
    }

    # Si on essaye de modifier l'adresse mail -> Internal server error
    # (No yet implemented)
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "user_name": "jean.dupont",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY

    # Si on patch sur un domaine qui n'existe pas -> not found
    # Il faut être admin pour pouvoir essayer de toucher un domaine qui
    # n'existe pas, les gens normaux auront un permisison denied.
    response = await async_client.patch(
        "/domains/pas-un-domaine/mailboxes/hop",
        json={},
        headers={"Authorization": f"Bearer {admin_token}"},
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # Si on patch sur un domaine où on n'a pas les droits (qui n'existe pas,
    # dans notre cas) -> forbidden
    response = await async_client.patch(
        "/domains/pas-un-domaine/mailboxes/hop",
        json={},
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # Si on patch sur un domaine qui existe et où on a les droits
    # -> 422 (not yet implemented)
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "domain": other_domain_name,
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY

    # Si on patch sur une mailbox qui n'existe pas -> not found
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/pas-une-boite",
        json={},
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # Si on veut modifier le domaine pour un domaine qui n'existe pas -> not found
    # Il faut être admin pour pouvoir essayer de toucher un domaine qui
    # n'existe pas, les gens normaux auront un permission denied.
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/pas-une-boite",
        json={
            "domain": "pas-un-domaine",
        },
        headers={"Authorization": f"Bearer {admin_token}"},
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # Si on veut modifier le domaine pour un domaine où on n'a pas les
    # droits (ici, un domaine qui n'existe pas) -> forbidden
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/pas-une-boite",
        json={
            "domain": "pas-un-domaine",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN


    # Le virgin_user ne peut pas GET de mailbox -> forbidden
    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes",
        headers={"Authorization": f"Bearer {virgin_token}"},
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # On get toutes les boites du domaine, on doit remonter notre
    # boite neuve
    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert len(json) == 2
    assert json[0]["email"] == f"address@{domain_name}"
    assert json[0]["status"] == "ok"
    assert json[0]["active"] == "yes"
    assert json[1]["email"] == f"oxadmin@{domain_name}"
    assert json[1]["status"] == "broken"
    assert json[1]["active"] == "unknown" # La boite n'est pas dans dovecot

    # On supprime une mailbox qui n'existe pas -> not found
    response = await async_client.delete(
        f"/domains/{domain_name}/mailboxes/pas-une-boite",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # On supprime une mailbox sur un domaine qui n'existe pas (donc,
    # je n'ai pas les droits) -> forbidden
    response = await async_client.delete(
        "/domains/pas-un-domaine/mailboxes/coincoin",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # On supprime une mailbox sur un domaine qui n'existe pas (mais
    # on a les droits, parce qu'on est admin) -> not found
    response = await async_client.delete(
        "/domains/pas-un-domaine/mailboxes/coincoin",
        headers={"Authorization": f"Bearer {admin_token}"},
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # On supprime une mailbox qui existe, et on a les droits
    # -> 204 not content
    response = await async_client.delete(
        f"/domains/{domain_name}/mailboxes/address",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_204_NO_CONTENT


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "virgin_user",
    ["bidi:toto"],
    indirect=True,
)       
# Les domaines tutu.net et target.fr sont dans le CTX dimain
# Le domaine other.org est dans le CTX ctx2
@pytest.mark.parametrize(
    "domain_web",
    ["tutu.net,sub.dom.example.com,dom.example.com,sub.dom:dimail"],
    indirect=True,
)   
async def test_domain_separation(
        log,
        async_client,
        normal_user,
        virgin_user,
        domain_web,
        db_dovecot_session,
        admin_token
):
    token = normal_user["token"]
#    virgin_token = virgin_user["token"]
#    admin_token = admin_token["token"]
#    domain_name = domain_web["name"]
#    other_domain_name = domain_web["all_domains"][1]
    
    # Dans le domaine sub.dom.example.com, je crée une boite mail
    response = await async_client.post(
        f"/domains/sub.dom.example.com/mailboxes/essai1",
        json={
            "surName": "Essai",
            "givenName": "Un",
            "displayName": "Essai Un",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Je liste les boites de tutu.net, je m'attend à trouver oxadmin
    response = await async_client.get(
        "/domains/tutu.net/mailboxes/",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    emails = [ x["email"] for x in json ]
    assert len(json) == 1
    assert emails == [ "oxadmin@tutu.net" ]

    # Je liste les boites de sub.dom.example.com, je veux voir ma boite
    response = await async_client.get(
        "/domains/sub.dom.example.com/mailboxes/",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    emails = [ x["email"] for x in json ]
    assert len(json) == 1
    assert emails == [ "essai1@sub.dom.example.com" ]

    # Je liste les boites de dom.example.com, je ne veux pas la voir.
    response = await async_client.get(
        "/domains/dom.example.com/mailboxes/",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    emails = [ x["email"] for x in json ]
    assert emails == []
    assert len(json) == 0

    # Je liste les boites de sub.dom, je ne veux pas la voir.
    response = await async_client.get(
        "/domains/sub.dom/mailboxes/",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    emails = [ x["email"] for x in json ]
    assert emails == []
    assert len(json) == 0




@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "virgin_user",
    ["bidi:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "domain_mail",
    ["tutu.org,target.fr"],
    indirect=True,
)
def test_without_webmail(log, client, normal_user, virgin_user, domain_mail, db_dovecot_session):
    token = normal_user["token"]
    virgin_token = virgin_user["token"]
    domain_name = domain_mail["name"]

    # Le virgin_user ne peut pas GET de mailbox -> forbidden
    response = client.get(
        f"/domains/{domain_name}/mailboxes/address",
        headers={"Authorization": f"Bearer {virgin_token}"},
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # On obtient un 404 sur une mailbox qui n'existe pas
    response = client.get(
        f"/domains/{domain_name}/mailboxes/address",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # On get toutes les boites du domaine, on doit remonter une liste vide
    # (pas de webmail, donc pas de boite oxadmin, puisque pas de ox)
    response = client.get(
        f"/domains/{domain_name}/mailboxes/",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert len(json) == 0

    # On crée la mailbox qui n'existait pas à la ligne précédente
    response = client.post(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "givenName": "",
            "surName": "",
            "displayName": "",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED
    got = response.json()
    assert got["email"] == f"address@{domain_name}"

    # Check the password is properly encoded in dovecot database
    imap_user = sql_dovecot.get_user(db_dovecot_session, "address", domain_name)

    assert isinstance(imap_user, sql_dovecot.ImapUser)
    assert imap_user.check_password(got["password"])

    # Reset the password of the mailbox
    response = client.post(
        f"/domains/{domain_name}/mailboxes/address/reset_password",
        json={},
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    got = response.json()
    assert got["email"] == f"address@{domain_name}"
    
    # Check the password is properly encoded in dovecot database
    # La session est une transaction, elle remontera toujours le même user, même
    # si elle refait un select. Il faut terminer la transaction.
    db_dovecot_session.rollback()
    imap_user = sql_dovecot.get_user(db_dovecot_session, "address", domain_name)
    assert isinstance(imap_user, sql_dovecot.ImapUser)
    assert imap_user.check_password(got["password"])

    # On refait un GET sur la boîte qu'on vient de créer
    response = client.get(
        f"/domains/{domain_name}/mailboxes/address",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == {
        "type": "mailbox",
        "status": "ok",
        "active": "yes",
        "email": f"address@{domain_name}",
        "surName": None,
        "givenName": None,
        "displayName": None,
        "additionalSenders": [],
    }

    # On get toutes les boites du domaine, on doit remonter notre
    # boite neuve
    response = client.get(
        f"/domains/{domain_name}/mailboxes/",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert len(json) == 1
    assert json[0]["email"] == f"address@{domain_name}"

    # On modifie la mailbox -> OK
    # Rien ne sera modifié (le givenName, surName et displayName sont ignorés)
    response = client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "givenName": "newGivenName",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == {
        "type": "mailbox",
        "status": "ok",
        "active": "yes",
        "email": f"address@{domain_name}",
        "surName": None,
        "givenName": None,
        "displayName": None,
        "additionalSenders": [],
    }

    # On suspend la mailbox
    response =  client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "active": "no",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == {
        "type": "mailbox",
        "status": "ok",
        "active": "no",
        "email": f"address@{domain_name}",
        "surName": None,
        "givenName": None,
        "displayName": None,
        "additionalSenders": [],
    }

    # On reactive la mailbox
    response =  client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "active": "yes",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == {
        "type": "mailbox",
        "status": "ok",
        "active": "yes",
        "email": f"address@{domain_name}",
        "surName": None,
        "givenName": None,
        "displayName": None,
        "additionalSenders": [],
    }


    # On supprime la mailbox -> no content
    response = client.delete(
        f"/domains/{domain_name}/mailboxes/address",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_204_NO_CONTENT


@pytest.mark.parametrize(
    "domain",
    ["tutu.org"],
    indirect=True,
)
def test_without_mail(client, admin_token, domain, db_dovecot_session):
    # Le domaine de la fixture 'domain' n'a pas de feature, et n'a
    # aucun user ajouté dans ses allows. Pour essayer de faire des
    # modifs dessus, le plus simple est d'être admin.
    admin_token = admin_token["token"]
    domain_name = domain["name"]

    # Si on demande a créer une mailbox, on va recevoir une erreur
    response = client.post(
        f"/domains/{domain_name}/mailboxes/address",
        json={},
        headers={"Authorization": f"Bearer {admin_token}"},
    )   
    assert response.status_code == fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY

    # Si on demande la liste des mailbox...
    response = client.get(
        f"/domains/{domain_name}/mailboxes",
        headers={"Authorization": f"Bearer {admin_token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    tmp = response.json()
    assert tmp == []

    # Si on demande a modifier une mailbox sur un domaine qui n'a pas
    # la feature 'mailbox', on va recevoir une erreur parce que le
    # domaine n'a pas la feature. -> 422
    response = client.patch(
        f"/domains/{domain_name}/mailboxes/toto",
        json={},
        headers={"Authorization": f"Bearer {admin_token}"},
    )
    assert response.status_code == fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY

    # Si on demande a supprimer une mailbox sur un domaine qui n'a pas
    # la feature 'mailbox' -> 422
    response = client.delete(
        f"/domains/{domain_name}/mailboxes/toto",
        headers={"Authorization": f"Bearer {admin_token}"},
    )
    assert response.status_code == fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)   
@pytest.mark.parametrize(
    "domain_mail",
    ["tutu.net"],
    indirect=True,
)   
def test_adding_webmail(
        log,
        client,
        normal_user,
        domain_mail,
        db_dovecot_session,
        admin_token
):          
    token = normal_user["token"]
    admin_auth = (admin_token["user"], admin_token["password"])
    admin_token = admin_token["token"]
    domain_name = domain_mail["name"]
    
    # On obtient un 404 sur une mailbox qui n'existe pas
    response = client.get(
        f"/domains/{domain_name}/mailboxes/address",
        headers={"Authorization": f"Bearer {token}"},
    )   
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # On crée la boite mail
    response = client.post(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "givenName": "",
            "surName": "",
            "displayName": "",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # On get la boite
    response = client.get(
        f"/domains/{domain_name}/mailboxes/address",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    # Elle est valide, et elle n'est pas dans OX
    assert json["status"] == "ok"
    assert json["active"] == "yes"
    assert json["givenName"] is None
    assert json["surName"] is None
    assert json["displayName"] is None

    # On ajoute la feature "webmail" sur le domaine
    response = client.patch(
        f"/domains/{domain_name}/",
        json={
            "features": ["+webmail"],
            "context_name": "dimail",
        },
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # On get la mailbox, elle est broken (parce qu'elle n'est pas dans OX)
    response = client.get(
        f"/domains/{domain_name}/mailboxes/address",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    # Elle est valide, et elle n'est pas dans OX
    assert json["status"] == "broken"
    assert json["active"] == "yes"
    assert json["givenName"] is None
    assert json["surName"] is None
    assert json["displayName"] is None

    # Si je patch pour créer le compte OX, tout redevient normal
    response = client.patch(
        f"/domains/{domain_name}/mailboxes/address",
        json={
            "givenName": "Jean",
            "surName": "Bon",
            "displayName": "de Bayonne",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Je get la boite, elle est ok, le compte OX a été créé !
    response = client.get(
        f"/domains/{domain_name}/mailboxes/address",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    # Elle est valide, et elle n'est pas dans OX
    assert json["status"] == "ok"
    assert json["active"] == "yes"
    assert json["givenName"] == "Jean"
    assert json["surName"] == "Bon"
    assert json["displayName"] == "de Bayonne"


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "virgin_user",
    ["bidi:toto"],
    indirect=True,
)
# Les domaines tutu.net et target.fr sont dans le CTX dimain
# Le domaine other.org est dans le CTX ctx2
@pytest.mark.parametrize(
    "domain_web",
    ["tutu.net,target.fr,sub.dom.example.com,dom.example.com:dimail;other.org:ctx2"],
    indirect=True,
)
async def test_with_senders(
        log,
        async_client,
        normal_user,
        virgin_user,
        domain_web,
        db_dovecot_session,
        admin_token,
):
    token = normal_user["token"]
    virgin_token = virgin_user["token"]
    admin_token = admin_token["token"]
    domain_name = domain_web["name"]
    other_domain_name = domain_web["all_domains"][1]

    # On crée une boite sans senders
    response = await async_client.post(
        f"/domains/{domain_name}/mailboxes/essai1",
        json = {
            "surName": "Essai",
            "givenName": "Un",
            "displayName": "Jean Pierre",
            "additionalSenders": [],
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/essai1",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body["additionalSenders"] == []
    assert body["status"] == "ok"

    # On crée une boite avec deux senders
    response = await async_client.post(
        f"/domains/{domain_name}/mailboxes/essai2",
        json = {
            "surName": "Essai",
            "givenName": "Deux",
            "displayName": "Jean Paul",
            "additionalSenders": [ "jean@gmail.com", "paul@gmail.com" ],
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/essai2",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert len(body["additionalSenders"]) == 2
    assert "jean@gmail.com" in body["additionalSenders"]
    assert "paul@gmail.com" in body["additionalSenders"]
    assert body["status"] == "ok"

    # On change les senders de iessai2
    response = await async_client.patch(
        f"/domains/{domain_name}/mailboxes/essai2",
        json = {
            "additionalSenders": [ "-jean@gmail.com", "+jean@outlook.com", "+pontifex@boss.va" ],
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert len(body["additionalSenders"]) == 3
    assert "paul@gmail.com" in body["additionalSenders"]
    assert "jean@outlook.com" in body["additionalSenders"]
    assert "pontifex@boss.va" in body["additionalSenders"]
    assert body["status"] == "ok"

    
