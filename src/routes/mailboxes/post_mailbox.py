import logging
import secrets
import uuid

import fastapi

from ... import auth, business, enums, sql_api, sql_dovecot, utils, web_models
from .. import dependencies, routers


@routers.mailboxes.post(
    "/{user_name}",
    description="Create a mailbox in dovecot and OX",
    status_code=201,
    response_model=web_models.NewMailbox,
    responses={
        201: {"description": "Mailbox created"},
        403: {"description": "Permission denied"},
        404: {"description": "Domain not found in OX"},
        410: {"description": "Domain is moving away to another platform"},
    },
)
async def post_mailbox(
    create: web_models.CreateMailbox,
    user: auth.DependsTokenUser,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,
    api: dependencies.DependsApiDb,
    user_name: str,
    domain_name: str,
) -> web_models.NewMailbox:
    """Create a mailbox in dovecot and OX.

    Args:
        create (web_models.CreateMailbox): Mailbox information
        user (auth.DependsTokenUser): User credentials
        dovecot (dependencies.DependsDovecotDb): Dovecot database session
        api (dependencies.DependsApiDb): API database session
        user_name (str): User name
        domain_name (str): Domain name

    Returns:
        web_models.NewMailbox: New mailbox information

    Raises:
        fastapi.HTTPException: Permission denied
        Exception: Domain not found in OX

    See Also:
        * https://fastapi.tiangolo.com/tutorial/path-params
        * https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt
        * https://fastapi.tiangolo.com/tutorial/security/simple-verify-token

    Dependencies:
        auth.DependsTokenUser
        dependencies.DependsApiDb
        dependencies.DependsDovecotDb
        sql_api.get_domain
        sql_dovecot.create_user
        web_models.CreateMailbox
        web_models.NewMailbox

    Exemples:
        * POST /domains/test.com/mailboxes/test.user
        import client

        client.post(
        "domains/test.com/mailboxes/test.user",
        json={"givenName": "Test", "surName": "User"},
        headers={"Authorization: Bearer token"}
        )
    """
    log = logging.getLogger(__name__)

    # Attention : on contrôle les droits avant de tester si le domaine existe, mais
    # après le lookup DB, c'est plus rapide, et ça fait qu'un admin ne va pas avoir
    # un 403 sur un domaine qui n'existe pas, mais bien un 404.
    # A l'opposé, un non-admin aura un 403 au lieu d'un 404 (il ne peut pas faire la
    # différence entre un domaine qui lui est interdit et un domaine qui n'existe pas)
    db_domain = sql_api.get_domain(api, domain_name)
    if not user.can_manage_domain(db_domain):
        log.info(f"Cet utilisateur n'a pas les droits sur le domaine {domain_name}")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if db_domain is None:
        log.info(f"Le domaine {domain_name} n'existe pas dans la base API")
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
        api_domain = db_domain,
    )
    if b_domain.move_from_me():
        move_to = b_domain.moving_to()
        raise fastapi.HTTPException(
            status_code = fastapi.status.HTTP_410_GONE,
            detail = f"The domain {domain_name} is moving away to platform {move_to}",
        )

    if not db_domain.has_feature(enums.Feature.Mailbox):
        raise fastapi.HTTPException(status_code=422, detail="This domain does not have the 'mailbox' feature")

    imap_user = sql_dovecot.get_user(dovecot, user_name, domain_name)
    if imap_user is not None:
        raise fastapi.HTTPException(status_code=409, detail="Mailbox already exists")

    with_webmail = False
    names = {}
    if db_domain.has_feature(enums.Feature.Webmail):
        with_webmail = True
        if create.givenName is None or create.surName is None or create.displayName is None:
            raise fastapi.HTTPException(status_code=422, detail="When a domain has the 'webmail' feature, the attributes givenName, surName and displayName are mandatory")
        names["givenName"] = create.givenName
        names["surName"] = create.surName
        names["displayName"] = create.displayName

    mailbox = business.Mailbox(
        dovecot_session = dovecot,
        postfix_session = postfix,
        username = user_name,
        domain = domain_name,
        webmail = with_webmail,
    )
    await mailbox.create_mailbox(**names)
    if create.additionalSenders is not None:
        old_senders, _ = await mailbox.get_senders()
        print(f"Old senders: {old_senders}")
        new_senders = utils.read_args_list("senders", None, old_senders, create.additionalSenders)
        print(f"New senders: {new_senders}")
        await mailbox.update_senders(new_senders)
    
    return mailbox.to_web_new()

