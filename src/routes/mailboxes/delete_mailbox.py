import logging

import fastapi

from ... import auth, business, sql_api, sql_dovecot
from .. import dependencies, routers

@routers.mailboxes.delete(
    "/{user_name:str}",
    status_code=204,
    responses={
        204: {"description": "Deleted"},
        403: {"description": "Permission denied"},
        404: {"description": "Domain not found or Feature "
                             "'mailbox' not available on "
                             "the domain or Mailbox not found"},
        410: {"description": "Domain moving away to another platform"},
    },
    description="Delete a mailbox by name",
)
async def delete_mailbox(
    domain_name: str,
    user_name: str,
    user: auth.DependsTokenUser,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,
    api: dependencies.DependsApiDb,
) -> None:
    """Deletes a mailbox.

    Args:
        domain_name (str): Domain name
        user_name (str): User name
        user (auth.DependsTokenUser): User credentials
        dovecot (dependencies.DependsDovecotDb): Dovecot database session
        api (dependencies.DependsApiDb): API database session

    Returns:
        None: No content

    Raises:
        fastapi.HTTPException: Permission denied
        fastapi.HTTPException: Domain not found
        fastapi.HTTPException: Feature 'mailbox' not available on the domain
        fastapi.HTTPException: Mailbox not found

    See Also:
        * https://fastapi.tiangolo.com/tutorial/path-params
        * https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt
        * https://fastapi.tiangolo.com/tutorial/security/simple-verify-token

    Dependencies:
        auth.DependsTokenUser
        dependencies.DependsApiDb
        dependencies.DependsDovecotDb
        sql_api.get_domain
        sql_dovecot.delete_user
        sql_dovecot.get_user
    """
    log = logging.getLogger(__name__)
    email = f"{user_name}@{domain_name}"
    log.info(f"Nous supprimons {email}")

    # Attention : on contrôle les droits avant de tester si le domaine existe, mais
    # après le lookup DB, c'est plus rapide, et ça fait qu'un admin ne va pas avoir
    # un 403 sur un domaine qui n'existe pas, mais bien un 404.
    # A l'opposé, un non-admin aura un 403 au lieu d'un 404 (il ne peut pas faire la
    # différence entre un domaine qui lui est interdit et un domaine qui n'existe pas)
    db_domain = sql_api.get_domain(api, domain_name)
    if not user.can_manage_domain(db_domain):
        log.info(f"Cet utilisateur n'a pas les droits sur le domaine {domain_name}")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if db_domain is None:
        log.info(f"Le domaine {domain_name} n'existe pas dans la base API")
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
        api_domain = db_domain,
    )
    if b_domain.move_from_me():
        move_to = b_domain.moving_to()
        raise fastapi.HTTPException(
            status_code = fastapi.status.HTTP_410_GONE,
            detail = f"The domain {domain_name} is moving away to platform {move_to}",
        )

    if "mailbox" not in db_domain.features:
        log.info(f"Missing feature 'mailbox' on domain {domain_name}")
        raise fastapi.HTTPException(
            status_code=422,
            detail="Feature 'mailbox' not available on the domain"
        )

    db_user = sql_dovecot.get_user(dovecot, user_name, domain_name)
    if db_user is None:
        log.info(f"La boite {user_name} n'existe pas pour le domain {domain_name}")
        raise fastapi.HTTPException(status_code=404, detail="Mailbox not found")

    with_webmail = False
    if "webmail" in db_domain.features:
        with_webmail = True

    mailbox = business.Mailbox(
        dovecot_session = dovecot,
        postfix_session = postfix,
        username = user_name,
        domain = domain_name,
        webmail = with_webmail,
    )
    if mailbox.dovecot_mailbox() is None:
        log.info(f"La boite {user_name} n'existe pas pour le domain {domain_name} dans la base dovecot")
        raise fastapi.HTTPException(status_code=404, detail="Mailbox not found")
    if with_webmail:
        if await mailbox.ox_ctx() is None:
            log.info("Aucun contexte ne gère le domaine {domain_name} chez OX")
            raise fastapi.HTTPException(status_code=404, detail="Domain not found in open-xchange")
        if await mailbox.ox_mailbox() is None:
            log.info("Le contexte OX ne connait pas cet email")
            raise fastapi.HTTPException(status_code=404, detail="Mailbox not found in open-xchange")

    await mailbox.delete_mailbox()
    return None

