import logging

import fastapi

from ... import auth, business, enums, sql_api, sql_dovecot, utils, web_models
from .. import dependencies, routers

@routers.mailboxes.patch(
    "/{user_name}",
    status_code=200,
    response_model=web_models.Mailbox,
    responses={
        200: {"description": "Mailbox updated"},
        403: {"description": "Permission denied"},
        404: {"description": "Domain not found"},
        410: {"description": "Domain moving away to another platform"},
        422: {"description": "Feature 'mailbox' not available on the domain"},
    },
    description="Update a mailbox",
)
async def patch_mailbox(
    domain_name: str,
    user_name: str,
    updates: web_models.UpdateMailbox,
    user: auth.DependsTokenUser,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,
    api: dependencies.DependsApiDb,
) -> web_models.Mailbox:
    """Updates a mailbox. All the fields are optional and are the information
    to update on that mailbox. Some updates are not yet implemented.

    Args:
        domain_name (str): Domain name
        user_name (str): Mailbox username
        updates (web_models.UpdateMailbox): Mailbox information to update
        user (auth.DependsTokenUser): User credentials
        dovecot (dependencies.DependsDovecotDb): Dovecot database session
        api (dependencies.DependsApiDb): API database session

    Returns:
        web_models.Mailbox: Updated mailbox information

    Raises:
        fastapi.HTTPException: Permission denied
        fastapi.HTTPException: Domain not found
        fastapi.HTTPException: Feature 'mailbox' not available on the domain
        fastapi.HTTPException: Mailbox not found
        fastapi.HTTPException: Not yet implemented

    See Also:
        * https://fastapi.tiangolo.com/tutorial/path-params
        * https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt
        * https://fastapi.tiangolo.com/tutorial/security/simple-verify-token

    Dependencies:
        auth.DependsTokenUser
        dependencies.DependsApiDb
        dependencies.DependsDovecotDb
        sql_api.get_domain
        sql_dovecot.get_user
        business.Mailbox.to_web
    """
    log = logging.getLogger(__name__)
    email = f"{user_name}@{domain_name}"
    log.info(f"Nous modifions {email}")

    # Attention : on contrôle les droits avant de tester si le domaine existe, mais
    # après le lookup DB, c'est plus rapide, et ça fait qu'un admin ne va pas avoir
    # un 403 sur un domaine qui n'existe pas, mais bien un 404.
    # A l'opposé, un non-admin aura un 403 au lieu d'un 404 (il ne peut pas faire la
    # différence entre un domaine qui lui est interdit et un domaine qui n'existe pas)
    db_domain = sql_api.get_domain(api, domain_name)
    if not user.can_manage_domain(db_domain):
        log.info(f"Cet utilisateur n'a pas les droits sur le domaine {domain_name}")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if db_domain is None:
        log.info(f"Le domaine {domain_name} n'existe pas dans la base API")
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
        api_domain = db_domain,
    )
    if b_domain.move_from_me():
        move_to = b_domain.moving_to()
        raise fastapi.HTTPException(
            status_code = fastapi.status.HTTP_410_GONE,
            detail = f"The domain {domain_name} is moving away to platform {move_to}",
        )

    if updates.domain is not None:
        new_domain_db = sql_api.get_domain(api, updates.domain)
        if not user.can_manage_domain(new_domain_db):
            log.info(f"Permission denied on target domain {updates.domain} for current user")
            raise fastapi.HTTPException(status_code=403, detail="Permission denied")
        if new_domain_db is None:
            log.info(f"Target domain not found in database {updates.domain}")
            raise fastapi.HTTPException(status_code=404, detail="Target domain not found")
        # TODO si new_domain_db is moving from me, peut être, il faut grogner.
        log.error("Le deplacement d'une mailbox d'un domaine a l'autre n'est pas encore possible")
        raise fastapi.HTTPException(status_code=422, detail="Not yet implemented")

    if "mailbox" not in db_domain.features:
        log.info(f"Missing feature 'mailbox' on domain {domain_name}")
        raise fastapi.HTTPException(
            status_code=422,
            detail="Feature 'mailbox' not available on the domain"
        )

    db_user = sql_dovecot.get_user(dovecot, user_name, domain_name)
    if db_user is None:
        log.info(f"La boite {user_name} n'existe pas pour le domain {domain_name}")
        raise fastapi.HTTPException(status_code=404, detail="Mailbox not found")

    if updates.user_name is not None:
        log.error("Le renommage d'une mailbox n'est pas encore possible")
        raise fastapi.HTTPException(status_code=422, detail="Not yet implemented")

    with_webmail = False
    new_names = {}
    if "webmail" in db_domain.features:
        with_webmail = True
        if updates.givenName is not None:
            new_names["givenName"] = updates.givenName

        if updates.surName is not None:
            new_names["surName"] = updates.surName

        if updates.displayName is not None:
            new_names["displayName"] = updates.displayName

    mailbox = business.Mailbox(
        username=user_name,
        domain=domain_name,
        webmail=with_webmail,
        dovecot_session = dovecot,
        postfix_session = postfix,
    )
    if updates.active is not None:
        if mailbox.is_active() == "wait" or updates.active == "wait":
            if not user.is_real_admin():
                raise fastapi.HTTPException(status_code = 403, detail="Only admins can change a mailbox with active=wait")
        await mailbox.update_active(enums.MailboxActive(updates.active), new_names)
    if updates.additionalSenders is not None:
        old_senders, _ = await mailbox.get_senders()
        new_senders = utils.read_args_list("senders", None, old_senders, updates.additionalSenders)
        await mailbox.update_senders(new_senders)
    if with_webmail:
        if len(new_names.keys()) > 0:
            await mailbox.update_names(new_names)

    return await mailbox.to_web()

