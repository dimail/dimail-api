import logging
import secrets
import uuid

import fastapi

from ... import auth, business, enums, sql_api, sql_dovecot, utils, web_models
from .. import dependencies, routers


@routers.mailboxes.post(
    "/{user_name}/reset_password",
    description="Reset the password of a mailbox in dovecot and OX",
    status_code=200,
    response_model=web_models.NewMailbox,
    responses={
        200: {"description": "Mailbox updated"},
        403: {"description": "Permission denied"},
        404: {"description": "Domain not found in OX"},
        410: {"description": "Domain is moving away to another platform"},
    },
)
async def reset_password(
    user: auth.DependsTokenUser,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,
    api: dependencies.DependsApiDb,
    user_name: str,
    domain_name: str,
) -> web_models.NewMailbox:
    """Resets the password of a mailbox in dovecot and OX.

    Exemples:
        * POST /domains/test.com/mailboxes/test.user/reset_password
        import client

        client.post(
        "domains/test.com/mailboxes/test.user/reset_password",
        json={},
        headers={"Authorization: Bearer token"}
        )
    """
    log = logging.getLogger(__name__)

    # Attention : on contrôle les droits avant de tester si le domaine existe, mais
    # après le lookup DB, c'est plus rapide, et ça fait qu'un admin ne va pas avoir
    # un 403 sur un domaine qui n'existe pas, mais bien un 404.
    # A l'opposé, un non-admin aura un 403 au lieu d'un 404 (il ne peut pas faire la
    # différence entre un domaine qui lui est interdit et un domaine qui n'existe pas)
    db_domain = sql_api.get_domain(api, domain_name)
    if not user.can_manage_domain(db_domain):
        log.info(f"Cet utilisateur n'a pas les droits sur le domaine {domain_name}")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if db_domain is None:
        log.info(f"Le domaine {domain_name} n'existe pas dans la base API")
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
        api_domain = db_domain,
    )
    if b_domain.move_from_me():
        move_to = b_domain.moving_to()
        raise fastapi.HTTPException(
            status_code = fastapi.status.HTTP_410_GONE,
            detail = f"The domain {domain_name} is moving away to platform {move_to}",
        )

    if not db_domain.has_feature(enums.Feature.Mailbox):
        raise fastapi.HTTPException(status_code=422, detail="This domain does not have the 'mailbox' feature")

    imap_user = sql_dovecot.get_user(dovecot, user_name, domain_name)
    if imap_user is None:
        raise fastapi.HTTPException(status_code=404, detail="Mailbox not found")

    with_webmail = False
    names = {}
    if db_domain.has_feature(enums.Feature.Webmail):
        with_webmail = True

    mailbox = business.Mailbox(
        dovecot_session = dovecot,
        postfix_session = postfix,
        username = user_name,
        domain = domain_name,
        webmail = with_webmail,
    )
    mailbox.reset_password()
    
    return mailbox.to_web_new()

