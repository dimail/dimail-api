import logging

import fastapi

from ... import auth, business, sql_api, sql_dovecot, web_models
from .. import dependencies, routers


@routers.mailboxes.get(
    "/",
    responses={
        200: {"description": "Get users from query request"},
        403: {"description": "Permission denied, insuficient permissions to perform the request"},
        404: {"description": "No users matched the query"},
        410: {"description": "Domain moving away to another platform"},
    },
    description="Get all mailboxes in a domain",
)
async def get_mailboxes(
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    user: auth.DependsTokenUser,
    domain_name: str,
    #  page_size: int = 20,
    #  page_number: int = 0,
) -> list[web_models.Mailbox]:
    """Get all mailboxes in a domain.

    Args:
        domain_name (str): Domain name
        user (auth.DependsTokenUser): User credentials
        dovecot (dependencies.DependsDovecotDb): Dovecot database session
        api (dependencies.DependsApiDb): API database session

    Returns:
        list[web_models.Mailbox]: List of mailboxes

    Raises:
        fastapi.HTTPException: Permission denied
        fastapi.HTTPException: Domain not found

    See Also:
        * https://fastapi.tiangolo.com/tutorial/path-params
        * https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt
        * https://fastapi.tiangolo.com/tutorial/security/simple-verify-token

    Dependencies:
        auth.DependsTokenUser
        dependencies.DependsApiDb
        dependencies.DependsDovecotDb
        sql_api.get_domain
        sql_dovecot.get_users
        business.MailboxMaker
        business.Mailbox.to_web
    """
    log = logging.getLogger(__name__)
    log.info(f"Searching mailboxes in domain {domain_name}\n")

    # Attention : on contrôle les droits avant de tester si le domaine existe, mais
    # après le lookup DB, c'est plus rapide, et ça fait qu'un admin ne va pas avoir
    # un 403 sur un domaine qui n'existe pas, mais bien un 404.
    # A l'opposé, un non-admin aura un 403 au lieu d'un 404 (il ne peut pas faire la
    # différence entre un domaine qui lui est interdit et un domaine qui n'existe pas)
    db_domain = sql_api.get_domain(api, domain_name)
    if not user.can_manage_domain(db_domain):
        log.info(f"Cet utilisateur n'a pas les droits sur le domaine {domain_name}")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if db_domain is None:
        log.info(f"Le domaine {domain_name} n'existe pas dans la base API")
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
        api_domain = db_domain,
    )
    if b_domain.move_from_me():
        move_to = b_domain.moving_to()
        raise fastapi.HTTPException(
            status_code = fastapi.status.HTTP_410_GONE,
            detail = f"The domain {domain_name} is moving away to platform {move_to}",
        )

    with_webmail = False
    if "webmail" in db_domain.features:
        with_webmail = True

    maker = business.MailboxMaker(
        postfix_session = postfix,
        dovecot_session = dovecot,
    )
    mailboxes = await maker.all_in_domain(domain_name, with_webmail)
    result = [ await x.to_web() for x in mailboxes ]
    return result
