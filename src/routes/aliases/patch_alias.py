import logging

import fastapi

from ... import auth, business, sql_api, sql_postfix, utils, web_models
from .. import dependencies, routers


@routers.aliases.patch(
    "/{user_name}/{destination}",
    description="Updates an alias. The only possible modification is the allow_to_send attribute.",
    status_code=fastapi.status.HTTP_200_OK,
    response_model=web_models.Alias,
    responses={
        200: {"description": "Alias modified"},
        403: {"description": "Permission denied"},
        404: {"description": "Alias not found"},
        410: {"description": "Domain moving away to another platform"},
    },
)
async def patch_alias(
    updates: web_models.UpdateAlias,
    user: auth.DependsTokenUser,
    postfix: dependencies.DependsPostfixDb,
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    domain_name: str,
    user_name: str,
    destination: str,
) -> web_models.Alias:
    log = logging.getLogger(__name__)

    # Attention : on contrôle les droits avant de tester si le domaine existe, mais
    # après le lookup DB, c'est plus rapide, et ça fait qu'un admin ne va pas avoir
    # un 403 sur un domaine qui n'existe pas, mais bien un 404.
    # A l'opposé, un non-admin aura un 403 au lieu d'un 404 (il ne peut pas faire la
    # différence entre un domaine qui lui est interdit et un domaine qui n'existe pas)
    db_domain = sql_api.get_domain(api, domain_name)
    if not user.can_manage_domain(db_domain):
        log.info(f"Cet utilisateur n'a pas les droits sur le domaine {domain_name}")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if db_domain is None:
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
        api_domain = db_domain,
    )
    if b_domain.move_from_me():
        move_to = b_domain.moving_to()
        raise fastapi.HTTPException(
            status_code = fastapi.status.HTTP_410_GONE,
            detail = f"The domain {domain_name} is moving away to platform {move_to}",
        )

    with_webmail = False
    _, dest_domain = utils.split_email(destination, strict = False)
    if dest_domain is not None:
        db_dest_domain = sql_api.get_domain(api, dest_domain)
        if db_dest_domain is not None:
            if db_dest_domain.has_feature("webmail"):
                with_webmail = True

    alias = business.Alias(
        postfix_session = postfix,
        webmail = with_webmail,
        username = user_name,
        domain = domain_name,
        destination = destination,
    )
    if updates.allow_to_send is not None:
        await alias.update_allow(updates.allow_to_send)

    return alias.to_web()


