import logging

import fastapi

from ... import auth, business, utils, sql_api
from .. import dependencies, routers


@routers.aliases.delete(
    "/{user_name}/{destination}",
    description="Deletes an alias. "
        "When destination=all, will remove all the destinations for an alias.",
    status_code=fastapi.status.HTTP_204_NO_CONTENT,
    response_model=None,
    responses={
        204: {"description": "Alias deleted"},
        403: {"description": "Permission denied"},
        404: {"description": "Alias or domain not found"},
        410: {"description": "Domain moving away to another platform"},
    },
)
async def delete_alias(
    domain_name: str,
    user_name: str,
    destination: str,
    user: auth.DependsTokenUser,
    postfix: dependencies.DependsPostfixDb,
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
) -> None:
    log = logging.getLogger(__name__)

    # Attention : on contrôle les droits avant de tester si le domaine existe, mais
    # après le lookup DB, c'est plus rapide, et ça fait qu'un admin ne va pas avoir
    # un 403 sur un domaine qui n'existe pas, mais bien un 404.
    # A l'opposé, un non-admin aura un 403 au lieu d'un 404 (il ne peut pas faire la
    # différence entre un domaine qui lui est interdit et un domaine qui n'existe pas)
    db_domain = sql_api.get_domain(api, domain_name)

    if not user.can_manage_domain(db_domain):
        log.info(f"Cet utilisateur n'a pas les droits sur le domaine {domain_name}")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    # Il ne faut pas tester l'existence du domaine ici (s'il n'existe pas et
    # qu'il traine de vieux alias, un admin peut les supprimer).
    # if db_domain is None:
    #     raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
        api_domain = db_domain,
    )
    if b_domain.move_from_me():
        move_to = b_domain.moving_to()
        raise fastapi.HTTPException(
            status_code = fastapi.status.HTTP_410_GONE,
            detail = f"The domain {domain_name} is moving away to platform {move_to}",
        )

   
    if destination == "all":
        log.info(f"On demande la suppression de toutes les destinations pour {user_name}@{domain_name}")
        maker = business.AliasMaker(
            api_session = api,
            postfix_session = postfix,
        )
        aliases = maker.get_aliases(
            username = user_name,
            domain = domain_name,
            destination = "",
        )
        if len(aliases) == 0:
            raise fastapi.HTTPException(status_code=404, detail="Alias not found")
        for alias in aliases:
            await alias.delete_alias()
        return None

    log.info("On supprime un alias exact")

    with_webmail = False
    _, destination_domain = utils.split_email(destination, strict=False)
    if destination_domain is not None:
        db_destination_domain = sql_api.get_domain(api, destination_domain)
        if db_destination_domain is not None:
            with_webmail = db_destination_domain.has_feature('webmail')

    alias = business.Alias(
        postfix_session = postfix,
        webmail = with_webmail,
        username = user_name,
        domain = domain_name,
        destination = destination,
    )
    await alias.delete_alias()
    return None

