import logging

import fastapi

from ... import auth, business, sql_api, web_models
from .. import dependencies, routers


@routers.aliases.get(
    "/",
    description="Gets aliases for a domain",
    response_model=list[web_models.Alias],
    status_code=fastapi.status.HTTP_200_OK,
    responses={
        200: {"description": "Get aliases"},
        403: {"description": "Permission denied"},
        404: {"description": "Alias not found"},
        410: {"description": "Domain moving away to another platform"},
    },
)
async def get_alias(
    domain_name: str,
    user: auth.DependsTokenUser,
    postfix: dependencies.DependsPostfixDb,
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    user_name: str = "",
    destination: str = "",
) -> list[web_models.Alias]:
    log = logging.getLogger(__name__)

    # Attention : on contrôle les droits avant de tester si le domaine existe, mais
    # après le lookup DB, c'est plus rapide, et ça fait qu'un admin ne va pas avoir
    # un 403 sur un domaine qui n'existe pas, mais bien un 404.
    # A l'opposé, un non-admin aura un 403 au lieu d'un 404 (il ne peut pas faire la
    # différence entre un domaine qui lui est interdit et un domaine qui n'existe pas)
    db_domain = sql_api.get_domain(api, domain_name)
    if not user.can_manage_domain(db_domain):
        log.info(f"Cet utilisateur n'a pas les droits sur le domaine {domain_name}")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if db_domain is None:
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
        api_domain = db_domain,
    )
    if b_domain.move_from_me():
        move_to = b_domain.moving_to()
        raise fastapi.HTTPException(
            status_code = fastapi.status.HTTP_410_GONE,
            detail = f"The domain {domain_name} is moving away to platform {move_to}",
        )

    maker = business.AliasMaker(
        api_session = api,
        postfix_session = postfix,
    )
    aliases = maker.get_aliases(
        domain = domain_name,
        username = user_name,
        destination = destination,
    )
    return [ x.to_web() for x in aliases ]

