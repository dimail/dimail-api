import fastapi.testclient
import pytest

@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "domain_mail",
    ["example.com"],
    indirect=True,
)
def test_allow__create_errors(client, log, admin, normal_user, domain_mail):
    auth=(admin["user"], admin["password"])

    # If the user does not exist -> not found
    response = client.post(
        "/allows/",
        json={"domain": "example.com", "user": "unknown"},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # If the domain does not exist -> not found
    response = client.post(
        "/allows/",
        json={"domain": "unknown.com", "user": normal_user["user"]},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # If the allows already exists -> conflict
    response = client.post(
        "/allows/",
        json={"domain": "example.com", "user": normal_user["user"]},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_409_CONFLICT


@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "domain_mail",
    ["example.com"],
    indirect=True,
)
def test_allow_get_allows(client, log, admin, normal_user, domain_mail):
    auth=(admin["user"], admin["password"])
    user=(normal_user["user"], normal_user["password"])

    # Je crée un deuxième user, toto
    response = client.post(
        "/users/",
        json={
            "name": "toto",
            "password": "toto",
            "is_admin": False,
            "perms": []
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Je crée un deuxième domaine, target.net
    response = client.post(
        "/domains/",
        json={
            "name": "target.fr",
            "delivery": "virtual",
            "features": [ "mailbox" ],
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # J'allow le deuxième user sur le deuxième domaine
    response = client.post(
        "/allows/",
        json={
            "user": "toto",
            "domain": "target.fr",
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Si le user normal demande la liste des allows -> 403
    response = client.get(
        "/allows/",
        params={},
        auth=user,
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # Si l'admin demande la liste des allows, ça marche
    response = client.get(
        "/allows/",
        params={},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert len(body) == 2
    assert {'domain': 'example.com', 'user': 'bidibule'} in body
    assert {'domain': 'target.fr', 'user': 'toto' } in body

    # Si je demande pour un seul domaine, ça marche
    response = client.get(
        "/allows/",
        params={
            "domain": "target.fr",
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body == [ {'domain': 'target.fr', 'user': 'toto' } ]

    # Si je demande pour l'autre domaine, ça marche aussi
    response = client.get(
        "/allows/",
        params={
            "domain": "example.com",
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body == [ {'domain': 'example.com', 'user': 'bidibule' } ]

    # Si je demande pour un seul user, ça marche
    response = client.get(
        "/allows/",
        params={
            "username": "toto",
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body == [ {'domain': 'target.fr', 'user': 'toto' } ]

