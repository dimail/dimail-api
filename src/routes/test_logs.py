import pytest

import fastapi

@pytest.mark.parametrize(
    "normal_user",
    ["normal:toto"],
    indirect=True,
)
def test_get_logs(db_api_session, log, client, normal_user, admin):
    admin_auth = (admin["user"], admin["password"])
    user_auth = (normal_user["user"], normal_user["password"])
    
    # Un user normal ne peut pas lire les logs
    response = client.get("/logs/", auth=user_auth)
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # Un admin peut lire les logs
    response = client.get("/logs/", auth=admin_auth)
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json() 
    assert "Ce sont les logs du serveur API" in body["logs"]

    # Si je donne la permission "get_logs" au user normal...
    response = client.patch(
        f"/users/{normal_user['user']}",
        json={
            "perms": ["+get_logs"],
        },
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    
    # ... alors il peut demander les logs
    response = client.get("/logs/", auth=user_auth)
    assert response.status_code == fastapi.status.HTTP_200_OK

@pytest.mark.parametrize(
    "normal_user",
    ["normal:toto"],
    indirect=True,
)
def test_get_crash(log, client, normal_user, admin, crash):
    admin_auth = (admin["user"], admin["password"])
    user_auth = (normal_user["user"], normal_user["password"])

    # Un user normal ne peut pas demander la liste des crash du serveur
    response = client.get("/logs/crashes/", auth=user_auth)
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # Un admin peut demander cette liste, elle est vide
    response = client.get("/logs/crashes/", auth=admin_auth)
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 0

    # Si je donne la perm 'get_logs' au user normal...
    response = client.patch(
        f"/users/{normal_user['user']}",
        json={
            "perms": ["+get_logs"],
        },
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # ... alors il peut demander les crash logs
    response = client.get("/logs/crashes/", auth=user_auth)
    assert response.status_code == fastapi.status.HTTP_200_OK


    # Si je liste les utilisateurs avec log_request=force ...
    response = client.get(
        "/users/",
        params={
            "log_request": "force",
        },
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # ... j'ai maintenant 1 crash en stock
    response = client.get("/logs/crashes/", auth=admin_auth)
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 1
    assert body[0]["name"] == "crash.log.1"

    # Je peux demander ce crash log
    response = client.get(
        f"/logs/crashes/crash.log.1",
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    full_body = "\n".join(body["logs"])
    assert "log_request = force" in full_body

    # Je peux demander qu'on supprime ce crash log
    response = client.delete(
        "/logs/crashes/crash.log.1",
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_204_NO_CONTENT

    # Maintenant, quand je demande la liste, elle est de nouveau vide
    response = client.get("/logs/crashes/", auth=admin_auth)
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 0

    # Et si je provoque un log...
    response = client.get(
        "/users/",
        params={
            "log_request": "force",
        },
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # J'ai le crash numéro 2 dans les stocks
    response = client.get("/logs/crashes/", auth=admin_auth)
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 1
    assert body[0]["name"] == "crash.log.2"


    

