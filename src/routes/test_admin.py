import pytest

import fastapi.testclient

from .. import sql_api, remote


def test_users__create(db_api, ox_cluster, client, log):
    # At the beginning of time, database is empty, random users are
    # accepted and are admins
    response = client.get("/users", auth=("useless", "useless"))
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == []

    # If we GET the user, it is not found
    response = client.get("/users/first_admin", auth=("useless", "useless"))
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    response = client.post(
        "/users",
        json={"name": "first_admin", "password": "toto", "is_admin": True, "perms": []},
        auth=("useless", "useless"),
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED
    # TODO : Comment piloter la génération des uuid depuis les tests pour les rendre prédictibles ?
    uuid = response.json()["uuid"]
    assert response.json() == {"name": "first_admin", "uuid": uuid, "is_admin": True, "perms": []}

    # Database is not empty anymore, only admins can do admin requests
    response = client.get("/users", auth=("first_admin", "toto"))
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == [{"name": "first_admin", "uuid": uuid, "is_admin": True, "perms": []}]

    # Check we can get the user we newly created
    response = client.get("/users/first_admin", auth=("first_admin", "toto"))
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == {"name": "first_admin", "uuid": uuid, "is_admin": True, "perms": []}

    # If we GET a user that does not exist
    response = client.get("/users/nobody", auth=("first_admin", "toto"))
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # without auth, we got a 401 error
    response = client.post(
        "/users",
        json={"name": "testing", "password": "titi", "is_admin": False, "perms": []},
    )
    assert response.status_code == fastapi.status.HTTP_401_UNAUTHORIZED

    # With auth, but with the wrong password, still fails
    response = client.post(
        "/users",
        json={"name": "testing", "password": "titi", "is_admin": False, "perms": []},
        auth=("first_admin", "wrong_password"),
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # With auth, but creating a user that already exists (and is myself, by the way)
    # will fail
    response = client.post(
        "/users",
        json={"name": "first_admin", "password": "toto", "is_admin": False, "perms": []},
        auth=("first_admin", "toto"),
    )
    assert response.status_code == fastapi.status.HTTP_409_CONFLICT
    assert response.json() == {"detail": "User already exists"}

    # Creating a normal user
    response = client.post(
        "/users",
        json={"name": "normal", "password": "toto", "is_admin": False, "perms": []},
        auth=("first_admin", "toto"),
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # If the normal user tries to create a user, it fails
    response = client.post(
        "/users",
        json={"name": "now_one", "password": "toto", "is_admin": False, "perms": []},
        auth=("normal", "toto"),
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # Creating a user with no password
    response = client.post(
        "/users",
        json={"name": "now_two", "password": "no", "is_admin": False, "perms": []},
        auth=("first_admin", "toto"),
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # That new user cannot authenticate
    response = client.get(
        "/users/now_two",
        auth=("now_two", "no"),
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN


def test__update_a_user(log, client, admin):
    auth = (admin["user"], admin["password"])

    # If patch a user without the correct creds -> forbidden
    response = client.patch(
        "/users/does-not-exist",
        json={"password": "something"},
        auth=("not-a-user", "not-a-password"),
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # If patch a user that does not exist -> not found
    response = client.patch(
        "/users/does-not-exist",
        json={"password": "burps"},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # We create a normal user, so that we can modify it later
    response = client.post(
        "/users/",
        json={
            "name": "normal",
            "password": "toto",
            "is_admin": False,
            "perms": [],
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Vérifier que l'utilisateur n'est pas admin
    response = client.get(
        "/users/normal",
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert not response.json()["is_admin"]

    # We can change the is_admin
    response = client.patch(
        "/users/normal",
        json={"is_admin": True},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Vérifier que l'utilisateur est bien devenu admin
    response = client.get(
        "/users/normal",
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json()["is_admin"]

    # On lui retire ses droits d'admin
    response = client.patch(
        "/users/normal",
        json={"is_admin": False},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Verifier qu'on peut obtenir un token avec l'ancien mot de passe
    response = client.get(
        "/token",
        auth=("normal", "toto"),
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # We can set a new password
    response = client.patch(
        "/users/normal",
        json={"password": "coincoin"},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Verifier qu'on peut obtenir un token avec le nouveau mot de passe
    response = client.get(
        "/token",
        auth=("normal", "coincoin"),
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # L'utilisateur normal ne peut pas se modifier
    response = client.patch(
        "/users/normal",
        json={"password": "toto"},
        auth=("normal", "coincoin"),
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # Verifier qu'il n'a pas de perms
    response = client.get("/users/normal", auth=auth)
    assert response.json()["perms"] == []

    # On lui ajoute la perm "new_domain"
    response = client.patch(
        "/users/normal",
        json={"perms": ["+new_domain"]},
        auth=auth
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Verifier que maintenant il a bien la perm "new_domain"
    response = client.get("/users/normal", auth=auth)
    assert response.json()["perms"] == [ "new_domain" ]

    # Si on ne touche pas a ses perms...
    reponse = client.patch(
        "/users/normal",
        json={},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Alors le user a toujours la permission "new_domain"
    response = client.get("/users/normal", auth=auth)
    assert response.json()["perms"] == [ "new_domain" ]

    # Si je parle d'une perm qui n'existe pas, j'ai une erreur (422)
    response = client.patch(
        "/users/normal",
        json={"perms": ["+coincoin"]},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY

    # Si je mélange set et diff, j'ai une erreur (422)
    response = client.patch(
        "/users/normal",
        json={"perms": [ "new_domain", "+new_domain", "-new_domain" ]},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY

    # Si je set les perms sur une liste vide, ça retire les perms
    response = client.patch(
        "/users/normal",
        json={"perms": []},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    response = client.get("/users/normal", auth=auth)
    assert response.json()["perms"] == []

    # Si je set les perms sur une liste non vide, ca passe
    response = client.patch(
        "/users/normal",
        json={"perms": ["new_domain"]},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    response = client.get("/users/normal", auth=auth)
    assert response.json()["perms"] == ["new_domain"]

    # Si je del une perm, elle est retirée
    response = client.patch(
        "/users/normal",
        json={"perms": ["-new_domain"]},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    response = client.get("/users/normal", auth=auth)
    assert response.json()["perms"] == []

    # Verifier qu'on peut obtenir un token avec le nouveau mot de passe
    response = client.get(
        "/token",
        auth=("normal", "coincoin"),
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # On lui supprime son mot de passe
    response = client.patch(
        "/users/normal",
        json={"password": "no"},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Il ne peut plus s'authentifier pour demander un token
    response = client.get(
        "/token",
        auth=("normal", "no"),
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # On lui remet un mot de passe
    response = client.patch(
        "/users/normal",
        json={"password": "plop"},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Il peut de nouveau demander un token
    response = client.get(
        "/token",
        auth=("normal", "plop"),
    )
    assert response.status_code == fastapi.status.HTTP_200_OK


def test_patch_admin(log, client, admin):
    admin_auth = (admin["user"], admin["password"])

    # On change le mot de passe du admin_user
    response = client.patch(
        f"/users/{admin['user']}",
        json = {
            "password": "Tototrssecret",
        },
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert json["is_admin"] is True
    print(f"Reponse: {json}")

    # Avec l'ancien mot de passe on ne peut pas lire le user
    response = client.get(
        f"/users/{admin['user']}",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # Avec le nouveau, on peut
    admin_auth = ( admin["user"], "Tototrssecret")
    response = client.get(
        f"/users/{admin['user']}",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert json["is_admin"]


@pytest.mark.parametrize(
    "normal_user",
    ["normal:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "domain_mail",
    ["domaine-1,domaine-2"],
    indirect=True,
)
def test_delete_user(db_api_session, log, client, admin, normal_user, domain_mail):
    """Check that we can delete a user and that it will remove all the
    associated allows."""
    admin_auth = (admin["user"], admin["password"])
    user_auth = (normal_user["user"], normal_user["password"])

    # Check that we can fetch the two allows
    response = client.get(
        "/allows/",
        params={
            "username": "normal",
        },
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    items = response.json()
    assert len(items) == 2
    assert {"domain": "domaine-1", "user": "normal"} in items
    assert {"domain": "domaine-2", "user": "normal"} in items

    # If we try to delete a user without the prems -> forbiden
    response = client.delete(
        f"/users/{normal_user['user']}",
        auth=user_auth
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # If we try to delete a user that does not exist -> not found
    response = client.delete(
        "/users/not-a-user",
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # If we delete the user -> ok, no content
    response = client.delete(
        "/users/normal",
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_204_NO_CONTENT

    # And now, if we fetch the user -> not found
    response = client.get(
        "/users/normal",
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # And now, if we fetch the allows -> empty list
    response = client.get(
        "/allows",
        params={
            "username": "normal",
        },
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert len(response.json()) == 0


def test_domains__create_fails(db_api_session, log, client, admin):
    """Cannot create domain with no name."""
    auth=(admin["user"], admin["password"])

    # Sans le nom, ça ne passe pas
    response = client.post(
        "/domains",
        json={
            "context_name": "context",
            "name": None,
            "delivery": "virtual",
            "features": ["mailbox", "webmail", "alias"],
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.json() == {
        "detail": [
            {
                "type": "string_type",
                "loc": ["body", "name"],
                "msg": "Input should be a valid string",
                "input": None,
            },
        ]
    }

    # Si on met un delivery idiot, ça ne passe pas
    response = client.post(
        "/domains",
        json={
            "context_name": "context",
            "name": "example.com",
            "delivery": "plop",
            "features": ["mailbox", "webmail", "alias"],
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.json()["detail"][0]["loc"] == [ "body", "delivery" ]
    assert response.json()["detail"][0]["type"] == "enum"
    # Pas de détail intelligent ?

    # Si on met un delivery = "relay" sans transport, ça ne passe pas
    response = client.post(
        "/domains",
        json={
            "context_name": "context",
            "name": "example.com",
            "delivery": "relay",
            "features": ["mailbox", "webmail", "alias"],
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY
    assert "Transport is mandatory" in response.json()["detail"]
    # Pas de détail intelligent...

    # Si on ne met pas de features, ça ne passe pas
    response = client.post(
        "/domains",
        json={
            "context_name": "context",
            "name": "example.com",
            "delivery": "relay",
            "transport": "stupid",
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.json()["detail"][0]["loc"] == ["body", "features"]
    assert response.json()["detail"][0]["type"] == "missing"


def test_domains__create_successful(db_api_session, db_postfix_session, log, client, admin):
    """Succesfully create domain."""

    # La creation d'un domaine par un admin fonctionne.
    response = client.post(
        "/domains/",
        json={
            "context_name": "context",
            "name": "domain",
            "delivery": "virtual",
            "features": ["mailbox", "webmail", "alias"],
        },
        auth=(admin["user"], admin["password"]),
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED
    no_test = {"code": "no_test", "detail": "Did not check yet"}
    assert response.json() == {
        "name": "domain",
        "valid": False,
        "state": "new",
        "features": ["mailbox", "webmail", "alias"],
        "delivery": "virtual",
        "transport": None,
        "webmail_domain": None,
        "imap_domain": None,
        "smtp_domain": None,
        "context_name": "context",
        "domain_exist": {"ok": False, "errors": [no_test], "internal": False},
        "mx": {"ok": False, "errors": [no_test], "internal": False},
        "cname_imap": {"ok": False, "errors": [no_test], "internal": False},
        "cname_smtp": {"ok": False, "errors": [no_test], "internal": False},
        "cname_webmail": {"ok": False, "errors": [no_test], "internal": False},
        "spf": {"ok": False, "errors": [no_test], "internal": False},
        "dkim": {"ok": False, "errors": [no_test], "internal": False},
        "postfix": {"ok": False, "errors": [no_test], "internal": True},
        "ox": {"ok": False, "errors": [no_test], "internal": True},
        "cert": {"ok": False, "errors": [no_test], "internal": True},
    }

    # La creation d'un deuxieme domaine par un admin, dans le même contexte
    # fonctionne aussi (le second domaine ne suit pas le même chemin de code,
    # car le contexte existe déjà)
    response = client.post(
        "/domains/",
        json={
            "context_name": "context",
            "name": "domain2",
            "delivery": "alias",
            "features": ["mailbox", "webmail", "alias"],
        },
        auth=(admin["user"], admin["password"]),
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED
    obj = response.json()
    assert obj["delivery"] == "alias"
    assert obj["transport"] is None

    # La creation d'un domaine en delivery = "relay" fonctionne aussi
    response = client.post(
        "/domains/",
        json={
            "context_name": "context",
            "name": "domain3",
            "delivery": "relay",
            "transport": "smtp:[other]",
            "features": ["mailbox", "webmail", "alias"],
        },
        auth=(admin["user"], admin["password"]),
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED
    obj = response.json()
    assert obj["delivery"] == "relay"
    assert obj["transport"] == "smtp:[other]"

    # La création d'un domaine sans contexte (et sans webmail) fonctionne aussi
    # La creation d'un domaine en delivery = "relay" fonctionne aussi
    response = client.post(
        "/domains/",
        json={
            "name": "domain4",
            "delivery": "virtual",
            "features": ["mailbox", "alias"],
        },
        auth=(admin["user"], admin["password"]),
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED



def test_allows__create_allows(db_api_session, db_postfix_session, log, client):
    """Create "allows" object, granting an user management permissions to a domain."""

    # Create first admin for later auth
    auth = ("admin", "admin_password")
    sql_api.create_user(
        db_api_session,
        name=auth[0],
        password=auth[1],
        is_admin=True,
        perms=[],
    )

    # If we GET all the domains, we get an empty list
    response = client.get("/domains/", auth=("admin", "admin_password"))
    assert response.status_code == fastapi.status.HTTP_200_OK
    assert response.json() == []

    # Create user and domain before access
    user = sql_api.create_user(
        db_api_session,
        name="user",
        password="password",
        is_admin=False,
        perms=[],
    )
    domain = sql_api.create_domain(
        db_api_session,
        name="domain",
        features=[],
    )

    # If we GET all the domains, we get the one newly created
    response = client.get("/domains/", auth=("admin", "admin_password"))
    assert response.status_code == fastapi.status.HTTP_200_OK
    infos = response.json()
    assert len(infos) == 1
    assert infos[0]["name"] == "domain"

    # Create allows for this user on this domain
    response = client.post(
        "/allows/", json={"domain": domain.name, "user": user.name}, auth=auth
    )

    assert response.status_code == fastapi.status.HTTP_201_CREATED
    assert response.json() == {"user": user.name, "domain": domain.name}

    # GET list of "allows" should return 1 result
    assert len(client.get("/allows/", auth=auth).json()) == 1


def test_allows__delete_allows(db_api_session, log, client):
    """Delete "allows" object."""

    # Create first admin for later auth
    auth = ("admin", "admin_password")
    sql_api.create_user(db_api_session, name=auth[0], password=auth[1], is_admin=True, perms=[])

    # Create allows and related user and domain
    user = sql_api.create_user(
        db_api_session, name="user", password="password", is_admin=False, perms=[]
    )
    domain = sql_api.create_domain(
        db_api_session,
        name="domain",
        features=[],
    )
    sql_api.allow_domain_for_user(
        db_api_session,
        user=user.name,
        domain=domain.name,
    )

    # Try to delete allow for an invalid user
    response = client.delete(f"/allows/{domain.name}/invalid_user", auth=auth)
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # Try to delete allow for an invalid domain
    response = client.delete(f"/allows/invalid_domain/{user.name}", auth=auth)
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # Delete allows
    response = client.delete(f"/allows/{domain.name}/{user.name}", auth=auth)
    assert response.status_code == fastapi.status.HTTP_204_NO_CONTENT
    assert response.content == b""

    # Try to delete an allow that does not exist (but the user and the domain
    # exist)
    response = client.delete(f"/allows/{domain.name}/{user.name}", auth=auth)
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND


def test_version(log, client, admin):
    admin_auth = (admin["user"], admin["password"])
    response = client.get(
        "/system/version",
    )
    assert response.status_code == fastapi.status.HTTP_401_UNAUTHORIZED

    response = client.get(
        "/system/version",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert "version" in body
    assert "changelog" in body
    assert body["version"] == "Unknown"
    assert body["changelog"] == [""]

def test_shutdown(log, client, admin):
    admin_auth = (admin["user"], admin["password"])
    response = client.get(
        "/system/shutdown",
    )
    assert response.status_code == fastapi.status.HTTP_401_UNAUTHORIZED

    response = client.get(
        "/system/shutdown",
        auth = ( "jean", "pierre" ),
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    response = client.get(
        "/system/shutdown",
        auth = admin_auth,
    )
    if not remote.fake_mode():
        assert response.status_code == fastapi.status.HTTP_422_UNPROCESSABLE_ENTITY
    else:
        assert response.status_code == fastapi.status.HTTP_200_OK

