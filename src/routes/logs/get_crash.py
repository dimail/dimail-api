"""Get the logs of a crash
"""
import fastapi
import logging

from ... import auth, logs, web_models
from .. import routers


@routers.logs.get("/crashes/{crash_name}")
async def get_crash(
    crash_name: str,
    user: auth.DependsBasicUser,
) -> web_models.ServerLogs:
    log = logging.getLogger(__name__)
    if not user.can_get_server_logs():
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    crash_lines = logs.get_crash_file(crash_name)

    return web_models.ServerLogs(
        logs = crash_lines
    )

