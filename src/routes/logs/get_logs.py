"""Get logs route.
"""
import fastapi
import logging

from ... import auth, remote, web_models
from .. import routers


@routers.logs.get("/")
async def get_logs(
    user: auth.DependsBasicUser,
) -> web_models.ServerLogs:
    """Route to get the logs from the API server.

    Args:
        user (User): the user requesting the status

    Raises:
        HTTPException: if the user is not a basic user
    """
    log = logging.getLogger(__name__)
    if not user.can_get_server_logs():
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    logs = []
    try:
        if remote.fake_mode():
            logs = "Ce sont les logs du serveur API\n"
        else:
            api_server = remote.get_host_by_role("api")
            logs = remote.run_on_host(api_server, [ "/usr/bin/api_get_logs.sh" ])
    except Exception as e:
        log.info(f"FAILED to fetch logs with exception '{e}'")
        raise fastapi.HTTPException(status_code=500, detail="Failed to fetch logs")

    return web_models.ServerLogs(
        logs = logs.split("\n"),
    )
