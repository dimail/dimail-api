"""Get a list of the crashes.
"""
import fastapi
import logging
import os.path

from ... import auth, logs, web_models
from .. import routers


@routers.logs.get("/crashes")
async def get_crashes(
    user: auth.DependsBasicUser,
) -> list[web_models.CrashInfo]:
    log = logging.getLogger(__name__)
    if not user.can_get_server_logs():
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    crash_files = logs.list_crash_files()
    return [
        web_models.CrashInfo(
            name = x["name"],
            size = x["size"],
            date = x["date"],
        ) for x in crash_files
    ]

