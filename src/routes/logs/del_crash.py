"""Delete the logs of a crash
"""
import fastapi
import logging

from ... import auth, logs, web_models
from .. import routers


@routers.logs.delete(
    "/crashes/{crash_name}",
    status_code=204,
    description="Delete the log file produced during a crash."
)
async def del_crash(
    crash_name: str,
    user: auth.DependsBasicUser,
) -> None:
    log = logging.getLogger(__name__)
    if not user.can_get_server_logs():
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    logs.del_crash_file(crash_name)
    return None

