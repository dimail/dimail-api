# ruff: noqa: E712
import fastapi.testclient
import pytest
import unittest

from .. import sql_api
from .. import sql_dkim

@pytest.mark.asyncio
@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "domain_web",
    ["tutu.net:dimail"],
    indirect=True,
)
async def test_domains__get_domain_allowed_user(db_api, db_dovecot, log, async_client, normal_user, domain_web):
    """When being allowed on a domain, user can get domains details."""

    token = normal_user["token"]
    domain_name = domain_web["name"]

    # Get domain
    response = await async_client.get(
        f"/domains/{domain_name}",
        headers={"Authorization": f"Bearer {token}"}
    )

    assert response.status_code == fastapi.status.HTTP_200_OK
    no_test = {"code": "no_test", "detail": "Did not check yet"}
    assert response.json() == {
        "name": domain_name,
        "valid": False,
        "state": "new",
        "delivery": "virtual",
        "features": ["mailbox", "webmail"],
        "transport": None,
        "webmail_domain": None,
        "imap_domain": None,
        "smtp_domain": None,
        "context_name": "dimail",
        "domain_exist": {"ok": False, "errors": [no_test], "internal": False},
        "mx": {"ok": False, "errors": [no_test], "internal": False},
        "cname_imap": {"ok": False, "errors": [no_test], "internal": False},
        "cname_smtp": {"ok": False, "errors": [no_test], "internal": False},
        "cname_webmail": {"ok": False, "errors": [no_test], "internal": False},
        "spf": {"ok": False, "errors": [no_test], "internal": False},
        "dkim": {"ok": False, "errors": [no_test], "internal": False},
        "postfix": {"ok": False, "errors": [no_test], "internal": True},
        "ox": {"ok": False, "errors": [no_test], "internal": True},
        "cert": {"ok": False, "errors": [no_test], "internal": True},
    }


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "domain_web",
    ["tutu.net:dimail"],
    indirect=True,
)
async def test_update_domain(db_api, db_dovecot, db_postfix, log, async_client, domain_web, admin):
    """Checks the calls to patch_domain."""

    domain_name = domain_web["name"]
    admin_auth = (admin["user"], admin["password"])

    response = await async_client.patch(
        f"/domains/{domain_name}",
        json={
            "imap_domain": "imap.example.com",
            "smtp_domain": "smtp.example.com",
            "webmail_domain": "webmail.example.com",
            "delivery": "alias",
            "features": [ "mailbox" ],
        },
        auth=admin_auth,
        #headers={"Authorization": f"Bearer {admin_token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    got = response.json()
    assert got["delivery"] == "alias"
    assert got["imap_domain"] == "imap.example.com"
    assert got["smtp_domain"] == "smtp.example.com"
    assert got["webmail_domain"] == "webmail.example.com"

    # Un update sans infos ne change rien
    response = await async_client.patch(
        f"/domains/{domain_name}",
        json={},
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    got = response.json()
    assert got["imap_domain"] == "imap.example.com"
    assert got["smtp_domain"] == "smtp.example.com"
    assert got["webmail_domain"] == "webmail.example.com"

    # Un update avec une chaine vide retire les valeurs
    response = await async_client.patch(
        f"/domains/{domain_name}",
        json={
            "delivery": "virtual",
            "imap_domain": "",
            "smtp_domain": "",
            "webmail_domain": "",
        },
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    got = response.json()
    assert got["delivery"] == "virtual"
    assert got["imap_domain"] is None
    assert got["smtp_domain"] is None
    assert got["webmail_domain"] is None



@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "domain",
    ["example.com"],
    indirect=True,
)
def test_domains__get_domain_not_authorized(db_dovecot, normal_user, domain, client):
    """Cannot access details to a domain to which you have no allows."""

    token = normal_user["token"]
    domain_name = domain["name"]
    # Access is not granted to this user
    response = client.get(f"/domains/{domain_name}/", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN
    assert response.json() == {"detail": "Permission denied"}


@pytest.mark.parametrize(
    "domain",
    ["example.com"],
    indirect=True,
)
def test_domains__get_domain_admin_always_authorized(domain, admin, client):
    """Admin can access details to all domains."""
    domain_name = domain["name"]
    domain_features = domain["features"]

    # Get a token for the admin user
    response = client.get("/token/", auth=(admin["user"], admin["password"]))
    token = response.json()["access_token"]

    # Admin user is not given allows to any domain, but still can get
    # access to the details of the domain
    response = client.get(f"/domains/{domain_name}/", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == fastapi.status.HTTP_200_OK
    no_test = {"code": "no_test", "detail": "Did not check yet"}
    assert response.json() == {
        "name": domain_name,
        "valid": False,
        "state": "new",
        "delivery": "virtual",
        "features": domain_features,
        "transport": None,
        "webmail_domain": None,
        "imap_domain": None,
        "smtp_domain": None,
        "context_name": None,
        "domain_exist": {"ok": False, "errors": [no_test], "internal": False},
        "mx": {"ok": False, "errors": [no_test], "internal": False},
        "cname_imap": {"ok": False, "errors": [no_test], "internal": False},
        "cname_smtp": {"ok": False, "errors": [no_test], "internal": False},
        "cname_webmail": {"ok": False, "errors": [no_test], "internal": False},
        "spf": {"ok": False, "errors": [no_test], "internal": False},
        "dkim": {"ok": False, "errors": [no_test], "internal": False},
        "postfix": {"ok": False, "errors": [no_test], "internal": True},
        "ox": {"ok": False, "errors": [no_test], "internal": True},
        "cert": {"ok": False, "errors": [no_test], "internal": True},
    }

    # If the domain does not exist -> not found
    response = client.get("/domains/unknown_domain/", headers={"Authorization": f"Bearer {token}"})
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND


@pytest.mark.parametrize(
    "domain",
    ["example.com"],
    indirect=True,
)
@pytest.mark.parametrize(
    "virgin_user",
    ["bob:toto"],
    indirect=True,
)
def test_domains_delete_failed(admin, virgin_user, log, client, domain):
    admin_auth=(admin["user"], admin["password"])
    user_auth=(virgin_user["user"], virgin_user["password"])

    # For a domain that does not exists -> not found
    response = client.delete(
        "/domains/not-a-domain",
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # For a user who is not allowed -> permission denied
    response = client.delete(
        "/domains/example.com",
        auth=user_auth,
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # When allowed, we have a internal error (not yep implemented)
    response = client.delete(
        "/domains/example.com",
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_501_NOT_IMPLEMENTED

@pytest.mark.parametrize(
    "domain",
    ["example.com"],
    indirect=True,
)
@pytest.mark.parametrize(
    "virgin_user",
    ["bob:toto"],
    indirect=True,
)
def test_domains_create_failed(db_api_session, admin, virgin_user, log, client, domain):
    admin_auth=(admin["user"], admin["password"])
    user_auth=(virgin_user["user"], virgin_user["password"])

    # A domain with the feature 'webmail' MUST have a context_name
    response = client.post("/domains/",
                json={"name": "new.com", "delivery": "virtual", "features": ["mailbox", "webmail"], "context_name": None},
                auth=admin_auth
        )
    assert response.status_code == fastapi.status.HTTP_409_CONFLICT

    # If the domain already exists -> conflict
    response = client.post("/domains/",
                json={
                    "name": "example.com",
                    "delivery": "virtual",
                    "features": ["mailbox", "webmail"],
                    "context_name": "dimail"
                },
                auth=admin_auth
           )
    assert response.status_code == fastapi.status.HTTP_409_CONFLICT

    # If the normal user tries to create a domain -> permission denied
    # (already tested in test_perms)
    response = client.post("/domains/",
                json={
                    "name": "new.net",
                    "delivery": "virtual",
                    "features": ["mailbox", "webmail"],
                    "context_name": "dimail"
                },
                auth=user_auth
            )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "domain_web",
    ["example.com:dimail"],
    indirect=True
)
async def test_domains_check_domain(admin, log, async_client, normal_user, domain_web):
    auth=(admin["user"], admin["password"])

    domain_name = domain_web["name"]
    response = await async_client.get(f"/domains/{domain_name}/check", auth=auth)
    assert response.status_code == fastapi.status.HTTP_200_OK
    infos = response.json()
    assert infos["name"] == "example.com"
    assert infos["state"] == "broken"
    assert infos["valid"] == False
    for key in [ "domain_exist", "mx", "cname_imap", "cname_smtp", "cname_webmail", "spf", "dkim" ]:
        assert key in infos
        assert "ok" in infos[key]
        assert "errors" in infos[key]
        if infos[key]["ok"]:
            assert len(infos[key]["errors"]) == 0
        else:
            assert len(infos[key]["errors"]) > 0
    assert infos["domain_exist"]["ok"] is True
    assert infos["mx"]["ok"] is False
    assert len(infos["mx"]["errors"]) == 1
    assert infos["mx"]["errors"][0]["code"] == "wrong_mx"

    assert infos["cname_imap"]["ok"] is False
    assert len(infos["cname_imap"]["errors"]) == 1
    assert infos["cname_imap"]["errors"][0]["code"] == "no_cname_imap"

    assert infos["cname_smtp"]["ok"] is False
    assert len(infos["cname_smtp"]["errors"]) == 1
    assert infos["cname_smtp"]["errors"][0]["code"] == "no_cname_smtp"

    assert infos["cname_webmail"]["ok"] is False
    assert len(infos["cname_webmail"]["errors"]) == 1
    assert infos["cname_webmail"]["errors"][0]["code"] == "no_cname_webmail"

    assert infos["spf"]["ok"] is False
    assert infos["dkim"]["ok"] is False
    assert len(infos["dkim"]["errors"]) == 1
    assert infos["dkim"]["errors"][0]["code"] == "wrong_dkim"


def test_domains_spec_domain(db_dkim_session, admin, log, client):
    auth=(admin["user"], admin["password"])

    payload = {
        "name":         "sub.top.org",
        "delivery":     "virtual",
        "features":     ["mailbox", "webmail"],
        "context_name": "dimail",
    }
    response = client.post("/domains", json=payload, auth=auth)
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Replace public key with constant value
    domain = sql_dkim.get_domain(
        db_dkim_session,
        "sub.top.org"
    )
    sql_dkim.update_domain(
        db_dkim_session,
        "sub.top.org",
        selector = domain.selector,
        private = domain.private,
        public = "v=DKIM1; h=sha256; k=rsa; p=xxx"
    )

    response = client.get(f"/domains/sub.top.org/spec", auth=auth)
    infos = response.json()

    # Compare deux listes indépendament de l'ordre (et pas juste le nombre
    # d'éléments, cf doc de Counter()
    ut = unittest.TestCase()
    ut.maxDiff = None
    ut.assertCountEqual(infos, [
        {"target":"dimail._domainkey","type":"txt","value":"v=DKIM1; h=sha256; k=rsa; p=xxx"},
        # {"target":"_dmarc","type":"txt","value":"v=DMARC1; p=reject; rua=mailto:abuse@mail.numerique.gouv.fr; adkim=s; aspf=s;"},
        {"target":"imap","type":"cname","value":"imap.ox.numerique.gouv.fr."},
        {"target":"smtp","type":"cname","value":"smtp.ox.numerique.gouv.fr."},
        {"target":"webmail","type":"cname","value":"webmail.ox.numerique.gouv.fr."},
        {"target":"","type":"mx","value":"mx.ox.numerique.gouv.fr."},
        {"target":"","type":"txt","value":"v=spf1 include:_spf.ox.numerique.gouv.fr -all"},
    ])

@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
def test_domains_fix_domain(db_api_session, admin, log, client, normal_user):
    auth=(admin["user"], admin["password"])

    # On crée le domaine à la main pour qu'il soit cassé
    sql_api.create_domain(
        db_api_session,
        name = "example.com",
        features = [ "mailbox", "webmail" ],
    )

    response = client.get("/domains/example.com/check",
        auth = auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body["dkim"]["ok"] is False
    assert body["dkim"]["errors"][0]["code"] == "no_key"
    assert body["postfix"]["ok"] is False
    assert body["postfix"]["errors"][0]["code"] == "no_delivery"
    assert body["context_name"] == "broken"
    assert body["ox"]["ok"] is False
    assert body["ox"]["errors"][0]["code"] == "no_context"

    response = client.get("/domains/example.com/fix",
        auth = auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    response = client.get("/domains/example.com/check",
        auth = auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body["dkim"]["ok"] is False
    assert body["dkim"]["errors"][0]["code"] == "wrong_dkim"
    assert body["postfix"]["ok"] is True
    assert body["ox"]["ok"] is True
    assert body["context_name"] == "fix_auto_context"


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "normal_user",
    ["login:pass"],
    indirect=True,
)
@pytest.mark.parametrize(
    "domain_web",
    ["example.com:ctx1;example.org:ctx2;example.net:ctx3"],
    indirect=True,
)
async def test_ctx_creation(normal_user, domain_web):
    assert True


