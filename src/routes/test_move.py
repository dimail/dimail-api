import fastapi
from .. import others

def test_post_database(log, client, admin):
    admin_auth = (admin["user"], admin["password"])

    response = client.post(
        f"/system/database/ox_test_database/pas_encore",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert json["name"].startswith("ox_test_database_")
    assert not json["name"] == "ox_test_database_5"
    new_name = json["name"]

    response = client.post(
        f"/system/database/ox_test_database/{new_name}",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert json["name"] == new_name


def test_check_contexts(log, client, admin):
    admin_auth = (admin["user"], admin["password"])

    # Je fais mon contrôle pour un contexte...
    response = client.post(
        "/system/check_contexts",
        json = {
            "contexts": [{
                "cid": "42",
                "name": "coincoin",
                "domains": ["example.com", "example.net"],
                "schema_name": "trouloulou",
            }],
        },
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    # ... qui est tout à fait disponible
    json = response.json()
    assert len(json["contexts"]) == 1
    assert json["contexts"][0]["comment"] == []
    assert json["contexts"][0]["status"] == "available"
    assert json["schemas"] == {}

    # Je crée un schéma d'après le nom "trouloulou" chez moi
    response = client.post(
        f"/system/database/ox_test_database/trouloulou",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    new_name = json["name"]

    # Puis je crée mes deux domaines dans le contexte qui va dans le schéma que
    # je viens de créer
    response = client.post(
        f"/domains/",
        json = {
            "name": "example.com",
            "delivery": "virtual",
            "features": [ "mailbox", "webmail" ],
            "context_id": "42",
            "context_name": "coincoin",
            "schema_name": new_name
        },
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    response = client.post(
        f"/domains/",
        json = {
            "name": "example.net",
            "delivery": "virtual",
            "features": [ "mailbox", "webmail" ],
            "context_id": "42",
            "context_name": "coincoin",
            "schema_name": new_name
        },
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Puis je re-demande le check de mon contexte...
    response = client.post(
        "/system/check_contexts",
        json = {
            "contexts": [{
                "cid": "42",
                "name": "coincoin",
                "domains": ["example.com", "example.net"],
                "schema_name": "trouloulou",
            }],
        },
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    # ... qui doit arriver en "same", avec un schéma dans le mapping
    json = response.json()
    assert len(json["contexts"]) == 1
    assert json["contexts"][0]["comment"] == []
    assert json["contexts"][0]["status"] == "same"
    assert json["schemas"] == { "trouloulou": new_name }




def test_move_from(log, client, admin, set_others):
    # Le scénario est celui où nous recevons un domaine dans le
    # cadre d'un transfert (nous sommes le 'to' du move_from)
    admin_auth = (admin["user"], admin["password"])

    response = client.post(
        f"/system/database/ox_test_database/pas_encore",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    new_name = json["name"]


    response = client.post(
        f"/domains/",
        json = {
            "name": "example.com",
            "delivery": "virtual",
            "features": [ "mailbox", "webmail" ],
            "context_name": "example.com",
            "schema_name": new_name,
        },
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    response = client.patch(
        f"/system/import_domain/example.com",
        json = {
            "moving_from": "recent",
            "moving_mailboxes": [ {
                "username": "joe",
                "active": "Y",
                "password": "{PLAIN]dalton",
            } ],
            "moving_aliases": [ {
                "alias": "joe.dalton@example.com",
                "destination": "joe@example.com",
            } ],
            "moving_senders": [ {
                "login": "joe@example.com",
                "sender": "joe.dalton@example.com",
            } ],
            "moving_dkim": {
                "selector": "ox3615",
                "private": "This a secret private key",
                "public": "The public dns record",
            },
        },
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

def test_move_to(log, client, admin_token, set_others):
    # Le scénario est celui où on a un domaine qu'on va déménager
    # vers quelqu'un (nous sommes le 'from' du move_from)
    admin_auth = (admin_token["user"], admin_token["password"])
    token = admin_token["token"]

    response = client.post(
        f"/system/database/ox_test_database/pas_encore",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    new_name = json["name"]

    response = client.post(
        f"/domains/",
        json = {
            "name": "example.com",
            "delivery": "virtual",
            "features": [ "mailbox", "webmail" ],
            "context_name": "example.com",
            "schema_name": new_name,
        },
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    response = client.post(
        f"/domains/example.com/mailboxes/joe",
        json = {
            "givenName": "Joe",
            "surName": "Dalton",
            "displayName": "Joe Dalton",
        },
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    response = client.post(
        f"/domains/example.com/mailboxes/many",
        json = {
            "givenName": "Ma",
            "surName": "Dalton",
            "displayName": "Ma Dalton",
        },
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    response = client.post(
        "/domains/example.com/aliases/",
        json = {
            "user_name": "joe.dalton",
            "destination": "joe@example.com",
            "allow_to_send": True,
        },
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Avant de bouger le domaine, il est dans la liste
    response = client.get(
        "/domains",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert len(json) == 1
    assert json[0]["name"] == "example.com"

    response = client.patch(
        "/system/database/ox_test_database",
        json = {
            "move_to": "recent",
        },
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # En GET, le domaine est Gone
    response = client.get(
        "/domains/example.com",
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_410_GONE

    # En PATCH, le domaine est Gone
    response = client.patch(
        "/domains/example.com",
        json = { },
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_410_GONE

    # Quand on liste les domaines, il a disparu
    response = client.get(
        "/domains",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert len(json) == 0

    # Si je delete la mailbox, le domaine est Gone
    response = client.delete(
        "/domains/example.com/mailboxes/joe",
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_410_GONE

    # Si je get la mailbox, le domaine est Gone
    response = client.get(
        "/domains/example.com/mailboxes/joe",
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_410_GONE

    # Si je liste les mailboxes, le domaine est Gone
    response = client.get(
        "/domains/example.com/mailboxes",
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_410_GONE

    # Si je modifie la mailbox, le domaine est Gone
    response = client.patch(
        "/domains/example.com/mailboxes/joe",
        json = {},
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_410_GONE

    # Si je crée une mailbox, le domaine est Gone
    response = client.post(
        "/domains/example.com/mailboxes/jack",
        json = {
            "givenName": "Jack",
            "surName": "Dalton",
            "displayName": "Jack Dalton",
        },
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_410_GONE

    # Si je delete l'alias, le domaine est Gone
    response = client.delete(
        "/domains/example.com/aliases/joe.dalton/joe@example.com",
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_410_GONE

    # Si je get l'alias, le domaine est Gone
    response = client.get(
        "/domains/example.com/aliases",
        params={
            "user_name": "joe.dalton",
            "destination": "joe@example.com",
        },
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_410_GONE

    # Si je liste les aliases, le domaine est Gone
    response = client.get(
        "/domains/example.com/aliases",
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_410_GONE

    # Si je patch l'alias, le domaine est Gone
    response = client.patch(
        "/domains/example.com/aliases/joe.dalton/joe.dalton@example.com",
        json = {},
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_410_GONE

    # Si je crée un alias, le domaine est Gone
    response = client.post(
        "/domains/example.com/aliases/",
        json = {
            "user_name": "jack.dalton",
            "destination": "jack@example.com",
            "allow_to_send": True,
        },
        headers = {"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_410_GONE

    # On va chercher le client vers "others"
    dest_clt = others.get_other("recent").client
    dest_token = dest_clt.get_token()
    response = dest_clt.client.get(
        "/system/move_mailbox/example.com/joe",
        headers = {"Authorization": f"Bearer {dest_token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Sur la boite many@example.com, les scripts en mode fake vont répondre qu'il reste
    # deux mails en attente. Donc le déplacement va échouer.
    response = dest_clt.client.get(
        "/system/move_mailbox/example.com/many",
        headers = {"Authorization": f"Bearer {dest_token}"},
    )
    assert response.status_code == fastapi.status.HTTP_424_FAILED_DEPENDENCY

    
