import pytest

import fastapi

@pytest.mark.asyncio
@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
@pytest.mark.parametrize(
    "virgin_user",
    ["virgin:empty"],
    indirect=True,
)
@pytest.mark.parametrize(
    "domain_web",
    ["tutu.net:dimail"],
    indirect=True,
)
async def test_sender_on_alias(
        async_client,
        normal_user,
        virgin_user,
        domain_web,
        db_postfix
    ):

    token = normal_user["token"]
    virgin_token = virgin_user["token"]
    domain_name = domain_web["name"]

    # from@tutu.net -> other@tutu.net, allow_to_send = True
    response = await async_client.post(
        f"/domains/{domain_name}/aliases/",
        json = {
            "user_name": "from",
            "destination": f"other@{domain_name}",
            "allow_to_send": True,
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    print(f"{response.json()}")
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # from@tutu.net -> palui@tutu.net, allow_to_send = False
    response = await async_client.post(
        f"/domains/{domain_name}/aliases/",
        json = {
            "user_name": "from",
            "destination": f"palui@{domain_name}",
            "allow_to_send": False,
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Si je crée la mailbox other@tutu.net... (sans préciser de additionalSenders)
    response = await async_client.post(
        f"/domains/{domain_name}/mailboxes/other",
        json = {
            "givenName": "Olivier",
            "surName": "Ther",
            "displayName": "Other",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # elle récupère le additional sender from@tutu.net
    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/other",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body["additionalSenders"] == [ f"from@{domain_name}" ]


    # Si je crée palui@tutu.net... (sans préciser de additionalSeders)
    response = await async_client.post(
        f"/domains/{domain_name}/mailboxes/palui",
        json = {
            "givenName": "Pal",
            "surName": "Ui",
            "displayName": "Palui",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # elle ne récupère PAS de additional sender
    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/palui",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body["additionalSenders"] == [ ]

    # Maintenant, on test quand on force les senders à la création de la mailbox 
    # from2@tutu.net -> rother@tutu.net, allow_to_send = True
    response = await async_client.post(
        f"/domains/{domain_name}/aliases/",
        json = {
            "user_name": "from2",
            "destination": f"rother@{domain_name}",
            "allow_to_send": True,
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    print(f"{response.json()}")
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # from2@tutu.net -> toujourspalui@tutu.net, allow_to_send = False
    response = await async_client.post(
        f"/domains/{domain_name}/aliases/",
        json = {
            "user_name": "from2",
            "destination": f"toujourspalui@{domain_name}",
            "allow_to_send": False,
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Si je crée la mailbox rother@tutu.net... (en donnant le additionalSenders)
    response = await async_client.post(
        f"/domains/{domain_name}/mailboxes/rother",
        json = {
            "givenName": "Roger",
            "surName": "Ther",
            "displayName": "Rother",
            "additionalSenders": [],
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # elle NE récupère PAS le additional sender from2@tutu.net
    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/rother",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body["additionalSenders"] == [ ]

    # quand on GET sur l'alias from2 -> rother, allow_to_send est devenu False
    response = await async_client.get(
        f"/domains/{domain_name}/aliases/",
        params = {
            "user_name": "from2",
            "destination": f"rother@{domain_name}",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert len(body) == 1
    assert body[0]["allow_to_send"] == False

    # Si je crée toujourspalui@tutu.net... (en précisant le additionalSeders)
    response = await async_client.post(
        f"/domains/{domain_name}/mailboxes/toujourspalui",
        json = {
            "givenName": "Tou",
            "surName": "Jourpaslui",
            "displayName": "Toujourspaslui",
            "additionalSenders": [ f"from@{domain_name}" ],
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # elle ne récupère PAS de additional sender venant de l'alias
    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/toujourspalui",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body["additionalSenders"] == [ f"from@{domain_name}" ]

    # from@tutu.net est dans les senders de deux mailbox:
    # - toujourspaslui@tutu.net (qui n'est pas destinataire de l'alias
    # - other@tutu.net (qui est destinataire)
    # Si je supprimer toutes les destinations de l'alias, seul other va
    # perdre son sender
    response = await async_client.delete(
        f"/domains/{domain_name}/aliases/from/all",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_204_NO_CONTENT

    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/other",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body["additionalSenders"] == []

    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/toujourspalui",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body["additionalSenders"] == [ f"from@{domain_name}" ]
    # En plus, c'est cohérent entre OX et postfix, les deux acceptent ce sender
    # (qui est un alias qui n'existe plus)
    assert body["status"] == "ok"
        
    # Il nous reste from2 -> rother, qui n'a pas le allow_to_send
    # Si j'ajoute le allow_to_send à l'alias...
    response = await async_client.patch(
        f"/domains/{domain_name}/aliases/from2/rother@{domain_name}",
        json = {
            "allow_to_send": True,
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # ... la mailbox récupère le sender
    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/rother",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body["additionalSenders"] == [ f"from2@{domain_name}" ]
    assert body["status"] == "ok"

    # Si on retire le allow_to_send de l'alias...
    response = await async_client.patch(
        f"/domains/{domain_name}/aliases/from2/rother@{domain_name}",
        json = {
            "allow_to_send": False,
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # ... la mailbox perd le sender
    response = await async_client.get(
        f"/domains/{domain_name}/mailboxes/rother",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    body = response.json()
    assert body["additionalSenders"] == []
    assert body["status"] == "ok"
