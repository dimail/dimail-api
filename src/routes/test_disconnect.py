
import fastapi

def test_disconnect(setup_remote, client, admin):
    admin_auth = (admin["user"], admin["password"])

    response = client.get(
        "/system/disconnect/exmaple.com/essai",
        auth = ( "pasbien", "pasbien" ),
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    response = client.get(
        "/system/disconnect/example.com/test",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    response = client.get(
        "/system/disconnect/example.com/many",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK


