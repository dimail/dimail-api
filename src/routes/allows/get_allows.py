import fastapi

from ... import auth, sql_api, web_models
from .. import dependencies, routers


@routers.allows.get(
    "/",
    response_model=list[web_models.Allowed],
    responses={
        200: {"description": "Allows"},
    },
    status_code=200,
    description="Get all allows",
)
async def get_allows(
    api: dependencies.DependsApiDb,
    user: auth.DependsBasicUser,
    username: str = "",
    domain: str = "",
) -> list[web_models.Allowed]:
    """Get all allows in a domain.

    To get all allows in a domain, you must be an admin user.

    Args:
        api (dependencies.DependsApiDb): Database session
        user (auth.DependsBasicUser): User credentials
        username (str): User name
        domain (str): Domain name

    Returns:
        list[web_models.Allowed]: List of allows

    Raises:
        No raises.

    See Also:
        * https://fastapi.tiangolo.com/tutorial/path-params
        * https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt
        * https://fastapi.tiangolo.com/tutorial/security/simple-verify-token

    Dependencies:
        auth.DependsBasicUser
        dependencies.DependsApiDb
        sql_api.get_allows
    """
    # FIXME Quel droit tester ?
    # Si je ne donne aucun argument, je vais lister tous les allows de tous les domaines
    # Si je donne un domaine, je liste les utilisateurs ayant des droits dessus
    # Si je donne un utilisateur, je liste tous ses domaines
    if not user.is_admin:
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    allows = sql_api.get_allows(api, username, domain)
    return [web_models.Allowed(user = allow.user, domain = allow.domain) for allow in allows]
