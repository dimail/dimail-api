import logging

import fastapi

from ... import auth, sql_api
from .. import dependencies, routers


@routers.allows.delete(
    "/{domain_name}/{user_name}",
    status_code=204,
    description="Remove user allows of a domain",
    responses={
        204: {"description": "Deleted"},
        404: {"description": "Domain not found"
                             "Queried user does not have permissions for this domain."},
    },
)
async def delete_allow(
    api: dependencies.DependsApiDb,
    user: auth.DependsBasicUser,
    domain_name: str,
    user_name: str,
) -> None:
    """Remove user allows of a domain.

    To remove a user's allows of a domain, you must be an admin user.

    Args:
        api (dependencies.DependsApiDb): Database session
        user (auth.DependsBasicUser): User credentials
        domain_name (str): Domain name
        user_name (str): User name

    Returns:
        None: No content

    Raises:
        fastapi.HTTPException: Domain not found
        fastapi.HTTPException: Not found
        fastapi.HTTPException: Queried user does not have permissions for this domain.

    See Also:
        * https://fastapi.tiangolo.com/tutorial/path-params
        * https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt
        * https://fastapi.tiangolo.com/tutorial/security/simple-verify-token

    Dependencies:
        auth.DependsBasicUser
        dependencies.DependsApiDb
        routers
        sql_api.deny_domain_for_user
        sql_api.get_allowed
        sql_api.get_domain
        sql_api.get_user
    """
    log = logging.getLogger(__name__)

    # Attention : on contrôle les droits avant de tester si le domaine existe, mais
    # après le lookup DB, c'est plus rapide, et ça fait qu'un admin ne va pas avoir
    # un 403 sur un domaine qui n'existe pas, mais bien un 404.
    # A l'opposé, un non-admin aura un 403 au lieu d'un 404 (il ne peut pas faire la
    # différence entre un domaine qui lui est interdit et un domaine qui n'existe pas)
    db_domain = sql_api.get_domain(api, domain_name)
    if not user.can_allow_for_domain(db_domain):
        log.info(f"Cet utilisateur n'a pas les droits sur le domaine {domain_name} pour changer les allow")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if db_domain is None:
        log.info(f"Le domaine {domain_name} n'existe pas dans la base API")
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    db_user = sql_api.get_user(api, user_name)
    if db_user is None:
        raise fastapi.HTTPException(status_code=404, detail="User not found")

    db_allowed = sql_api.get_allowed(api, user_name, domain_name)
    if db_allowed is None:
        raise fastapi.HTTPException(
            status_code=404,
            detail="Queried user does not have permissions for this domain.",
        )

    return sql_api.deny_domain_for_user(api, user=user_name, domain=domain_name)
