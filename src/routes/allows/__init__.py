# ruff: noqa: E402
"""This module contains the allows routes.

The allows routes are used to manage allows.

Permitted roles:
    * admin

The allows routes are:
    * DELETE /allows/{user_name}
    * GET /allows
    * POST /allows
"""
from .delete_allow import delete_allow
from .post_allow import post_allow
from .get_allows import get_allows

__all__ = [
    delete_allow,
    get_allows,
    post_allow,
]

