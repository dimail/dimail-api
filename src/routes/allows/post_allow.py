import logging

import fastapi

from ... import auth, sql_api, web_models
from .. import dependencies, routers


@routers.allows.post(
    "/",
    status_code=201,
    response_model=web_models.Allowed,
    responses={
        201: {"description": "Allow created"},
        403: {"description": "Permission denied"},
        404: {"description": "Domain not found"},
        409: {"description": "Domain already allowed for this user"},
    },
    description="Give allows of a domain to a user",
)
async def post_allow(
    api: dependencies.DependsApiDb,
    user: auth.DependsBasicUser,
    allow: web_models.Allowed,
) -> web_models.Allowed:
    """Give ownership of a domain to a user.

    To give ownership of a domain to a user, you must be an admin user.

    Args:
        api (dependencies.DependsApiDb): Database session
        user (auth.DependsBasicUser): User credentials
        allow (web_models.Allowed): Allow information

    Returns:
        web_models.Allowed: Allowed information

    Raises:
        fastapi.HTTPException: Domain not found
        fastapi.HTTPException: User not found
        fastapi.HTTPException: Domain already allowed for this user

    See Also:
        * https://fastapi.tiangolo.com/tutorial/path-params
        * https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt
        * https://fastapi.tiangolo.com/tutorial/security/simple-verify-token

    Dependencies:
        auth.DependsBasicUser
        dependencies.DependsApiDb
        routers
        sql_api.allow_domain_for_user
        sql_api.get_allowed
        sql_api.get_domain
        sql_api.get_user
    """
    log = logging.getLogger(__name__)
    # Attention : on contrôle les droits avant de tester si le domaine existe, mais
    # après le lookup DB, c'est plus rapide, et ça fait qu'un admin ne va pas avoir
    # un 403 sur un domaine qui n'existe pas, mais bien un 404.
    # A l'opposé, un non-admin aura un 403 au lieu d'un 404 (il ne peut pas faire la
    # différence entre un domaine qui lui est interdit et un domaine qui n'existe pas)
    db_domain = sql_api.get_domain(api, allow.domain)
    if not user.can_allow_for_domain(db_domain):
        log.info(f"Cet utilisateur n'a pas les droits sur le domaine {allow.domain} pour changer les allow")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if db_domain is None:
        log.info(f"Le domaine {allow.domain} n'existe pas dans la base API")
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    db_user = sql_api.get_user(api, allow.user)
    if db_user is None:
        raise fastapi.HTTPException(status_code=404, detail="User not found")

    db_allowed = sql_api.get_allowed(api, allow.user, allow.domain)
    if db_allowed is not None:
        raise fastapi.HTTPException(
            status_code=409, detail="Domain already allowed for this user"
        )

    db_allowed = sql_api.allow_domain_for_user(api, user=allow.user, domain=allow.domain)
    return web_models.Allowed(
        user = db_allowed.user,
        domain = db_allowed.domain,
    )
