import fastapi

from .. import dns

def test_get_tech_domain(log, client, admin):

    previous = dns.get_tech_domain()
    assert previous == "ox.numerique.gouv.fr"

    auth = (admin["user"], admin["password"])
    response = client.get(
        "/system/tech_domain/",
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert json["host_name"] == "Sait pas"
    assert json["tech_name"] == previous

    dns.set_tech_domain("toto.gouv.fr")

    response = client.get(
        "/system/tech_domain",
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert json["tech_name"] == "toto.gouv.fr"

    dns.set_tech_domain(previous)
