import fastapi

from ... import auth, business, dkcli, enums, oxcli, sql_api, sql_dkim, sql_postfix, web_models
from .. import dependencies, routers


@routers.domains.post(
    "/",
    status_code=fastapi.status.HTTP_201_CREATED,
    summary="Create a domain",
    description="Create a new domain",
    response_model=web_models.Domain,
)
async def post_domain(
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,
    user: auth.DependsBasicUser,
    domain: web_models.CreateDomain,
    bg: fastapi.BackgroundTasks,
    no_check: str = "false",
) -> web_models.Domain:
    if not user.can_create_domain():
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    api_domain = sql_api.get_domain(api, domain.name)
    if api_domain is not None:
        raise fastapi.HTTPException(status_code=409, detail="Domain already exists")

    if domain.delivery not in enums.Delivery:
        raise fastapi.HTTPException(status_code=422, detail="Delivery attribute is not valid")
    if domain.delivery == "relay" and domain.transport is None:
        raise fastapi.HTTPException(status_code=422, detail="Transport is mandatory when delivery='relay'")

    if domain.context_name is not None:
        if "." in domain.context_name:
            domain.context_name = domain.context_name.replace(".", "_")

    if "webmail" in domain.features:
        if domain.context_name is None:
            raise fastapi.HTTPException(
                status_code=409, detail="OX context name is mandatory for mailbox feature"
            )
        ox_cluster = oxcli.OxCluster()
        ctx = await ox_cluster.get_context_by_domain(domain.name)
        if ctx is not None and ctx.name != domain.context_name:
            raise fastapi.HTTPException(
                status_code=409,
                detail=f"The domain is currently mapped to OX context {ctx.name}",
            )

    # Something...
    b_domain = business.Domain(
        name = domain.name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
    )
    await b_domain.create_domain(
        features = domain.features,
        delivery = domain.delivery,
        transport = domain.transport,
        context_id = domain.context_id,
        context_name = domain.context_name,
        schema_name = domain.schema_name,
    )
    b_domain.update_domains(
        imap_domain = domain.imap_domain,
        smtp_domain = domain.smtp_domain,
        webmail_domain = domain.webmail_domain
    )

    api_domain = sql_api.get_domain(api, domain.name)
    pf_domain  = sql_postfix.get_domain(postfix, domain.name)

    if api_domain is None:
        raise fastapi.HTTPException(status_code=500, detail="Failed to insert the domain in database")
    if not user.is_admin:
        sql_api.allow_domain_for_user(api, user.name, domain.name)
    if no_check == "false":
        bg.add_task(business.background_check_new_domain, domain.name)

    return await b_domain.to_web()
