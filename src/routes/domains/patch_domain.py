import fastapi
import logging

from ... import auth, business, config, enums, others, sql_api, sql_dkim, sql_postfix, utils, web_models
from .. import routers, dependencies

@routers.domains.patch(
    "/{domain_name}",
    response_model = web_models.Domain,
    summary = "Update a domain",
    description = "Update a domain by name",
    status_code = fastapi.status.HTTP_200_OK,
    responses={
        200: {"description": "Domain updated"},
        403: {"description": "Permission denied"},
        404: {"description": "Domain not found in OX"},
        410: {"description": "Domain is moving away to another platform"},
    },
)
async def patch_domain(
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,
    user: auth.DependsBasicUser,
    domain_name: str,
    updates: web_models.UpdateDomain,
) -> web_models.Domain:
    log = logging.getLogger(__name__)
    # Attention : on contrôle les droits avant de tester si le domaine existe, mais
    # après le lookup DB, c'est plus rapide, et ça fait qu'un admin ne va pas avoir
    # un 403 sur un domaine qui n'existe pas, mais bien un 404.
    # A l'opposé, un non-admin aura un 403 au lieu d'un 404 (il ne peut pas faire la
    # différence entre un domaine qui lui est interdit et un domaine qui n'existe pas)
    api_domain = sql_api.get_domain(api, domain_name)
    pf_domain = sql_postfix.get_domain(postfix, domain_name)
    if not user.can_modify_domain(domain_name):
        log.info(f"Cet utilisateur n'a pas les droits pour modifier le domaine {domain_name}")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if api_domain is None:
        log.info(f"Le domaine {domain_name} n'existe pas dans la base API")
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")
    if pf_domain is None:
        log.info(f"Le domaine {domain_name} n'existe pas dans la base Postfix")
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    if updates.delivery is not None:
        if updates.delivery == "relay" and updates.transport is None:
            raise fastapi.HTTPEception(status_code=422, detail="In 'relay' delivery mode, transport is mandatory")

    set_features = []
    if updates.features is not None:
        set_features = utils.read_args_list("features", enums.Feature, api_domain.features, updates.features)
        if set_features is None:
            raise fastapi.HTTPException(status_code=422, detail="Failed to understand the features")

    # All the arguments are OK, we proceed to do the update.
    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
        api_domain = api_domain,
        postfix_domain = pf_domain,
    )

    if b_domain.move_from_me():
        move_to = b_domain.moving_to()
        raise fastapi.HTTPException(
            status_code = fastapi.status.HTTP_410_GONE,
            detail = f"This domain is moving away to platform {move_to}"
        )

    if updates.delivery is not None:
        b_domain.update_delivery(updates.delivery, updates.transport)

    if updates.smtp_domain is not None or updates.imap_domain is not None or updates.webmail_domain is not None:
        b_domain.update_domains(
            imap_domain = updates.imap_domain,
            smtp_domain = updates.smtp_domain,
            webmail_domain = updates.webmail_domain,
        )

    if updates.context_name is not None:
        if "." in updates.context_name:
            updates.context_name = updates.context_name.replace(".", "_")

    if updates.features is not None:
        await b_domain.update_features(set_features, updates.context_name)

    return await b_domain.to_web()
