import fastapi

from ... import auth, business, sql_api, sql_dkim, sql_postfix, web_models
from .. import dependencies, routers


@routers.domains.get(
    "/",
    response_model=list[web_models.Domain],
    summary="Get all domains",
    description="Get all domains",
)
async def get_domains(
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,
    user: auth.DependsBasicUser,
) -> list[web_models.Domain]:
    # FIXME Il faut retravailler ce droit...
    # Un admin peut lister tous les domaines, sans filtre
    # Mais pour un non-admin, il faut filtrer sur ses droits, par exemple
    # via la table des allows.
    if not user.can_list_domains():
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    result = []
    domains = sql_api.get_domains(api)
    for domain in domains:
        b_domain = business.Domain(
            name = domain.name,
            api_session = api,
            dkim_session = dkim,
            dovecot_session = dovecot,
            postfix_session = postfix,
            api_domain = domain,
        )
        if b_domain.move_from_me():
            continue
        web_domain = await b_domain.to_web()
        if web_domain is not None:
            result.append(web_domain)
    return result
