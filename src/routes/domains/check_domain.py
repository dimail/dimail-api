import logging

import fastapi

from ... import auth, business, sql_api, sql_dkim, sql_postfix, web_models
from .. import dependencies, routers


@routers.domains.get("/{domain_name}/check")
async def check_domain(
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,
    user: auth.DependsBasicUser,
    domain_name: str,
) -> web_models.Domain:
    log = logging.getLogger(__name__)

    # Attention : on contrôle les droits avant de tester si le domaine existe, mais
    # après le lookup DB, c'est plus rapide, et ça fait qu'un admin ne va pas avoir
    # un 403 sur un domaine qui n'existe pas, mais bien un 404.
    # A l'opposé, un non-admin aura un 403 au lieu d'un 404 (il ne peut pas faire la
    # différence entre un domaine qui lui est interdit et un domaine qui n'existe pas)
    api_domain = sql_api.get_domain(api, domain_name)
    if not user.can_manage_domain(api_domain):
        log.info(f"Cet utilisateur n'a pas les droits sur le domaine {domain_name}")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if api_domain is None:
        log.info(f"Le domaine {domain_name} n'existe pas dans la base API")
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        postfix_session = postfix,
        api_domain = api_domain,
        dovecot_session = dovecot,
    )
    await b_domain.foreground_check()
    log.info(f"Domain state after check is {api_domain.state}")
    assert api_domain.state in [ "ok", "broken" ]
    return await b_domain.to_web()

