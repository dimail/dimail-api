# ruff: noqa: E402
"""This module contains the domains routes.

The domains routes are used to manage domains.

Permitted roles:
    * admin

The domains routes are:
    * GET /domains/{domain_name}
    * GET /domains
    * POST /domains
"""
from .check_domain import check_domain
from .delete_domain import delete_domain
from .fix_domain import fix_domain
from .get_domain import get_domain
from .get_domains import get_domains
from .patch_domain import patch_domain
from .post_domain import post_domain
from .spec_domain import spec_domain

__all__ = [
    check_domain,
    delete_domain,
    get_domain,
    get_domains,
    patch_domain,
    post_domain,
]

