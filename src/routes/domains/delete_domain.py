import fastapi

from ... import auth, config, sql_api, sql_dkim, sql_postfix
from .. import dependencies, routers

@routers.domains.delete(
    "/{domain_name}",
    status_code=fastapi.status.HTTP_204_NO_CONTENT,
    summary="Delete a domain",
    description="Delete a domain and all its data",
    response_model=None,
)
def delete_domain(
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,
    user: auth.DependsBasicUser,
    domain_name: str,
):
    """Delete a domain by name.

    To delete a domain, you must have special permissions.
    To delete we need to remove all data related to the domain.
        - mailboxes
        - aliases
        - subdomains
        - domain itself
    If the domain has a webmail feature, we need to remove the context from OX.
    To remove the context from OX, we need to remove all mailboxes and aliases from the context.
        - ox users
        - aliases
        - calendars
        - contacts
        - tasks
        - folders
        - settings
        - context itself (if no more domains are mapped to it)

    Args:
        api (dependencies.DependsApiDb): Database session
        user (auth.DependsBasicUser): User credentials
        domain_name (str): Domain name

    Returns:
        None: No content

    Raises:
        fastapi.HTTPException: Domain not found
        fastapi.HTTPException: Not implemented

    See Also:
        * https://fastapi.tiangolo.com/tutorial/path-params
        * https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt
        * https://fastapi.tiangolo.com/tutorial/security/simple-verify-token

    Dependencies:
        dependencies.DependsApiDb
        routers
        sql_api.delete_domain
        sql_api.get_domain
        web_models
    """
    domain_db = sql_api.get_domain(api, domain_name)
    if domain_db is None:
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    if not user.can_delete_domain(domain_name):
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    raise fastapi.HTTPException(status_code=501, detail="Not implemented")

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
    )
    sql_api.delete_domain(api, domain_name)
    return None
