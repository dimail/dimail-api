import logging

import fastapi

from ... import auth, business, sql_api, sql_dkim, sql_postfix, web_models
from .. import dependencies, routers


@routers.domains.get("/{domain_name}/spec")
async def spec_domain(
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,
    domain_name: str,
):
    log = logging.getLogger(__name__)

    # cf. commentaire concernant /check
    api_domain = sql_api.get_domain(api, domain_name)
    if api_domain is None:
        log.info(f"Le domaine {domain_name} n'existe pas dans la base API")
        raise fastapi.HTTPException(status_code=404, detail="Domain not found")

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
        api_domain = api_domain,
    )
    return b_domain.get_specs()
