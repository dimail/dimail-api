import logging

import fastapi

from ... import auth, business, sql_api, sql_dkim, sql_postfix, web_models
from .. import dependencies, routers


@routers.domains.get(
    "/{domain_name}",
    response_model=web_models.Domain,
    summary="Get a domain",
    description="Get a domain by name",
)
async def get_domain(
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,
    user: auth.DependsTokenUser,
    domain_name: str,
) -> web_models.Domain:
    """Get a domain by name.

    Args:
        api (dependencies.DependsApiDb): Database session to API
        postfix (dependencies.DependsPostfixDb): Detabase session to Postfix
        user (auth.DependsTokenUser): User credentials
        domain_name (str): Domain name

    Returns:
        web_models.Domain: Domain information

    Raises:
        fastapi.HTTPException: Domain not found
        fastapi.HTTPException: Not authorized

    See Also:
        * https://fastapi.tiangolo.com/tutorial/path-params
        * https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt
        * https://fastapi.tiangolo.com/tutorial/security/simple-verify-token

    Dependencies:
        dependencies.DependsApiDb
        routers
        sql_api.get_domain
        web_models
    """
    log = logging.getLogger(__name__)

    api_domain = sql_api.get_domain(api, domain_name)
    if api_domain is None:
        log.info(f"Domain {domain_name} not found.")
        raise fastapi.HTTPException(
            status_code = fastapi.status.HTTP_404_NOT_FOUND,
            detail = "Domain not found"
        )

    if not user.can_manage_domain(api_domain):
        log.info(f"Permission denied on domain {domain_name} for user.")
        raise fastapi.HTTPException(
            status_code = fastapi.status.HTTP_403_FORBIDDEN,
            detail = "Permission denied"
        )

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
        api_domain = api_domain,
    )
    if b_domain.move_from_me():
        move_to = b_domain.moving_to()
        raise fastapi.HTTPException(
            status_code = fastapi.status.HTTP_410_GONE,
            detail = f"This domain is moving away to platform {move_to}"
        )
    return await b_domain.to_web()
