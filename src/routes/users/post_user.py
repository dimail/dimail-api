import fastapi
import logging

from ... import auth, sql_api, web_models
from .. import dependencies, routers


@routers.users.post(
    "/",
    status_code=fastapi.status.HTTP_201_CREATED,
    response_model=web_models.User,
    responses={
        201: {"description": "Created"},
        409: {"description": "User already exists"},
    },
    description="Create a user. The password is mandatory, the special value "
    "password=no means the user has no password and cannot be authenticated using a "
    "password (the user can still get a token, from an admin or from his manager if "
    "they are managed).",
)
async def post_user(
    api: dependencies.DependsApiDb,
    user_auth: auth.DependsBasicUser,
    user: web_models.CreateUser,
) -> web_models.User:
    """Create user.

    To create a user, you must have the corresponding privilege (or be an admin).
    To create a user who is an admin, you need special permission, not just user
    creation capability.

    Args:
        api (dependencies.DependsApiDb): Database session
        admin (auth.DependsBasicUser): User credentials
        user (web_models.CreateUser): User data

    Returns:
        web_models.User: Created user

    Raises:
        fastapi.HTTPException: User already exists

    See Also:
        * https://fastapi.tiangolo.com/tutorial/path-params
        * https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt
        * https://fastapi.tiangolo.com/tutorial/security/simple-verify-token

    Dependencies:
        auth.DependsBasicUser
        dependencies.DependsApiDb
        routers
        sql_api.create_user
        sql_api.get_user
        web_models.CreateUser
        web_models.User
    """
    log = logging.getLogger(__name__)
    if not user_auth.can_create_user():
        log.info(f"L'utilisateur {user_auth.name} ne peut pas créer d'utilisateurs")
        raise fastapi.HTTPException(status_code=403, detail="Permisison denied")
    if user.is_admin and not user_auth.can_create_admin():
        log.info(f"L'utilisateur {user_auth.name} essaye de créer un admin, et c'est pas autorisé")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if user_auth.is_fake_admin() and not user.is_admin:
        print("L'utilisateur connecté est un FAKE admin, accepté parce qu'on a une base vide.")
        print("Il essaye de créer un premier utilisateur, qui n'est pas admin. Il ne faut pas faire ça.")
        print("Corrige tes tests. Tu dois créer un premier admin.")
        raise fastapi.HTTPException(status_code=403, detail="FAKE admin user is trying to create a first user which is not an admin. Everything is going to fail. Fix your test suite.")
    user_db = sql_api.get_user(api, user.name)

    if user_db is not None:
        raise fastapi.HTTPException(status_code=409, detail="User already exists")

    if user.perms is not None and len(user.perms) > 0:
        if not user_auth.can_set_perms():
            log.info(f"L'utilisateur {user_auth.name} essaye de mettre des global perms sur un nouvel utilisateur, et c'est pas autorisé")
            raise fastapi.HTTPException(status_code=403, detail="Permission denied")
        user.perms = auth.clean_perms_in_arg(user.perms)
    else:
        user.perms = []

    user_db = sql_api.create_user(
        api, name=user.name, password=user.password, is_admin=user.is_admin, perms=user.perms
    )
    if user_db is None:
        raise fastapi.HTTPException(status_code=500, detail="Failed to insert the user into the database.")
    if not user_auth.is_admin and user_auth.has_global_perm("manage_users"):
        managed = sql_api.create_managed(api, user=user_db.name, managed_by=user_auth.name)
        if managed is None:
            raise fastapi.HTTPException(status_code=500, detail="Failed to insert the 'managed' into the database.")

    return user_db.to_web()
