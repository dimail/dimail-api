import fastapi

from ... import auth, sql_api
from .. import dependencies, routers


@routers.users.delete(
    "/{user_name}",
    status_code=fastapi.status.HTTP_204_NO_CONTENT,
    responses={
        204: {"description": "Deleted"},
        403: {"description": "Permission denied"},
        404: {"description": "Not found"},
    },
    response_model=None,
    description="Delete a user by name",
)
async def delete_user(
    api: dependencies.DependsApiDb,
    user: auth.DependsBasicUser,
    user_name: str,
) -> None:
    """Delete a user by name.

    To delete a user, you must have appropriate permissions (be an admin, or have
    a global perm to create_users, and be a manager for that specific user). And you
    can never delete yourself.

    Args:
        api (dependencies.DependsApiDb): Database session
        user (auth.DependsBasicUser): User credentials
        user_name (str): User name of the user to be deleted

    Returns:
        None: No content

    Raises:
        fastapi.HTTPException: Not found

    See Also:
        * https://fastapi.tiangolo.com/tutorial/path-params
        * https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt
        * https://fastapi.tiangolo.com/tutorial/security/simple-verify-token

    Dependencies:
        auth.DependsBasicUser
        dependencies.DependsApiDb
        routers
        sql_api.delete_allows_by_user
        sql_api.delete_user
        sql_api.get_user
    """
    user_db = sql_api.get_user(api, user_name)
    if user_db is None:
        raise fastapi.HTTPException(status_code=404, detail="Not found")
    if not user.can_delete_user(user_db):
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    sql_api.delete_allows_by_user(api, user_name)
    sql_api.delete_user(api, user_name)
    return None

