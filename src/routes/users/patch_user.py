import fastapi

from ... import auth, enums, sql_api, web_models, utils
from .. import dependencies, routers


@routers.users.patch(
    "/{user}",
    status_code=200,
    response_model=web_models.User,
    responses={
        200: {"description": "User updated"},
        404: {"description": "Not found"},
    },
    description="Updates a user",
)
async def patch_user(
    user: str,
    api: dependencies.DependsApiDb,
    user_auth: auth.DependsBasicUser,
    updates: web_models.UpdateUser,
) -> web_models.User:
    """Updates a user."""

    user_db = sql_api.get_user(api, user)

    if user_db is None:
        raise fastapi.HTTPException(status_code=404, detail="Not found")
    if not user_auth.can_modify_user(user_db):
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if user_db.is_admin:
        if not user_auth.can_create_admin():
            raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    if updates.is_admin is not None:
        if not user_auth.can_create_admin():
            raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    set_perms = []
    if updates.perms is not None:
        if not user_auth.can_set_perms():
            raise fastapi.HTTPException(status_code=403, detail="Permission denied")
        set_perms = utils.read_args_list("perms", enums.Perm, user_db.perms, updates.perms)

    if updates.password is not None:
        user_db = sql_api.update_user_password(api, user, updates.password)

    if updates.is_admin is not None:
        user_db = sql_api.update_user_is_admin(api, user, updates.is_admin)
    if updates.perms is not None:
        if set_perms is not None:
            user_db = sql_api.update_user_perms(api, user, set_perms)

    return user_db.to_web()
