from ... import auth, sql_api, web_models
from .. import dependencies, routers


@routers.users.get(
    "/",
    response_model=list[web_models.User],
    responses={
        200: {"description": "Users"},
    },
    status_code=200,
    description="Get all users",
)
async def get_users(
    api: dependencies.DependsApiDb,
    user: auth.DependsBasicUser,
) -> list[web_models.User]:
    """Get all users.

    This will list all the users you are allowed to see, so at least
    yourself, and if you are an admin everybody. With special permissions,
    you may be allowed to see more or less users.

    Args:
        api (dependencies.DependsApiDb): Database session
        user (auth.DependsBasicUser): User credentials

    Returns:
        list[web_models.User]: List of users

    See Also:
        * https://fastapi.tiangolo.com/tutorial/path-params
        * https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt
        * https://fastapi.tiangolo.com/tutorial/security/simple-verify-token

    Dependencies:
        auth.DependsBasicUser
        dependencies.DependsApiDb
        routers
        sql_api.get_users
        web_models.User
    """

    # FIXME On remonte *tous* les utilisateurs, et on post-filtre sur les droits.
    # On pourrait être plus efficaces : pour les non-admins, les users qu'ils ont
    # le droit de voir sont ceux qu'ils managent (plus eux-même), ça évite de tout
    # remonter pour rien.
    users = sql_api.get_users(api)
    result = []
    for item in users:
        if user.can_see_user(item):
            result.append(item.to_web())
    return result
