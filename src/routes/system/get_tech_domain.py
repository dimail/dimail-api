"""Get API server tech domain
"""
import fastapi
import logging

from ... import auth, dns, web_models
from .. import routers


@routers.system.get("/tech_domain")
async def get_tech_domain(
    user: auth.DependsBasicUser,
) -> web_models.TechDomain:
    """Route to get the tech domain of the API server.

    Args:
        user (User): the user requesting the status

    Returns:
        ...

    Raises:
        HTTPException: if the user is not a basic user
    """
    log = logging.getLogger(__name__)

    if not user.can_get_tech_domain():
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    return web_models.TechDomain(
        tech_name= dns.get_tech_domain(),
        host_name= "Sait pas",
    )
