import fastapi
import logging

from ... import auth, oxcli, web_models
from .. import dependencies, routers


@routers.system.post("/database/{db_name}/{schema_name}")
async def post_database(
    db_name: str,
    schema_name: str,
    user: auth.DependsBasicUser,
) -> web_models.SchemaName:
    log = logging.getLogger(__name__)
    if not user.can_post_database():
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    log.info(f"Tu demandes à créer le schéma {schema_name} dans {db_name}")
    ox_clus = oxcli.OxCluster()
    list_db = await ox_clus.list_databases()

    found = False
    for db in list_db:
        if db.db_name == db_name:
            found = True
            log.info(f"J'ai trouvé la base {db_name}")
            break
    if not found:
        log.info(f"Je n'ai pas trouvé la base {db_name}, il faut la créer (ça va échouer)")
        ox_clus.create_database(db_name)

    list_schemas = await ox_clus.list_schemas()
    found = False
    for schema in list_schemas:
        if schema.name == schema_name:
            log.info(f"J'ai trouvé le schema {schema_name} dans la base")
            found = True
            break
    if not found:
        log.info(f"Je n'ai pas trouvé le schema que tu demandes, alors j'en crée un nouveau")
        new_schema = await ox_clus.create_schema(db_name)
        log.info(f"J'ai créé le schema {new_schema} et donc je retourne le nom {new_schema.name}")
        return web_models.SchemaName(name = new_schema.name)

    log.info(f"J'ai trouvé le schema que tu demandes, donc je retourne le nom {schema_name}")
    return web_models.SchemaName(name = schema_name)
    
