import fastapi
import logging
import os
import signal

from ... import auth, remote
# dkcli, oxcli, remote, sql_api, sql_dkim, sql_dovecot, sql_postfix, web_models
from .. import dependencies, routers


@routers.system.get("/shutdown")
async def get_shutdown(
    user: auth.DependsBasicUser,
):
    log = logging.getLogger(__name__)
    if not user.can_shutdown():
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    if not remote.fake_mode():
        raise fastapi.HTTPException(status_code=422, detail="Never do that, except in fake mode")

    exe = os.readlink("/proc/self/exe")
    log.info(f"Je tourne {exe}")

    with open("/proc/self/cmdline", "r") as file:
        cmdline = file.read()
    args = cmdline.split("\0")
    log.info(f"Args: {args}")
    if "pytest" in args[1]:
        # Dans les tests, on ne se suicide pas
        return None

    os.kill(os.getpid(), signal.SIGINT)
    return None
