"""Get API server health check status.
"""
import fastapi
import logging

from ... import auth, cbcli, dkcli, oxcli, remote, sql_api, sql_dkim, sql_dovecot, sql_postfix, web_models
from .. import dependencies, routers


@routers.system.get("/health_check")
async def get_health_check(
    user: auth.DependsBasicUser,
    api: dependencies.DependsApiDb,
) -> web_models.ServerStatus:
    """Route to get the health check status of the API server.

    Args:
        user (User): the user requesting the status

    Returns:
        Details, per module, of the health check status of the server.
        Only admins are allowed.

    Raises:
        HTTPException: if the user is not a basic user
    """
    log = logging.getLogger(__name__)
    if not user.can_health_check():
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    g_status = True

    remote_mod = remote.health_check()
    if not remote_mod.status:
        g_status = False

    cb_mod = cbcli.health_check()
    if not cb_mod.status:
        g_status = False

    dk_mod = dkcli.health_check()
    if not dk_mod.status:
        g_status = False

    ox_mod = await oxcli.health_check()
    if not ox_mod.status:
        g_status = False

    api_mod = sql_api.health_check()
    if not api_mod.status:
        g_status = False

    dkim_mod = sql_dkim.health_check()
    if not dkim_mod.status:
        g_status = False

    dovecot_mod = sql_dovecot.health_check()
    if not dovecot_mod.status:
        g_status = False

    postfix_mod = sql_postfix.health_check()
    if not postfix_mod.status:
        g_status = False

    return web_models.ServerStatus(
        remote_module = remote_mod,
        certbot_module = cb_mod,
        opendkim_module = dk_mod,
        openxchange_module = ox_mod,
        sql_api_module = api_mod,
        sql_dkim_module = dkim_mod,
        sql_imap_module = dovecot_mod,
        sql_postfix_module = postfix_mod,
        status = g_status,
    )
