import fastapi
import logging

from ... import auth, business, others, oxcli, web_models
# dkcli, oxcli, remote, sql_api, sql_dkim, sql_dovecot, sql_postfix, web_models
from .. import dependencies, routers


@routers.system.patch("/import_domain/{domain_name}")
async def import_domain(
    domain_name: str,
    updates: web_models.ImportDomain,
    user: auth.DependsBasicUser,
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,

):
    log = logging.getLogger(__name__)
    if not user.can_patch_database():
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    other = others.get_other(updates.moving_from)
    if other is None:
        raise fastapi.HTTPException(status_code=422, detail=f"Unknown from platform '{updates.moving_from}'")

    
    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
    )
    await b_domain.move_from(
        other_name = other.name,
        moving_mailboxes = updates.moving_mailboxes,
        moving_aliases = updates.moving_aliases,
        moving_senders = updates.moving_senders,
        moving_dkim = updates.moving_dkim,
    )

