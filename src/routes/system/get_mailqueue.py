import logging

import fastapi

from ... import auth, remote
from .. import dependencies, routers

@routers.system.get(
    "/mailqueue/{domain_name}/{user_name}",
    responses={
        200: {"description": "Return the list of queue_id"},
        403: {"description": "Permission denied"},
        404: {"description": "Something not found"},
    },
    description="List the queue_id for a destination address",
)
async def get_mailqueue(
    domain_name: str,
    user_name: str,
    user: auth.DependsBasicUser,
) -> list[str]:
    log = logging.getLogger(__name__)
    log.info(f"Tu demandes la mailqueue pour {user_name}@{domain_name}")

    if remote.fake_mode():
        if user_name == "many":
            return [ "ID_fake_01", "ID_fake_02" ]
        return []

    result = []
    for smtp in remote.get_hosts_by_role("smtp"):
        tmp = remote.run_on_host(smtp, f"sudo /usr/bin/api_postfix_queue.sh '{domain_name}' '{user_name}'", want_success=True)
        if tmp == "":
            continue
        elems = tmp.split("\n")
        for elem in elems:
            if not elem == "":
                result.append(elem.strip("\""))
    return result
