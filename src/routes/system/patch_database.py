import fastapi
import logging

from ... import auth, business, others, oxcli, web_models
# dkcli, oxcli, remote, sql_api, sql_dkim, sql_dovecot, sql_postfix, web_models
from .. import dependencies, routers


@routers.system.patch("/database/{db_name}")
async def patch_database(
    db_name: str,
    user: auth.DependsBasicUser,
    updates: web_models.UpdateDatabase,
    api: dependencies.DependsApiDb,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    postfix: dependencies.DependsPostfixDb,

):
    log = logging.getLogger(__name__)
    log.info(f"Tu demandes à déménager {db_name} vers {updates.move_to}")
    if not user.can_patch_database():
        log.info("Permission denied.")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    # Vérifier que move_to: <other> est cohérent
    other = None
    try:
        other = others.get_other(updates.move_to)
        log.info(f"Je connais la plateforme {other.name}")
    except Exception as e:
        log.info(f"Je ne trouve pas {updates.move_to} dans les plateformes connues. ECHEC.")
        raise

    log.info(f"Étape 1 - lister les contextes")
    ox_clus = oxcli.OxCluster()
    list_ctx = await ox_clus.list_contexts_in_db(db_name)

    # TODO: créer une base à ce nom là si elle n'existe pas à l'autre bout

    log.info(f"Étape 2 - verifier la dispo des cid pour déménager {db_name} vers {other.name}")
    contexts = []
    for ctx in list_ctx:
        contexts.append({
            "cid": ctx.cid,
            "name": ctx.name,
            "domains": list(ctx.domains),
            "schema_name": await ctx.get_schema_name(),
        })
    check = other.client.check_contexts(contexts)
    log.info(f"Résultat du contrôle: {check}")

    failed = False
    ctx_status = {}
    for ctx in check["contexts"]:
        cid = ctx["cid"]
        name = ctx["name"]
        status = ctx["status"]
        ctx_status[ctx["cid"]] = status
        if status == "same":
            log.info(f"Le context cid={cid} name={name} existe déjà, conforme, chez {other.name}")
        elif status == "available":
            log.info(f"Le context cid={cid} name={name} est disponible chez {other.name}")
        elif status == "partial":
            log.info(f"Le context cid={cid} name={name} est partiellement chez {other.name}")
        else:
            failed = True
            log.error(f"Le context cid={cid} ne peut pas migrer.")
    if failed:
        raise Exception(f"Cannot move {db_name} to {other.name}")

    schemas = check["schemas"]
    log.info(f"Étape 3 - copier les domaines de la base {db_name} vers {other.name}...")
    for ctx in list_ctx:
        log.info(f"- nous déménageons le contexte {ctx.name} id={ctx.cid}")
        schema = await ctx.get_schema_name()
        log.info(f"  il utilise le schema {schema}")
        if schema in schemas:
            log.info(f"  ce schema existe déjà chez {other.name} sous le nom {schemas[schema]}")
        else:
            schemas[schema] = other.client.post_database(db_name, schema)
            log.info(f"  ce schema vient d'être créé chez {other.name} sous le nom {schemas[schema]}")

        for dom in ctx.domains:
            log.info(f"  - nous déménageons le domaine {dom}")
            b_domain = business.Domain(
                name = dom,
                api_session = api,
                dkim_session = dkim,
                dovecot_session = dovecot,
                postfix_session = postfix,
            )
            await b_domain.move_to(
                other_name = other.name,
                context_id = str(ctx.cid),
                context_name = ctx.name,
                schema_name = schemas[schema],
            )

    # TODO: prendre le dump mysql des bases concernées (schemas)
    # TODO: injecter le dump mysql à l'autre bout
    # TODO: les mots de passe des admins des contextes sont dans la base mysql qu'on va déménager,
    # il faut mettre ceux de la nouvelle plateforme après l'injection (changer le user admin du
    # contexte, connaissant l'ancien password, pour mettre le nouveau)
    # TODO: avec la base mysql, il faut embarquer le filestore...


