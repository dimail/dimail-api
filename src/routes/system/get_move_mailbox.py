import logging

import fastapi

from ... import auth, business, enums, sql_api, sql_dovecot, web_models
from .. import dependencies, routers

@routers.system.get(
    "/move_mailbox/{domain_name}/{user_name}",
    responses={
        200: {"description": "Mailbox moved to destination platform"},
        403: {"description": "Permission denied"},
        404: {"description": "Something not found"},
        424: {"description": "Failed to move mailbox. Check logs on the server."},
    },
    description="When a domain is moving <from>:<to>, moves a mailbox from <from> to <to>"
)
async def get_move_mailbox(
    user_name: str,
    domain_name: str,
    user: auth.DependsTokenUser,
    dkim: dependencies.DependsDkimDb,
    dovecot: dependencies.DependsDovecotDb,
    api: dependencies.DependsApiDb,
    postfix: dependencies.DependsPostfixDb,
) -> None:
    log = logging.getLogger(__name__)
    log.info(f"Tu demandes à déménager {user_name}@{domain_name}")
    if not user.can_patch_database():
        log.info("Permission denied.")
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    b_domain = business.Domain(
        name = domain_name,
        api_session = api,
        dkim_session = dkim,
        dovecot_session = dovecot,
        postfix_session = postfix,
    )
    if not b_domain.exists():
        raise fastapi.HTTPException(status_code=404, detail="The domain does not exist")
    if not b_domain.move_to_me():
        raise fastapi.HTTPException(status_code=404, detail="The domain does not move to me")

    b_mailbox = business.Mailbox(
        username = user_name,
        domain = domain_name,
        webmail = b_domain.has_feature(enums.Feature.Webmail),
        dovecot_session = dovecot,
        postfix_session = postfix,
    )
    if not b_mailbox.exists():
        raise fastapi.HTTPException(status_code=404, detail="The mailbox does not exist")
    if not b_mailbox.need_moving():
        return None

    res = await b_mailbox.move(from_name = b_domain.moving_from())

    if not res:
        raise fastapi.HTTPException(status_code=424, detail="Failed to move mailbox")
    return None
