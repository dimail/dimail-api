"""Get version route.
"""
import fastapi
import logging

from ... import auth, version, web_models
from .. import dependencies, routers


@routers.system.get("/version")
async def get_version(
    user: auth.DependsBasicUser,
) -> web_models.ApiVersion:
    """Route to get the running version of the API server

    Args:
        user (User): the user requesting the status

    Returns:
        Informations about the running version

    Raises:
        HTTPException: if the user is not a basic user
    """
    log = logging.getLogger(__name__)
    if not user.can_get_version():
        raise fastapi.HTTPException(status_code=403, detail="Permission denied")

    ver, changelog = version.get_version_info()
    return web_models.ApiVersion(
        version=ver,
        changelog=changelog,
    )
