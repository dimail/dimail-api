import fastapi
import logging

from ... import auth, enums, web_models, oxcli
from .. import dependencies, routers


@routers.system.post("/check_contexts")
async def check_contexts(
    user: auth.DependsBasicUser,
    contexts: web_models.CheckContexts,
):
    log = logging.getLogger(__name__)
    result = []
    clus = oxcli.OxCluster()
    for context in contexts.contexts:
        res = web_models.CheckedContext(
            cid = context.cid,
            name = context.name,
            domains = context.domains,
            schema_name = context.schema_name,
            status = enums.ContextStatus.Available,
            comment = [],
        )
        log.info(f"On regarde le contexte cid={res.cid}, name={res.name}, schema={res.schema_name}, domains={res.domains}")

        ctx = await clus.get_context(context.cid)
        schemas = {}
        if ctx is not None:
            log.info(f"- on le trouve pas son id")
            res.status = enums.ContextStatus.Same
            if not ctx.cid == context.cid:
                log.info(f"  ... il n'a pas le même id mais {ctx.cid}(WTF??)")
                res.status = enums.ContextStatus.Differ
                res.comment.append("We are in the panic HERE")
            if not ctx.name == context.name:
                log.info(f"  ... il n'a pas le même nom mais {ctx.name}")
                res.status = enums.ContextStatus.Differ
                res.comment.append(f"I have a context with id={ctx.cid} but its name is {ctx.name} instead of {context.name}")
            a_doms = sorted(ctx.domains)
            b_doms = sorted(context.domains)
            if not sorted(ctx.domains) == sorted(context.domains):
                for dom in b_doms:
                    if dom in a_doms:
                        a_doms.remove(dom)
                if len(a_doms) == 0:
                    log.info(f"  ... tous les domaines ne sont pas parvenus jusqu'à moi")
                    res.status = enums.ContextStatus.Partial
                else:
                    log.info(f"  ... il n'a pas les même domaines mais {a_doms}")
                    res.status = enums.ContextStatus.Differ
                    res.comment.append(f"The domains are not the same, I have {a_doms}, you want {b_doms}")
            a_name = await ctx.get_schema_name()
            b_name = context.schema_name
            if b_name not in schemas:
                log.info(f"  ... il est dans le schema {a_name}, je note dans la map {b_name} => {a_name}")
                schemas[b_name] = a_name
            if not schemas[b_name] == a_name:
                log.info("  ... il est dans un schema de ma map, mais pas associé au schema que tu dis")
                res.status = enums.ContextStatus.Differ
                res.comment.append(f"The schema is not the same (here, it is {a_name}, also used for something else...")

            result.append(res)
            continue

        ctx = await clus.get_context_by_name(context.name)
        if ctx is not None:
            lof.info(f"- on le trouve par son nom (pas bien ça...)")
            res.status = enums.ContextStatus.Differ
            res.comment.append(f"Context id {context.cid} is not used, but I already have a context named {ctx.name} with id={ctx.cid} used for domains {ctx.domains}")
        for dom in context.domains:
            ctx = await clus.get_context_by_domain(dom)
            if ctx is not None:
                log.info(f"- on a déjà un contexte pour le domaine {dom} cid={ctx.cid}, name={ctx.name}")
                res.status = enums.ContextStatus.Differ
                res.comment.append(f"The domain {dom} is already known here, and managed by context cid={ctx.cid}, name={ctx.name}, used for domains {ctx.domains}")
#        schema = await clus.get_schema_by_name(context.schema_name)
#        if schema is not None:
#            log.info("- on a déjà un schema qui s'appelle {schema_name}")
#            res.status = enums.ContextStatus.Differ
#            res.comment.append(f"The schema named {context.schema_name} is already used here. You should not use that...")

        result.append(res)

    return web_models.CheckedContexts(
        contexts = result,
        schemas = schemas,
    )
