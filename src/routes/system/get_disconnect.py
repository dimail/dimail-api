import logging

import fastapi

from ... import auth, remote
from .. import dependencies, routers

@routers.system.get(
    "/disconnect/{domain_name}/{user_name}",
    responses={
        200: {"description": "The user have been disconnected"},
        403: {"description": "Permission denied"},
        404: {"description": "Something not found"},
    },
    description="Terminate imap sessions of a user",
)
async def get_disconnect(
    domain_name: str,
    user_name: str,
    user: auth.DependsBasicUser,
) -> None:
    log = logging.getLogger(__name__)
    log.info(f"Tu demandes la déconnexion de {user_name}@{domain_name}")

    if remote.fake_mode():
        return None

    smtp = remote.get_host_by_role("imap")
    _ = remote.run_on_host(smtp, f"sudo /usr/bin/api_dovecot_kick.sh imap '{domain_name}' '{user_name}'", want_success=True)
    return None
