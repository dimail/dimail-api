import fastapi

def test_health_check(client, admin):
    admin_auth = (admin["user"], admin["password"])

    response = client.get(
        "/system/health_check",
        auth = ( "je suis", "partout"),
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    response = client.get(
        "/system/health_check",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert json["status"] == True
