import pytest

import fastapi

@pytest.fixture(scope="function")
def la_regie(log, client, admin):
    auth = (admin["user"], admin["password"])

    response = client.post(
        "/users/",
        json={
            "name": "la_regie",
            "password": "toto",
            "is_admin": False,
            "perms": ["new_domain", "create_users", "manage_users"],
        },
        auth=auth
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    response = client.get(
        "/token/",
        params={"username": "la_regie"},
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    token = response.json()["access_token"]

    log.info("SETUP user la_regie")
    yield {"user": "la_regie", "password": "toto", "token": token}
    log.info("TEARDOWN user la_regie")


def test_fake_admin(log, client):
    auth=("pas un user", "faux mot de passe")

    # Mon FAKE admin ne peut pas créer un domaine (ou faire quoi que ce soit, d'ailleurs)
    response = client.post(
        "/domains/",
        json={
            "name": "example.com",
            "features": ["webmail", "mailbox"],
            "context_name": "dimail",
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN
    json = response.json()
    assert "FAKE admin" in json["detail"]

    # Mon FAKE admin ne peut pas créer un utilisateur qui n'est pas admin.
    response = client.post(
        "/users/",
        json={
            "name": "tartempion",
            "is_admin": False,
            "password": "secret",
            "perms": [],
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN
    json = response.json()
    assert "FAKE admin" in json["detail"]

    # Mon FAKE admin *peut* créer un admin.
    response = client.post(
        "/users/",
        json={
            "name": "jojo",
            "password": "l'démago",
            "is_admin": True,
            "perms": [],
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Une fois qu'il existe un user (ici, un admin), mon FAKE ne peut plus créer d'admin.
    response = client.post(
        "/users/",
        json={
            "name": "mercaillon",
            "password": "trèssecretdéfense",
            "is_admin": True,
            "perms": [],
        },
        auth=auth,
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN
    json = response.json()
    assert "Permission denied" in json["detail"]


def test_regie(db_api_session, db_dkim_session, db_dovecot_session, db_postfix_session, ox_cluster, dk_manager, log, client, la_regie):
    auth_regie = ( la_regie["user"], la_regie["password"] )
    token_regie = la_regie["token"]

    # Créer un domaine
    response = client.post(
        "/domains/",
        json={
            "name": "example.com",
            "features": ["webmail", "mailbox"],
            "context_name": "dimail",
        },
        auth=auth_regie,
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Get le domaine (auth la régie, le client n'existe pas encore)
    response = client.get(
        "/domains/example.com",
        headers={"Authorization": f"Bearer {token_regie}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Demander un check du domaine
    # FIXME Est-ce que c'est possible au nom du client, donc un token ?
    # Pour le moment, c'est un auth normale, si on veut le faire au nom
    # du client, il faut stocker son mot de passe...
    response = client.get(
        "/domains/example.com/check",
        auth=auth_regie,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Création d'un user (auth la régie)
    # Le user n'a pas de mot de passe (password=no), il ne peut pas s'authentifier directement
    response = client.post(
        "/users/",
        json={
            "name": "example",
            "is_admin": False,
            "password": "no",
            "perms": [],
        },
        auth=auth_regie
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Create a user with an uuid as user name
    response = client.post(
        "/users/",
        json={
            "name": "ea00652e-742b-11ef-86d3-879b2ea77f4b",
            "is_admin": False,
            "password": "no",
            "perms": [],
        },
        auth=auth_regie,
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED
    body = response.json()
    assert body["name"] == "ea00652e-742b-11ef-86d3-879b2ea77f4b"

    response = client.delete(
        "/users/ea00652e-742b-11ef-86d3-879b2ea77f4b",
        auth=auth_regie,
    )
    assert response.status_code == fastapi.status.HTTP_204_NO_CONTENT

    # Allow le client pour le domaine (auth la régie)
    response = client.post(
        "/allows/",
        json={
            "domain": "example.com",
            "user": "example",
        },
        auth=auth_regie
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Demander un token au nom du client (auth la régie)
    response = client.get(
        "/token/",
        params={"username": "example"},
        auth=auth_regie,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    token = response.json()["access_token"]

    # Maintenant, le client existe, il est allowed sur le domaine, on a un token
    # en son nom.

    # Get le domaine (auth le client)
    response = client.get(
        "/domains/example.com",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

# Ce n'est plus possible, le client n'a plus de mot de passe.
#    # Demande un check du domaine (auth le client avec mot de passe #FIXME)
#    response = client.get(
#        "/domains/example.com/check",
#        auth=("example", "toto"),
#    )
#    assert response.status_code == fastapi.status.HTTP_200_OK

    # Création d'une mailbox (auth le client)
    response = client.post(
        "/domains/example.com/mailboxes/john.doe",
        json={
            "givenName": "John",
            "surName": "Doe",
            "displayName": "John Doe",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Création d'une mailbox avec une quote (auth, le client)
    response = client.post(
        "/domains/example.com/mailboxes/quotes",
        json={
            "givenName": "Région",
            "surName": "Provence-Alpes-Côte-d'Azure",
            "displayName": "Provence-Alpes-Côte-d'Azure",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Création d'une mailbox avec une double-quote (auth, le client)
    response = client.post(
        "/domains/example.com/mailboxes/quotes2",
        json={
            "givenName": "Région2",
            "surName": "Provence-Alpes-Côte-d\"Azure2",
            "displayName": "Provence-Alpes-Côte-d\"Azure2",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Modification d'une mailbox (auth le client)
    response = client.patch(
        "/domains/example.com/mailboxes/john.doe",
        json={
            "givenName": "Jean",
            "surName": "Doh",
            "displayName": "Jean Doh",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Suspension d'une mailbox
    response = client.patch(
        "/domains/example.com/mailboxes/john.doe",
        json={
            "active": "no",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Ré-activation d'une mailbox
    response = client.patch(
        "/domains/example.com/mailboxes/john.doe",
        json={
            "active": "yes",
            "givenName": "Jean",
            "surName": "Doh",
            "displayName": "Jean Doh",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Création d'un alias (auth le client)
    response = client.post(
        "/domains/example.com/aliases/",
        json={
            "user_name": "jean.doh",
            "destination": "john.doe@example.com",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Get de toutes les mailboxes du domaine (auth le client)
    response = client.get(
        "/domains/example.com/mailboxes/",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Get d'une mailbox spécifique (auth le client)
    response = client.get(
        "/domains/example.com/mailboxes/john.doe",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Get tous les alias du domaine (auth le client)
    response = client.get(
        "/domains/example.com/aliases/",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Delete d'un alias précis (auth le client)
    response = client.delete(
        "/domains/example.com/aliases/jean.doh/john.doe@example.com",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_204_NO_CONTENT

    # Création d'un alias (auth le client)
    response = client.post(
        "/domains/example.com/aliases/",
        json={
            "user_name": "contact",
            "destination": "pierre@gmail.com",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Deuxième destination pour le même alias (auth le client)
    response = client.post(
        "/domains/example.com/aliases/",
        json={
            "user_name": "contact",
            "destination": "paul@outlook.com",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_201_CREATED

    # Get toutes les destinations de l'alias contact@example.com (auth le client)
    response = client.get(
        "/domains/example.com/aliases/",
        params={
            "user_name": "contact",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # Test si une destination existe (auth le client)
    response = client.get(
        "/domains/example.com/aliases/",
        params={
            "user_name": "contact",
            "destination": "robert@laposte.net",
        },
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_404_NOT_FOUND

    # Supprime toutes les destinations pour un alias (auth le client)
    response = client.delete(
        "/domains/example.com/aliases/contact/all",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_204_NO_CONTENT
    
    # Supprime une mailbox (auth le client)
    response = client.delete(
        "/domains/example.com/mailboxes/john.doe",
        headers={"Authorization": f"Bearer {token}"},
    )
    assert response.status_code == fastapi.status.HTTP_204_NO_CONTENT

    # Supprime le domaine (auth la régie)
    # FIXME Not yet implemented
    response = client.delete(
        "/domains/example.com",
        auth=auth_regie,
    )
    assert response.status_code == fastapi.status.HTTP_501_NOT_IMPLEMENTED



