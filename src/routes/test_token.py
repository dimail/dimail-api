import pytest

import fastapi
import jwt
import datetime

from .. import config


@pytest.mark.parametrize(
    "normal_user",
    ["bidibule:toto"],
    indirect=True,
)
def test_token(db_api, db_dovecot, log, client, normal_user, admin):
    user_auth = (normal_user["user"], normal_user["password"])
    admin_auth = (admin["user"], admin["password"])
    secret = config.settings["JWT_SECRET"]
    algo = "HS256"

    # If the normal user asks a token for (implicitly) himself, it is ok
    # (this is already tested in the fixture 'normal_user', but we are more
    # explicit here, checking that the token can be decoded and has the right
    # sub).
    response = client.get(
        "/token/",
        auth=user_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    token = response.json()["access_token"]
    clear_token = jwt.decode(token, secret, algo)
    assert clear_token["sub"] == normal_user["user"]

    # If the normal user asks a token for (explicitly) himself, it is ok
    response = client.get(
        "/token/",
        params={
            "username": normal_user["user"],
        },
        auth=user_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    token = response.json()["access_token"]
    clear_token = jwt.decode(token, secret, algo)
    assert clear_token["sub"] == normal_user["user"]

    # If the normal user asks a token for someone that does not exist, it fails
    response = client.get(
        "/token/",
        params={
            "username": "inconnu",
        },
        auth=user_auth,
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # If the admin user asks a token for someone that does not exist, it fails
    response = client.get(
        "/token/",
        params={
            "username": "inconnu",
        },
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # If the normal user asks a token for the admin user, it fails
    response = client.get(
        "/token/",
        params={
            "username": admin["user"],
        },
        auth=user_auth,
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    # If the admin user asks a token for the normal user, it is ok
    response = client.get(
        "/token/",
        params={
            "username": normal_user["user"],
        },
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    token = response.json()["access_token"]
    clear_token = jwt.decode(token, secret, algo)
    assert clear_token["sub"] == normal_user["user"]

    # We remove the password for the normal user
    response = client.patch(
        "/users/"+normal_user["user"],
        json={"password": "no"},
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

    # The admin can still get a token for the normal user
    response = client.get(
        "/token/",
        params={
            "username": normal_user["user"],
        },
        auth=admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK

