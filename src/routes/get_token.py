"""Get token route.

This module contains the route to get a token for the user.

Example:
    To use the route, you need to import the route in the main file and add it to the FastAPI app.

    ```python

    from fastapi import FastAPI
    from src.routes import get_token

    app = FastAPI()

    app.include_router(get_token.router)
    ```

    In this example, the route `/token` will return a token if the user is a basic user.

The export function is:
    - login_for_access_token: route to get a token for the user
"""
import fastapi
import logging

from .. import auth, sql_api, web_models
from . import dependencies, routers


@routers.token.get("/")
async def login_for_access_token(
    user: auth.DependsBasicUser,
    api: dependencies.DependsApiDb,
    username: str | None = None,
) -> web_models.Token:
    """Route to get a token for the user.

    Args:
        user (User): the user requesting the token
        username (str): the user to get the token for, if it is not the same

    Returns:
        Token: the token for the user (user, or username if we are dealing
        with a managed user and user is managing username)

    Raises:
        HTTPException: if the user is not a basic user
    """
    log = logging.getLogger(__name__)
    if username is not None:
        target_user = sql_api.get_user(api, username)
        if target_user is None:
            log.info(f"Tu me demandes un token pour un user qui n'existe pas. Permission denied.")
            raise fastapi.HTTPException(status_code=403, detail="Permission denied")
        if not user.can_manage_user(target_user):
            log.info(f"Le user {user.name} demande un token pour {target_user.name}, qu'il ne manage pas. Permission denied.")
            raise fastapi.HTTPException(status_code=403, detail="Permission denied")
    else:
        target_user = user
    token = target_user.create_token()
    return web_models.Token(access_token=token, token_type="bearer")
