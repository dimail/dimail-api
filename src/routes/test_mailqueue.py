import fastapi

def test_mailqueue(setup_remote, client, admin):
    admin_auth = (admin["user"], admin["password"])

    response = client.get(
        "/system/mailqueue/exmaple.com/essai",
        auth = ( "pasbien", "pasbien" ),
    )
    assert response.status_code == fastapi.status.HTTP_403_FORBIDDEN

    response = client.get(
        "/system/mailqueue/example.com/test",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert json == []

    response = client.get(
        "/system/mailqueue/example.com/many",
        auth = admin_auth,
    )
    assert response.status_code == fastapi.status.HTTP_200_OK
    json = response.json()
    assert len(json) == 2
    assert "ID_fake_02" in json
    assert "ID_fake_01" in json


