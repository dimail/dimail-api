from .. import sql_dkim

def test_dkim(db_dkim_session, log):

    domain = sql_dkim.create_domain(
        db_dkim_session,
        name = "example.com",
        private = "priv key",
        public = "pub key",
        selector = "selector",
    )
    assert isinstance(domain, sql_dkim.Domain)
    assert domain.name == "example.com"
    assert domain.selector == "selector"
    assert domain.private == "priv key"
    assert domain.public == "pub key"

    # Get un domaine qui n'existe pas -> None
    domain = sql_dkim.get_domain(db_dkim_session, "toto.net")
    assert domain is None

    # Get un domaine qui existe
    domain = sql_dkim.get_domain(db_dkim_session, "example.com")
    assert isinstance(domain, sql_dkim.Domain)

    # Update un domaine sans rien changer
    domain = sql_dkim.update_domain(db_dkim_session, "example.com", None, None, None)
    assert isinstance(domain, sql_dkim.Domain)
    assert domain.selector == "selector"
    assert domain.private == "priv key"
    assert domain.public == "pub key"

    # Update un domaine pour passer les valeurs à NULL
    domain = sql_dkim.update_domain(db_dkim_session, "example.com", "", "", "")
    assert isinstance(domain, sql_dkim.Domain)
    assert domain.selector is None
    assert domain.private is None
    assert domain.public is None

    # Update un domain pour mettre des valeurs
    domain = sql_dkim.update_domain(db_dkim_session, "example.com",
        selector = "dimail",
        private = "private_key",
        public = "public_key",
    )
    assert isinstance(domain, sql_dkim.Domain)
    assert domain.selector == "dimail"
    assert domain.private == "private_key"
    assert domain.public == "public_key"

    # Update un domaine qui n'existe pas -> None
    domain = sql_dkim.update_domain(db_dkim_session, "toto.net", "a", "b", "c")
    assert domain is None

    # Delete un domaine qui n'existe pas -> 0
    count = sql_dkim.delete_domain(db_dkim_session, "toto.net")
    assert count == 0

    # Delete un domaine qui existe -> 1
    count = sql_dkim.delete_domain(db_dkim_session, "example.com")
    assert count == 1

    # Get du domaine après qu'on l'a supprimé -> None
    domain = sql_dkim.get_domain(db_dkim_session, "example.com")
    assert domain is None

def test_health_check(db_dkim_session):
    status = sql_dkim.health_check()
    assert status.status is True
    assert "sql_dkim_status = OK" in status.detail

