# ruff: noqa: E711
"""SQL DKIM for domain operations.

This module contains functions to interact with the database for domain operations.

Functions:
    - get_domain: Get a domain from the database.
    - create_domain: Create a domain in the database.

Classes:
    - None

Exceptions:
    - None
"""
import sqlalchemy.orm as orm

from . import models


def get_domain(
    session: orm.Session,
    domain_name: str
) -> models.Domain:
    """Get a domain from the database.

    Args:
        session: ORM session object.

    Returns:
        Domain object from the database.
    """
    return session.get(models.Domain, domain_name)


def create_domain(
    session: orm.Session,
    name: str,
    selector: str | None = None,
    private: str | None = None,
    public: str | None = None,
) -> models.Domain:
    """Create a domain in the database.

    Args:
        session: ORM session object.
        name: Domain name.
        selector: the dkim selector
        private: the private dkim key
        public: the public dkim key

    Returns:
        Domain object from the database

    Raises:
        None
    """
    db_domain = models.Domain(name=name)
    if selector is not None:
        db_domain.selector = selector
    if private is not None:
        db_domain.private = private
    if public is not None:
        db_domain.public = public
    session.add(db_domain)
    session.commit()
    session.refresh(db_domain)
    return db_domain


def delete_domain(
    session: orm.Session,
    name: str,
) -> int:
    """Delete a domain.

    Args:
        session: ORM session
        name: the domain name

    Returns:
        The number of domains deleted
    """
    domain = get_domain(session, name)
    if domain is None:
        return 0
    session.delete(domain)
    session.commit()
    return 1


def update_domain(
    session: orm.Session,
    name: str,
    selector: str | None,
    private: str | None,
    public: str | None,
) -> models.Domain:
    """Updates the 3 columns about DKIM keys of a domain.

    Args:
        session: ORM session object
        name: domain name
        selector: DKIM selector
        private: DKIM private key
        public: DKIM public key as distributed by DNS

    Returns:
        Domain object from the database.

    All the 3 values can be None, they will then be set to NULL in database.
    """
    db_domain = get_domain(session, name)
    if db_domain is None:
        return None
    try:
        if selector is not None:
            if selector == "":
                db_domain.selector = None
            else:
                db_domain.selector = selector
        if private is not None:
            if private == "":
                db_domain.private = None
            else:
                db_domain.private = private
        if public is not None:
            if public == "":
                db_domain.public = None
            else:
                db_domain.public = public
        session.flush()
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_domain)
    return db_domain

