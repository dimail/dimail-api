"""This modules contains the models for the SQL database.

Classes:
    - Domain: a domain as seen by opendkim
"""
# from passlib.hash import argon2
import sqlalchemy as sa

from .. import config
from .database import Dkim

class Domain(Dkim):
    """A domain of the system.

    Attributes:
        name: the domain name
        features: the features of the domain
        webmail_domain: the webmail domain
        imap_domain: the IMAP domains
        smtp_domain: the SMTP domains
        users: the users that have access to the domain

    Functions:
        has_feature: check if the domain has a feature
    """
    __tablename__ = "domains"
    name = sa.Column(sa.String(200), primary_key=True)
    selector = sa.Column(sa.String(50), nullable=True)
    private = sa.Column(sa.Text, nullable=True)
    public = sa.Column(sa.Text, nullable=True)

