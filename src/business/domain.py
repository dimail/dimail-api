import logging
import sqlalchemy.orm as orm

from .. import dns, cbcli, dkcli, enums, others, oxcli, sql_api, sql_dkim, sql_dovecot, sql_postfix, web_models


class Domain:
    def __init__(
        self,
        name: str,
        api_session: orm.Session,
        dkim_session: orm.Session,
        dovecot_session: orm.Session,
        postfix_session: orm.Session,
        api_domain: sql_api.Domain | None = None,
        dkim_domain: sql_dkim.Domain | None = None,
        postfix_domain: sql_postfix.Domain | None = None,
    ):
        self.domain_name = name
        self.api_session = api_session
        self._api_domain = api_domain
        self.dkim_session = dkim_session
        self._dkim_domain = dkim_domain
        self.postfix_session = postfix_session
        self._postfix_domain = postfix_domain
        self.dovecot_session = dovecot_session

    ###
    ### Attributs d'un domaine
    ###

    def exists(self) -> bool:
        api_domain = self.api_domain()
        if api_domain is None:
            return False
        return True

    def api_domain(self) -> sql_api.Domain:
        if self._api_domain is None:
            self._api_domain = sql_api.get_domain(self.api_session, self.domain_name)
        return self._api_domain

    def postfix_domain(self) -> sql_postfix.Domain:
        if self._postfix_domain is None:
            self._postfix_domain = sql_postfix.get_domain(self.postfix_session, self.domain_name)
        return self._postfix_domain

    def dkim_domain(self) -> sql_dkim.Domain:
        if self._dkim_domain is None:
            self._dkim_domain = sql_dkim.get_domain(self.dkim_session, self.domain_name)
        return self._dkim_domain

    def has_feature(self, feature: str | enums.Feature) -> bool:
        api_domain = self.api_domain()
        if api_domain is None:
            return None
        return api_domain.has_feature(feature)

    def cert_name(self):
        api_domain = self.api_domain()
        if api_domain is not None:
            if ( not api_domain.has_feature(enums.Feature.Mailbox) and
                 not api_domain.has_feature(enums.Feature.Webmail) ):
                return None
        return f"client_{self.domain_name}"

    def imap_domain(self):
        api_domain = self.api_domain()
        if api_domain is not None and api_domain.has_feature(enums.Feature.Mailbox):
            return api_domain.get_domain("imap")

    def smtp_domain(self):
        api_domain = self.api_domain()
        if api_domain is not None and api_domain.has_feature(enums.Feature.Mailbox):
            return api_domain.get_domain("smtp")

    def webmail_domain(self):
        api_domain = self.api_domain()
        if api_domain is not None and api_domain.has_feature(enums.Feature.Webmail):
            return api_domain.get_domain("webmail")

    def get_target(self, name: str) -> str:
        if name == "spf":
            return "_spf." + self.get_tech_domain()
        return name + "." + self.get_tech_domain() + "."

    def moving(self) -> str:
        if hasattr(self,"_moving"):
            return self._moving
        api_domain = self.api_domain()
        if api_domain is None:
            return None
        if api_domain.moving is None:
            self._moving = None
            self._moving_from = None
            self._moving_to = None
            return None
        self._moving = api_domain.moving

        m_from, m_to = self._moving.split(":")
        self._moving_from = m_from
        self._moving_to = m_to
        return self._moving

    def moving_from(self) -> str:
        if not hasattr(self, "_moving_from"):
            self.moving()
        return self._moving_from

    def moving_to(self) -> str:
        if not hasattr(self, "_moving_to"):
            self.moving()
        return self._moving_to

    def move_from_me(self) -> bool:
        if not self.moving():
            return False
        if self.moving_from() == others.get_myself():
            return True
        return False

    def move_to_me(self) -> bool:
        if not self.moving():
            return False
        if self.moving_to() == others.get_myself():
            return True
        return False

    def get_tech_domain(self) -> str:
        if self.move_from_me():
            return others.get_other(self.moving_to()).get_tech_domain()

        return dns.get_tech_domain()

    ###
    ### Création et update d'un domaine
    ###

    async def create_domain(self,
        features: list[enums.Feature],
        delivery: str,
        transport: str | None,
        context_name: str | None,
        context_id: int | None = None,
        schema_name: str | None = None,
    ) -> sql_api.Domain:
        kwargs = {}
        if enums.Feature.Webmail in features and context_name is not None:
            ox_cluster = oxcli.OxCluster()
            ctx = await ox_cluster.get_context_by_domain(self.domain_name)
            if ctx is None:
                ctx = await ox_cluster.get_context_by_name(context_name)
                if ctx is None:
                    ctx = await ox_cluster.create_context(context_id, context_name, self.domain_name, schema_name = schema_name)
                else:
                    await ctx.add_mapping(self.domain_name)

        if enums.Feature.Mailbox in features:
            self.update_dkim_keys()

        self._postfix_domain = sql_postfix.create_domain(
            self.postfix_session,
            self.domain_name,
            delivery, transport
        )
        if self._postfix_domain is None:
            raise Exception(f"Failed to add domain to postfix")

        self._api_domain = sql_api.create_domain(self.api_session,
            name = self.domain_name,
            features = features,
            **kwargs
        )
        return self._api_domain

    def update_dkim_keys(self, force: bool = False):
        dkim_domain = self.dkim_domain()
        if dkim_domain is None:
            (dkim_private, dkim_public) = dkcli.make_dkim_key("dimail", self.domain_name)
            self._dkim_domain = sql_dkim.create_domain(
                self.dkim_session,
                name = self.domain_name,
                selector = "dimail",
                private = dkim_private,
                public = dkim_public,
            )
        elif force:
            (dkim_private, dkim_public) = dkcli.make_dkim_key("dimail", self.domain_name)
            sql_dkim.update_domain(
                self.dkim_session,
                name = self.domain_name,
                selector = "dimail",
                private = dkim_private,
                public = dkim_public,
            )

    async def update_ox_context(self, context_name: str | None):
        ox_cluster = oxcli.OxCluster()
        ctx = await ox_cluster.get_context_by_domain(self.domain_name)
        if ctx is None:
            if context_name is None:
                # Pas de context_name, et on est dans aucun contexte, tout va bien
                return
            ctx = await ox_cluster.get_context_by_name(context_name)
            if ctx is None:
                ctx = await ox_cluster.create_context(None, context_name, self.domain_name)
            else:
                await ctx.add_mapping(self.domain_name)
        else:
            if context_name is None:
                # Tu demandes à sortir du contexte, on ne fait pas encore ça
                raise Exception("Retirer un domaine d'OX, on ne sait pas encore le faire")
                # Si on le faisait, ce serait ça:
                # ctx.remove_mapping(self.domain_name)
            if ctx.name != context_name:
                # Tu demandes à changer de contexte, on ne fait pas encore ça
                raise Exception("Changer le contexte OX d'un domaine, on ne sait pas encore le faire")


    async def update_features(self, features: list[enums.Feature], context_name: str | None):
        api_domain = self.api_domain()

        # Si on ajoute "webmail", il faut ajouter un context_name...
        if enums.Feature.Webmail in features and not api_domain.has_feature(enums.Feature.Webmail):
            await self.update_ox_context(context_name)

        # Si on retire "webmail"... faut-il supprimer le context OX ? Probablement pas.
        #if enums.Feature.Webmail not in features and api_domain.has_feature(enums.Feature.Webmail):
        #    ox_cluster = oxcli.OxCluster()
        #    ctx = ox_cluster.get_context_by_domain(self.domain_name)
        #    if ctx is not None:
        #        ctx.remove_mapping(self.domain_name)

        # Si on ajoute "mailbox", il faut créer les clefs dkim
        if enums.Feature.Mailbox in features and not api_domain.has_feature(enums.Feature.Mailbox):
            self.update_dkim_keys()
            
        # Si on retire "mailbox"... faut-il retirer les clefs dkim ?
        #if enums.Feature.Mailbox not in features and api_domain.has_feature(enums.Feature.Mailbox):
        #    ... Pas encore moyen de faire ça

        sql_api.update_domain_features(self.api_session, self.domain_name, features)

    def update_domains(self, imap_domain: str | None, smtp_domain: str | None, webmail_domain: str | None):
        if imap_domain is not None:
            if imap_domain == "":
                sql_api.update_domain_imap_domain(self.api_session, self.domain_name, None)
            else:
                sql_api.update_domain_imap_domain(self.api_session, self.domain_name, imap_domain)

        if smtp_domain is not None:
            if smtp_domain == "":
                sql_api.update_domain_smtp_domain(self.api_session, self.domain_name, None)
            else:
                sql_api.update_domain_smtp_domain(self.api_session, self.domain_name, smtp_domain)

        if webmail_domain is not None:
            if webmail_domain == "":
                sql_api.update_domain_webmail_domain(self.api_session, self.domain_name, None)
            else:
                sql_api.update_domain_webmail_domain(self.api_session, self.domain_name, webmail_domain)

    def update_delivery(self, delivery: enums.Delivery, transport: str | None) -> bool:
        if delivery == enums.Delivery.Relay and transport is None:
            return False
        postfix_domain = self.postfix_domain()
        if postfix_domain is None:
            sql_postfix.create_domain(self.postfix_session, self.domain_name, delivery, transport)
            return True
        sql_postfix.update_domain(self.postfix_session, self.domain_name, delivery, transport)
        return True

    async def move_from(self,
        other_name: str,
        moving_mailboxes: list[web_models.MovingMailbox],
        moving_aliases: list[web_models.MovingAlias],
        moving_senders: list[web_models.MovingSender],
        moving_dkim: web_models.MovingDkim,
    ):
        log = logging.getLogger(__name__)
        other = others.get_other(other_name)
        if self.moving() is not None:
            if not self.move_to_me() or not self.moving_from() == other_name:
                raise Exception(f"Le domaine {self.domain_name} est déjà en cours de migration {self._moving} (tu me dis qu'il move from {other_name}).")

        # Vérifier que j'ai une ligne "other_name" dans "domain_proxy" dans la base dovecot
        # FIXME Il me faut master/password pour le proxy...
        proxy = sql_dovecot.get_proxy(
            session = self.dovecot_session,
            domain = self.domain_name,
        )
        if proxy is None:
            proxy = sql_dovecot.create_proxy(
                session = self.dovecot_session,
                domain = self.domain_name,
                host = "imap." + other.get_tech_domain(),
                master = "masterlogin",
                password = "masterpass",
            )

        # [mailbox] Je dois recevoir les boites pour dovecot, injecter avec "proxy = Y", injecter le password déjà hashé
        for mailbox in moving_mailboxes:
            db_user = sql_dovecot.get_user(
                session = self.dovecot_session,
                domain = self.domain_name,
                username = mailbox.username
            )
            if db_user is None:
                db_user = sql_dovecot.insert_user(
                    session = self.dovecot_session,
                    domain = self.domain_name,
                    username = mailbox.username,
                    password = mailbox.password,
                    active = mailbox.active,
                    proxy = "Y",
                )
            else:
                if not db_user.active == mailbox.active:
                    db_user = sql_dovecot.update_user_active(
                        session = self.dovecot_session,
                        domain = self.domain_name,
                        username = mailbox.username,
                        active = mailbox.active,
                    )
                if not db_user.password == mailbox.password:
                    db_user = sql_dovecot.update_user_raw_password(
                        session = self.dovecot_session,
                        domain = self.domain_name,
                        username = mailbox.username,
                        password = mailbox.password,
                    )
                if not db_user.proxy == "Y":
                    db_user = sql_dovecot.update_user_proxy(
                        session = self.dovecot_session,
                        domain = self.domain_name,
                        username = mailbox.username,
                        proxy = "Y",
                    )

        # Je dois recevoir les alias
        for alias in moving_aliases:
            username, domain = alias.alias.split("@")
            if not domain == self.domain_name:
                # Heu...
                continue
            db_alias = sql_postfix.get_alias(
                session = self.postfix_session,
                alias = alias.alias,
                destination = alias.destination,
            )
            if db_alias is None:
                db_alias = sql_postfix.create_alias(
                    session = self.postfix_session,
                    username = username,
                    domain = domain,
                    destination = alias.destination,
                )
            else:
                pass

        # Je dois recevoir les senders
        for sender in moving_senders:
            username, domain = sender.login.split("@")
            if not domain == self.domain_name:
                # Heu...
                continue
            db_sender = sql_postfix.get_sender(
                session = self.postfix_session,
                login = sender.login,
                sender = sender.sender,
            )
            if db_sender is None:
                db_sender = sql_postfix.create_sender(
                    session = self.postfix_session,
                    username = username,
                    domain = domain,
                    sender = sender.sender,
                )

        if ( moving_dkim is not None and
             moving_dkim.selector is not None and
             moving_dkim.private is not None and
             moving_dkim.public is not None ):
            old_dkim = sql_dkim.get_domain(self.dkim_session, self.domain_name)
            if old_dkim is None:
                sql_dkim.create_domain(
                    session = self.dkim_session,
                    name = self.domain_name,
                    selector = moving_dkim.selector,
                    private = moving_dkim.private,
                    public = moving_dkim.public,
                )
            else:
                sql_dkim.update_domain(
                    session = self.dkim_session,
                    name = self.domain_name,
                    selector = moving_dkim.selector,
                    private = moving_dkim.private,
                    public = moving_dkim.public,
                )

        # Et à la fin, on le marque dans la base de données
        self._moving_from = other_name
        self._moving_to = others.get_myself()
        self._moving = f"{self._moving_from}:{self._moving_to}"
        sql_api.update_domain_moving(self.api_session, self.domain_name, self._moving)

    async def move_to(self,
        other_name: str,
        context_name: str | None,
        context_id: str | None,
        schema_name: str | None,
    ):
        log = logging.getLogger(__name__)
        api_domain = self.api_domain()
        if api_domain is None:
            log.info(f"Le domaine {self.domain_name} n'est pas dans la base API, je ne le demenage pas")
            return
        if self.moving() is not None:
            if self.moving_to() != other_name:
                raise Exception(f"Le domaine {self.domain_name} est déjà en cours de migration ailleurs {self._moving}")

        other = others.get_other(other_name)

        pf_domain = self.postfix_domain()
        if pf_domain is None:
            log.info(f"Le domaine {self.domain_name} n'est pas dans la base Postfix, je ne le déménage pas")
            return

        if api_domain.has_feature(enums.Feature.Webmail):
            if context_name is None or schema_name is None or context_id is None:
                raise Exception("Il faut parler avec OX")

        # Sur un update, None, ça dit de ne pas changer, "" ça dit qu'on veut None...
        # Sur un post, c'est équivalent, None ou ""
        smtp_domain = api_domain.smtp_domain
        if smtp_domain is None:
            smtp_domain = ""
        imap_domain = api_domain.imap_domain
        if imap_domain is None:
            imap_domain = ""
        webmail_domain = api_domain.webmail_domain
        if webmail_domain is None:
            webmail_domain = ""

        # Il faut créer le domaine à l'autre bout, reprendre delivery, transport, features
        already = other.client.get_domain(self.domain_name)
        if already is None:
            already = other.client.post_domain(
                domain_name = self.domain_name,
                # delivery = pf_domain.delivery,
                delivery = "relay",
                features = api_domain.features,
                # transport = pf_domain.transport,
                transport = "smtp:[" + dns.get_tech_domain() + "]",
                context_name = context_name,
                context_id = context_id,
                smtp_domain = smtp_domain,
                imap_domain = imap_domain,
                webmail_domain = webmail_domain,
                schema_name = schema_name,
            )

        # Un peu de contrôle de cohérence
        # On ne peut pas contrôler le delivery, parce qu'on le change en fin de process, donc
        # si on rejoue, ça ne va plus.
        # if not already["delivery"] == pf_domain.delivery:
        #     raise Exception(f"Dans le transfert du domaine {self.domain_name} vers {other_name}, " +
        #         "le delivery ne colle pas. Ici, j'ai '{pf_domain.delivery}', de l'autre côté " + 
        #         "j'ai '{already['delivery']}'. J'arrête de chercher.")
        if not sorted(already["features"]) == sorted(api_domain.features):
            raise Exception(f"Dans le transfert du domaine {self.domain_name} vers {other_name}, " + 
                "les features ne collent pas. Ici, j'ai '{api_domain.features}', de l'autre côté " + 
                "j'ai '{already['features']}'. J'arrête de chercher.")
        if ( not already["smtp_domain"] == api_domain.smtp_domain or
             not already["imap_domain"] == api_domain.imap_domain or
             not already["webmail_domain"] == api_domain.webmail_domain ):
            raise Exception(f"Dans le transfert du domaine {self.domain_name} vers {other_name}, " +
                "les domaines ne collent pas. Ici, j'ai '{api_domain}', de l'autre côté, j'ai " +
                "'{already}'. J'arrête de chercher.")

        mailboxes = []
        if api_domain.has_feature(enums.Feature.Mailbox):
            db_users = sql_dovecot.get_users(self.dovecot_session, self.domain_name)
            for db_user in db_users:
                mailbox = {}
                mailbox["username"] = db_user.username
                mailbox["active"] = db_user.active
                mailbox["password"] = db_user.password
                mailboxes.append(mailbox)

        aliases = []
        db_aliases = sql_postfix.get_aliases_by_domain(self.postfix_session, self.domain_name)
        for db_alias in db_aliases:
            alias = {}
            alias["alias"] = db_alias.alias
            alias["destination"] = db_alias.destination
            aliases.append(alias)

        senders = []
        db_senders = sql_postfix.get_senders_by_domain(self.postfix_session, self.domain_name)
        for db_sender in db_senders:
            sender = {}
            sender["login"] = db_sender.login
            sender["sender"] = db_sender.sender

        dkim_domain = sql_dkim.get_domain(self.dkim_session, self.domain_name)
        moving_dkim = {
            "selector": None,
            "private": None,
            "public": None,
        }
        if dkim_domain is not None:
            moving_dkim = {
                "selector": dkim_domain.selector,
                "private": dkim_domain.private,
                "public": dkim_domain.public,
            }

        # Il faut dire à <other> que le domaine move_to lui (et donc, move_from <myself>)
        await other.client.import_domain(
            domain_name = self.domain_name,
            moving_from = others.get_myself(),
            moving_mailboxes = mailboxes,
            moving_aliases = aliases,
            moving_senders = senders,
            moving_dkim = moving_dkim,
        )

        # Dire à postfix que si un mail se présente, il faut l'envoyer à <other>
        # sql_postfix.update_domain(
        #     self.postfix_session,
        #     domain = self.domain_name,
        #     delivery = "relay",
        #     transport = "smtp." + other.get_tech_domain(),
        # )

        # Et à la fin, on le marque dans la base de données
        self._moving_to = other_name
        self._moving_from = others.get_myself()
        self._moving = f"{self._moving_from}:{self._moving_to}"
        sql_api.update_domain_moving(self.api_session, self.domain_name, self._moving)

        # FIXME Il faudrait...
        # - retirer le mapping OX
        # - retirer le contexte s'il est devenu vide
        # - retirer la feature webmail
        # - purger les alias (on ne peut pas rejouer) ==> retirer la feature 'alias'
        # cf. issue 23

    ###
    ### Gestion des enregistrement attendus sur un domaine
    ###

    def get_specs(self):
        api_domain = self.api_domain()
        if api_domain is None:
            return None

        result = []
    
        result.append({"target": "", "type": "mx", "value": self.get_target("mx")})
        
        if api_domain.has_feature("mailbox"):
            dkim_domain = self.dkim_domain()
            if dkim_domain is not None:
                want_dkim = dns.DkimInfo(dkim_domain.public)
                result.append({
                    "target": dkim_domain.selector + "._domainkey",
                    "type": "txt",
                    "value": want_dkim.to_spec()
                })
            # {"target":"_dmarc","type":"txt","value":"v=DMARC1; p=reject; rua=mailto:abuse@mail.numerique.gouv.fr; adkim=s; aspf=s;"},
            
            result.append({
                "target": api_domain.get_imap_name(),
                "type": "cname",
                "value": self.get_target("imap")
            })
            result.append({
                "target": api_domain.get_smtp_name(),
                "type": "cname",
                "value": self.get_target("smtp")
            })
            result.append({
                "target":"",
                "type":"txt",
                "value": "v=spf1 include:" + self.get_target("spf") + " -all"
            })

        if api_domain.has_feature("webmail"):
            result.append({
                "target": api_domain.get_webmail_name(),
                "type":"cname",
                "value": self.get_target("webmail")
            })

        return result

    ###
    ### Gestion du check domain
    ###

    def add_error(self, err: dns.Err | None):
        if err is None:
            return
        self.valid = False
        self.errors.append(err)

    def check_postfix(self) -> dns.Err | None:
        # FIXME Si le domaine est moving_to_me, le delivery doit être "relay" et
        # le transport smtp:[smtp. other.get_tech_domain()]

        pf_domain = self.postfix_domain()
        if pf_domain is None:
            print(f"Le domain n'est pas dans la base postfix")
            return dns.Err(
                name="postfix",
                code="no_delivery",
                detail="Le domaine n'est pas déclaré dans la base postfix",
            )
        if pf_domain.delivery == "relay" and pf_domain.transport is None:
            print(f"Le domaine est en 'relay', sans transport")
            return dns.Err(
                name="postfix",
                code="no_transport",
                detail="Le domaine est en delivery='relay' et n'a pas de transport",
            )
        return None

    async def check_ox(self) -> dns.Err | None:
        api_domain = self.api_domain()
        if not api_domain.has_feature(enums.Feature.Webmail):
            print("Pas de feature 'webmail', donc pas de check sur OX")
            return None

        ox_cluster = oxcli.OxCluster()
        ctx = await ox_cluster.get_context_by_domain(self.domain_name)
        if ctx is None:
            print(f"Le domaine n'est pas déclaré chez OX")
            return dns.Err(
                name = "ox",
                code = "no_context",
                detail = "Le domaine a la feature 'webmail' mais n'est dans aucun contexte d'open-xchange",
            )
        return None

    async def check_dkim(self) -> dns.Err | None:
        api_domain = self.api_domain()
        if not api_domain.has_feature(enums.Feature.Mailbox):
            return None
        dkim_domain = self.dkim_domain()
        if dkim_domain is None or dkim_domain.selector is None or dkim_domain.private is None or dkim_domain.public is None:
            print(f"Le domaine n'est pas dans la base dkim, ou seulement partiellement")
            return dns.Err(
                name="dkim",
                code="no_key",
                detail="Nous n'avons pas de clef pour un domaine avec la feature 'mailbox'"
            )
        try:
            want_dkim = dns.DkimInfo(dkim_domain.public)
        except Exception:
            print(f"La clef publique en base dkim est illisible pour moi")
            want_dkim = "I have a broken DKIM public key in the database"
            return dns.Err(
                name="dkim",
                code="broken_key",
                detail="La clef DKIM en base pour ce domaine est invalide"
            )
        err = await dns.check_dkim(self.domain_name, dkim_domain.selector, want_dkim)
        return err

    async def check_dns(self) -> bool:
        err = await dns.check_mx(self.domain_name, self.get_target("mx"))
        self.add_error(err)
        api_domain = self.api_domain()
        if api_domain.has_feature(enums.Feature.Webmail):
            err = await dns.check_cname(
                domain = api_domain.get_domain("webmail"),
                name = "webmail",
                target = self.get_target("webmail")
            )
            self.add_error(err)
        if api_domain.has_feature(enums.Feature.Mailbox):
            err = await dns.check_cname(
                domain = api_domain.get_domain("imap"),
                name = "imap",
                target = self.get_target("imap")
            )
            self.add_error(err)
            err = await dns.check_cname(
                domain = api_domain.get_domain("smtp"),
                name = "smtp",
                target = self.get_target("smtp")
            )
            self.add_error(err)
        if len(self.errors) == 0:
            return True
        return False

    async def internal_check(self) -> (bool, list[dns.Err]):
        self.valid = True
        self.errors = []

        api_domain = self.api_domain()
        if api_domain is None:
            raise Exception(f"Tu me fais controler le domain {self.domain_name} qui n'existe pas dans la base API.")

        err = self.check_postfix()
        self.add_error(err)

        err = await self.check_ox()
        self.add_error(err)

        err = self.check_cert()
        self.add_error(err)

        err = await dns.check_exists(self.domain_name)
        if err:
            return False, [err]
        await self.check_dns() # Will add_error when needed

        if api_domain.has_feature(enums.Feature.Mailbox):
            err = await dns.check_spf(self.domain_name, required_spf = self.get_target("spf"))
            self.add_error(err)
            err = await self.check_dkim()
            self.add_error(err)

        print(f"A la fin du check valid = {self.valid}, errors = {self.errors}")
        return self.valid, self.errors

    async def foreground_check(self):
        valid, errors = await self.internal_check()
        sql_api.update_domain_dtchecked(self.api_session, self.domain_name, "now")
        if valid:
            sql_api.update_domain_state(self.api_session, self.domain_name, "ok")
            sql_api.update_domain_errors(self.api_session, self.domain_name, None)
        else:
            sql_api.update_domain_state(self.api_session, self.domain_name, "broken")
            errs = []
            for err in errors:
                errs.append({"test": err.name, "code": err.code, "detail": err.detail})
            sql_api.update_domain_errors(self.api_session, self.domain_name, errs)
        return self

    ###
    ### Gestion du fix domaine
    ###

    async def fix_domain(self):
        print(f"J'essaye de corriger le domain {self.domain_name}")
        err = self.check_postfix()
        if err is not None:
            # Erreurs possibles :
            # err.code == "no_delivery" : il manque la ligne de delivery
            # err.code == "no_transport" : le domaine est en "relay" sans transport
            # Dans les deux cas, en le passant en "virtual" il marchera mieux.
            print(f"Probleme dans la config postfix : {err.code}")
            self.update_delivery(enums.Delivery.Virtual, None)

        err = await self.check_dkim()
        if err is not None:
            # Erreurs possibles :
            # err.code == "no_key" : il manque la ligne dans la base dkim
            # err.code == "broken_key" : la clef publique est illisible dans la base
            # Dans ces deux cas, en produisant une nouvelle clef on devrait corriger
            # err.code == "no_dkim" : il manque le record chez le client
            # err.code == "wrong_dkim" : la clef dans la zone n'est pas la bonne
            # Dans ces deux cas, il faut que le client corrige la zone
            if err.code == "no_key" or err.code == "broken_key":
                print(f"Probleme dans la config dkim : {err.code}")
                self.update_dkim_keys(force=True)

        err = await self.check_ox()
        if err is not None:
            # Erreurs possibles :
            # err.code == 'no_context'
            print(f"Probleme dans la config ox : {err.code}")
            await self.update_ox_context("fix_auto_context")

        err = self.check_cert()
        if err is not None:
            # Quand on va essayer de faire le certificat, on va vérifier la config DNS
            print(f"Probleme sur le certificat : {err.code}")
            await self.make_cert()

        # FIXME Si le domaine est moving_to_me, il doit avoir un delivery 'relay' et un
        # transport 'smtp:[smtp. other.get_tech_domain()]'

        print("J'ai corrigé ce que je pouvais")

    ###
    ### Gestion du certificat
    ###

    async def make_cert(self):
        self.errors = []
        err = await dns.check_exists(self.domain_name)
        if err:
            self.errors.append(err)
            return
        if not await self.check_dns():
            return
        cert_name = self.cert_name()
        if cert_name is None:
            return
        if self.move_from_me():
            # Si le domaine part de chez moi, le certificat ne doit *pas* exister chez moi
            return cbcli.make_nocert(cert_name = cert_name)
        return cbcli.make_cert(
            cert_name = cert_name,
            imap_name = self.imap_domain(),
            smtp_name = self.smtp_domain(),
            webmail_name = self.webmail_domain(),
        )

    def check_cert(self) -> dns.Err | None:
        cert_name = self.cert_name()
        if cert_name is None:
            return None
        if self.move_from_me():
            # Si le domaine part de chez moi, le certificat ne doit *pas* exister chez moi
            return cbcli.check_nocert(cert_name = cert_name)
        return cbcli.check_cert(
            cert_name = cert_name,
            imap_name = self.imap_domain(),
            smtp_name = self.smtp_domain(),
            webmail_name = self.webmail_domain(),
        )

    ###
    ### Export sur le web_model
    ###

    async def to_web(self) -> web_models.Domain | None:
        api_domain = self.api_domain()
        if api_domain is None:
            return None
        result = web_models.Domain(
            name = self.domain_name,
            delivery = "broken",
            state = api_domain.state,
            features = api_domain.features,
            webmail_domain = api_domain.webmail_domain,
            imap_domain = api_domain.imap_domain,
            smtp_domain = api_domain.smtp_domain,
        )
        if api_domain.state == "new":
            result.valid = False
        if api_domain.dtchecked is None:
            result.set_no_test()
        if api_domain.errors is not None:
            result.add_errors(api_domain.errors)

        postfix_domain = self.postfix_domain()
        if postfix_domain is not None:
            result.delivery = postfix_domain.delivery
            result.transport = postfix_domain.transport

        if api_domain.has_feature(enums.Feature.Webmail):
            ox_cluster = oxcli.OxCluster()
            ctx = await ox_cluster.get_context_by_domain(self.domain_name)
            if ctx is not None:
                result.context_name = ctx.name
            else:
                result.context_name = "broken"
                result.state = "broken"

        return result

async def background_check_new_domain(name: str):
    log = logging.getLogger(__name__)
    api_maker = sql_api.get_maker()
    api_session = api_maker()
    dkim_maker = sql_dkim.get_maker()
    dkim_session = dkim_maker()
    dovecot_maker = sql_dovecot.get_maker()
    dovecot_session = dovecot_maker()
    postfix_maker = sql_postfix.get_maker()
    postfix_session = postfix_maker()
    try:
        b_domain = Domain(
            name = name,
            api_session = api_session,
            dkim_session = dkim_session,
            dovecot_session = dovecot_session,
            postfix_session = postfix_session,
        )
        await b_domain.foreground_check()
    except Exception as e:
        log.error(f"J'ai échoué à vérifier le domain {name}, avec l'exception e = {e}. Probablement un domaine qui n'existe pas.")
    api_session.close()
    postfix_session.close()
    dkim_session.close()
    dovecot_session.close()
