import pytest

from .. import business, dns, others, sql_api, sql_dkim, sql_dovecot, sql_postfix, web_models

@pytest.mark.asyncio
async def test_moving(log, db_api_session, db_dkim_session, db_postfix_session, db_dovecot_session, set_others, dns_tech, setup_remote):

    assert others.get_myself() == "testing"

    b_domain = business.Domain(
        name = "example.com",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    await b_domain.create_domain(
        features = [ ],
        delivery = "alias",
        transport = None,
        context_name = None,
    )

    assert b_domain.moving() is None
    assert b_domain.moving_from() is None
    assert b_domain.moving_to() is None
    assert b_domain.move_from_me() is False
    assert b_domain.move_to_me() is False
    assert b_domain.get_tech_domain() == dns_tech

    db_dom = sql_api.update_domain_moving(db_api_session, "example.com", "recent:testing")
    b_domain = business.Domain(
        name = "example.com",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    assert b_domain.moving() == "recent:testing"
    assert b_domain.moving_from() == "recent"
    assert b_domain.moving_to() == "testing"
    assert b_domain.move_from_me() is False
    assert b_domain.move_to_me() is True
    assert b_domain.get_tech_domain() == dns_tech

    # Je bouge une mailbox
    db_box = sql_dovecot.insert_user(
        session = db_dovecot_session,
        username = "toto",
        domain = "example2.com",
        password = "coincoin",
        active = "Y",
        proxy = "Y",
    )
    b_mailbox = business.Mailbox(
        username = "toto",
        domain = "example2.com",
        postfix_session = db_postfix_session,
        dovecot_session = db_dovecot_session,
        webmail = True,
    )
    res = await b_mailbox.move(from_name = b_domain.moving_from())
    assert res is True


    db_dom = sql_api.update_domain_moving(db_api_session, "example.com", None)
    b_domain = business.Domain(
        name = "example.com",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    assert b_domain.moving() is None

    db_dom = sql_api.update_domain_moving(db_api_session, "example.com", None)

    b_domain = business.Domain(
        name = "example.com",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    assert b_domain.moving() is None

    await b_domain.move_from(
        other_name = "recent",
        moving_mailboxes = [
            web_models.MovingMailbox(
                username = "jean",
                active = "Y",
                password = "{PLAIN}toto",
            )
        ],
        moving_aliases = [
            web_models.MovingAlias(
                alias = "pierre@example.com",
                destination = "pierre@gmail.com",
            )
        ],
        moving_senders = [
            web_models.MovingSender(
                login = "jean@example.com",
                sender = "contact@example.com",
            )
        ],
        moving_dkim = web_models.MovingDkim(
            selector = "coincoin",
            private = "The private key",
            public = "The public dns record",
        )
    )
    assert b_domain.moving() == "recent:testing"

    # recent est une API en v0.1.5+, qui sait faire le GET /tech_domain
    db_dom = sql_api.update_domain_moving(db_api_session, "example.com", "testing:recent")
    b_domain = business.Domain(
        name = "example.com",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    assert b_domain.moving() == "testing:recent"
    assert b_domain.moving_from() == "testing"
    assert b_domain.moving_to() == "recent"
    assert b_domain.move_from_me() is True
    assert b_domain.move_to_me() is False
    assert b_domain.get_tech_domain() == "new1.nox.numerique.gouv.fr"

    # Je crée un deuxième domaine
    b_domain = business.Domain(
        name = "example3.com",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    await b_domain.create_domain(
        features = [ ],
        delivery = "alias",
        transport = None,
        context_name = None,
    )
    # Je lui dit de déménager vers "recent"
    await b_domain.move_to(
        other_name = "recent",
        context_id = "42",
        context_name = "testing",
        schema_name = "ox_test_database_12",
    )


@pytest.mark.asyncio
async def test_cert_name(db_api_session, db_dkim_session, db_dovecot_session, db_postfix_session, async_client):
    b_domain = business.Domain(
        name = "example.com",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    await b_domain.create_domain(
        features = [ ],
        delivery = "alias",
        transport = None,
        context_name = None,
    )
    
    # Sans feature, pas de certificat
    name = b_domain.cert_name()
    assert name is None

    await b_domain.update_features(features = ["mailbox"], context_name = None)

    # Avec la feature "mailbox", il faut un certificat
    name = b_domain.cert_name()
    assert name == "client_example.com"


@pytest.mark.asyncio
async def test_create_domain(db_api_session, db_dkim_session, db_dovecot_session, db_postfix_session, async_client):
    b_domain = business.Domain(
        name = "example.com",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    await b_domain.create_domain(
        features = [ "webmail", "mailbox" ],
        delivery = "virtual",
        transport = None,
        context_name = "dimail",
    )
    b_domain.update_domains(
        imap_domain = "imap.weird.example.com",
        smtp_domain = "smtp.weird.example.com",
        webmail_domain = "webmail.weird.example.com",
    )

    api_domain = sql_api.get_domain(db_api_session, "example.com")
    assert isinstance(api_domain, sql_api.Domain)
    assert api_domain.name == "example.com"
    assert api_domain.imap_domain == "imap.weird.example.com"
    assert api_domain.smtp_domain == "smtp.weird.example.com"
    assert api_domain.webmail_domain == "webmail.weird.example.com"

    pf_domain = sql_postfix.get_domain(db_postfix_session, "example.com")
    assert isinstance(pf_domain, sql_postfix.Domain)
    assert pf_domain.domain == "example.com"
    assert pf_domain.delivery == "virtual"
    assert pf_domain.transport is None

    dkim_domain = sql_dkim.get_domain(db_dkim_session, "example.com")
    assert isinstance(dkim_domain, sql_dkim.Domain)

    b_domain.update_delivery("relay", "smtp:[mx.example.com]")
    pf_domain = sql_postfix.get_domain(db_postfix_session, "example.com")
    assert isinstance(pf_domain, sql_postfix.Domain)
    assert pf_domain.domain == "example.com"
    assert pf_domain.delivery == "relay"
    assert pf_domain.transport == "smtp:[mx.example.com]"


@pytest.mark.asyncio
async def test_specs_domain(db_api_session, db_dkim_session, db_dovecot_session, db_postfix_session, async_client):
    b_domain = business.Domain(
        name = "example.com",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    await b_domain.create_domain(
        features = [],
        delivery = "virtual", 
        transport = None,
        context_name = None,
    )

    specs = b_domain.get_specs()
    values = [ x["value"] for x in specs ]
    assert "mx.ox.numerique.gouv.fr." in values
    assert "imap.ox.numerique.gouv.fr." not in values
    assert "smtp.ox.numerique.gouv.fr." not in values
    assert "v=spf1 include:_spf.ox.numerique.gouv.fr -all" not in values
    assert "webmail.ox.numerique.gouv.fr." not in values
    targets = [ x["target"] for x in specs ]
    assert "dimail._domainkey" not in targets

    await b_domain.update_features(
        features = [ "mailbox" ],
        context_name = None,
    )

    specs = b_domain.get_specs()
    values = [ x["value"] for x in specs ]
    assert "mx.ox.numerique.gouv.fr." in values
    assert "imap.ox.numerique.gouv.fr." in values
    assert "smtp.ox.numerique.gouv.fr." in values
    assert "v=spf1 include:_spf.ox.numerique.gouv.fr -all" in values
    assert "webmail.ox.numerique.gouv.fr." not in values
    targets = [ x["target"] for x in specs ]
    assert "dimail._domainkey" in targets

    await b_domain.update_features(
        features = [ "mailbox", "webmail" ],
        context_name = "dimail",
    )

    specs = b_domain.get_specs()
    values = [ x["value"] for x in specs ]
    assert "mx.ox.numerique.gouv.fr." in values
    assert "imap.ox.numerique.gouv.fr." in values
    assert "smtp.ox.numerique.gouv.fr." in values
    assert "v=spf1 include:_spf.ox.numerique.gouv.fr -all" in values
    assert "webmail.ox.numerique.gouv.fr." in values
    targets = [ x["target"] for x in specs ]
    assert "dimail._domainkey" in targets


@pytest.mark.asyncio
async def test_domain_fix(db_api_session, db_dkim_session, db_dovecot_session, db_postfix_session, ox_cluster, async_client):

    # Quand je fais un fix sur un domaine qui n'existe pas, la clef dkim n'est pas modifiée
    na_domain = business.Domain(
        name = "this-is-not-a-domain-coincoin.fr",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    await na_domain.create_domain(
        features = [ "webmail", "mailbox" ],
        delivery = "virtual",
        transport = None,
        context_name = "dimail",
    )

    dkim = sql_dkim.get_domain(db_dkim_session, "this-is-not-a-domain-coincoin.fr")
    priv_key = dkim.private

    await na_domain.fix_domain()

    dkim = sql_dkim.get_domain(db_dkim_session, "this-is-not-a-domain-coincoin.fr")
    assert priv_key == dkim.private


@pytest.mark.asyncio
async def test_domain_check(db_api_session, db_dkim_session, db_dovecot_session, db_postfix_session, ox_cluster, async_client):

    # Quand on check un domaine qui n'est pas dans la base API, une exception
    b_domain = business.Domain(
        name = "example.com",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    with pytest.raises(Exception) as e:
        _, _ = await b_domain.internal_check()
    assert "existe pas" in str(e.value)

    # Quand on check un domaine qui n'existe pas sur Internet, on a une erreur
    not_a_domain = sql_api.create_domain(
        db_api_session,
        name="this-is-not-a-domain-coincoin.fr",
        features=["mailbox"],
    )
    na_domain = business.Domain(
        name = "this-is-not-a-domain-coincoin.fr",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    valid, errors = await na_domain.internal_check()
    assert valid == False
    assert len(errors) == 1
    assert errors[0].name == 'domain_exist'
    assert errors[0].code == "must_exist"

    # Sur le domaine example.com, on a des erreurs
    domain = sql_api.create_domain(
        db_api_session,
        name="example.com",
        features=["mailbox"],
    )
    valid, errors = await b_domain.internal_check()
    assert valid == False


    domain = sql_api.create_domain(
        db_api_session,
        name = "fdn.fr",
        features = [ "webmail", "mailbox" ],
    )
    dkim_domain = sql_dkim.create_domain(
        db_dkim_session,
        name = "fdn.fr",
        selector = "ox2024",
        public = "Broken key",
        private = "Coin coin très secret",
    )
    b_domain = business.Domain(
        name = "fdn.fr",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    valid, errors = await b_domain.internal_check()
    assert valid is False
    codes = [ err.code for err in errors ]
    assert "wrong_mx" in codes
    assert "wrong_cname_webmail" in codes
    assert "wrong_cname_imap" in codes
    assert "no_cname_smtp" in codes
    assert "wrong_spf" in codes
    assert "broken_key" in codes
    assert "no_delivery" in codes
    assert "no_context" in codes
    assert "no_cert" in codes
    assert len(errors) == 9

    domain = sql_api.create_domain(
        db_api_session,
        name="mail.numerique.gouv.fr",
        imap_domain="imap.numerique.gouv.fr",
        smtp_domain="smtp.numerique.gouv.fr",
        webmail_domain="webmail.numerique.gouv.fr",
        features=["webmail", "mailbox"],
    )
    pf_domain = sql_postfix.create_domain(
        db_postfix_session,
        domain = "mail.numerique.gouv.fr",
        delivery = "virtual",
        transport = None,
    )
    dkim_domain = sql_dkim.create_domain(
        db_dkim_session,
        name = "mail.numerique.gouv.fr",
        selector = "mecol",
        public = ("mecol._domainkey.mail.numerique.gouv.fr IN TXT (" +
        """"v=DKIM1; h=sha256; k=rsa; "
                "p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2dg8Bt0+p4EEIGF3udBpR"
                "psTd9B0UUzZPTJo64fwijJxFo8RgVUOe8vV6xzhGI22ldMAl6fYNsXih7p/AhEk+CpH"
                "QBFuittufD6Q8XyNrYMblHHfUKlkdy63Bi9v784qc1bWVI+/YRuFzEVnxQkNlbNyKFr"
                "ulZ6J/f7LR1sreSZakMHgy3ePp0QS9oUxs8tYxzWTSfnTS/VAv7"
                "GD4VoZMvLSa+u1fikagc5t3xg76P9twzBOjuFFqIFg+wPGzZZWpzSh/yfcMWHg+eLxk"
                "sxcronXnNZNnfPppNdu2Id28amHB/WB/4vqmgeM3xYIZWETDvZZIjVOzlxGtfgLuNlV"
                "LwIDAQAB") ; -- This is a real from production domain
        """),
        private = "Coin coin très secret",
    )
    await ox_cluster.create_context(None, "dimail", "mail.numerique.gouv.fr")
    b_domain = business.Domain(
        name = "mail.numerique.gouv.fr",
        api_session = db_api_session,
        dkim_session = db_dkim_session,
        dovecot_session = db_dovecot_session,
        postfix_session = db_postfix_session,
    )
    valid, errors = await b_domain.internal_check()
    # C'est un vrai domain de production, dont nous n'avons pas le certificat.
    assert valid is False
    assert len(errors) == 1
    assert errors[0].code == "no_cert"



