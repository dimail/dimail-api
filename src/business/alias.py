import fastapi
import sqlalchemy.orm as orm
import logging

from .. import oxcli, sql_api, sql_postfix, utils, web_models

class Alias:
    def __init__(
        self,
        *,
        #webmail: bool,
        destination: str,
        postfix_session: orm.Session,
        #dovecot_session: orm.Session,
        username: str | None = None,
        domain: str | None = None,
        webmail: bool | None = None,
        alias: str | None = None,
        #dovecot_mailbox: sql_dovecot.ImapUser | None | str = "rien",
        ox_dest_mailbox: oxcli.OxUser | None | str = "rien",
        db_alias: sql_postfix.Alias | None | str = "rien",
    ):
        if username is None and domain is None and alias is not None:
            username, domain = utils.split_email(alias)
        if alias is None and username is not None and domain is not None:
            alias = username + "@" + domain
        if alias is None or username is None or domain is None:
            raise Exception("Il faut fournir soit (username,domain), soit 'alias'.")
        if not alias == username + "@" + domain:
            raise Exception("Si tu fournis les trois valeurs (username, domain, alias) il faut qu'elles soient cohérentes.")
        if destination is None:
            raise Exception("Il faut fournir une destination pour un alias.")
        self.username = username
        self.domain = domain
        self.webmail = webmail
        self.alias = alias
        self.destination = destination
        self.postfix_session = postfix_session
        if not db_alias == "rien":
            self.db_alias = db_alias
        if ox_dest_mailbox != "rien":
            self._ox_dest_mailbox = ox_dest_mailbox

    ###
    ### Gestion du cache
    ###

    def get_db_alias(self) -> sql_postfix.Alias:
        if hasattr(self, 'db_alias'):
            return self.db_alias
        self.db_alias = sql_postfix.get_alias(self.postfix_session, self.alias, self.destination)
        if self.db_alias is not None:
            if self.db_alias.domain != self.domain:
                raise Exception(
                   "The alias in database is not consistent (alias and domain disagree)"
                )
        return self.db_alias

    def get_db_sender(self) -> sql_postfix.Sender:
        if hasattr(self, 'db_sender'):
            return self.db_sender
        self.db_sender = sql_postfix.get_sender(
            self.postfix_session,
            login = self.destination,
            sender = self.alias,
        )
        return self.db_sender

    async def ox_dest_mailbox(self) -> oxcli.OxUser:
        log = logging.getLogger(__name__)
        if not hasattr(self, '_ox_mailbox'):
            ox = oxcli.OxCluster()
            self._ox_dest_mailbox = await ox.get_user_by_email(self.destination)
            if self.webmail and self._ox_dest_mailbox is None:
                log.debug("Le contexte OX ne connait pas cet email (ce n'est pas normal)")
            if not self.webmail and self._ox_dest_mailbox:
                log.debug("Le contexte OX connait cet email (ce n'est pas normal)")
        return self._ox_dest_mailbox
    
    ###
    ### Méthodes métier
    ###

    def create_alias(self):
        db_alias = self.get_db_alias()
        if db_alias is not None:
            print("Cet alias existe deja")
            raise fastapi.HTTPException(status_code=409, detail="Alias already exists")
        db_alias = sql_postfix.create_alias(self.postfix_session, self.domain, self.username, self.destination)
        self.db_alias = db_alias

    async def delete_alias(self):
        sql_postfix.delete_sender(
            self.postfix_session,
            login = self.destination,
            sender = self.alias,
        )
        self.db_sender = None

        if self.webmail:
            ox_box = await self.ox_dest_mailbox() 
            if ox_box is not None:
                await ox_box.remove_sender(self.alias)

        count = sql_postfix.delete_alias(
            self.postfix_session,
            alias = self.alias,
            destination = self.destination,
        )
        if count == 0:
            print("Cet alias n'existe pas")
            raise fastapi.HTTPException(status_code=404, detail="Not found")
        self.db_alias = None

    async def update_allow(self, allow_to_send: bool):
        log = logging.getLogger(__name__)
        sender = self.get_db_sender()

        if allow_to_send and sender is None:
            s_username, s_domain = utils.split_email(self.destination)
            sender = sql_postfix.create_sender(
                self.postfix_session,
                domain = s_domain,
                username = s_username,
                sender = self.alias,
            )
            self.db_sender = sender

        if not allow_to_send and sender is not None:
            sql_postfix.delete_sender(
                self.postfix_session,
                login = self.destination,
                sender = self.alias,
            )
            self.db_sender = None
        
        if self.webmail:
            ox_box = await self.ox_dest_mailbox()
            if ox_box is not None:
                if allow_to_send and self.alias not in ox_box.senders:
                    await ox_box.add_senders([self.alias])
                if not allow_to_send and self.alias in ox_box.senders:
                    await ox_box.remove_sender(self.alias)

    ###
    ### Export vers les web_models
    ###

    def to_web(self) -> web_models.Alias:
        db_alias = self.get_db_alias()

        allow_to_send = False
        sender = self.get_db_sender()
        if sender is not None:
            allow_to_send = True

        return web_models.Alias(
            destination = self.destination,
            domain = self.domain,
            username = self.username,
            allow_to_send = allow_to_send,
        )

class AliasMaker:
    def __init__(
        self,
        *,
        api_session: orm.Session,
        postfix_session: orm.Session,
        #dovecot_session: orm.Session,
    ):
        self.api_session = api_session
        self.postfix_session = postfix_session

    def get_aliases(self,
        domain: str,
        username: str = "",
        destination: str = "",
    ) -> list[Alias]:
        if domain == "" or domain is None:
            raise Exception("Il me faut un domaine.")
        if username is None:
            username = ""
        if destination is None:
            destination = ""
        if username == "" and not destination == "":
            raise fastapi.HTTPException(
                status_code=412,
                detail="It is forbiden to only provide the destination in a request"
            )
        alias = username + "@" + domain

        aliases = []
        if username == "" and destination == "":
            print(f"Pas de username, pas de destination, on cherche tous les alias de {domain}")
            aliases = sql_postfix.get_aliases_by_domain(self.postfix_session, domain)
        elif destination == "":
            print(f"Pas de destination, on cherche toutes les destinations pour {alias}")
            aliases = sql_postfix.get_aliases_by_name(self.postfix_session, alias)
        else:
            print(f"On cherche un alias exact {alias}->{destination}")
            db_alias = sql_postfix.get_alias(self.postfix_session, alias, destination)
            if db_alias is None:
                print("Cet alias n'existe pas")
                raise fastapi.HTTPException(status_code=404, detail="Alias not found")
            aliases = [ db_alias ]

        result = []
        for alias in aliases:
            with_webmail = False
            _, dest_domain = utils.split_email(alias.destination, strict=False)
            if dest_domain is not None:
                db_dest_domain = sql_api.get_domain(self.api_session, dest_domain)
                if db_dest_domain is not None:
                    if db_dest_domain.has_feature('webmail'):
                        with_webmail = True
            result.append( Alias(
                postfix_session = self.postfix_session,
                alias = alias.alias,
                destination = alias.destination,
                db_alias = alias,
                webmail = with_webmail,
            ))
        return result



