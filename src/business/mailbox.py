import asyncio
import logging
import fastapi
import secrets
import sqlalchemy.orm as orm

from .. import enums, others, oxcli, remote, sql_dovecot, sql_postfix, web_models

class Mailbox:
    def __init__(
        self,
        *,
        webmail: bool,
        postfix_session: orm.Session,
        dovecot_session: orm.Session,
        username: str | None = None,
        domain: str | None = None,
        email: str | None = None,
        dovecot_mailbox: sql_dovecot.ImapUser | None | str = "rien",
        ox_mailbox: oxcli.OxUser | None | str = "rien",
        ox_ctx: oxcli.OxContext | None | str = "rien",
    ):
        if username is None and domain is None and email is not None:
            username, domain = email.split("@")
        if username is None or domain is None or webmail is None:
            raise Exception("Les attributs ['username'+'domain' ou 'email'] et 'webmail' sont obligatoires")
        self.username = username
        self.domain = domain
        self.webmail = webmail
        self.email = self.username + "@" + self.domain
        self.postfix_session = postfix_session
        self.dovecot_session = dovecot_session
        if ox_ctx != "rien":
            self._ox_ctx = ox_ctx
        if ox_mailbox != "rien":
            self._ox_mailbox = ox_mailbox
        if dovecot_mailbox != "rien":
            self._dovecot_mailbox = dovecot_mailbox

    ###
    ### Gestion du cache
    ###

    def dovecot_mailbox(self) -> sql_dovecot.ImapUser:
        log = logging.getLogger(__name__)
        if not hasattr(self, '_dovecot_mailbox'):
            self._dovecot_mailbox = sql_dovecot.get_user(
                session = self.dovecot_session,
                username = self.username,
                domain = self.domain,
            )
            if self._dovecot_mailbox is None:
                log.debug("La base dovecot ne contient pas cette adresse.")
        return self._dovecot_mailbox

    async def ox_ctx(self) -> oxcli.OxContext:
        log = logging.getLogger(__name__)
        if not hasattr(self, '_ox_ctx'):
            ox = oxcli.OxCluster()
            self._ox_ctx = await ox.get_context_by_domain(self.domain)
            if self.webmail and self._ox_ctx is None:
                log.debug("Aucun contexte ne gère le domaine chez OX (ce n'est pas normal)")
            if not self.webmail and self._ox_ctx:
                log.debug("Il y a un context pour ce domaine chez OX (ce n'est pas normal)")
        return self._ox_ctx


    async def ox_mailbox(self) -> oxcli.OxUser:
        log = logging.getLogger(__name__)
        if not hasattr(self, '_ox_mailbox'):
            ox = oxcli.OxCluster()
            self._ox_mailbox = await ox.get_user_by_email(self.email)
            if self.webmail and self._ox_mailbox is None:
                log.debug("Le contexte OX ne connait pas cet email (ce n'est pas normal)")
            if not self.webmail and self._ox_mailbox:
                log.debug("Le contexte OX connait cet email (ce n'est pas normal)")
        return self._ox_mailbox

    ###
    ### Méthodes métier
    ###

    async def get_senders(self) -> (list[str], bool):
        log = logging.getLogger(__name__)
        db_senders = sql_postfix.get_senders_by_login(
            self.postfix_session,
            self.email,
        )
        db_senders = [ x.sender for x in db_senders ]

        ox_senders = []
        if self.webmail:
            ox_box = await self.ox_mailbox()
            if ox_box is not None:
                ox_senders = ox_box.senders
                ox_senders.remove(self.email)

        log.debug(f"ox_senders = {ox_senders}, db_senders = {db_senders}")
        senders = set(db_senders + ox_senders)
        senders = list(senders)

        consistent = True
        if self.webmail:
            if len(senders) == len(ox_senders) and len(ox_senders) == len(db_senders):
                consistent = True
            else:
                consistent = False

        return list(senders), consistent


    async def create_mailbox(self,
        *,
        givenName: str | None = None,
        surName: str | None = None,
        displayName: str | None = None,
        senders: list[str] | None = None,
    ):
        log = logging.getLogger(__name__)
        if self.webmail:
            if givenName is None or surName is None or displayName is None:
                raise Exception("Pour le webmail, il me faut des noms")
            ox_ctx = await self.ox_ctx()
            if ox_ctx is None:
                log.debug(f"Le domaine {self.domain} est inconnu du cluster OX")
                raise Exception("Le domaine est connu de la base API, mais pas de OX")

            ox_box = await ox_ctx.create_user(
                givenName = givenName,
                surName = surName,
                displayName = displayName,
                username = self.username,
                domain = self.domain,
            )
            self._ox_mailbox = ox_box

        password = secrets.token_urlsafe(12)
        self.clear_password = password
        db_box = sql_dovecot.create_user(self.dovecot_session, self.username, self.domain, password)

        if senders is not None:
            for sender in senders:
                sql_postfix.create_sender(
                    self.postfix_session,
                    domain = self.domain,
                    username = self.username,
                    sender = sender,
                )
            if self.webmail:
                ox_box = await self.ox_mailbox()
                await ox_box.add_senders(senders)

    def reset_password(self):
        log = logging.getLogger(__name__)
        db_box = self.dovecot_mailbox()
        if db_box is None:
            raise Exception("Je ne peux pas reset le password d'une mailbox qui n'existe pas dans dovecot")

        password = secrets.token_urlsafe(12)
        self.clear_password = password
        sql_dovecot.update_user_password(
            session = self.dovecot_session,
            username = self.username,
            domain = self.domain,
            password = password,
        )

    async def delete_mailbox(self):
        if self.webmail:
            ox_box = await self.ox_mailbox()
            if ox_box is None:
                raise Exception("Je ne peux pas supprimer une boite qui n'existe pas")
            await ox_box.delete()
            ox_box = None
            self._ox_mailbox = None
        sql_dovecot.delete_user(self.dovecot_session, self.username, self.domain)
        self._dovecot_mailbox = None
        sql_postfix.delete_senders_by_login(self.postfix_session, self.email)

    async def update_active(self, active: enums.MailboxActive, names: dict | None):
        db_box = self.dovecot_mailbox()
        ox_box = await self.ox_mailbox()
        ox_ctx = await self.ox_ctx()
        if db_box is None:
            raise Exception("Je ne peux pas modifier une boite qui n'existe pas.")

        if active == enums.MailboxActive.No and db_box.is_active() == enums.MailboxActive.Yes:
            if self.webmail and ox_box is not None:
                await ox_box.delete()
                self._ox_mailbox = None
                ox_box = None
            sql_dovecot.update_user_active(self.dovecot_session, self.username, self.domain, 'N')
            return
        if active == enums.MailboxActive.Yes and self.dovecot_mailbox().is_active() == enums.MailboxActive.No:
            if self.webmail:
                if ox_ctx is None:
                    raise Exception("OX ne connait pas le domaine, on va avoir du mal a traiter...")
                if not "givenName" in names or not "surName" in names or not "displayName" in names:
                    raise fastapi.HTTPException(status_code=422, detail="When you make a mailbox active, you must provide givenName, surName and displayName for the webmail feature")
                await ox_ctx.create_user(
                    givenName = names["givenName"],
                    surName = names["surName"],
                    displayName = names["displayName"],
                    username = self.username,
                    domain = self.domain,
                )
                ox_box = await ox_ctx.get_user_by_email(self.email)
                self._ox_mailbox = ox_box
            sql_dovecot.update_user_active(self.dovecot_session, self.username, self.domain, 'Y')

    async def update_names(self, names: dict):
        log = logging.getLogger(__name__)
        if not self.webmail:
            return
        ox_ctx = await self.ox_ctx()
        if ox_ctx is None:
            raise("OX ne connait pas ce domaine, on va avoir du mal à traiter...")
        ox_box = await self.ox_mailbox()
        db_box = self.dovecot_mailbox()

        if ox_box is None:
            if "givenName" in names and "surName" in names and "displayName" in names:
                await ox_ctx.create_user(
                    givenName = names["givenName"],
                    surName = names["surName"],
                    displayName = names["displayName"],
                    username = self.username,
                    domain = self.domain,
                )
                ox_box = await ox_ctx.get_user_by_email(self.email)
                senders = sql_postfix.get_senders_by_login(
                    self.postfix_session,
                    login = self.email,
                )
                if len(senders) > 0:
                    await ox_box.set_senders(senders)
                    ox_box = await ox_ctx.get_user_by_email(self.email)
                self._ox_mailbox = ox_box
            else:
                if db_box.is_active() == enums.MailboxActive.Yes:
                    log.debug("Le contexte OX ne connait pas cet email")
                    raise fastapi.HTTPException(status_code=404, detail="Mailbox not found in open-xchange")
        else:
            await ox_box.change(**names)
            ox_box = await ox_ctx.get_user_by_email(self.email)
            self._ox_mailbox = ox_box

    async def update_senders(self, new_senders: list[str]):
        log = logging.getLogger(__name__)
        log.debug(f"new senders = {new_senders}")
        if self.webmail:
            ox_box = await self.ox_mailbox()
            if ox_box is not None:
                await ox_box.set_senders(new_senders)

        old_senders = sql_postfix.get_senders_by_login(
            self.postfix_session,
            login = self.email,
        )
        old_senders = [ x.sender for x in old_senders ]
        log.debug(f"old senders = {old_senders}")
        for sender in new_senders:
            if sender not in old_senders:
                log.debug(f"J'ajoute {sender}")
                sql_postfix.create_sender(
                    self.postfix_session,
                    username = self.username,
                    domain = self.domain,
                    sender = sender,
                )
        for sender in old_senders:
            if sender not in new_senders:
                log.debug(f"J'enlève {sender}")
                sql_postfix.delete_sender(
                    self.postfix_session,
                    login = self.email,
                    sender = sender,
                )

    ###
    ### Moving mailboxes
    ###

    def exists(self):
        db_box = self.dovecot_mailbox()
        if db_box is None:
            return False
        return True

    def is_active(self) -> enums.MailboxActive:
        db_box = self.dovecot_mailbox()
        if db_box is None:
            raise Exception(f"Tu me parles d'une mailbox qui n'existe pas dans dovecot... {self.email}")
        return db_box.is_active()

    def need_moving(self):
        db_box = self.dovecot_mailbox()
        if db_box is None:
            raise Exception(f"Tu me parles d'une mailbox qui n'existe pas dans dovecot... {self.email}")
        if db_box.proxy == 'Y':
            return True
        return False

    def is_moving(self):
        db_box = self.dovecot_mailbox()
        if db_box is None:
            raise Exception(f"Tu me parles d'une mailbox qui n'existe pas dans dovecot... {self.email}")
        if db_box.active == 'W':
            return True
        return False

    async def move(self,
        from_name: str,
    ) -> bool:
        log = logging.getLogger(__name__)
        log.info(f"Je bouge la boite mail {self.email} from {from_name}")
        if not self.exists():
            log.error("La boite n'existe pas. Je ne peux pas la bouger.")
            return False

        other = others.get_other(from_name)
        imap = remote.get_host_by_role("imap")
        tech_domain = other.get_tech_domain()
        log.info(f"J'ai other = {other}, imap = {imap}, le tech_domain est {tech_domain}")

        # FIXME Si la boite n'était pas active, je vais la rendre active en cas d'échec (ou en cas de succès).
        # Ce n'est pas bien.

        # 1. Faire le synchro de la boîte et de la home (sieve)
        if not remote.fake_mode():
            log.info(f"Ce n'est pas un FAKE, donc on fait la première synchro de la boîte")
            _ = remote.run_on_host(imap, f"sudo /usr/bin/api_sync_box.sh {tech_domain} {self.domain} {self.username}", want_success = True)
        
        # 2. active = W, l'accès est fermé
        db_box = sql_dovecot.update_user_active(
            session = self.dovecot_session,
            username = self.username,
            domain = self.domain,
            active = "W",
        )
        if db_box is None:
            raise Exception("Mailbox not found")
        log.info("J'ai passé le user en active = 'W'")

        """
          - regarder s'il reste du mail en attente sur la plateforme de départ:
            F postqueue -j | jq 'select(has("recipients")) | select(.recipients[] |select(.address == "www-data@celtik.com")) | .queue_id'
            api_postfix_queue.sh domain username
            -> renvoie les queue_id
            Echec (il reste des queue_id) : ... ?
            Rejouer dans 1-2-3 secondes (la queue devrait se vider)
            S'il reste toujours des mails en queue, j'ai bloqué le compte...
            T -> ré-ouvrir le compte (active = Y)
        """
        count = other.client.count_mail_in_queue(
            domain_name = self.domain,
            user_name = self.username,
        )
        log.info(f"Chez {from_name}, il reste {count} mail(s) dans la queue")
        if count > 0:
            log.info(f"Il y a encore du mail en attente de delivery pour {self.email} (count = {count}). On attend 3 seconde et on avise.")
            await asyncio.sleep(3)
            count = other.client.count_mail_in_queue(
                domain_name = self.domain,
                user_name = self.username,
            )
        if count > 0:
            log.info(f"Il y a du mail bloqué en attente de delivery pour {self.email} (count = {count}). On abandonne la migration.")
            sql_dovecot.update_user_active(
                session = self.dovecot_session,
                username = self.username,
                domain = self.domain,
                active = "Y",
            )
            return False
        log.info("Il n'y a plus de mail en attente, on avance.")
        """
          - fermer la proxification en cours sur dovecot:
            T doveadm -f flow proxy list | grep username=joseph@test.ox.numerique.gouv.fr
            T doveadm proxy kick joseph@test.ox.numerique.gouv.fr
            api_dovecot_kick.sh proxy domain username
            (pas d'échec possible, donc Exception)
        """
        if not remote.fake_mode():
            log.info(f"On n'est pas en FAKE, donc on ferme les connexion proxy vers {from_name}")
            remote.run_on_host(imap, f"sudo /usr/bin/api_dovecot_kick.sh proxy {self.domain} {self.username}", want_success = True)

        """
          - fermer les sessions en cours sur dovecot:
            F doveadm who -1 "$USER@$DOMAIN" : liste les connexions (plus une ligne d'en-tete ?)
            F doveadm kick "$USER@$DOMAIN" : shoot les connexions
            api_dovecot_kick.sh imap domain username
            (pas d'échec possible, donc Exception)
        """
        other.client.disconnect(
            domain_name = self.domain,
            user_name = self.username,
        )
        log.info(f"J'ai shooté les connexions imap chez {from_name}")
        """
          - T rsync de la boite mail
          - T rsync de sieve
            api_sync_box.sh
            Echec: ... ?
            T -> ré-oucrir le compte (active = Y)
        """
        if not remote.fake_mode():
            log.info("On n'est pas en mode FAKE, je fais donc la deuxième synchro pour de vrai")
            _ = remote.run_on_host(imap, f"sudo /usr/bin/api_sync_box.sh {tech_domain} {self.domain} {self.username}", want_success = True)
        """    
          - ré-ouvrir l'accès:
            T set proxy = N, active = Y sur la platforme d'arrivée
            sql
            (pas d'échec possible, donc Exception)
        """
        sql_dovecot.update_user_proxy(
            session = self.dovecot_session,
            username = self.username,
            domain = self.domain,
            proxy = "N",
        )
        sql_dovecot.update_user_active(
            session = self.dovecot_session,
            username = self.username,
            domain = self.domain,
            active = "Y",
        )
        log.info("J'ai mis proxy=N et active=Y, la boite est migrée.")
        # FIXME Passer la boite en "active = N" sur other ?
        # FIXME (Sur other) Déplacer la mailbox dans <domain>/deleted ?
        # FIXME Est-ce simplement un DELETE /domains/<domain>/mailboxes/<username> ?
        # Vérifier : normalement, le mapping n'existe plus, et le contexte n'existe plus
        # i.e.: le domaine n'a plus la feature "webmail"
        return True

    ###
    ### Export pour le web
    ###

    async def to_web(self) -> web_models.Mailbox:
        in_db_user = self.dovecot_mailbox()
        in_ox_user = await self.ox_mailbox()

        if in_ox_user is None and in_db_user is None:
            return None
        if in_ox_user and not self.email == in_ox_user.email:
            raise Exception("J'ai une boite ox qui n'est pas moi... Caisstufou ?")
        if in_db_user and not self.email == in_db_user.email():
            raise Exception("J'ai une boite dovecot qui n'est pas moi... Cavachié !")

        (senders, senders_ok) = await self.get_senders()
        web = web_models.Mailbox(
            type = enums.MailboxType.Mailbox,
            status = enums.MailboxStatus.Unknown,
            active = enums.MailboxActive.Unknown,
            email = self.email,
            additionalSenders = senders,
        )
        if in_db_user:
            web.active = in_db_user.is_active()

        if in_ox_user:
            if in_ox_user.is_traced:
                print(f"Voilà la vie de {in_ox_user}:")
                for l in in_ox_user.get_traces():
                    print(l)
            web.givenName = in_ox_user.givenName
            web.surName = in_ox_user.surName
            web.displayName = in_ox_user.displayName
    
        if self.webmail:
            if in_db_user and in_ox_user:
                web.status = enums.MailboxStatus.OK
            else:
                web.status = enums.MailboxStatus.Broken
        if not self.webmail:
            if in_db_user and in_ox_user is None:
                web.status = enums.MailboxStatus.OK
            else:
                web.status = enums.MailboxStatus.Broken
        if web.active == enums.MailboxActive.No and in_ox_user is None:
            web.status = enums.MailboxStatus.OK
        if not senders_ok: # La liste des senders est incohérente entre OX et postfix
            web.status = enums.MailboxStatus.Broken
        return web

    def to_web_new(self) -> web_models.NewMailbox:
        if not hasattr(self, 'clear_password'):
            raise Exception("On ne peut faire l'export d'une mailbox en NewMailbox que lors de sa création. Après, ce n'est plus possible.")
        return web_models.NewMailbox(
            email = self.email,
            password = self.clear_password,
        )


class MailboxMaker:
    def __init__(
        self,
        postfix_session: orm.Session,
        dovecot_session: orm.Session,
    ):
        self.postfix_session = postfix_session
        self.dovecot_session = dovecot_session

    async def all_in_domain(self, domain: str, webmail: bool) -> list[Mailbox]:
        log = logging.getLogger(__name__)
        ox_cluster = oxcli.OxCluster()
        ctx = await ox_cluster.get_context_by_domain(domain)
        if webmail and ctx is None:
            log.debug(f"Le domaine {domain} est inconnu du cluster OX (ce n'est pas normal)")
        if not webmail and ctx:
            log.debug(f"Le domaine {domain} est connu du cluster OX (ce n'est pas normal)")

        ox_users = []
        if ctx:
            ox_users = await ctx.list_users_in_domain(domain)
        db_users = sql_dovecot.get_users(self.dovecot_session, domain)

        emails = set([user.email for user in ox_users] + [user.email() for user in db_users])
        emails = sorted(emails) # Ca redevient une liste, mais avec unicité
        ox_users_dict = {user.email: user for user in ox_users}
        db_users_dict = {user.email(): user for user in db_users}

        result = [
            Mailbox(
                email = email,
                webmail = webmail,
                postfix_session = self.postfix_session,
                dovecot_session = self.dovecot_session,
                ox_ctx = ctx,
                ox_mailbox = ox_users_dict.get(email),
                dovecot_mailbox = db_users_dict.get(email),
            )
            for email in emails
        ]
        return result


