import logging
import os
import pathlib
import subprocess
import time
import typing

import fastapi
import fastapi.testclient
import pytest
import sqlalchemy as sa
import testcontainers.core as tc_core
import testcontainers.mysql as tc_mysql

import alembic.command
import alembic.config

from . import config, cbcli, dkcli, oxcli, remote, sql_api, sql_dkim, sql_dovecot, sql_postfix

pytest_plugins = [
    "src.fixtures.log", # Le logger
    "src.fixtures.crash", # tourner sur un crash dir spécifique
    "src.fixtures.mode", # mode : real ou fake
    "src.fixtures.docker", # gérer les containers (optionnel)
    "src.fixtures.db", # créer les bases de données
    "src.fixtures.orm", # préparer les session pour l'orm
    "src.fixtures.remote", # setup des machines qu'on veut joindre, setup des modules qui leur parlent
    "src.fixtures.client", # Le client fastapi
    "src.fixtures.data", # les données de test (users, domaines, etc)
]
assert_ok = 0
def pytest_assertion_pass(item, lineno, orig, expl):
    global assert_ok
    assert_ok += 1

def pytest_terminal_summary(terminalreporter, exitstatus, config):
    global assert_ok
    terminalreporter.ensure_newline()
    terminalreporter.section("assert statistics", sep="=")
    terminalreporter.line(f"total asserts passed : {assert_ok}")
    cmd_count = oxcli.cmd_count()
    terminalreporter.line(f"total calls to ox cli : {cmd_count}")


