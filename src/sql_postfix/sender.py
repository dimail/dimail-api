"""Gestion de la table sender_login_map"""

import sqlalchemy as sa
import sqlalchemy.orm as orm

from . import models

def get_sender(
    session: orm.Session,
    login: str,
    sender: str
) -> models.Sender:
    """Get a sender by primary key (login,sender)

    Args:
        session (orm.Session): ORM session.
        login (str): Login name.
        sender (str): Sender name..

    Returns:
        models.Sender: Postfix Alias.
    """
    return session.get(models.Sender, {"login": login, "sender": sender})


def get_senders_by_domain(
    session: orm.Session,
    domain: str
) -> list[models.Sender]:
    """Get all senders for a domain.

    Args:
        session (orm.Session): ORM session.
        domain (str): Domain name.

    Returns:
        List[models.Sender]: List of Postfix Sender.
    """
    q = session.query(models.Sender).filter(models.Sender.domain == domain)
    return q.all()


def get_senders_by_login(
    session: orm.Session,
    login: str
) -> list[models.Sender]:
    """Get all senders for a login.

    Args:
        session (orm.Session): ORM session.
        login (str): Login.

    Returns:
        List[models.Sender]: List of Postfix Sender.
    """
    return session.query(models.Sender).filter(models.Sender.login == login).all()


def create_sender(
    session: orm.Session,
    domain: str,
    username: str,
    sender: str
) -> models.Sender:
    """Create a new additional sender.

    Args:
        session (orm.Session): ORM session.
        domain (str): Domain name.
        username (str): Username.
        sender (str): Allowed sender address.

    Returns:
        models.Sender: Postfix Alias.

        None: If an error occurred.
    """
    if username is None:
        return None
    if domain is None:
        return None
    if sender is None:
        return None
    try:
        login = username + "@" + domain
        db_sender = models.Sender(
            login=login,
            domain=domain,
            sender=sender,
        )
        session.add(db_sender)
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_sender)
    return db_sender


def delete_sender(
    session: orm.Session,
    login: str,
    sender: str
) -> int:
    """Delete a sender.

    Args:
        session (orm.Session): ORM session.
        login (str): Login name.
        sender (str): Additional sender.

    Returns:
        int: Number of deleted aliases

        0: If no sender was deleted.
    """
    db_sender = get_sender(session, login=login, sender=sender)
    if db_sender is not None:
        session.delete(db_sender)
        session.commit()
        return 1
    return 0


def delete_senders_by_login(
    session: orm.Session,
    login: str
) -> int:
    """Delete all additional senders for a login.

    Args:
        session (orm.Session): ORM session.
        login (str): Login name.

    Returns:
        int: Number of deleted senders

        0: If no sender was deleted.
    """
    res = session.execute(sa.delete(models.Sender).where(models.Sender.login == login))
    session.commit()
    return res.rowcount





