"""CRUD operations for Postfix Aliases.

This module implements the CRUD operations for Postfix Aliases.

Functions:
    - get_alias (models.PostfixAlias): Get an alias by its name and destination.
    - get_aliases_by_domain (List[models.PostfixAlias]): Get all aliases for a domain.
    - get_aliases_by_name (List[models.PostfixAlias]): Get all aliases by their name.
    - create_alias (models.PostfixAlias): Create a new alias.
    - delete_alias (int): Delete an alias.
    - delete_aliases_by_name (int): Delete all aliases by their name.
"""
import sqlalchemy as sa
import sqlalchemy.orm as orm

from . import models


def get_domain(
    session: orm.Session,
    domain: str
) -> models.Domain:
    """Gets domain from the postfix map by domain name.

    Args:
        session: ORM session
        name: domain name

    Returns:
        the entry in the domain map of Postfix, if it exists
    """
    return session.get(models.Domain, {"domain": domain})

def create_domain(
    session: orm.Session,
    domain: str,
    delivery: str,
    transport: str | None = None,
) -> models.Domain:
    """Adds a domain in the postfix map. When delivery is 'relay' then 'transport' is
    mandatory.

    Args:
        session: ORM session
        domain: domain name
        delivery: delivery mode ("virtual", "alias" or "relay")
        transport: default transport relay when delivery == "relay"

    Returns:
        the domain object from the database
    """
    if domain is None:
        return None
    if delivery not in [ "virtual", "alias", "relay" ]:
        return None
    if delivery == "relay" and transport is None:
        return None

    try:
        db_dom = models.Domain(
            domain = domain,
            delivery = delivery,
            transport = transport,
        )
        session.add(db_dom)
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_dom)
    return db_dom

def delete_domain(
    session: orm.Session,
    domain: str
) -> int:
    """Delete the domain from the postfix maps.

    Args:
        session: ORM session
        domain: domain name

    Returns:
        the number of deleted lines
    """
    db_dom = get_domain(session, domain)
    if db_dom is not None:
        session.delete(db_dom)
        session.commit()
        return 1
    return 0

def update_domain(
    session: orm.Session,
    domain: str,
    delivery: str,
    transport: str | None = None,
) -> models.Domain:
    db_dom = session.get(models.Domain, {"domain": domain})
    if db_dom is None:
        return None
    if delivery not in [ "virtual", "alias", "relay" ]:
        return None
    if delivery == "relay" and transport is None:
        return None
    try:
        db_dom.delivery = delivery
        db_dom.transport = transport
        session.flush()
        session.commit()
    except Exception as e:
        print(str(e))
        session.rollback()
    session.refresh(db_dom)
    return db_dom

