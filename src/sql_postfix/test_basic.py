import pytest

from . import database
from .. import sql_postfix


def test_database():
    database.maker = None
    with pytest.raises(Exception) as e:
        database.get_maker()
    assert "Please init the postfix database by giving me an url..." in str(e.value)

    database.init_db("sqlite:///:memory:")

    maker = database.get_maker()
    assert maker is not None

def test_health_check(db_postfix_session):
    status = sql_postfix.health_check()
    assert status.status is True
    assert "sql_postfix_status = OK" in status.detail

def test_alias__create_and_get_an_alias(db_postfix_session):
    """Proves that we can create an alias in postfix db, and fetch it."""
    alias = sql_postfix.get_alias(
        db_postfix_session, "from@example.com", "to@example.com"
    )
    assert alias is None

    # On vérifie que si une valeur est idiote on ne peut pas creer d'alias
    alias = sql_postfix.create_alias(db_postfix_session, None, "from", "to@example.com")
    assert alias is None

    alias = sql_postfix.create_alias(db_postfix_session, "example.com", None, "to@example.com")
    assert alias is None

    alias = sql_postfix.create_alias(db_postfix_session, None, "from", None)
    assert alias is None

    alias = sql_postfix.create_alias(
        db_postfix_session, "example.com", "from", "to@example.com"
    )
    assert isinstance(alias, sql_postfix.Alias)

    alias = sql_postfix.get_alias(
        db_postfix_session, "from@example.com", "to@example.com"
    )
    assert isinstance(alias, sql_postfix.Alias)
    assert alias.alias == "from@example.com"
    assert alias.destination == "to@example.com"
    assert alias.domain == "example.com"

    alias = sql_postfix.create_alias(
        db_postfix_session, "example.com", "from", "other@gmail.com"
    )
    assert isinstance(alias, sql_postfix.Alias)

    count = sql_postfix.delete_alias(
        db_postfix_session, "from@example.com", "to@example.com"
    )
    assert count == 1

    alias = sql_postfix.create_alias(
        db_postfix_session, "example.com", "from", "something@gmail.com"
    )
    assert isinstance(alias, sql_postfix.Alias)

    count = sql_postfix.delete_aliases_by_name(
        db_postfix_session, "from@example.com"
    )
    assert count == 2

    count = sql_postfix.delete_aliases_by_name(
        db_postfix_session, "from@example.com"
    )
    assert count == 0


def test__domain(db_postfix_session):
    """Proves we can create, fetch and delete mailbox domains in postfix db"""
    dom = sql_postfix.get_domain(db_postfix_session, "example.com")
    assert dom is None

    # On vérifie qu'une valeur None n'est pas admise, ni pour domain ni pour delivery, ni pour transport quand delivery=relay
    dom = sql_postfix.create_domain(db_postfix_session, domain=None, delivery="virtual", transport=None)
    assert dom is None

    dom = sql_postfix.create_domain(db_postfix_session, "example.com", None, None)
    assert dom is None

    dom = sql_postfix.create_domain(db_postfix_session, "example.com", "relay", None)
    assert dom is None

    # On vérifie qu'on peut faire une création normale
    dom = sql_postfix.create_domain(db_postfix_session, "example.com", "virtual", None)
    assert isinstance(dom, sql_postfix.Domain)

    dom = sql_postfix.get_domain(db_postfix_session, "example.com")
    assert isinstance(dom, sql_postfix.Domain)
    assert dom.domain == "example.com"
    assert dom.delivery == "virtual"
    assert dom.transport is None

    # On vérifie qu'on ne peut pas créer un deuxième domain pour le même domain
    db_postfix_session.expunge_all()
    dom = sql_postfix.create_domain(db_postfix_session, "example.com", "alias", None)
    assert dom is None

    # On vérifie qu'on peut supprimer le domain qu'on vient de créer
    count = sql_postfix.delete_domain(db_postfix_session, "example.com")
    assert count == 1

    # On vérifie qu'on ne peut pas supprimer un domain qui n'existe pas
    count = sql_postfix.delete_domain(db_postfix_session, "existepas.fr")
    assert count == 0

    # On crée un domaine en alias
    dom = sql_postfix.create_domain(db_postfix_session, "example.com", "alias", None)
    assert isinstance(dom, sql_postfix.Domain)
    assert dom.domain == "example.com"
    assert dom.delivery == "alias"
    assert dom.transport is None

    # On crée un domain en relay
    dom = sql_postfix.create_domain(db_postfix_session, "example.net", "relay", "smtp:[coincoin.example.com]")
    assert isinstance(dom, sql_postfix.Domain)
    assert dom.domain == "example.net"
    assert dom.delivery == "relay"
    assert dom.transport == "smtp:[coincoin.example.com]"

    # On vérifie qu'un delivery idiot ne passe pas
    dom = sql_postfix.create_domain(db_postfix_session, "example.org", "plop", None)
    assert dom is None

    # On passe un domain relay en virtual
    dom = sql_postfix.update_domain(db_postfix_session, "example.net", "virtual", None)
    assert isinstance(dom, sql_postfix.Domain)
    assert dom.delivery == "virtual"
    assert dom.transport is None

    # On passe un domain alias en relay
    dom = sql_postfix.update_domain(db_postfix_session, "example.com", "relay", "smtp:[mx1.ovh.net]")
    assert isinstance(dom, sql_postfix.Domain)
    assert dom.delivery == "relay"
    assert dom.transport == "smtp:[mx1.ovh.net]"

    # On passe un domain virtual en alias
    dom = sql_postfix.update_domain(db_postfix_session, "example.net", "alias", None)
    assert isinstance(dom, sql_postfix.Domain)
    assert dom.delivery == "alias"
    assert dom.transport is None


def test_sender(db_postfix_session):

    # Pour le moment, le sender n'existe pas
    send  = sql_postfix.get_sender(db_postfix_session, login="essai@example.com", sender="vilain@gmail.com")
    assert send is None

    # On ne peut pas créer un sender avec sender=None
    send = sql_postfix.create_sender(db_postfix_session, username="essai", domain="example.com", sender=None)
    assert send is None

    # On ne peut pas créer un sender avec username=None
    send = sql_postfix.create_sender(db_postfix_session, username=None, domain="example.com", sender="vilain@gmail.com")
    assert send is None

    # On ne peut pas créer avec un domain=None
    send = sql_postfix.create_sender(db_postfix_session, username="essai", domain=None, sender="vilain@gmail.com")
    assert send is None

    # On peut créer un sender
    send = sql_postfix.create_sender(db_postfix_session, username="essai", domain="example.com", sender="vilain@gmail.com")
    assert isinstance(send, sql_postfix.Sender)
    assert send.login == "essai@example.com"
    assert send.sender == "vilain@gmail.com"

    # On ne peut pas le créer une deuxième fois
    send = sql_postfix.create_sender(db_postfix_session, username="essai", domain="example.com", sender="vilain@gmail.com")
    assert send is None

    # On peut ajouter un deuxième sender sur le même login
    send = sql_postfix.create_sender(db_postfix_session, username="essai", domain="example.com", sender="gentil@elysee.fr")
    assert isinstance(send, sql_postfix.Sender)
    assert send.login == "essai@example.com"
    assert send.sender == "gentil@elysee.fr"

    # On peut créer un sender sur un autre login
    send = sql_postfix.create_sender(db_postfix_session, username="autre", domain="example.com", sender="vilain@gmail.com")
    assert isinstance(send, sql_postfix.Sender)
    assert send.login == "autre@example.com"
    assert send.sender == "vilain@gmail.com"

    # Quand on demande les senders de 'essai@example.com' on a trouve deux
    sends = sql_postfix.get_senders_by_login(db_postfix_session, login="essai@example.com")
    assert isinstance(sends, list)
    assert len(sends) == 2
    senders = [ x.sender for x in sends ]
    assert "vilain@gmail.com" in senders
    assert "gentil@elysee.fr" in senders

    # Quand on demande les senders du domaine, on en trouve trois
    sends = sql_postfix.get_senders_by_domain(db_postfix_session, domain="example.com")
    assert isinstance(sends, list)
    assert len(sends) == 3

    # Si je supprime un sender qui n'existe pas, il ne se passe rien
    nb = sql_postfix.delete_sender(db_postfix_session, login="pasmoi@example.com", sender="paslui@hotmail.com")
    assert nb == 0

    # Si je supprime un sender qui existe, il disparait
    nb = sql_postfix.delete_sender(db_postfix_session, login="autre@example.com", sender="vilain@gmail.com")
    assert nb == 1

    # Si je get le sender supprimé, je ne le trouve pas
    send = sql_postfix.get_sender(db_postfix_session, login="autre@example.com", sender="vilain@gmail.com")
    assert send is None

    # Si je get en liste, je ne le trouve pas
    sends = sql_postfix.get_senders_by_login(db_postfix_session, login="autre@example.com")
    assert isinstance(sends, list)
    assert len(sends) == 0

    # Si je supprime en liste pour essai@example.com, j'en trouve 2
    nb = sql_postfix.delete_senders_by_login(db_postfix_session, login="essai@example.com")
    assert nb == 2

    # Maintenant, je n'ai plus de senders pour le domaine example.com
    sends = sql_postfix.get_senders_by_domain(db_postfix_session, domain="example.com")
    assert isinstance(sends, list)
    assert len(sends) == 0

    

