"""Postfix models for SQLAlchemy ORM.

This module contains the SQLAlchemy ORM models for the Postfix database.

Classes:
    PostfixAlias: Postfix aliases table model.
"""
import sqlalchemy as sa

from . import database


class Alias(database.Postfix):
    """Postfix aliases table model.

    This class represents the aliases table in the Postfix database.

    Attributes:
        alias: The alias for the email address.
        domain: The domain for the email address.
        destination: The destination email address.
    """
    __tablename__ = "aliases"
    alias = sa.Column(sa.String(256), nullable=False, primary_key=True)
    domain = sa.Column(sa.String(128), nullable=False, index=True)
    destination = sa.Column(sa.String(256), nullable=False, primary_key=True)

class Sender(database.Postfix):
    __tablename__ = "sender_login_map"
    sender = sa.Column(sa.String(256), nullable=False, primary_key=True)
    login = sa.Column(sa.String(256), nullable=False, primary_key=True, index=True)
    domain = sa.Column(sa.String(128), nullable=False, index=True)

class Domain(database.Postfix):
    __tablename__ = "domains"
    domain = sa.Column(sa.String(128), nullable=False, primary_key=True)
    delivery = sa.Column(sa.String(20), nullable=False, default="virtual")
    transport = sa.Column(sa.String(200))
    __table_args__ = (sa.Index("dom_delivery", "domain", "delivery"), )
