"""CRUD operations for Postfix Aliases.

This module implements the CRUD operations for Postfix Aliases.

Functions:
    - get_alias (models.PostfixAlias): Get an alias by its name and destination.
    - get_aliases_by_domain (List[models.PostfixAlias]): Get all aliases for a domain.
    - get_aliases_by_name (List[models.PostfixAlias]): Get all aliases by their name.
    - create_alias (models.PostfixAlias): Create a new alias.
    - delete_alias (int): Delete an alias.
    - delete_aliases_by_name (int): Delete all aliases by their name.
"""
import sqlalchemy as sa
import sqlalchemy.orm as orm

from . import models


def get_alias(
    session: orm.Session,
    alias: str,
    destination: str
) -> models.Alias:
    """Get an alias by its name and destination.

    Args:
        session (orm.Session): ORM session.
        alias (str): Alias name.
        destination (str): Alias destination.

    Returns:
        models.Alias: Postfix Alias.
    """
    return session.get(models.Alias, {"alias": alias, "destination": destination})


def get_aliases_by_domain(
    session: orm.Session,
    domain: str
) -> list[models.Alias]:
    """Get all aliases for a domain.

    Args:
        session (orm.Session): ORM session.
        domain (str): Domain name.

    Returns:
        List[models.Alias]: List of Postfix Aliases.
    """
    q = session.query(models.Alias).filter(models.Alias.domain == domain)
    return q.all()


def get_aliases_by_name(
    session: orm.Session,
    name: str
) -> list[models.Alias]:
    """Get all aliases by their name.

    Args:
        session (orm.Session): ORM session.
        name (str): Alias name.

    Returns:
        List[models.Alias]: List of Postfix Aliases.
    """
    return session.query(models.Alias).filter(models.Alias.alias == name).all()


def create_alias(
    session: orm.Session,
    domain: str,
    username: str,
    destination: str
) -> models.Alias:
    """Create a new alias.

    Args:
        session (orm.Session): ORM session.
        domain (str): Domain name.
        username (str): Username.
        destination (str): Destination.

    Returns:
        models.Alias: Postfix Alias.

        None: If an error occurred.
    """
    try:
        alias = username + "@" + domain
        db_alias = models.Alias(
            alias=alias,
            domain=domain,
            destination=destination,
        )
        session.add(db_alias)
        session.commit()
    except Exception:
        session.rollback()
        return None
    session.refresh(db_alias)
    return db_alias


def delete_alias(
    session: orm.Session,
    alias: str,
    destination: str
) -> int:
    """Delete an alias.

    Args:
        session (orm.Session): ORM session.
        alias (str): Alias name.
        destination (str): Alias destination.

    Returns:
        int: Number of deleted aliases

        0: If no alias was deleted.
    """
    db_alias = get_alias(session, alias, destination)
    if db_alias is not None:
        session.delete(db_alias)
        session.commit()
        return 1
    return 0


def delete_aliases_by_name(
    session: orm.Session,
    name: str
) -> int:
    """Delete all aliases by their name.

    Args:
        session (orm.Session): ORM session.
        name (str): Alias name.

    Returns:
        int: Number of deleted aliases

        0: If no alias was deleted.
    """
    res = session.execute(sa.delete(models.Alias).where(models.Alias.alias == name))
    session.commit()
    return res.rowcount


