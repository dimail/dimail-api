import functools
import inspect
import types

def wrapper(method, prefix):
    @functools.wraps(method)
    def wrapped(*args, **kwargs):
        self = args[0]
        class_ = self.__class__
        class_name = class_.__name__
        func_name = method.__name__

        caller = inspect.currentframe().f_back.f_code.co_qualname
        file = inspect.currentframe().f_back.f_code.co_filename
        if file.startswith(prefix):
            file = "." + file[len(prefix):]
        line = inspect.currentframe().f_back.f_code.co_firstlineno

        self.add_trace(f"{file}:{line} in {caller}:\n    self={args[0]}\n    {class_name}.{func_name}({args[1:]}, {kwargs})")

        return method(*args, **kwargs)
    return wrapped

def add_trace(self, trace: str):
    if not hasattr(self, "__traces"):
        self.__traces = []
    self.__traces.append(trace)

def get_traces(self) -> list[str]:
    if not hasattr(self, "__traces"):
        return []
        raise Exception(f"L'objet {self} ne semble pas être traçable.")
    return self.__traces

def no_add_trace(self, trace: str):
    pass

def no_get_traces(self):
    return []

class Traceable(type):
    def __new__(meta, classname, bases, classDict, debug: bool = False):
        if not debug:
            classDict["get_traces"] = no_get_traces
            classDict["add_trace"] = no_add_trace
            classDict["is_traced"] = False
            return type.__new__(meta, classname, bases, classDict)
        newClassDict = {}
        file = inspect.currentframe().f_back.f_code.co_filename
        prefix, file = file.split("dimail-api")
        prefix = prefix + "dimail-api"
        for attributeName, attribute in classDict.items():
            if isinstance(attribute, types.FunctionType) and not attributeName == "__repr__":
                # replace it with a wrapped version
                # print(f"Sur {classname} je surcharge la fonction {attributeName} = {attribute}")
                attribute = wrapper(attribute, prefix)
            elif isinstance(attribute, property):
                # wrap the getter, the setter and the deleter
                new_get = None
                if attribute.fget is not None:
                    new_get = wrapper(attribute.fget, prefix)
                new_set = None
                if attribute.fset is not None:
                    new_set = wrapper(attribute.fset, prefix)
                new_del = None
                if attribute.fdel is not None:
                    new_del = wrapper(attribute.fdel, prefix)
                attribute = property(new_get, new_set, new_del, attribute.__doc__)
                # print(f"Sur {classname} je surcharge la propriété {attributeName} = {attribute} doc={attribute.__doc__}")
            elif isinstance(attribute, str): # Pas intéressant
                pass
            else:
                pass
                # what = type(attribute)
                # print(f"Sur {classname} je ne surcharge pas {attribute} type {what}")
            newClassDict[attributeName] = attribute
        newClassDict["get_traces"] = get_traces
        newClassDict["add_trace"] = add_trace
        newClassDict["is_traced"] = True
        return type.__new__(meta, classname, bases, newClassDict)


