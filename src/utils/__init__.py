"""This module contains utility functions for the project.

Attributes:
    * __all__: List of all modules in the package.

Modules:
    * mail: Functions for handling email addresses.

Functions:
    * split_email: Splitting an email address into username and domain.
"""
from .args_list import read_args_list
from .cli import make_protected_cli
from .mail import split_email
from .traceable import Traceable

__all__ = [
    make_protected_cli,
    read_args_list,
    split_email,
    Traceable,
]
