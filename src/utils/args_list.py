import fastapi

def read_args_list(
    name: str,
    known_items: dict | None,
    current: list[str],
    args: list[str],
) -> list[str] | None:
    is_diff = True
    add_items = []
    del_items = []
    set_items = []
    # print(f"J'ai {current}, la modif qu'il faut prendre en compte: {args}")
    if len(args) == 0:
        # print("C'est un set vide, donc il faut vider")
        return []

    for item in args:
        # print(f"Je regarde l'item {item}")
        if item.startswith("+"):
            add_items.append(item[1:])
            # print(f"C'est un ajout, maintenant les add_items sont {add_items}")
        elif item.startswith("-"):
            del_items.append(item[1:])
            # print(f"C'est un retrait, maintenant les del_items sont {del_items}")
        else:
            is_diff = False
            set_items.append(item)
            # print(f"C'est un set, donc is_diff = {is_diff}, et les set_items sont {set_items}")
    if is_diff and len(set_items) != 0:
        raise fastapi.HTTPException(status_code=422, detail=f"Inconsistent items in list ({name})")
    if not is_diff and len(add_items)+len(del_items) != 0:
        raise fastapi.HTTPException(status_code=422, detail=f"Inconsistent items in list ({name})")

    # print("Je regarde si tout ce beau monde existe")
    if known_items is not None:
        for item in add_items + del_items + set_items:
            if item not in known_items:
                raise fastapi.HTTPException(status_code=422, detail=f"Unknown item {item} in list ({name})")

    if len(del_items)+len(add_items) != 0:
        set_items = [ x for x in current ]
        # print(f"C'est un diff, dont le point de depart est {set_items}")
    for item in del_items:
        if item in set_items:
            set_items = [ i for i in set_items if i != item ]
            # print(f"Il faut retirer {item}, ce qui amène à {set_items}")
    for item in add_items:
        if item not in set_items:
            set_items.append(item)
            # print(f"Il faut ajouter {item}, ce qui amène à {set_items}")
    # print(f"Donc, il faut faire un set sur {set_items}")
    return set_items

