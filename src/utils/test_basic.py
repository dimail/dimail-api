import pytest

from .. import enums, utils


def test_split_email():
    (username, domain) = utils.split_email("test@example.com")
    assert username == "test"
    assert domain == "example.com"

    with pytest.raises(Exception) as e:
        (username, domain) = utils.split_email("not-an-email")
    assert (
        str(e)
        == "<ExceptionInfo Exception('The email address <not-an-email> is not valid') tblen=2>"
    )

def test_diff_list(log):
    known = {
        "bla": "",
        "blu": "",
        "bli": "",
    }
    already = [ "bla" ]

    res = utils.read_args_list("test", known, already, [])
    assert res == []
    res = utils.read_args_list("test", known, already, [ "+bli" ])
    assert res == [ "bla", "bli" ]
    res = utils.read_args_list("test", known, already, [ "+blu", "-bla" ])
    assert res == [ "blu" ]
    res = utils.read_args_list("test", known, already, [ "bli" ])
    assert res == [ "bli" ]

    res = utils.read_args_list("bla", enums.Perm, [], [ "+new_domain" ])
    assert res == [ "new_domain" ]
    res = utils.read_args_list("bla", enums.Perm, [ "new_domain" ], [ "create_users" ])
    assert res == [ "create_users" ]
    res = utils.read_args_list("bla", enums.Perm, [ "new_domain" ], [ "-new_domain", "+create_users" ])
    assert res == [ "create_users" ]
    with pytest.raises(Exception) as e:
        res = utils.read_args_list("bla", enums.Perm, [ "new_domain" ], [ "coincoin" ])
    assert "coincoin" in str(e.value)

    known = None
    already = [
        "bob@example.com",
        "essai@gmail.com",
    ]

    res = utils.read_args_list("essai", known, already, [])
    assert res == []
    res = utils.read_args_list("essai", known, already, [ "lotre@example.com" ])
    assert res == [ "lotre@example.com" ]
    res = utils.read_args_list("essai", known, already, [ "+lotre@example.com" ])
    assert len(res) == 3
    assert "bob@example.com" in res
    assert "essai@gmail.com" in res
    assert "lotre@example.com" in res
    res = utils.read_args_list("essai", known, already, [ "+lotre@example.com", "-pala@example.com", "-bob@example.com" ])
    assert len(res) == 2
    assert "essai@gmail.com" in res
    assert "lotre@example.com" in res

import subprocess

def test_protected_cli(log):
    # Python does not work
    item = "Turlututu"
    tmp = "plop" + item.replace("u", "x") + "coin"
    assert tmp == "plopTxrlxtxtxcoin"

    item = 'D"Azure'
    tmp = '"' + item.replace('"', '\\"') + '"'
    assert tmp == '"D\\"Azure"'

    res = utils.make_protected_cli(["coin", "pan", "kaï"])
    assert res == "coin pan \"kaï\""

    res = utils.make_protected_cli(["coin", "pan pan", "kaï"])
    assert res == "coin \"pan pan\" \"kaï\""

    res = utils.make_protected_cli(["coin", "d'essai", "e.val"])
    assert res == "coin \"d'essai\" e.val"

    res = utils.make_protected_cli(["coin", "d\"essai", "e.val"])
    assert res == "coin \"d\\\"essai\" e.val"


