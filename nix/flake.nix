{ 
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
  outputs = { self, nixpkgs }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in
    {
      dimail-api = pkgs.dockerTools.buildLayeredImage {
        name = "dimail-api";
        tag = "latest";
        architecture = "amd64";
        contents = ( import ./contents.nix { inherit pkgs; } );
        config = { 
          Cmd = [ "/opt/dimail-api/entrypoint.sh" ];
        };
      };
      devShell.x86_64-linux = pkgs.mkShell {
        nativeBuildInputs = ( import ./contents.nix { inherit pkgs; } );
        shellHook = ''
          export ANSIBLE_DISPLAY_OK_HOSTS=yes
          export PS1="\n\[\033[1;36m\][\[\e]0;\u@\h: \w\a\]\u@\h:\w]\$\[\033[0m\] "
        '';
      };      
      ci = pkgs.mkShell {
        packages = [ pkgs.nushell pkgs.netcat ];
        nativeBuildInputs = ( import ./contents.nix { inherit pkgs; } );
      };
      
    };
}
