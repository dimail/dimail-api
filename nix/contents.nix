{ pkgs }:
let
  # dynaconf is not available in distributed python packages
  # lets create the derivation for its python package
  dynaconf = pkgs.python312.pkgs.buildPythonPackage rec {
    pname = "dynaconf";
    version = "3.2.6";
    pyproject = true;

    src = pkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-dMwYlzljgLuVdzDrNBzAl27pw4u8tT0zB8UMrtCu37g=";
    };

    build-system = [
      pkgs.python312.pkgs.setuptools
    ];

    # we avoid tests
    doCheck = false;
  };

  dimail-api = pkgs.runCommand "dimail-api" {} ''
        mkdir -p $out/opt/dimail-api/src -p
        mkdir -p $out/opt/dimail-api/config
        cp -r ${../src}/* $out/opt/dimail-api/src
        cp -r ${../config}/* $out/opt/dimail-api/config
        cp -r ${../config}/.* $out/opt/dimail-api/config
        cp ${../container/entrypoint.sh} $out/opt/dimail-api/entrypoint.sh
      '';

  in with pkgs; [
    bash
    busybox
    openssh
    envsubst
    dimail-api
    dockerTools.fakeNss
    git
    (python312.withPackages (packageSources: with packageSources; [
      fastapi
      uvicorn
#      alembic
      sqlalchemy
      pymysql
#      pytest nécessaire uniquement pour les tests API
      dynaconf
      # ruff est un linter, pas besoin à l'exec
      # pre-commit pas besoin a l'exec
      passlib
      argon2_cffi
      python-multipart
      pyjwt
      # testcontainers nécessaire uniquement pour les tests API
      # docker nécessaire uniquement pour les tests API
      # python-on-whales nécessaire uniquement pour les tests API
      httpx
      dnspython
      jinja2
      aiocsv
      packaging
#      anyio
      # packages supplémentaires pour nix
#      wrapt
#      josepy
    ]))
  ]
