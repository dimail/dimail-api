#!/bin/bash
cd /opt/dimail-api/
if [ ! -f src/alembic.ini ]; then
	cd src
	envsubst < alembic.ini.tpl > alembic.ini
	cd ..
fi
if [ "$DIMAIL_MODE" != "FAKE" ]; then
	cd src
	alembic -n production upgrade head
	cd ..
	while true; do
		ALL=oui
		for info in $DIMAIL_REMOTE_CONFIG; do
			echo $info | sed 's/^.*@\([^;]*\);.*$/\1/'
		done | xargs ssh-keyscan -H > /root/.ssh/known_hosts
		for info in $DIMAIL_REMOTE_CONFIG; do
			host=`echo $info | sed 's/^.*\(ssh:\/\/.*@[^;]*\);.*$/\1/'`
			if ! ssh $host "echo OK"; then
				echo Will wait for $host...
				ALL=non
			fi
		done
		if [ "$ALL" == "oui" ]; then
			break
		fi
		sleep 1
	done
fi
if [ "$DIMAIL_PORT" == "" ]; then
	export DIMAIL_PORT=8000
fi
while true; do
	if ! uvicorn src.main:app --host 0.0.0.0 --port "$DIMAIL_PORT" --log-level info; then
		echo Uvicorn est mort salement, on attend 5 secondes avant de relancer
		sleep 5
	else
		echo Uvicorn est mort proprement, on relance tout de suite
	fi
done
