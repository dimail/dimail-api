set -e

trap 'echo "Quelque chose vient de tomber, je dois nettoyer uvicorn (pid $pid) pour le salut des générations futures."; kill $pid; exit 1;' ERR

ROOT_DIR=$(git rev-parse --show-toplevel)

start_api() {
	pushd "${ROOT_DIR}"
        timeout 5 sh -c 'until ! nc -z $0 $1; do sleep 1; done' localhost 8000
	echo "Je lance l'API en mode fake"
	DIMAIL_JWT_SECRET='toto' DIMAIL_MODE='FAKE' DIMAIL_TECH_DOMAIN='testingnu.fr' uvicorn src.main:app --log-level info --host 0.0.0.0 --port 8000 &
	pid=$!
	echo "Uvicorn est sur le pid $pid"
	echo "J'attends que l'API soit disponible sur le port 8000"
	if ! which nc; then
		echo Je ne trouve pas nc... Tout va échouer.
	fi
	timeout 5 sh -c 'until nc -z $0 $1; do sleep 1; done' localhost 8000
	popd
}

start_api

echo "Port 8000 ouvert, je lance les tests"
cd "${ROOT_DIR}/tools/client-nushell/"
if ! which nu; then
	docker run -it \
		--mount type=bind,source=.,destination=/opt/dimail \
		--add-host=localhost:host-gateway \
		--workdir=/opt/dimail \
		ghcr.io/nushell/nushell:0.100.0-bookworm \
			test/tests.nu
	# L'api ne tourne plus, à la fin des tests
	start_api
	docker run -it \
		--mount type=bind,source=.,destination=/opt/dimail \
		--add-host=localhost:host-gateway \
		--workdir=/opt/dimail \
		ghcr.io/nushell/nushell:0.100.0-bookworm \
			test/methods.nu
else
	nu test/tests.nu
	# L'api ne tourne plus, à la fin des tests
	start_api
	nu test/methods.nu
fi

kill $pid
timeout 22 sh -c 'while nc -z $0 $1; do sleep 1; done' localhost 8000
echo "Félicitations, les tests passent 🎉"
