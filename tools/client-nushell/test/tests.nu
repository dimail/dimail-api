use std assert

# decode-jwt brings token in structured data
def decode-jwt (): string -> any {
  let parts = ($in | split row '.')
  return [
    ($parts.0 | decode base64 --nopad | decode | from json),
    ($parts.1 | decode base64 --nopad | decode | from json),
    $parts.2
  ]
}

print "🔧 Création de l'administrateur:"
$env.API_USER = "admin"
$env.API_PASS = "password"
$env.API_URL = "http://localhost:8000"

let admin_user = {
    name: $env.API_USER,
    password: $env.API_PASS,
    is_admin: true,
    perms: []
  }
let admin_result = http post --content-type application/json -u foo -p bar $"($env.API_URL)/users/" $admin_user
print "Résultat:"
print $admin_result

print "Chargement du client"
use ../client.nu
print "Informations du token"
print (client get token | decode-jwt | to json)

# FIXME, cela ne fonctionne pas comme attendu ?
# On profite d'être identifié en admin pour rouler les tests des crash logs
print "🔧 Création d'un crash log"
let create_crash_log = client get version --log_request

print "🔧 Listing des crash log"
let list_crash_log = client get logs crashes --full
let last_crash_log = ($list_crash_log.body | last).name 
assert ($list_crash_log.status == 200)

print "🔧 Consultation d'un crash log"
let read_crash_log = client get logs crashes $last_crash_log --full
assert ($read_crash_log.status == 200)

print "🔧 Suppression d'un crashlog"
let delete_crash_log = client delete logs crashes $last_crash_log --full
assert ($delete_crash_log.status == 204)

print "🔧 Test du GET tech_domain"
let tech_domain = client get tech_domain --full
assert ($tech_domain.status == 200)
assert ($tech_domain.body.tech_name == "testingnu.fr")

print "Création de l'utilisateur la_regie"
let user_result = client post users --perms=["new_domain", "create_users", "manage_users"] "la_regie" "toto" --full
assert ($user_result.status == 201)

print "🔧  Login comme utilisateur la_regie"
hide-env API_TOKEN
$env.API_USER = "la_regie"
$env.API_PASS = "toto"
use ../client.nu

print "🔧  Création d'un domaine"
let create_domain = client post domains example.com --features=["webmail", "mailbox"] --context_name="dimail" --full
assert ($create_domain.status == 201)
print "🔧  Attente du check pour la lisibilité"
sleep 3sec

print "🔧 Lecture du domaine"
let read_domain = client get domains example.com --full
assert ($read_domain.status == 200)

print "🔧 Check du domaine"
let check_domain = client get domains check example.com --full
assert ($check_domain.status == 200)

print "🔧 Création d'un utilisateur (auth la régie)"
let create_user = client post users example --full
assert ($create_user.status == 201)

print "🔧 Création d'un utilisateur avec UUID comme nom"
let create_uuid_user = client post users ea00652e-742b-11ef-86d3-879b2ea77f4b --full
assert ($create_uuid_user.status == 201)
assert ($create_uuid_user.body.name == ea00652e-742b-11ef-86d3-879b2ea77f4b)

print "🔧 Suppression de l'utilisateur avec UUID comme nom"
let delete_uuid_user = client delete users ea00652e-742b-11ef-86d3-879b2ea77f4b --full
assert ($delete_uuid_user.status == 204)

print "🔧 Allow l'utilisateur example pour le domaine example.com (auth la_regie)"
let allow_example = client post allows "example" "example.com" --full
assert ($allow_example.status == 201)

hide-env API_TOKEN
$env.API_USER = "la_regie"
$env.API_PASS = "toto"
$env.API_SUBUSER = "example"
use ../client.nu
let example_token = (client get token | decode-jwt)
assert ($example_token.1.sub == example)

print "🔧 Lecture d'un domaine en tant qu'utilisateur example"
let read_domain_example = client get domains example.com --full
assert ($read_domain_example.status == 200)

print "🔧 Création d'une mailbox (auth = example)"
let create_mailbox_example = client post mailboxes example.com john.doe John Doe "John Doe" --full
assert ($create_mailbox_example.status == 201)

print "🔧 Réinitialisation du mot de passe"
let create_mailbox_example = client post mailboxes reset_password example.com john.doe --full
assert (($create_mailbox_example.body.password | str length) >= 16)
assert ($create_mailbox_example.status == 200)
# FIXME Vérifier que le nouveau mot de passe n'est pas le même que l'ancien


print "🔧 Création d'une mailbox avec un additionalSender (auth = example)"
let create_mailbox_example_additional_sender = client post mailboxes example.com bali.balo Bali Balo "Bali Balo" --additionalSenders=["balo.bali@gmail.com", "ploum.ploum@yahoo.fr"] --full
assert ($create_mailbox_example_additional_sender.status == 201)
let read_mailbox_example_additionnal_sender = client get mailboxes example.com bali.balo
assert (($read_mailbox_example_additionnal_sender.additionalSenders | length) > 0)

print "🔧 Création d'une mailbox avec une quote (auth = example)"
let create_mailbox_example_quote = client post mailboxes example.com quotes "Région" "Provence-Alpes-Côte-d'Azure" "Provence-Alpes-Côte-d'Azure" --full
assert ($create_mailbox_example_quote.status == 201)

print "🔧 Création d'une mailbox avec une double-quote (auth = example)"
let create_mailbox_example_quote2 = client post mailboxes example.com quotes2 "Région" 'Provence-Alpes-Côte-d"Azure2' 'Provence-Alpes-Côte-d"Azure2' --full
assert ($create_mailbox_example_quote2.status == 201)

print "🔧 Modification d'un utilisateur (auth = example)"
let patch_mailbox_example = client patch mailboxes example.com john.doe --givenName=Jean --surName=Doh --displayName="Jean Doh" --full
assert ($patch_mailbox_example.status == 200)

print "🔧 Ajout d'un additionalSender à un utilisateur (auth = example)"
let patch_mailbox_example_additionalSender = client patch mailboxes example.com john.doe --additionalSenders=['baba@yahoo.fr'] --full
assert ($patch_mailbox_example.status == 200)
let get_mailbox_example_additionalSender = client get mailboxes example.com john.doe
print $get_mailbox_example_additionalSender.additionalSenders
assert ($get_mailbox_example_additionalSender.additionalSenders == ['baba@yahoo.fr'])
assert ($get_mailbox_example_additionalSender.status == 'ok')

print "🔧 Suspension d'une mailbox (auth=example)"
let suspend_mailbox_example = client patch mailboxes example.com john.doe --deactivate --full
assert ($suspend_mailbox_example.status == 200)

print "🔧 Réactivation d'une mailbox (auth=example)"
let resume_mailbox_example = client patch mailboxes example.com john.doe --activate --givenName=Jean --surName=Doh --displayName="Jean Doh" --full
assert ($resume_mailbox_example.status == 200)

print "🔧 Création d'un alias (auth = example)"
let create_alias_example = client post aliases example.com jean.doh john.doe@example.com --full
assert ($create_alias_example.status == 201)

print "🔧 Création d'un alias qui autorise l'envoi (auth = example)"
let create_alias_example_allowed_to_send = client post aliases example.com jane.doe john.doe@example.com true --full
assert ($create_alias_example_allowed_to_send.status == 201)
let mailbox_with_additionnal_sender = client get mailboxes example.com john.doe
print ($mailbox_with_additionnal_sender | to json)
assert ("jane.doe@example.com" in $mailbox_with_additionnal_sender.additionalSenders)
assert ($mailbox_with_additionnal_sender.status == "ok")

print "🔧 Lecture des mailboxes du domaine example.com"
let list_mailboxes_example = client get mailboxes example.com --full
assert ($list_mailboxes_example.status == 200)

print "🔧 Lecture de la mailbox john.doe du domaine example.com"
let list_mailboxes_john_doe_example = client get mailboxes example.com john.doe --full
assert ($list_mailboxes_john_doe_example.status == 200)

print "🔧 Lecture des aliases du domaine example.com"
let list_aliases_example = client get aliases example.com --full
assert ($list_aliases_example.status == 200)

print "🔧 Delete de l'alias jean.doh@example.com"
let delete_alias_jean_doh = client delete aliases example.com jean.doh john.doe@example.com --full
assert ($delete_alias_jean_doh.status == 204)

print "🔧 Création de l'alias contact@example.com -> pierre@gmail.com"
let create_alias_contact_pierre = client post aliases example.com contact pierre@gmail.com --full
assert ($create_alias_contact_pierre.status == 201)

print "🔧 Création de l'alias contact@example.com -> paul@gmail.com"
let create_alias_contact_pierre = client post aliases example.com contact paul@outlook.com --full
assert ($create_alias_contact_pierre.status == 201)

print "🔧 Lecture des destination de l'alias contact@example.com"
let read_aliases_contact = client get aliases example.com --user_name=contact --full
assert ($read_aliases_contact.status == 200)

print "🔧 Vérification de l'absence d'une destination"
let read_aliases_missing = client get aliases example.com --user_name=contact --destination=robert@laposte.net --full
assert ($read_aliases_missing.status == 404)

print "🔧 Suppression de toutes les destinations pour un alias"
let delete_aliases_contact_all = client delete aliases example.com contact all --full
assert ($delete_aliases_contact_all.status == 204)

print "🔧 Suppression d'une mailbox"
let delete_mailbox_john_doe = client delete mailboxes example.com john.doe --full
assert ($delete_mailbox_john_doe.status == 204)

print "🔧 Relogin en tant que la_regie"
hide-env API_SUBUSER
hide-env API_TOKEN
$env.API_USER = "la_regie"
$env.API_PASS = "toto"
use ../client.nu
print "🔧 Informations du token"
print (client get token | decode-jwt | to json)

# FIXME Not yet implemented
print "🔧 Suppression du domaine example.com en tant que la_regie"
let delete_domain_example = client delete domains example.com --full
assert ($delete_domain_example.status == 501)

# A laisser en dernier, ca éteint uvicorn
print "🔧 Relogin en tant que admin, pour faire le shutdown"
$env.API_USER = $admin_user.name
$env.API_PASS = $admin_user.password
use ../client.nu

print "🔧 Fermeture du service"
let get_shutdown = client get shutdown --full
assert ($get_shutdown.status == 200)


