def nushize [] {
  $in | str replace --all '{' '\(\$' | str replace --all '}' '\)'
}

print "🔧 Vérification de l'utilisation des routes"

let openapi = http get http://localhost:8000/openapi.json
let source = open client.nu | lines
let paths = ($openapi.paths 
	| transpose route methods
	| update methods { $in | transpose method params  } 
	| flatten 
	| select route methods.method
        | rename route method
        | insert orig_route { $in.route }
	| update route { $in | nushize })

for path in $paths {
	let regexp = $"http ($path.method) .*($path.route)\(\\?|\")"
	let found = $source | find -r $regexp

	if ($found | length) == 0 {
		print $"💥 la route ($path.method) ($path.orig_route) n'a pas été détectée dans les sources du client. La regexp est ($regexp)"
		exit 1
	}
	
	if ($found | length) > 1 {
		print $"💔 la route ($path.method) ($path.orig_route) a été détectée plusieurs fois"
	}
}

print "🔧 Toutes les routes ont été détectées, toutefois cela n'apporte pas la garantie que l'implémentation est correcte/complète."
 
