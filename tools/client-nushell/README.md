# CLI dimail-api pour nushell

## environnement
Le client utilise les variables d'environnement suivantes:
- `API_USER`: utilisateur de l'API
- `API_PASS`: mot de passe
- `API_HOST`: hostname où est hébergée l'API

Le fichier .env.example.nu contient les déclarations nécessaires en langage `nu`

## tourner nu dans un docker

docker run -it --mount type=bind,source=.,destination=/opt/dimail ghcr.io/nushell/nushell:0.100.0-bookworm

## utilisation
```
$ nu
     __  ,
 .--()°'.' Welcome to Nushell,
'|, . ,'   based on the nu language,
 !_-(_\    where all data is structured!

Please join our Discord community at https://discord.gg/NtAbbGn
Our GitHub repository is at https://github.com/nushell/nushell
Our Documentation is located at https://nushell.sh
Tweet us at @nu_shell
Learn how to remove this at: https://nushell.sh/book/configuration.html#remove-welcome-message

It's been this long since Nushell's first commit:
5yrs 6months 18days 13hrs 25mins 30secs 285ms 722µs 211ns 

Startup Time: 11ms 852µs 926ns
~/foo/bar> source .env.foo.nu
~/foo/bar> use client.nu
~/foo/bar| client <press TAB>
client delete aliases      delete a domain alias
client delete allows       delete a user allowance on a domain
client delete domains      delete domain
client delete mailboxes    delete an existing mailbox
client delete users        delete an api user
client get aliases         list domain aliases
client get allows          list allows for user or domain or both
client get domains         list domain(s) details
client get domains check   run a domain check
client get domains fix     run a domain fix
client get health_check    run health check
client get mailboxes       get one or all mailbox for domain
client get token           get one token for given user with environment variables or paramete
client get users           list one or all api users
client headers             internal helper for auth headers 
client main get mailboxes  get one or all mailbox for domain
client patch domains       modify a domain
client patch mailboxes     modify an existing mailbox
client patch users         modify an existing api user
client post aliases        create a domain alias
client post allows         allow a user to use domain
client post domains        create a new domain
client post mailboxes      create a new mailbox
client post users          create a new api user
```

Pour connaître les arguments à passer, chaque commande dispose d'une aide en ligne avec `--help`
```
> client get users --help
list one or all api users

Usage:
  > get users (username) 

Flags:
  -h, --help: Display the help message for this command

Parameters:
  username <string>:  (optional)

Input/output types:
  ╭───┬───────┬────────╮
  │ # │ input │ output │
  ├───┼───────┼────────┤
  │ 0 │ any   │ any    │
  ╰───┴───────┴────────╯
```

