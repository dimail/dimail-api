#!/usr/bin/env nu

export-env {
    if $env.API_TOKEN? != null {
        hide-env API_TOKEN
    }
    $env.API_TOKEN = (get token)
}


# get one token for given user with environment variables or parameters
export def "get token" [] {
    if $env.API_TOKEN? == null {
        mut parameters = {}
        if $env.API_SUBUSER? != null {
            $parameters = $parameters | insert username $env.API_SUBUSER
        }
        $env.API_TOKEN = (http get -f -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/token/?($parameters | url build-query)").body.access_token
    }
    return $env.API_TOKEN
}

# internal helper for auth headers 
export def headers [] {
    return ["Authorization" $"Bearer ($env.API_TOKEN? | default (get token))"]
}

# get one or all mailbox for domain
export def "get mailboxes" [domain_name: string, user_name?: string, --full] {
    if $user_name == null {
        http get --full=$full -e -H (headers) $"($env.API_URL)/domains/($domain_name)/mailboxes/"
    } else {
        http get --full=$full -e -H (headers) $"($env.API_URL)/domains/($domain_name)/mailboxes/($user_name)"
    }
}

# create a new mailbox
export def "post mailboxes" [domain_name: string, user_name: string, givenName: string, surName: string, displayName: string, --additionalSenders: list<string> --full] {
    mut parameters = {
        givenName: $givenName,
        surName: $surName,
        displayName: $displayName,
    }
    if $additionalSenders != null { $parameters.additionalSenders = $additionalSenders }
    http post --content-type=application/json --full=$full -e -H (headers) $"($env.API_URL)/domains/($domain_name)/mailboxes/($user_name)" $parameters
}

# delete an existing mailbox
export def "delete mailboxes" [domain_name: string, user_name: string, --full] {
    http delete --full=$full -e -H (headers) $"($env.API_URL)/domains/($domain_name)/mailboxes/($user_name)"
}

# modify an existing mailbox
export def "patch mailboxes" [domain_name: string, user_name: string, --deactivate, --activate, --givenName: string, --surName: string, --displayName: string, --additionalSenders: list<any>, --full] {
    mut parameters = {}
    if $givenName != null { $parameters.givenName = $givenName }
    if $surName != null { $parameters.surName = $surName }
    if $displayName != null { $parameters.displayName = $displayName }
    if $deactivate { $parameters.active = "no" }
    if $activate { $parameters.active = "yes" }
    if $additionalSenders != null { $parameters.additionalSenders = $additionalSenders }
    http patch --full=$full -e -H (headers) --content-type application/json $"($env.API_URL)/domains/($domain_name)/mailboxes/($user_name)" $parameters
}

# reset mailbox password
export def "post mailboxes reset_password" [domain_name: string, user_name: string, --full] {
    http post --full=$full -e -H (headers) --content-type application/json $"($env.API_URL)/domains/($domain_name)/mailboxes/($user_name)/reset_password" {}
}

# delete an api user
export def "delete users" [user_name: string, --full] {
    http delete --full=$full -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/users/($user_name)"
}

# list one or all api users
export def "get users" [user_name?: string, --full] {
    if user_name == null {
        http get --full=$full -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/users/"
    } else {
    	http get --full=$full -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/users/($user_name)"
    }
}

# create a new api user
export def "post users" [name: string, password?: string, --is_admin, --perms: list<string>, --full] {
    let user = {
        name: $name
        password: ($password | default "no")
        is_admin: ($is_admin | default false)
        perms: ($perms | default [])
    }
    http post --full=$full -e -u $env.API_USER -p $env.API_PASS --content-type application/json $"($env.API_URL)/users/" $user
}

# modify an existing api user
export def "patch users" [user: string, --password: string, --is_admin, --perms: list<string>, --full] {
    mut parameters = {}
    if $is_admin != null { $parameters.is_admin = $is_admin }
    if $perms != null { $parameters.perms = $perms }
    if $password != null { $parameters.password = $password }  
    http patch --full=$full -e -u $env.API_USER -p $env.API_PASS --content-type application/json $"($env.API_URL)/users/($user)" $parameters
}

# run a domain check
export def "get domains check" [domain_name: string, --full] {
    http get --full=$full -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/domains/($domain_name)/check"
}

# run a domain fix
export def "get domains fix" [domain_name: string, --full] {
    http get --full=$full -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/domains/($domain_name)/fix"
}

# get DNS records definitions for domains
export def "get domains spec" [domain_name: string, --full] {
    http get --full=$full -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/domains/($domain_name)/spec"
}

# delete domain
export def "delete domains" [domain_name: string, --full] {
    http delete --full=$full -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/domains/($domain_name)"
}

# list domain(s) details
export def "get domains" [domain_name?: string, --full] {
    if $domain_name == null {
        http get --full=$full -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/domains/?"
    } else {
        http get --full=$full -e -H (headers) $"($env.API_URL)/domains/($domain_name)?"
    }
}

# modify a domain
export def "patch domains" [
    domain_name: string
    --delivery: string
    --transport: string
    --features: list<string>
    --webmail_domain: string
    --imap_domain: string
    --smtp_domain: string
    --context_name: string
    --full
] {
    mut parameters = {}
    if $delivery != null { $parameters.delivery = $delivery }
    if $transport != null { $parameters.transport = $transport }
    if $features != null { $parameters.features = $features }
    if $webmail_domain != null { $parameters.webmail_domain = $webmail_domain }
    if $imap_domain != null { $parameters.imap_domain = $imap_domain }
    if $smtp_domain != null { $parameters.smtp_domain = $smtp_domain }
    if $context_name != null { $parameters.context_name = $context_name }
    http patch --full=$full -e -u $env.API_USER -p $env.API_PASS --content-type application/json $"($env.API_URL)/domains/($domain_name)" $parameters
}

# create a new domain
export def "post domains" [
    domain_name: string
    --delivery: string
    --transport: string
    --features: list<string>
    --webmail_domain: string
    --imap_domain: string
    --smtp_domain: string
    --context_name: string
    --full
] {
    mut parameters = {
        name: $domain_name
    }
    if $delivery != null { $parameters.delivery = $delivery }
    if $transport != null { $parameters.transport = $transport }
    if $features != null { $parameters.features = $features }
    if $webmail_domain != null { $parameters.webmail_domain = $webmail_domain }
    if $imap_domain != null { $parameters.imap_domain = $imap_domain }
    if $smtp_domain != null { $parameters.smtp_domain = $smtp_domain }
    if $context_name != null { $parameters.context_name = $context_name }
    http post --full=$full -e -u $env.API_USER -p $env.API_PASS --content-type application/json $"($env.API_URL)/domains/" $parameters
}

# list allows for user or domain or both
export def "get allows" [--username: string, --domain: string, --full] {
    mut parameters = {}
    if $username != null { $parameters.username = $username }
    if $domain != null { $parameters.domain = $domain }
    http get --full=$full -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/allows/?($parameters | url build-query)"
}

# allow a user to use domain
export def "post allows" [username: string, domain: string, --full] {
    http post --full=$full -e --content-type=application/json -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/allows/" {user: $username domain: $domain}
}

# delete a user allowance on a domain
export def "delete allows" [user_name: string, domain_name: string, --full] {
    http delete --full=$full -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/allows/($domain_name)/($user_name)"
 }

# list domain aliases
export def "get aliases" [domain_name: string, --user_name: string, --destination: string, --full] {
    mut parameters = {}
    if $user_name != null { $parameters.user_name = $user_name }
    if $destination != null { $parameters.destination = $destination }
    http get --full=$full -e -H (headers) $"($env.API_URL)/domains/($domain_name)/aliases/?($parameters | url build-query)"
}

# patch an alias (allow destination to send)
export def "patch aliases" [domain_name: string, user_name: string, destination: string, allow_to_send?: bool, --full] {
    mut parameters = {}
    if $allow_to_send != null { $parameters.allow_to_send = $allow_to_send }
    http patch --content-type=application/json --full=$full -e -H (headers) $"($env.API_URL)/domains/($domain_name)/aliases/($user_name)/($destination)" $parameters
}

# create an alias
export def "post aliases" [domain_name: string, user_name: string, destination: string, allow_to_send?: bool, --full] {
    mut alias = {user_name: $user_name, destination: $destination}
    if allow_to_send != null { $alias.allow_to_send = $allow_to_send }
    http post --full=$full -e -H (headers) --content-type application/json $"($env.API_URL)/domains/($domain_name)/aliases/" $alias
}

# delete an alias
export def "delete aliases" [domain_name: string, user_name: string, destination: string, --full] {
    http delete --full=$full -e -H (headers) $"($env.API_URL)/domains/($domain_name)/aliases/($user_name)/($destination)"
}

# run health check
export def "get health_check" [api_user?: string, api_password?: string, --full] {
    http get --full=$full -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/system/health_check?"
}

# get admin logs
export def "get logs" [api_user?: string, api_password?: string, --full] {
    http get --full=$full -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/logs/?"
}

# list crash logs
export def "get logs crashes" [crash_name?: string, --full] {
    if $crash_name == null {
        http get --full=$full  -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/logs/crashes"
    } else {
        http get --full=$full  -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/logs/crashes/($crash_name)"
    }
}

# remove crash logs from server
export def "delete logs crashes" [crash_name: string, --full] {
    http delete --full=$full  -e -u $env.API_USER -p $env.API_PASS $"($env.API_URL)/logs/crashes/($crash_name)"
}

# get version
export def "get version" [api_user?: string, api_password?: string, --full, --log_request] {
    mut parameters = {}
    if $log_request {
        $parameters.log_request = "force"
    }
    http get --full=$full -u $env.API_USER -p $env.API_PASS -e $"($env.API_URL)/system/version?($parameters | url build-query)"
}

# get tech_domain
export def "get tech_domain" [--full, --log_request] {
    mut parameters = {}
    if $log_request {
        $parameters.log_request = "force"
    }
    http get --full=$full -u $env.API_USER -p $env.API_PASS -e $"($env.API_URL)/system/tech_domain?($parameters | url build-query)"
}

# get shutdown
export def "get shutdown" [--full, --log_request] {
    mut parameters = {}
    if $log_request {
        $parameters.log_request = "force"
    }
    http get --full=$full -u $env.API_USER -p $env.API_PASS -e $"($env.API_URL)/system/shutdown?($parameters | url build-query)"
}

# patch import_domain
export def "patch import_domain" [--full, --log_request] {
    mut parameters = {}
    if $log_request {
        $parameters.log_request = "force"
    }
    echo "Not yet implemented"
    # http patch --full=$full -u $env.API_USER -p $env.API_PASS --content-type=application/json -H (headers) -e $"($env.API_URL)/system/import_domain/($domain_name)?($parameters | url build-query)" $import_data
}

# patch database
export def "patch database" [--full, --log_request] {
    # http patch --full=$full -u $env.API_USER -p $env.API_PASS --content-type=application/json -H (headers) -e $"($env.API_URL)/system/database/($db_name)?($parameters | url build-query)" $patch_db
    echo "Not yet implemented"
}

# post database
export def "post database" [--full, --log_request] {
    # http post --full=$full -u $env.API_USER -p $env.API_PASS --content-type=application/json -H (headers) -e $"($env.API_URL)/system/database/($db_name)/($schema_name)?($parameters | url build-query)" ""
    echo "Not yet implemented"
}

# check contexts
export def "check contexts" [--full, --log_request] {
    # http post --full=$full -u $env.API_USER -p $env.API_PASS --content-type=application/json -H (headers) -e $"($env.API_URL)/system/check_contexts?($parameters | url build-query)" ""
    echo "Not yet implemented"
}

# move mailbox
export def "move mailbox" [domain_name: string, user_name: string, --full, --log_request] {
    mut parameters = {}
    if $log_request {
        $parameters.log_request = "force"
    } 
    http get --full=$full -H (headers) -e $"($env.API_URL)/system/move_mailbox/($domain_name)/($user_name)?($parameters | url build-query)"
}

# get mailqueue
export def "get mailqueue" [--full, --log_request] {
    # http get --full=$full -u $env.API_USER -p $env.API_PASS  -H (headers) -e $"($env.API_URL)/system/mailqueue/($domain_name)/($user_name)?($parameters | url build-query)"
    echo "Not yet implemented"
}
  
# get disconnect
export def "get disconnect" [--full, --log_request] {
    # http get --full=$full -u $env.API_USER -p $env.API_PASS  -H (headers) -e $"($env.API_URL)/system/disconnect/($domain_name)/($user_name)?($parameters | url build-query)"
    echo "Not yet implemented"
}

