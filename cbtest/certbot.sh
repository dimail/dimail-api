#!/bin/bash

echo Nb args $#
while [ $# -gt 0 ]; do
	case "$1" in
		--version)
			echo Fake version 0.0.1-test
			exit 0
			;;
		-n)
			# OSEF
			;;
		-d)
			shift
			echo Add domain $1
			cert_domains="$cert_domains $1"
			;;
		--cert-name)
			shift
			echo Cert name $1
			cert_name=$1
			;;
		certonly)
			;;
		--webroot)
			;;
		*)
			echo Argument inconnu $1
			exit 1
	esac
	shift
done

if [ "$cert_name" = "" ]; then
	echo Le nom du certificat est obligatoire.
	exit 1
fi

arc_dir=/opt/certs/archive/$cert_name
count=1
if [ -d $arc_dir ]; then
	echo "Ce nom de certificat existe deja (archive)."
	while [ -f $arc_dir/cert$count.pem ]; do
		echo "- je trouve cert$count.pem"
		count=$(($count + 1))
	done
fi
# Un répertoire dans les archives
mkdir -p $arc_dir
touch $arc_dir/privkey$count.pem
touch $arc_dir/cert$count.pem
touch $arc_dir/chain$count.pem
touch $arc_dir/fullchain$count.pem

for dom in $cert_domains; do
	echo $dom >> $arc_dir/cert$count.pem
done

liv_dir=/opt/certs/live/$cert_name
mkdir -p $liv_dir
ln -sfT ../../archive/$cert_name/privkey$count.pem $liv_dir/privkey.pem
ln -sfT ../../archive/$cert_name/cert$count.pem $liv_dir/cert.pem
ln -sfT ../../archive/$cert_name/chain$count.pem $liv_dir/chain.pem
ln -sfT ../../archive/$cert_name/fullchain$count.perm $liv_dir/fullchain.pem

# /usr/bin/certbot -n
#    --cert-name {{ domain_cert_info.cert_name }}
#    -d {{ cert_manager_all_subdomains | join(' -d ') }}
#    certonly
#    --webroot
