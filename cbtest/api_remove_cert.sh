#!/bin/bash
if [ "$1" = "check" ]; then
	echo OK
	exit 0
fi
if [ "$1" == "" ]; then
	echo Tu vis dangereusement >&2
	exit 1
fi
case "$1" in
	client_*)
		true
		;;
	*)
		echo "Je ne supprime pas les certificats avec des noms debiles ($1)" >&2
		exit 1
		;;
esac
if [ -d "/opt/certs/live/$1" ]; then
	rm -rf "/opt/certs/live/$1"
fi
if [ -d "/opt/certs/archive/$1" ]; then
	rm -rf "/opt/certs/archive/$1"
fi
if [ -f "/opt/certs/renewal/$1.conf" ]; then
	rm -f "/opt/certs/renewal/$1.conf"
fi
