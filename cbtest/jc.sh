#!/bin/bash

# On recoit le "certificat" (fruit de certbot) sur stdin, c'est la liste des
# domaines, un par ligne.
# On ajoute une ligne "domains:" en premier, puis on fabrique une list yaml
# à injecter dans la CLI de jinja2
(echo "domains:"; while read a; do echo "  - $a"; done) | j2 -f yaml /root/cert_info.json -
