#!/bin/bash
if [ "$1" = "check" ]; then
	echo OK $2
	exit 0
fi
tech_name=$1
domain_name=$2
user_name=$3
prefix=`echo $USER | cut -b 1-2`
dir_name="$domain_name/$prefix"

rsync -av "imap.$tech_name::mail/imap_mailboxes/$dir_name/$user_name" "/var/mail/imap_mailboxes/$dir_name/"
rsync -av "imap.$tech_name::mail/sieve/$dir_name/$user_name" "/var/mail/sieve/$dir_name/"
